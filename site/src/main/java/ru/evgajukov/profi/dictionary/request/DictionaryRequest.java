package ru.evgajukov.profi.dictionary.request;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 15.06.17.
 * Класс запроса на получение справочников
 */
public class DictionaryRequest {

    public static final String DIC_PSERVICES = "pservices";
    public static final String DIC_MMETROS = "mmetros";

    List<String> codes;

    {
        codes = new ArrayList<>();
    }

    public DictionaryRequest add(String code) {

        codes.add("\"" + code + "\"");
        return this;
    }

    @Override
    public String toString() {

        if (codes.size() == 0) {
            return null;
        }

        return String.format("[%s]", String.join(",", codes));
    }
}