<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">${title}</div>
    <div class="panel-body">
        <table id="users-table" class="table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
                <th>#</th>
            </tr>
            </thead>
        </table>
    </div>
</div>