package ru.dom24x7.component;

/**
 * Created by timur on 03.03.17.
 * Для организации разметки Open Graph
 */
public class OpenGraph {

    public static final String OG_TYPE_WEBSITE = "website";
    public static final String OG_TYPE_ARTICLE = "article";

    private String title;
    private String description;
    private String image;
    private String type;
    private String url;

    public String getTitle() {

        return title;
    }

    public OpenGraph setTitle(String title) {

        this.title = title;
        return this;
    }

    public String getDescription() {

        return description;
    }

    public OpenGraph setDescription(String description) {

        this.description = description;
        return this;
    }

    public String getImage() {

        return image;
    }

    public OpenGraph setImage(String image) {

        this.image = image;
        return this;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }
}