package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.component.TableData;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.site.BaseController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 16.02.17.
 *
 * Административная панель: управление пользователями
 */
@Controller
public class AdminUserController extends BaseController {

    @RequestMapping("/admin/users")
    public String listAction(ModelMap model) {

        setModelMap(model, "Список пользователей");

        return "admin/user/list";
    }

    @RequestMapping("/admin/ajax/users/list")
    public @ResponseBody TableData listUsersAjax(HttpServletRequest request) {

        int draw = Integer.parseInt(request.getParameter("draw"));
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));
        String search = request.getParameter("search[value]");

        int count = new UserDAOImpl().count(search);
        List<User> users = new UserDAOImpl().list(start, length, search);

        if (users == null) {
            users = new ArrayList<User>();
        }

        TableData tableData = new TableData();
        tableData.setDraw(draw);
        tableData.setRecordsTotal(count);
        tableData.setRecordsFiltered(count);
        tableData.setData(users);

        return tableData;
    }
}