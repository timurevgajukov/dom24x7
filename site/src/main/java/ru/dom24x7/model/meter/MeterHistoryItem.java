package ru.dom24x7.model.meter;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.dom24x7.model.Model;
import ru.dom24x7.service.meter.MeterService;
import ru.dom24x7.utils.DateUtils;
import ru.dom24x7.utils.StrUtils;

/**
 * Элемент истории показаний счетчика
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MeterHistoryItem extends Model {
	
    private Meter meter;
    private Date date;
    private Double value;
    private Double cost;

    public MeterHistoryItem() {

        super();
    }

    public MeterHistoryItem(int id) {

        super(id);
    }

    public MeterHistoryItem(Meter meter, Date date) {

        super();
        this.meter = meter;
        this.date = date;
    }

    @JsonIgnore
    public Meter getMeter() {

        return meter;
    }

    public void setMeter(Meter meter) {

        this.meter = meter;
    }

    public Date getDate() {

        return date;
    }

    public String getStringDate() {

        return DateUtils.getStringDate(date);
    }

    public String getStringFullDate() {

        return new SimpleDateFormat("dd.MM.yyyy").format(date);
    }

    public void setDate(Date date) {

        this.date = date;
    }

    public Double getValue() {

        return value;
    }
    
    public String getStringValue() {
    	
    	return String.format("%s %s", StrUtils.format(value, meter.getCapacity(date), meter.getPrecision(date)),
    			meter.getType().getUnit());
    }

    public void setValue(Double value) {

        this.value = value;
    }

    public Double getCost() {

        return cost;
    }

    public String getStringCost() {

        return StrUtils.getStrAmount(cost, meter.getAddress().getCurrency().toString());
    }

    public void setCost(Double cost) {

        this.cost = cost;
    }

    @Override
    public String toString() {

        return String.format("%f", value);
    }

    /**
     * Сумма затрат относительно предыдущего показания
     *
     * @return
     */
    public String getStringCostByPrevValue() {

        if (meter.getTariff() == 0.0) {
            return "";
        }

        MeterHistoryItem prevItem = getPrevItem();
        if (prevItem == null) {
            return "";
        }

        double diff = new MeterService(meter).getDiffValues(prevItem.getValue(), value, prevItem.getDate(), date);


        return StrUtils.getStrAmount(diff * meter.getTariff(), meter.getAddress().getCurrency().getSymbol());
    }
    
    /**
     * Полностью копирует показание без идентификатора и счетчика
     * 
     * @param item
     */
    public void copy(MeterHistoryItem item) {
    	
    	this.date = item.date;
    	this.value = item.value;
    }

    /**
     * Возвращает предыдущее значение показаний счетчика
     *
     * @return
     */
    private MeterHistoryItem getPrevItem() {

        meter.loadHistory();
        for (int i = 0; i < meter.getHistory().size(); i++) {
            if (id == meter.getHistory().get(i).getId()) {
                if (i + 1 < meter.getHistory().size()) {
                    return meter.getHistory().get(i + 1);
                }
            }
        }

        return null;
    }
}