<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h1 class="m-b-xs">${title}</h1>
<small>Виджет предоставлен "Единой информационной аналитической системой" <a href="http://eias.fstrf.ru/">eias.fstrf.ru</a></small>

<iframe id="tariffs-frame" src="//eias.fstrf.ru/jkh_calc/" width="100%" frameborder="0"></iframe>

<script>
    $(document).ready(function () {
        var iframe = $('#tariffs-frame', parent.document.body);
        iframe.height($(document.body).height() - 220);
    });
</script>