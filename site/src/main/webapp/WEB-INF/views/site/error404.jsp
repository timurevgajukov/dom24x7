<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<i class="pe-7s-way text-success big-icon"></i>
<h1>404</h1>
<p class="h4 text-thin pad-btm mar-btm">
    Упс! Мы не нашли запрошенную вами страницу!
</p>
<div class="pad-top">
    <a href="/" class="btn-link">на главную</a>
</div>