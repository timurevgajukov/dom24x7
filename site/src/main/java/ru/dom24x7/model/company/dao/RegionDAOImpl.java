package ru.dom24x7.model.company.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.company.Region;
import ru.dom24x7.model.company.dao.interfaces.RegionDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 08.06.17.
 * Реализация интерфейса для работы с регионами
 */
public class RegionDAOImpl extends ModelDAOImpl implements RegionDAO {

    private static final String QUERY_LIST_WITH_COUNT = "{ call regions_list_with_count() }";

    @Override
    public List<Region> list() {

        List<Region> list = jdbcTemplate.query(QUERY_LIST_WITH_COUNT, new RowMapper<Region>() {
            @Override
            public Region mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private Region load(ResultSet rs) throws SQLException {

        return new Region(rs.getInt("id"), rs.getString("name"), rs.getInt("companies"));
    }
}