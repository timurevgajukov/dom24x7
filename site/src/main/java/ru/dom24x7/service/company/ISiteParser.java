package ru.dom24x7.service.company;

/**
 * Created by timur on 06.04.17.
 */
public interface ISiteParser {

    /**
     * Основной метод парсера
     * @return
     */
    int parse();
}
