package ru.dom24x7.model.photo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.dom24x7.model.Model;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.HashMap;
import java.util.Map;

/**
 * Хранение фотографии
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Photo extends Model {

	private Map<String, String> formats;

	{
		formats = new HashMap<String, String>();
		formats.put("jpg", "jpg");
		formats.put("jpeg", "jpg");
		formats.put("png", "png");
		formats.put("gif", "gif");
	}

	private String fileName;
	private byte[] resource;
	
	public Photo(int id) {

		super(id);
	}

	public Photo(int id, String fileName, Blob photo) {

		super(id);
		this.fileName = fileName;
		setResource(photo);
	}

	public Photo(String fileName, byte[] photo) {

		super();
		this.fileName = fileName;
		this.resource = photo;
	}

	public Photo(Blob photo) {

		super();
		setResource(photo);
	}

	public String getFileName() {

		return fileName;
	}

	public void setFileName(String fileName) {

		this.fileName = fileName;
	}

	@JsonIgnore
	public byte[] getResource() {

		return resource;
	}

	public void setResource(byte[] resource) {

		this.resource = resource;
	}

	public void setResource(Blob blob) {

		if (blob == null) {
			return;
		}

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			InputStream in = blob.getBinaryStream();

			int n = 0;
			while ((n = in.read(buf)) >= 0) {
				baos.write(buf, 0, n);
			}
			in.close();

			this.resource = baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getBase64() {

		if (resource == null) {
			return null;
		}

		BASE64Encoder encoder = new BASE64Encoder();
		String base64 = encoder.encode(resource);

		return "data:image/jpeg;base64," + base64;
	}

	@JsonIgnore
	public String getFormat() {

		String[] arr = fileName.split("\\.");
		return formats.get(arr[arr.length - 1].toLowerCase());
	}
}
