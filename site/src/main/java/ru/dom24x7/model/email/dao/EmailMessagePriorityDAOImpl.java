package ru.dom24x7.model.email.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.email.EmailMessagePriority;
import ru.dom24x7.model.reference.ReferenceDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 17.03.17.
 * Реализация интерфейса получения справочника приоритетов почтовых сообщений
 */
public class EmailMessagePriorityDAOImpl extends ModelDAOImpl implements ReferenceDAO<EmailMessagePriority> {

    private static final String QUERY_LIST = "{ call email_message_priorities_list() }";

    @Override
    public List<EmailMessagePriority> list() {

        List<EmailMessagePriority> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<EmailMessagePriority>() {
            @Override
            public EmailMessagePriority mapRow(ResultSet resultSet, int i) throws SQLException {

                return new EmailMessagePriority(resultSet.getInt("id"), resultSet.getString("name"));
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }
}