package ru.dom24x7.model.term.dao.interfaces;

import ru.dom24x7.model.term.Term;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы со словарем
 */
public interface TermDAO {

    /**
     * Возвращает список терминов
     *
     * @param count
     * @return
     */
    List<Term> list(int count);

    /**
     * Возвращает список букв, на которые у нас имеются термины
     *
     * @return
     */
    List<String> letters();

    /**
     * Возвращает список терминов с учетом фильтра
     *
     * @param filter
     * @return
     */
    List<Term> list(String filter);
}