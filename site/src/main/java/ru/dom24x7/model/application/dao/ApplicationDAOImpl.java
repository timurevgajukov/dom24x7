package ru.dom24x7.model.application.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.application.Application;
import ru.dom24x7.model.application.dao.interfaces.ApplicationDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса работы приложения через api
 */
public class ApplicationDAOImpl extends ModelDAOImpl implements ApplicationDAO {

    private static final String QUERY_AUTH = "{ call applications_get(?) }";
    private static final String QUERY_SAVE = "{ call applications_save(?,?,?,?,?) }";

    @Override
    public Application get(String token) {

        List<Application> list = jdbcTemplate.query(QUERY_AUTH, new RowMapper<Application>() {

            @Override
            public Application mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, token);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public boolean save(Application application) {

        Application newApp = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        application.getName(),
                        application.getToken(),
                        application.getEmail(),
                        application.getSite() == null || application.getSite().trim().length() == 0 ? null : application.getSite().trim(),
                        application.getNote()
                }, new RowMapper<Application>() {

                    @Override
                    public Application mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        application.setId(newApp.getId());
        application.setCreateDt(newApp.getCreateDt());

        return true;
    }

    private Application load(ResultSet rs) throws SQLException {

        Application app = new Application(rs.getInt("id"), rs.getString("name"));
        app.setCreateDt(rs.getDate("create_dt"));
        app.setToken(rs.getString("token"));
        app.setEmail(rs.getString("email"));
        app.setSite(rs.getString("site"));
        app.setNote(rs.getString("note"));
        app.setHold(rs.getBoolean("hold"));

        return app;
    }
}