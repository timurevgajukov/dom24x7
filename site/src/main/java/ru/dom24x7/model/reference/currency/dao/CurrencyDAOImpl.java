package ru.dom24x7.model.reference.currency.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.reference.ReferenceDAO;
import ru.dom24x7.model.reference.currency.Currency;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 18.09.16.
 *
 * Реализация интерфейса работы со справочником валют
 */
public class CurrencyDAOImpl extends ModelDAOImpl implements ReferenceDAO<Currency> {

    private static final String QUERY_LIST = "{ call currencies_list() }";

    @Override
    public List<Currency> list() {

        List<Currency> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Currency>() {

            @Override
            public Currency mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private Currency load(ResultSet rs) throws SQLException {

        return new Currency(rs.getInt("id"), rs.getString("name"), rs.getString("symbol"), rs.getString("code"));
    }
}