package ru.dom24x7.model.device.dao.interfaces;

import ru.dom24x7.model.device.Device;
import ru.dom24x7.model.meter.Meter;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с приборами учета коммунальных услуг
 */
public interface DeviceDAO {

    /**
     * Возвращает устройства, привязанные к счетчику
     *
     * @param meter
     * @return
     */
    List<Device> list(Meter meter);

    /**
     * Сохранение прибора учета коммунальных услуг
     *
     * @param device
     * @return
     */
    boolean save(Device device);
}
