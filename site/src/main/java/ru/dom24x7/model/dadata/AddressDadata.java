package ru.dom24x7.model.dadata;

import ru.dom24x7.model.Model;

/**
 * Информация по адресу в формате сервиса dadata.ru
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class AddressDadata extends Model {
	
	private String source;
	private String postalCode;
	private String country;
	private String regionType;
	private String regionTypeFull;
	private String region;
	private String areaType;
	private String areaTypeFull;
	private String area;
	private String cityType;
	private String cityTypeFull;
	private String city;
	private String settlementType;
	private String settlementTypeFull;
	private String settlement;
	private String streetType;
	private String streetTypeFull;
	private String street;
	private String houseType;
	private String houseTypeFull;
	private String house;
	private String blockType;
	private String block;
	private String flatType;
	private String flat;
	private String postalBox;
	private String kladrId;
	private String okato;
	private String oktmo;
	private String taxOffice;
	private String flatArea;
	private int qcComplete;
	private int qcHouse;
	private int qc;
	private String unparsedParts;
	
	public AddressDadata() {
		
		super();
	}
	
	public AddressDadata(int id) {
		
		super(id);
	}

	public String getSource() {
		
		return source;
	}

	public AddressDadata setSource(String source) {
		
		this.source = source;
		
		return this;
	}

	public String getPostalCode() {
		
		return postalCode;
	}

	public AddressDadata setPostalCode(String postalCode) {
		
		this.postalCode = postalCode;
		
		return this;
	}

	public String getCountry() {
		
		return country;
	}

	public AddressDadata setCountry(String country) {
		
		this.country = country;
		
		return this;
	}

	public String getRegionType() {
		
		return regionType;
	}

	public AddressDadata setRegionType(String regionType) {
		
		this.regionType = regionType;
		
		return this;
	}

	public String getRegionTypeFull() {
		
		return regionTypeFull;
	}

	public AddressDadata setRegionTypeFull(String regionTypeFull) {
		
		this.regionTypeFull = regionTypeFull;
		
		return this;
	}

	public String getRegion() {
		
		return region;
	}

	public AddressDadata setRegion(String region) {
		
		this.region = region;
		
		return this;
	}

	public String getAreaType() {
		
		return areaType;
	}

	public AddressDadata setAreaType(String areaType) {
		
		this.areaType = areaType;
		
		return this;
	}

	public String getAreaTypeFull() {
		
		return areaTypeFull;
	}

	public AddressDadata setAreaTypeFull(String areaTypeFull) {
		
		this.areaTypeFull = areaTypeFull;
		
		return this;
	}

	public String getArea() {
		
		return area;
	}

	public AddressDadata setArea(String area) {
		
		this.area = area;
		
		return this;
	}

	public String getCityType() {
		
		return cityType;
	}

	public AddressDadata setCityType(String cityType) {
		
		this.cityType = cityType;
		
		return this;
	}

	public String getCityTypeFull() {
		
		return cityTypeFull;
	}

	public AddressDadata setCityTypeFull(String cityTypeFull) {
		
		this.cityTypeFull = cityTypeFull;
		
		return this;
	}

	public String getCity() {
		
		return city;
	}

	public AddressDadata setCity(String city) {
		
		this.city = city;
		
		return this;
	}

	public String getSettlementType() {
		
		return settlementType;
	}

	public AddressDadata setSettlementType(String settlementType) {
		
		this.settlementType = settlementType;
		
		return this;
	}

	public String getSettlementTypeFull() {
		
		return settlementTypeFull;
	}

	public AddressDadata setSettlementTypeFull(String settlementTypeFull) {
		
		this.settlementTypeFull = settlementTypeFull;
		
		return this;
	}

	public String getSettlement() {
		
		return settlement;
	}

	public AddressDadata setSettlement(String settlement) {
		
		this.settlement = settlement;
		
		return this;
	}

	public String getStreetType() {
		
		return streetType;
	}

	public AddressDadata setStreetType(String streetType) {
		
		this.streetType = streetType;
		
		return this;
	}

	public String getStreetTypeFull() {
		
		return streetTypeFull;
	}

	public AddressDadata setStreetTypeFull(String streetTypeFull) {
		
		this.streetTypeFull = streetTypeFull;
		
		return this;
	}

	public String getStreet() {
		
		return street;
	}

	public AddressDadata setStreet(String street) {
		
		this.street = street;
		
		return this;
	}

	public String getHouseType() {
		
		return houseType;
	}

	public AddressDadata setHouseType(String houseType) {
		
		this.houseType = houseType;
		
		return this;
	}

	public String getHouseTypeFull() {
		
		return houseTypeFull;
	}

	public AddressDadata setHouseTypeFull(String houseTypeFull) {
		
		this.houseTypeFull = houseTypeFull;
		
		return this;
	}

	public String getHouse() {
		
		return house;
	}

	public AddressDadata setHouse(String house) {
		
		this.house = house;
		
		return this;
	}

	public String getBlockType() {
		
		return blockType;
	}

	public AddressDadata setBlockType(String blockType) {
		
		this.blockType = blockType;
		
		return this;
	}

	public String getBlock() {
		
		return block;
	}

	public AddressDadata setBlock(String block) {
		
		this.block = block;
		
		return this;
	}

	public String getFlatType() {
		
		return flatType;
	}

	public AddressDadata setFlatType(String flatType) {
		
		this.flatType = flatType;
		
		return this;
	}

	public String getFlat() {
		
		return flat;
	}

	public AddressDadata setFlat(String flat) {
		
		this.flat = flat;
		
		return this;
	}

	public String getPostalBox() {
		
		return postalBox;
	}

	public AddressDadata setPostalBox(String postalBox) {
		
		this.postalBox = postalBox;
		
		return this;
	}

	public String getKladrId() {
		
		return kladrId;
	}

	public AddressDadata setKladrId(String kladrId) {
		
		this.kladrId = kladrId;
		
		return this;
	}

	public String getOkato() {
		
		return okato;
	}

	public AddressDadata setOkato(String okato) {
		
		this.okato = okato;
		
		return this;
	}

	public String getOktmo() {
		
		return oktmo;
	}

	public AddressDadata setOktmo(String oktmo) {
		
		this.oktmo = oktmo;
		
		return this;
	}

	public String getTaxOffice() {
		
		return taxOffice;
	}

	public AddressDadata setTaxOffice(String taxOffice) {
		
		this.taxOffice = taxOffice;
		
		return this;
	}

	public String getFlatArea() {
		
		return flatArea;
	}

	public AddressDadata setFlatArea(String flatArea) {
		
		this.flatArea = flatArea;
		
		return this;
	}

	public int getQcComplete() {
		
		return qcComplete;
	}

	public AddressDadata setQcComplete(int qcComplete) {
		
		this.qcComplete = qcComplete;
		
		return this;
	}

	public int getQcHouse() {
		
		return qcHouse;
	}

	public AddressDadata setQcHouse(int qcHouse) {
		
		this.qcHouse = qcHouse;
		
		return this;
	}

	public int getQc() {
		
		return qc;
	}

	public AddressDadata setQc(int qc) {
		
		this.qc = qc;
		
		return this;
	}

	public String getUnparsedParts() {
		
		return unparsedParts;
	}

	public AddressDadata setUnparsedParts(String unparsedParts) {
		
		this.unparsedParts = unparsedParts;
		
		return this;
	}
}