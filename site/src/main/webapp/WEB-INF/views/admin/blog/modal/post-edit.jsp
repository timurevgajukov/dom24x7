<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/resources/themes/homer/vendor/summernote/dist/summernote.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/summernote/dist/summernote-bs3.css" />

<!-- EDIT POST MODAL WINDOW: BEGIN -->
<div id="post-edit-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 id="post-edit-modal-title" class="modal-title">Сохранение поста</h4>
            </div>
            <form id="meter-form" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="modal-post-title" type="text" class="form-control"  placeholder="Введите название поста"/>
                            </div>
                            <div class="form-group">
                                <div id="modal-post-text"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="modal-post-published" type="text" class="form-control" placeholder="Введите время публикации"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select id="modal-post-category" class="form-control" placeholder="Выберите категорию">
                                            <c:forEach items="${categories}" var="category">
                                                <option value="${category.id}">${category.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input id="modal-post-source" type="text" class="form-control" placeholder="Введите источник новости" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="modal-post-photo"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-post-save" class="ladda-button btn btn-success" type="button" data-style="zoom-in">Сохранить</button>
                    <button id="btn-post-delete" class="btn btn-danger" type="button">Удалить</button>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT POST MODAL WINDOW: END -->

<script src="/resources/themes/homer/vendor/summernote/dist/summernote.min.js"></script>
<script src="/resources/themes/homer/vendor/summernote/lang/summernote-ru-RU.js"></script>
<script src="/resources/js/field.picture.js"></script>
<script src="/resources/js/form.generator.js"></script>
<script src="/resources/js/field.block.js"></script>