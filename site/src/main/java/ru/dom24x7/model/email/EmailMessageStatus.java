package ru.dom24x7.model.email;

import ru.dom24x7.model.Model;

/**
 * Created by timur on 17.03.17.
 * Статус почтового сообщения
 */
public class EmailMessageStatus extends Model {

    public EmailMessageStatus(int id, String name) {

        super(id, name);
    }
}