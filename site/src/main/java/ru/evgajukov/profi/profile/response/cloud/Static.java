package ru.evgajukov.profi.profile.response.cloud;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class Static {

    private List<Price> price;

    public List<Price> getPrice() {

        return price;
    }

    public void setPrice(List<Price> price) {

        this.price = price;
    }
}
