package ru.evgajukov.profi;

import com.google.gson.Gson;
import ru.evgajukov.profi.dictionary.request.DictionaryRequest;
import ru.evgajukov.profi.dictionary.response.DictionaryResponse;
import ru.evgajukov.profi.profile.response.ProfileResponse;
import ru.evgajukov.profi.profile.request.ProfileRequest;
import ru.evgajukov.profi.profile.response.entry.Entry;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 15.06.17.
 * Основной класс для работы с Profi API
 */
public class Profi {

    private static final String API_URI = "https://api.profi.ru/web/client/v1/";

    private static final int TIMEOUT = 5000;

    private static Profi instance = null;

    public static Profi getInstance() {

        if (instance == null) {
            instance = new Profi();
        }

        return instance;
    }

    private HttpsURLConnection conn;
    private Gson gson;

    {
        gson = new Gson();
    }

    private String apiClient;
    private String apiKey;

    private Profi() {}

    public Profi setApiClient(String apiClient) {

        this.apiClient = apiClient;
        return this;
    }

    public Profi setApiKey(String apiKey) {

        this.apiKey = apiKey;
        return this;
    }

    /**
     * Возвращает список справочников
     *
     * @param data
     * @return
     */
    public DictionaryResponse get(DictionaryRequest data) {

        return (DictionaryResponse) get(
                Profi.API_URI + DictionaryResponse.API_NAME,
                DictionaryResponse.API_NAME,
                data == null ? null : data.toString(),
                DictionaryResponse.class);
    }

    /**
     * Возвращает список профилей мастеров
     * @param data
     * @return
     */
    public List<Profile> get(ProfileRequest data) {

        List<Profile> profiles = new ArrayList<>();
        ProfileResponse response = (ProfileResponse) get(
                Profi.API_URI,
                ProfileResponse.API_NAME,
                data == null ? null : data.toString(),
                ProfileResponse.class);
        for (Entry entry : response.getEntries()) {
            profiles.add(new Profile(entry));
        }

        return profiles;
    }

    /**
     * Общиу метод получения данных по PROFI API
     * @param url
     * @param method
     * @param data
     * @param classOf
     * @return
     */
    private Object get(String url, String method, String data, Class classOf) {

        Object result = null;

        try {
            createRequest(url, method, data);

            conn.connect();
            if (conn.getResponseCode() == conn.HTTP_OK) {
                result = gson.fromJson(read(), classOf);
            } else {
                // не нашли ресурс по ссылке
                System.out.println(">>>>> По указанной ссылке ресурс не найден");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            if (e.getMessage().indexOf("403 Forbidden") != -1) {
                // нет доступа
                System.out.println(">>>>> В доступе отказано");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }

    /**
     * Формирует объект запроса
     *
     * @return
     */
    private void createRequest(String url, String method, String data) throws IOException {

        System.setProperty("https.protocols", "TLSv1.1");
        conn = (HttpsURLConnection) new URL(url).openConnection();
        conn.setConnectTimeout(TIMEOUT);
        conn.setReadTimeout(TIMEOUT);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestProperty("API", method);
        conn.setRequestProperty("API_CLIENT", apiClient);
        conn.setRequestProperty("API_KEY", apiKey);

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(data == null ? null : data.toString());
        writer.flush();
        writer.close();
        os.close();
    }

    /**
     * Считывает данные из объекта ответа
     *
     * @return
     * @throws IOException
     */
    private String read() throws IOException {

        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        try {
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String strLine;
            while ((strLine = in.readLine()) != null) {
                result.append(strLine);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result.toString();
    }
}