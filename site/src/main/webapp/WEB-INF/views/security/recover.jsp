<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:choose>
    <c:when test="${security.auth}">
        <div class="row">
            <div class="col-md-6">
                <div id="recover-panel" class="hpanel hblue">
                    <div id="recover-panel-alert" class="alert" style="display: none;">
                        <span id="recover-panel-alert-message" class="error-message"></span>
                    </div>
                    <div class="panel-body">
                        <form id="modal-reg-form" method="post" action="/security/recover/change">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input class="form-control" type="password" id="recover-password1" name="password1" required
                                           placeholder="Введите пароль"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input class="form-control" type="password" id="recover-password2" name="password2" required
                                           placeholder="Введите пароль повторно"/>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button id="btn-send-recover" class="ladda-button btn btn-success text-uppercase" type="button" data-style="zoom-in">
                                    Восстановить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <h1>Некорректный код восстановления пароля</h1>
        <p>Перейдите на <a href="/">начальную страницу</a> и запросите код восстановления повторно.</p>
    </c:otherwise>
</c:choose>

<script>
    $(document).ready(function () {
        var btnSendRecover = $('#btn-send-recover').ladda();
        btnSendRecover.click(function () {
            var data = {
                pwd1: $('#recover-password1').val(),
                pwd2: $('#recover-password2').val()
            }

            if (data.pwd1 == null || data.pwd1.length == 0) {
                showPanelAlert('recover-panel', 'Необходимо указать новый пароль', 'hblue', 3000);
                $('#recover-password1').focus();
                return;
            }

            if (data.pwd1 != data.pwd2) {
                showPanelAlert('recover-panel', 'Пароли должны совпадать', 'hblue', 3000);
                $('#recover-password2').focus();
            }

            // теперь можем отправить запрос на восстановление пароля
            btnSendRecover.ladda('start');
            $.post('/security/recover/change', data, function (result) {
                btnSendRecover.ladda('stop');
                if (!result) {
                    showPanelAlert('recover-panel', 'Неизвестная ошибка.', null, 5000);
                    return;
                }

                if (result.code != 0) {
                    if (result.body != null && result.body.length != 0) {
                        showPanelAlert('recover-panel', result.body.body, null, 5000);
                    } else {
                        showPanelAlert('recover-panel', 'Неизвестная ошибка. Код: ' + result.code, null, 5000);
                    }
                } else {
                    showPanelAlert('recover-panel', 'Пароль успешно сменен', null, 5000, 'alert-info');
                }
            }).error(function () {
                btnSendRecover.ladda('stop');
            });
        });
    });
</script>