package ru.dom24x7.model.post.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.post.Category;
import ru.dom24x7.model.reference.ReferenceDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 01.03.17.
 * Реализация интерфейса получения справочника категорий поста
 */
public class CategoryDAOImpl extends ModelDAOImpl implements ReferenceDAO<Category> {

    private static final String QUERY_LIST = "{ call categories_list() }";

    @Override
    public List<Category> list() {

        List<Category> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Category>() {
            @Override
            public Category mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private Category load(ResultSet rs) throws SQLException {

        return new Category(rs.getInt("id"), rs.getString("name"), rs.getString("url_name"));
    }
}