package ru.dom24x7.utils;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by timur on 12.10.16.
 *
 * Утилита для работы с запросами и заголовками
 */
public class HttpUtils {

    /**
     * Из заголовка запроса вызвращает нужный параметр в UTF-8
     *
     * @param request
     * @param paramName
     * @return
     */
    public static String getQueryParam(HttpServletRequest request, String paramName) {

        try {
            String queryString = URLDecoder.decode(request.getQueryString(), "UTF-8");
            String[] params = queryString.split("&");
            for (int i = 0; i < params.length; i++) {
                if (params[i].indexOf(paramName) == 0) {
                    // нашли нужный параметр
                    String[] keyValue = params[i].split("=");
                    if (keyValue.length == 2) {
                        return params[i].split("=")[1];
                    } else {
                        return null;
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {}

        return null;
    }
}