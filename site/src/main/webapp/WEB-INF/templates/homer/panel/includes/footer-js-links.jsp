<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Vendor scripts -->
<script src="/resources/themes/homer/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/themes/homer/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="/resources/themes/homer/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/resources/themes/homer/vendor/jquery-flot/jquery.flot.js"></script>
<script src="/resources/themes/homer/vendor/jquery-flot/jquery.flot.resize.js"></script>
<script src="/resources/themes/homer/vendor/jquery-flot/jquery.flot.pie.js"></script>
<script src="/resources/themes/homer/vendor/flot.curvedlines/curvedLines.js"></script>
<script src="/resources/themes/homer/vendor/jquery.flot.spline/index.js"></script>
<script src="/resources/themes/homer/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="/resources/themes/homer/vendor/iCheck/icheck.min.js"></script>
<script src="/resources/themes/homer/vendor/peity/jquery.peity.min.js"></script>
<script src="/resources/themes/homer/vendor/sparkline/index.js"></script>
<script src="/resources/lib/sweetalert/dist/sweetalert.min.js"></script>
<script src="/resources/themes/homer/vendor/toastr/build/toastr.min.js"></script>
<script src="/resources/themes/homer/vendor/ladda/dist/spin.min.js"></script>
<script src="/resources/themes/homer/vendor/ladda/dist/ladda.min.js"></script>
<script src="/resources/themes/homer/vendor/ladda/dist/ladda.jquery.min.js"></script>

<script src="/resources/lib/dot/doT.min.js"></script>

<script src="/resources/lib/moment/moment.min.js"></script>
<script src="/resources/lib/moment/moment-timezone.min.js"></script>
<script src="/resources/lib/moment/locales.min.js"></script>

<script src="/resources/themes/homer/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/v/bs/dt-1.10.13/datatables.min.js"></script>

<!-- App scripts -->
<script src="/resources/themes/homer/scripts/homer.js"></script>
<script src="/resources/themes/homer/scripts/charts.js"></script>

<script src="/resources/js/panel.js"></script>

<script src="/resources/js/company.list.js"></script>