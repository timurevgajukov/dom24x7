package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.dom24x7.site.BaseController;

/**
 * Created by timur on 17.02.17.
 *
 * Административная панель: управление шаблонами
 */
@Controller
public class AdminTemplateController extends BaseController {

    @RequestMapping("/admin/templates")
    public String listAction(ModelMap model) {

        setModelMap(model, "Список шаблонов");

        return "admin/template/list";
    }
}