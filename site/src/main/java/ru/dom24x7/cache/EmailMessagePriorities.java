package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.email.EmailMessagePriority;
import ru.dom24x7.model.email.dao.EmailMessagePriorityDAOImpl;

import java.util.List;

/**
 * Created by timur on 17.03.17.
 * Контейнер для хранения справочника приоритетов почтовых сообщений
 */
public class EmailMessagePriorities implements ICache<EmailMessagePriority> {

    private static EmailMessagePriorities instance = null;

    private List<EmailMessagePriority> list;

    public static EmailMessagePriorities getInstance() {

        if (instance == null) {
            instance = new EmailMessagePriorities();
        }

        return instance;
    }

    private EmailMessagePriorities() {

        list = new EmailMessagePriorityDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<EmailMessagePriority> list() {

        return list;
    }

    @Override
    public EmailMessagePriority get(int id) {

        if (list == null) {
            return null;
        }

        for (EmailMessagePriority item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}