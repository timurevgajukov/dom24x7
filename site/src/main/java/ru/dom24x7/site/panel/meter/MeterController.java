package ru.dom24x7.site.panel.meter;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.dom24x7.cache.Companies;
import ru.dom24x7.cache.MeterTypes;
import ru.dom24x7.cache.Settings;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.model.meter.MeterType;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.model.meter.dao.MeterHistoryDAOImpl;
import ru.dom24x7.model.meter.validate.MeterHistoryItemValidator;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by timur on 22.09.16.
 *
 * Контроллер для работы со счетчиками
 */
@Controller
public class MeterController extends BaseController {

    private static final String ACTIVE = "meters";

    private static final int COUNT = 10;
    private int currentPage;

    @RequestMapping("/panel/addresses/{addressId}/meters/{meterId}")
    public String historyAction(@PathVariable int addressId, @PathVariable int meterId, ModelMap model) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            security.getMessages().add(ERROR_MESSAGES.get("ERROR_02"));
            return "redirect:/panel";
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            // счетчик не найден по указанному адресу
            security.getMessages().add(ERROR_MESSAGES.get("ERROR_03"));
            return "redirect:/panel/addresses/" + addressId;
        }

        currentPage = 1;

        meter.loadDevices();

        setModelMap(model, meter.getTitle(), ACTIVE);
        model.addAttribute("meter", meter);
        model.addAttribute("meters", new MeterDAOImpl().list(address));
        model.addAttribute("history", new MeterHistoryDAOImpl().list(meter, 0, COUNT));

        return "panel/meter/history";
    }

    @RequestMapping("/panel/addresses/{addressId}/meters/{meterId}/more")
    public String historyMoreAction(@PathVariable int addressId, @PathVariable int meterId, ModelMap model) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            security.getMessages().add(ERROR_MESSAGES.get("ERROR_02"));
            return "redirect:/panel";
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            // счетчик не найден по указанному адресу
            security.getMessages().add(ERROR_MESSAGES.get("ERROR_03"));
            return "redirect:/panel/addresses/" + addressId;
        }

        currentPage++;

        meter.loadDevices();

        setModelMap(model, meter.getTitle(), ACTIVE);
        model.addAttribute("meter", meter);
        model.addAttribute("history", new MeterHistoryDAOImpl().list(meter, (currentPage - 1) * COUNT, COUNT));

        return "panel/meter/history";
    }

    @RequestMapping("/panel/addresses/{addressId}/meters/{meterId}/history/{valueId}")
    public @ResponseBody MeterHistoryItem valueGetAction(
            @PathVariable int addressId, @PathVariable int meterId, @PathVariable int valueId) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            return null;
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            // счетчик не найден по указанному адресу
            return null;
        }

        return meter.getHistoryItem(valueId);
    }

    @RequestMapping(value = "/panel/addresses/{addressId}/meters/{meterId}/history/save", method = RequestMethod.POST)
    public @ResponseBody Response valueEditPostAction(
            @PathVariable int addressId,
            @PathVariable int meterId,
            @RequestParam(value = "id", required = false) Integer itemId,
            @RequestParam(value = "date", required = false) String strDate,
            @RequestParam(value = "value", required = false) String strValue) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            messages.add(ERROR_MESSAGES.get("ERROR_02"));
            return new Response(-1, "ERROR", messages);
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            // счетчик не найден по указанному адресу
            messages.add(ERROR_MESSAGES.get("ERROR_03"));
            return new Response(-1, "ERROR", messages);
        }

        meter.loadHistory();

        MeterHistoryItem item;
        if (itemId == null) {
            item = new MeterHistoryItem();
            item.setMeter(meter);
        } else {
            item = meter.getHistoryItem(itemId);
            if (item == null) {
                messages.add(new Message("Не найдено показание счетчика по идентификатору", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        Date date;
        if (strDate == null || strDate.trim().length() == 0) {
            messages.add(new Message("Необходимо указать дату снятия показания счетчика", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        } else {
            try {
                date = new SimpleDateFormat("dd.MM.yyyy").parse(strDate.trim());
            } catch (ParseException e) {
                messages.add(new Message("Дата должна быть в формате ДД.ММ.ГГГГ", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        Double value;
        if (strValue == null || strValue.trim().length() == 0) {
            messages.add(new Message("Необходимо указать показание счетчика", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        } else {
            try {
                value = Double.valueOf(strValue.trim().replaceAll(",", "."));
            } catch (NumberFormatException e) {
                messages.add(new Message("Неверный формат счетчика", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        item.setDate(date);
        if (meter.getType().isWithoutValue()) {
            item.setCost(value);
        } else {
            item.setValue(value);
        }

        // валидируем данные
        MeterHistoryItemValidator validator = new MeterHistoryItemValidator(item);
        if (!validator.validate()) {
            return new Response(-1, "ERROR", validator.getErrors());
        }

        // все хорошо, можем сохранить данные
        new MeterHistoryDAOImpl().save(item);

        return new Response(0, "OK");
    }

    @RequestMapping("/panel/addresses/{addressId}/meters/{meterId}/history/delete/{itemId}")
    public @ResponseBody Response deleteAction(
            @PathVariable int addressId,
            @PathVariable int meterId,
            @PathVariable int itemId) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            return new Response(-1, "ERROR", ERROR_MESSAGES.get("ERROR_02"));
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            // счетчик не найден по указанному адресу
            return new Response(-1, "ERROR", ERROR_MESSAGES.get("ERROR_03"));
        }

        meter.loadHistory();

        MeterHistoryItem item = meter.getHistoryItem(itemId);
        if (item == null) {
            return new Response(-1, "ERROR", ERROR_MESSAGES.get("ERROR_04"));
        }

        new MeterHistoryDAOImpl().delete(item);

        return new Response(0, "OK");
    }

    @RequestMapping(value = "/ajax/addresses/{addressId}/meters/save", method = RequestMethod.POST)
    public @ResponseBody Response saveMeterAction(
            @PathVariable int addressId,
            @RequestParam(value = "id", required = false) Integer meterId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "type", required = false) Integer typeId,
            @RequestParam(value = "tariff", required = false) String tariffStr,
            @RequestParam(value = "capacity", required = false) String capacityStr,
            @RequestParam(value = "precision", required = false) String precisionStr) {

        List<Message> messages = new ArrayList<Message>();

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            messages.add(new Message("Не удалось найти адрес с указанным идентификатором", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        Meter meter;
        if (meterId == null) {
            // добавление нового счетчика
            meter = new Meter();
            meter.setAddress(address);
        } else {
            // редактирование существующего счетчика
            meter = address.getMeter(meterId);
            if (meter == null) {
                messages.add(new Message("Не удалось найти счетчик с указанным идентификатором", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        if (title == null || title.trim().length() == 0) {
            messages.add(new Message("Необходимо указать наименование счетчика", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        MeterType type;
        if (typeId == null) {
            messages.add(new Message("Необходимо указать тип счетчика", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        } else {
            type = MeterTypes.getInstance().get(typeId);
            if (type == null) {
                messages.add(new Message("Не удалось найти тип счетчика с указанным идентификатором", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        double tariff;
        if (tariffStr == null || tariffStr.trim().length() == 0) {
            messages.add(new Message("Необходимо указать тариф", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        } else {
            try {
                tariff = Double.parseDouble(tariffStr.trim().replaceAll(",", "."));
                if (tariff <= 0) {
                    messages.add(new Message("Тариф должен быть строго положительным", Message.TYPE_ERROR));
                    return new Response(-1, "ERROR", messages);
                }
            } catch (Exception e) {
                messages.add(new Message("Тариф указан в неверном формате", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        // теперь можем заполнить данные по счетчику
        meter.setTitle(title.trim());
        meter.setType(type);
        meter.setTariff(tariff);
        meter.setCapacity(getCapacityPrecision(capacityStr));
        meter.setPrecision(getCapacityPrecision(precisionStr));

        // сохраняем данные
        new MeterDAOImpl().save(meter);

        return new Response(0, "OK", meter);
    }

    @RequestMapping("/ajax/addresses/{addressId}/meters/delete/{meterId}")
    public @ResponseBody Response deleteMeterAction(@PathVariable int addressId, @PathVariable int meterId) {

        List<Message> messages = new ArrayList<Message>();

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            messages.add(new Message("Не удалось найти адрес с указанным идентификатором", Message.TYPE_DANGER));
            return new Response(-1, "ERROR", messages);
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            messages.add(new Message("Не удалось найти счетчик с указанным идентификатором", Message.TYPE_DANGER));
            return new Response(-1, "ERROR", messages);
        }

        // вместо удаления счетчик просто блокируется, чтобы в случае чего можно было бы легко восстановить
        new MeterDAOImpl().hold(meter);

        return new Response(0, "OK");
    }

    @RequestMapping("/ajax/addresses/{addressId}/meters/{meterId}")
    public @ResponseBody Meter getMeterAjax(@PathVariable int addressId, @PathVariable int meterId) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            return null;
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            return null;
        }

        return meter;
    }

    @RequestMapping("/ajax/addresses/{addressId}/meters/{meterId}/years")
    public @ResponseBody Set<Integer> meterHistoryYearsAjax(@PathVariable int addressId, @PathVariable int meterId) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            return null;
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            return null;
        }

        meter.loadHistory();
        List<MeterHistoryItem> history = meter.getHistory();
        if (history == null || history.size() == 0) {
            return null;
        }

        SortedSet<Integer> years = new TreeSet<Integer>(new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {

                // сортируем по убыванию
                return o2.compareTo(o1);
            }
        });
        for (MeterHistoryItem item : history) {
            years.add(Integer.parseInt(new SimpleDateFormat("yyyy").format(item.getDate())));
        }

        return years;
    }

    @RequestMapping("/ajax/addresses/{addressId}/meters/{meterId}/email")
    public @ResponseBody Response getSendDataAjax(@PathVariable int addressId, @PathVariable int meterId) {

        List<Message> messages = new ArrayList<Message>();

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            messages.add(new Message("Не удалось найти адрес с указанным идентификатором", Message.TYPE_DANGER));
            return new Response(-1, "ERROR", messages);
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            messages.add(new Message("Не удалось найти счетчик с указанным идентификатором", Message.TYPE_DANGER));
            return new Response(-1, "ERROR", messages);
        }

        Company company = meter.getCompany();
        if (company == null) {
            messages.add(new Message("Нет привязки к компании", Message.TYPE_DANGER));
            return new Response(-1, "ERROR", messages);
        }

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("template", Settings.getInstance().get("MAIL_COMPANY_BODY"));
        result.put("address", address.getAddress());
        result.put("email", security.getUser().getEmail());
        result.put("account", meter.getCompany().getAccount(security.getUser()).getAccount());

        List<Meter> list = new MeterDAOImpl().list(address, true);
        if (list != null && list.size() != 0) {
            List<Object> meters = new ArrayList<Object>();
            for (Meter item : list) {
                if (item.getCompany() != null && item.getCompany().getId() == company.getId() && item.getHistory().size() != 0) {
                    Map<String, Object> meterInfo = new HashMap<String, Object>();
                    meterInfo.put("name", item.getTitle());
                    meterInfo.put("value", item.getHistory().get(0));
                    meters.add(meterInfo);
                }
            }
            result.put("meters", meters);
        }

        return new Response(0, "OK", result);
    }

    /**
     * Из строки формирует данные для точности/разрядности
     * @param str
     * @return
     */
    private int getCapacityPrecision(String str) {
        int result = -1;
        if (str != null && str.trim().length() != 0) {
            try {
                result = Integer.parseInt(str.trim());
                if (result < -1) {
                    result = -1;
                }
            } catch (Exception e) {}
        }

        return result;
    }
}