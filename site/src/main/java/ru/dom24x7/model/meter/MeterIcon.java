package ru.dom24x7.model.meter;

import ru.dom24x7.model.Model;

/**
 * Иконка для счетчика
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class MeterIcon extends Model {

	public MeterIcon() {
		
		super();
	}
	
	public MeterIcon(int id) {
		
		super(id);
	}
	
	public MeterIcon(int id, String name) {
		
		super(id, name);
	}

	public String getBigUrl() {

		return "/resources/images/icons/meters/big/" + name;
	}

	public String getSmallUrl() {

		return "/resources/images/icons/meters/small/" + name;
	}
}