<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>${title} | Dom24x7</title>

    <meta name="description" content="${description}"/>
    <meta name="keywords" content="${keywords}"/>

    <c:if test="${og != null}">
        <meta property="og:title" content="${og.title}"/>
        <meta property="og:description" content="${og.description}"/>
        <meta property="og:type" content="${og.type}"/>
        <meta property="og:url" content="${og.url}"/>
        <c:if test="${og.image != null}"><meta property="og:image" content="${og.image}"/></c:if>
    </c:if>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="/resources/images/favicon.ico" />

    <t:insertAttribute name="header-css-js-links" defaultValue="/WEB-INF/templates/homer/panel/includes/header-css-js-links.jsp"/>
    <t:insertAttribute name="vk-pixel" defaultValue="/WEB-INF/templates/homer/all/vk-pixel.jsp"/>

</head>
<body class="fixed-navbar fixed-sidebar">

<!-- Simple splash screen-->
<t:insertAttribute name="preloader" defaultValue="/WEB-INF/templates/homer/all/preloader.jsp"/>

<!-- Header -->
<t:insertAttribute name="header" defaultValue="/WEB-INF/templates/homer/panel/includes/header.jsp"/>

<!-- Navigation -->
<t:insertAttribute name="navigation" defaultValue="/WEB-INF/templates/homer/panel/includes/navigation.jsp"/>

<!-- Main Wrapper -->
<div id="wrapper">

    <div class="content animate-panel">
        <t:insertAttribute name="content"/>
    </div>

    <!-- Right sidebar -->
    <t:insertAttribute name="right-sidebar" defaultValue="/WEB-INF/templates/homer/panel/includes/right-sidebar.jsp"/>

    <!-- Footer-->
    <t:insertAttribute name="footer" defaultValue="/WEB-INF/templates/homer/panel/includes/footer.jsp"/>

</div>

<t:insertAttribute name="footer-js-links" defaultValue="/WEB-INF/templates/homer/panel/includes/footer-js-links.jsp"/>

<t:insertAttribute name="counters" defaultValue="/WEB-INF/templates/homer/all/counters.jsp"/>

<script>
    $(document).ready(function() {
        moment.locale('ru');

        toastr.options = {
            "debug": false,
            "newestOnTop": false,
            "positionClass": "toast-top-center",
            "closeButton": true,
            "toastClass": "animated fadeInDown",
        };

        $('[data-tooltip="true"]').tooltip();

        // если есть, то отобразим обычные нотификации
        <c:if test="${security != null && security.messages.has()}">
            <c:forEach items="${security.messages.listWithClear()}" var="msg">
                showAlert("${msg.body}", "${msg.type}");
            </c:forEach>
        </c:if>
    });
</script>

</body>
</html>