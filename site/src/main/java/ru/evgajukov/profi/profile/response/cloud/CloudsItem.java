package ru.evgajukov.profi.profile.response.cloud;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class CloudsItem {

    private String id;
    private List<Cloud> cloud;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public List<Cloud> getCloud() {

        return cloud;
    }

    public void setCloud(List<Cloud> cloud) {

        this.cloud = cloud;
    }

    @Override
    public String toString() {

        return id;
    }
}