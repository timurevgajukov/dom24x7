package ru.dom24x7.model.user;

import ru.dom24x7.model.Model;

/**
 * Created by timur on 14.02.17.
 *
 * Роль пользователя
 */
public class Role extends Model {

    public static final int ROLE_ADMIN   = 1;
    public static final int ROLE_USER    = 2;
    public static final int ROLE_COMPANY = 3;
    public static final int ROLE_LAWYER  = 4;
    public static final int ROLE_EDITOR  = 5;

    public Role(int id) {

        super(id);
    }

    public Role(int id, String name) {

        super(id, name);
    }
}