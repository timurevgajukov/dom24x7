/**
 * Created by timur on 26.12.16.
 * Фабрика для формирования различных типов полей
 */
function FormGenerator() {
    var self = this;

    // генератор форм
    this._generator = {
        'field': function (options) {
            options.index = options.index || 0; // если не указан (для неблочных полей)

            var data = options.data;

            options.value = '';
            if (data.values != null && data.values.length != 0 && options.index < data.values.length) {
                options.value = data.values[options.index];
            }
            options.required = '';
            if (data.required) {
                options.required = 'required';
            }
            options.readonly = '';
            if (data.readOnly) {
                options.readonly = 'readonly';
            }

            var func = self._generator[data.type];
            if (!func) {
                return new UnknownField({
                    data: data
                });
            }

            return self._generator[data.type](options);
        },
        'Text': function (options) {
            return new TextField(options);
        },
        'EditText': function (options) {
            return new EditTextField(options);
        },
        'Date': function (options) {
            return new DateField(options);
        },
        'Phone': function (options) {
            return new PhoneField(options);
        },
        'Email': function (options) {
            return new EmailField(options);
        },
        'ComboBox': function (options) {
            return new ComboBoxField(options);
        },
        'CheckBox': function (options) {
            return new CheckBoxField(options);
        },
        'Radio': function (options) {
            return new RadioField(options);
        },
        'Picture': function (options) {
            return new PictureField(options);
        }
    };
}

/**
 * Возвращает объект поля нужного типа
 * @param document документ, который пришел с сервера
 * @param data данные по конкретному полю, которые пришли с сервера
 * @param guid GUID формы
 */
FormGenerator.prototype.field = function (options) {
    return this._generator.field(options);
};