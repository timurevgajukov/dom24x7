<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>URL</th>
        <th>all</th>
        <c:if test="${roles != null && roles.size() != 0}">
            <c:forEach items="${roles}" var="role">
                <th>${role.name}</th>
            </c:forEach>
        </c:if>
    </tr>
    </thead>
    <tbody>
    <c:if test="${links != null && links.size() != 0}">
        <c:forEach items="${links}" var="link">
            <tr>
                <td>${link}</td>
                <td>
                    <div class="checkbox">
                        <label>
                            <input class="ch-access" type="checkbox" data-link="${link}" data-role="all"
                                   <c:if test="${matrix.get(link).get('all')}">checked</c:if> />
                        </label>
                    </div>
                </td>
                <c:if test="${roles != null && roles.size() != 0}">
                    <c:forEach items="${roles}" var="role">
                        <td>
                            <div class="checkbox">
                                <label>
                                    <input class="ch-access" type="checkbox" data-link="${link}" data-role="${role.name}"
                                           <c:if test="${matrix.get(link).get(role.name)}">checked</c:if> />
                                </label>
                            </div>
                        </td>
                    </c:forEach>
                </c:if>
            </tr>
        </c:forEach>
    </c:if>
    </tbody>
</table>

<script>
    $(document).ready(function () {
        var roles = JSON.parse('${rolesJson}');
        $('.ch-access').change(function () {
            var link = $(this).data('link');
            var role = $(this).data('role');
            var value = $(this).is(':checked');

            if ('all' == role && value) {
                // если ресурс для всех посетителей, то для всех ролей нужно сбросить
                for (var i = 0; i < roles.length; i++) {
                    $('input[data-link="' + link + '"][data-role="' + roles[i].name + '"]').removeAttr('checked');
                }
            } else {
                // если ресурс для конкретной роли, то для всех посетителей нужно сбросить
                $('input[data-link="' + link + '"][data-role="all"]').removeAttr('checked');
            }

            // отправляем запрос на сохранение изменений
            var data = {
                link: link,
                role: role,
                value: value
            };
            $.post('/admin/ajax/access/change', data, function (result) {
                if (!result) {
                    console.log('Запрос на смену уровня доступа не вернул результата');
                    return;
                }

                var msg = result.body;
                showAlert(msg.body, msg.type);
            });
        });
    });
</script>