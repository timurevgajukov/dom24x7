<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        Доли расходов по счетчикам
    </div>
    <div class="panel-body">
        <form>
            <div class="row">
                <div class="col-md-6">
                    <c:if test="${userAddresses != null && userAddresses.size() != 0}">
                        <div class="form-group">
                            <select id="r1-address" class="form-control" data-placeholder="Выберите адрес">
                                <c:forEach items="${userAddresses}" var="address">
                                    <option value="${address.id}">${address}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </c:if>
                    <div class="form-group">
                        <div class="input-group">
                            <input id="r1-date-from" class="form-control exp-date" type="text" placeholder="период с"/>
                            <span class="input-group-addon">до</span>
                            <input id="r1-date-to" class="form-control exp-date" type="text" placeholder="период до"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="checkbox"><label><input id="r1-type-meter-join" type="checkbox"> объединить по типу счетчика</label></div>
                </div>
            </div>
        </form>

        <div id="report-pie-container"></div>
    </div>
</div>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        История расходов по счетчику
    </div>
    <div class="panel-body">
        <form>
            <div class="col-md-6">
                <c:if test="${userAddresses != null && userAddresses.size() != 0}">
                    <div class="form-group">
                        <select id="r2-address" class="form-control" data-placeholder="Выберите адрес">
                            <c:forEach items="${userAddresses}" var="address">
                                <option value="${address.id}">${address}</option>
                            </c:forEach>
                        </select>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="r2-meter" class="form-control" data-placeholder="Выберите счетчик"></select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="r2-year" class="form-control" data-placeholder="Выберите год"></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="checkbox"><label><input id="r2-by-cost" type="checkbox"> по стоимости</label></div>
            </div>
        </form>

        <div id="report-column-container"></div>
    </div>
</div>

<script src="//code.highcharts.com/highcharts.js"></script>
<script src="/resources/js/reports.js"></script>

<script>
    $(document).ready(function () {
        $('.exp-date').datetimepicker({
            format: 'DD.MM.YYYY',
            locale: 'ru'
        });

        // начальное заполнение полей с датами
        $('#r1-date-from').val('01.' + moment(new Date()).format('MM.YYYY'));
        $('#r1-date-to').val(moment().format('DD.MM. YYYY'));

        showReportPie();
        showReportColumn();

        $('#r1-address').change(function () {
            showReportPie();
        });
        $('#r1-date-from').on('dp.change', function () {
            showReportPie();
        });
        $('#r1-date-to').on('dp.change', function () {
            showReportPie();
        });
        $('#r1-type-meter-join').iCheck({ checkboxClass: 'icheckbox_square-green' }).on('ifToggled', function (e) {
            showReportPie();
        });
        $('#r2-address').change(function () {
            showReportColumn();
        });
        $('#r2-meter').change(function () {
            showReportColumn();
        });
        $('#r2-year').change(function () {
            showReportColumn();
        });
        $('#r2-by-cost').iCheck({ checkboxClass: 'icheckbox_square-green' }).on('ifToggled', function (event) {
            showReportColumn();
        });

        window.setTimeout(function () {
            $('#r2-address').trigger('change');
        }, 1000);
        $('#r2-address').change(function () {
            var metersList = $('#r2-meter');
            metersList.empty();
            metersList.val('');
            $.post('/ajax/addresses/' + $(this).val() + '/meters', function (meters) {
                if (meters === undefined || meters == null) {
                    return;
                }

                for (var i = 0; i < meters.length; i++) {
                    var meter = meters[i];
                    var item = '<option value="' + meter.id + '">' + meter.title + '</option>';
                    metersList.append(item);
                }
                metersList.val('');
            });
        });
        $('#r2-meter').change(function () {
            var yearsList = $('#r2-year');
            yearsList.empty();
            yearsList.val('');

            var addressId = $('#r2-address').val();
            var meterId = $(this).val();
            if (addressId == null || addressId == 0 || meterId == null || meterId == 0) {
                return;
            }

            $.post('/ajax/addresses/' + addressId + '/meters/' + meterId + '/years', function (years) {
                if (years === undefined || years == null) {
                    return;
                }

                for (var i = 0; i < years.length; i++) {
                    var item = '<option value="' + years[i] + '">' + years[i] + '</option>';
                    yearsList.append(item);
                }
                yearsList.val('');
            });
        });

        function showReportPie() {
            var address = $('#r1-address');
            var dateFrom = $('#r1-date-from');
            var dateTo = $('#r1-date-to');
            var typeMeterJoin = document.getElementById('r1-type-meter-join');

            new Dom24x7Report().showReportPie($('#report-pie-container'), {
                'address': address.val(),
                'date-from': dateFrom.val(),
                'date-to': dateTo.val(),
                'by-meter-type':  typeMeterJoin.checked
            });
        }

        function showReportColumn() {
            var address = $('#r2-address');
            var meter = $('#r2-meter');
            var year = $('#r2-year');
            var byCost = document.getElementById('r2-by-cost');

            new Dom24x7Report().showReportColumn($('#report-column-container'), {
                'address': address.val(),
                'meter': meter.val(),
                'year': year.val(),
                'by-cost': byCost.checked
            });
        }
    });
</script>