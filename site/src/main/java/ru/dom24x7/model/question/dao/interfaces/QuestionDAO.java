package ru.dom24x7.model.question.dao.interfaces;

import ru.dom24x7.model.question.Question;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с вопросами
 */
public interface QuestionDAO {

    /**
     * Возвращает список вопросов
     *
     * @return
     */
    List<Question> list();
}