package ru.dom24x7.model.lawyer.dao.interfaces;

import ru.dom24x7.model.lawyer.LawyerMessage;
import ru.dom24x7.model.lawyer.LawyerMessageComment;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с комментариями юристов к сообщениям пользователей
 */
public interface LawyerMessageCommentDAO {

    /**
     * Возвращает все комментарии юристов к сообщению пользователя
     * @param message
     * @param offset
     * @param count
     * @return
     */
    List<LawyerMessageComment> list(LawyerMessage message, int offset, int count);

    /**
     * Возвращает количество ответов по вопросу пользователя
     * @param message
     * @return
     */
    int count(LawyerMessage message);

    /**
     * Сохранение комментария юриста
     * @param comment
     * @return
     */
    boolean save(LawyerMessageComment comment);
}