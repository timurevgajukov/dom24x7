package ru.evgajukov.dadata;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;

/**
 * Отправка сообщений по сети
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Net {

    private static Net _instance = null;

    private int errorno = 0;
    private String errormsg = "";
    private String result = "";

    public static Net getInstance() {
        if (_instance == null) {
            _instance = new Net();
        }

        return _instance;
    }

    private Net() {
        super();
    }

    public int getErrorNo() {
        return errorno;
    }

    public String getErrorMsg() {
        return errormsg;
    }

    public String getResult() {
        return result;
    }
    
    public synchronized String send(String link, int timeout, String request) throws Exception {
    	HashMap<String, String> properties = new HashMap<String, String>();
    	properties.put("Content-Type", "application/xml;charset=UTF-8");
    	
    	return send(link, timeout, request, properties);
    }

    public synchronized String send(String link, int timeout, String request, HashMap<String, String> properties) throws Exception {
        HttpURLConnection conn = null;
        try {
            Proxy proxy = Proxy.NO_PROXY;

            URL url = new URL(link);

            conn = (HttpURLConnection) url.openConnection(proxy);

            conn.setConnectTimeout(timeout);
            conn.setRequestMethod("POST");
            for (String key : properties.keySet()) {
            	conn.setRequestProperty(key, properties.get(key));
			}

            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            osw.write(request);
            osw.flush();
            osw.close();

            int code = conn.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                result = builder.toString();
                result = result.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&quot;", "\"");
                if (result.length() == 0) {
                    throw new Exception("Result length is null");
                }
            } else {
                // Получить сообщение из потока ошибок
                InputStream is = conn.getErrorStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                errorno = code;
                errormsg = builder.toString();
                if (errormsg.length() == 0) {
                    System.out.print("error result len = 0");
                } else {
                    System.out.println(errormsg);
                }

                throw new Exception("Server error. ErrorNo: " + errorno + ", Msg: " + errormsg);
            }
        } catch (UnknownHostException e) {
            System.out.println(e.toString());
            throw e;
        } catch (Exception e) {
            System.out.println(e.toString());
            throw new Exception(e);
        } finally {
            try {
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        return result;
    }
}