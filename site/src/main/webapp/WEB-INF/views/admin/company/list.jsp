<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<a href="/admin/companies/parse/reformagkh" class="btn btn-primary">Загрузить компании из Реформа ЖКХ</a>
<a href="/admin/companies/parse/giszkh" class="btn btn-primary">Загрузить компании из ГИС ЖКХ</a><br /><br />
<div class="hpanel">
    <div class="panel-heading hbuilt">${title}</div>
    <div class="panel-body">
        <table id="companies-table" class="table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Полное название</th>
                <th>Адрес</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#companies-table').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax/companies/list",
            "bAutoWidth": false,
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "fullName"},
                {"data": "address"}
            ],
            "responsive": true,
            "language": {
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
    })
</script>