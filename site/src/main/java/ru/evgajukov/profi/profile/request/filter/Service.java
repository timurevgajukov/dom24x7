package ru.evgajukov.profi.profile.request.filter;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class Service {

    private List<Value> values;

    public List<Value> getValues() {

        return values;
    }

    public void setValues(List<Value> values) {

        this.values = values;
    }
}