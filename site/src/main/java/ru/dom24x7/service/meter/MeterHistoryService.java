package ru.dom24x7.service.meter;

import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Сервис для работы с историей показаний счетчиков
 */
public class MeterHistoryService {

    private Meter meter;

    public MeterHistoryService(Meter meter) {

        this.meter = meter;
        meter.loadHistory();
    }

    /**
     * Возвращает общую сумму затрат за все время
     * @return
     */
    public double getTotal() {

        List<MeterHistoryItem> list = meter.getHistory();

        if (list == null) {
            return 0.0;
        }

        double total  = 0.0;

        for (int i = 0; i < list.size()-1; i++) {
            double diff = list.get(i).getValue() - list.get(i + 1).getValue();
            if (list.get(i).getMeter().getTariff() > 0.0) {
                total += diff * list.get(i).getMeter().getTariff();
            }
        }

        return total;
    }
}