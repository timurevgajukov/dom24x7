package ru.evgajukov.profi.profile.request.filter;

/**
 * Created by timur on 17.06.17.
 */
public class Gender {

    public static final String GENDER_MALE = "male";
    public static final String GENDER_FEMALE = "female";

    private String value;

    public String getValue() {

        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    @Override
    public String toString() {

        return value;
    }
}