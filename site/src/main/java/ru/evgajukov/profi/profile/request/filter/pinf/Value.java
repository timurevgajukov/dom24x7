package ru.evgajukov.profi.profile.request.filter.pinf;

/**
 * Created by timur on 18.06.17.
 */
public class Value {

    private int key;
    private String type;

    public int getKey() {

        return key;
    }

    public void setKey(int key) {

        this.key = key;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    @Override
    public String toString() {

        return String.format("%d: %s", key, type);
    }
}