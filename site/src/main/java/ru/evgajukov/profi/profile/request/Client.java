package ru.evgajukov.profi.profile.request;

/**
 * Created by timur on 15.06.17.
 */
public class Client {

    private String ip;
    private String sid;

    public String getIp() {

        return ip;
    }

    public Client setIp(String ip) {

        this.ip = ip;
        return this;
    }

    public String getSid() {

        return sid;
    }

    public Client setSid(String sid) {

        this.sid = sid;
        return this;
    }

    @Override
    public String toString() {

        return ip + " [" + sid + "]";
    }
}
