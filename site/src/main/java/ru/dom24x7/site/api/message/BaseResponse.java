package ru.dom24x7.site.api.message;

/**
 * Created by timur on 22.02.17.
 * API: базовый класс ответа
 */
public class BaseResponse {

    private ResponseHeader header;
    private BaseBody body;

    public BaseResponse(BaseRequest request) {

        getHeader().setMethod(request.getHeader().getMethod());
        getHeader().setToken(request.getHeader().getToken());
        getHeader().setSession(request.getHeader().getSession());
    }

    public BaseResponse setCode(int code) {

        header.setCode(code);
        if (code < 0) {
            header.setDescription("ERROR");
        } else {
            header.setDescription("OK");
        }

        return this;
    }

    public BaseResponse setErrorMessages(String message) {

        return this;
    }

    public ResponseHeader getHeader() {

        if (header == null) {
            header = new ResponseHeader();
        }

        return header;
    }

    public void setHeader(ResponseHeader header) {

        this.header = header;
    }

    public BaseBody getBody() {

        if (body == null) {
            body = new BaseBody();
        }

        return body;
    }

    public void setBody(BaseBody body) {

        this.body = body;
    }
}