<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">${title}</div>
    <div class="panel-body">
        <table id="addresses-table" class="table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Адрес</th>
                <th>Доп. информация</th>
                <th>Пользователь</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#addresses-table').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax/addresses/list",
            "bAutoWidth": false,
            "columns": [
                {"data": "id"},
                {
                    "data": function (row) {
                        return row.title ? row.title : '';
                    }
                },
                {
                    "data": function (row) {
                        return row.address;
                    }
                },
                {
                    "data": function (row) {
                        return '';
                    }
                },
                {
                    "data": function (row) {
                        var fio = row.user.fio;
                        if (fio) {
                            return fio;
                        } else {
                            return row.user.email;
                        }
                    }
                }
            ],
            "responsive": true,
            "language": {
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
    });
</script>