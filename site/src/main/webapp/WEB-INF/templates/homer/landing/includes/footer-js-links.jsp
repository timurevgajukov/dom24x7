<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Vendor scripts -->
<script src="/resources/themes/homer/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/resources/themes/homer/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="/resources/themes/homer/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/resources/themes/homer/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="/resources/themes/homer/vendor/iCheck/icheck.min.js"></script>
<script src="/resources/themes/homer/vendor/sparkline/index.js"></script>
<script src="/resources/themes/homer/vendor/ladda/dist/spin.min.js"></script>
<script src="/resources/themes/homer/vendor/ladda/dist/ladda.min.js"></script>
<script src="/resources/themes/homer/vendor/ladda/dist/ladda.jquery.min.js"></script>


<!-- App scripts -->
<script src="/resources/themes/homer/scripts/homer.js"></script>

<script src="/resources/js/panel.js"></script>