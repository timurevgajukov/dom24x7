<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/resources/themes/homer/vendor/summernote/dist/summernote.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/summernote/dist/summernote-bs3.css" />

<!-- SEND METERS TO COMPANY MODAL WINDOW: BEGIN -->
<div id="company-send-meters-modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div id="meter-value-content" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Отправка показаний счетчиков</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <select id="send-company" class="form-control" style="width: 100%;"></select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input id="send-email" type="email" class="form-control" placeholder="Эл. почта компании" />
                        </div>
                    </div>
                </div>
                <div id="modal-company-send-meters-text"></div>
            </div>
            <div class="modal-footer">
                <button id="btn-company-send-meters" class="ladda-button btn btn-success" type="submit" data-style="zoom-in">Отправить</button>
                <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- SEND METERS TO COMPANY MODAL WINDOW: END -->

<script src="/resources/themes/homer/vendor/summernote/dist/summernote.min.js"></script>
<script src="/resources/themes/homer/vendor/summernote/lang/summernote-ru-RU.js"></script>

<script>
    $(document).ready(function () {
        $('#modal-company-send-meters-text').summernote({
            height: 300
        });

        select2CompanyList($('#send-company'), 30, function (repo) {

        });

        var btnSender = $('#btn-company-send-meters').ladda();
        btnSender.click(function () {
            var data = {
                email: $('#send-email').val(),
                message: $('#modal-company-send-meters-text').code()
            };

            if (data.email == null || data.email.trim().length == 0) {
                showAlert('Необходимо указать адрес электронной почты, на которую будут отправлены показания', 'error');
                return;
            }

            btnSender.ladda('start');
            $.post('/ajax/company/${meter.company.id}/report/send', data, function (result) {
                btnSender.ladda('stop');
                if (!result) {
                    showAlert('Сервер не вернул ответ. Попробуйте чуть позже', 'error');
                    return;
                }

                var message = result.body[0];
                showAlert(message.body, message.type);
                if (result.code == 0) {
                    $('#company-send-meters-modal').modal('hide');
                }
            }).error(function (error) {
                btnSender.ladda('stop');
                console.log(error);
            })
        });
    });
</script>