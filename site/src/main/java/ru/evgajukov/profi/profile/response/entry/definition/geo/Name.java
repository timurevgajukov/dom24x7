package ru.evgajukov.profi.profile.response.entry.definition.geo;

/**
 * Created by timur on 17.06.17.
 */
public class Name {

    private String nominative;
    private String dative;
    private String genitive;
    private String prepositional;

    public String getNominative() {

        return nominative;
    }

    public void setNominative(String nominative) {

        this.nominative = nominative;
    }

    public String getDative() {

        return dative;
    }

    public void setDative(String dative) {

        this.dative = dative;
    }

    public String getGenitive() {

        return genitive;
    }

    public void setGenitive(String genitive) {

        this.genitive = genitive;
    }

    public String getPrepositional() {

        return prepositional;
    }

    public void setPrepositional(String prepositional) {

        this.prepositional = prepositional;
    }

    @Override
    public String toString() {

        return nominative;
    }
}