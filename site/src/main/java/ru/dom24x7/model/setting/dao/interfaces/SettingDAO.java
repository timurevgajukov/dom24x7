package ru.dom24x7.model.setting.dao.interfaces;

import java.util.HashMap;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с настройками системы
 */
public interface SettingDAO {

    /**
     * Возвращает настройки системы
     *
     * @return
     */
    HashMap<String, String> map();
}