package ru.dom24x7.model.access.dao.interfaces;

import ru.dom24x7.model.access.Access;
import ru.dom24x7.model.user.Role;

import java.util.List;

/**
 * Created by timur on 14.02.17.
 *
 * Интерфейс для работы с матрицей доступов
 */
public interface AccessDAO {

    /**
     * Возвращает признак, что есть доступ к ресурсу роли
     * @param url
     * @param role
     * @return
     */
    boolean access(String url, Role role);

    /**
     * Возвращает полный список доступов
     * @return
     */
    List<Access> list();

    /**
     * Возвращает список доступов для роли
     * @param role
     * @return
     */
    List<Access> list(Role role);

    /**
     * Возвращает список дотупов по ресурсу
     * @param url
     * @return
     */
    List<Access> list(String url);

    /**
     * Сохранение доступа
     * @param access
     * @return
     */
    boolean save(Access access);

    /**
     * Удаление доступа к ресурсу
     * @param access
     * @return
     */
    boolean delete(Access access);

    /**
     * Удаление доступов к ресурсу от всех ролей
     * @param url
     * @return
     */
    boolean delete(String url);
}
