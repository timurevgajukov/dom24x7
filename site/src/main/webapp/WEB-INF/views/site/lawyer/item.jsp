<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel forum-box">
    <div class="panel-body">
        <div class="media">
            <div class="media-image pull-left">
                <c:choose>
                    <c:when test="${question.user.iconUrl != null}">
                        <img src="${question.user.iconUrl}" alt="${question.user}">
                    </c:when>
                    <c:otherwise>
                        <img src="/resources/images/noavatar.svg" alt="${question.user}">
                    </c:otherwise>
                </c:choose>
                <div class="author-info">
                    <strong>${question.user}</strong><br>
                    ${question.createDtStr}
                </div>
            </div>
            <div class="media-body">${question}</div>
        </div>
    </div>
    <c:choose>
        <c:when test="${answers != null && answers.size() != 0}">
            <c:forEach items="${answers}" var="answer">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-image pull-left">
                            <c:choose>
                                <c:when test="${answer.answering.iconUrl != null}">
                                    <img src="${answer.answering.iconUrl}" alt="${answer.answering}">
                                </c:when>
                                <c:otherwise>
                                    <img src="/resources/images/noavatar.svg" alt="${answer.answering}">
                                </c:otherwise>
                            </c:choose>
                            <div class="author-info">
                                <strong>${answer.answering}</strong><br>
                                    ${answer.createDtStr}
                            </div>
                        </div>
                        <div class="media-body">${answer}</div>
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="panel-body">На вопрос пока еще не ответили</div>
        </c:otherwise>
    </c:choose>
</div>

<form method="post" action="/lawyer/answer/${question.id}">
    <div class="form-group">
                <textarea class="form-control" name="lawyer-answer" rows="8"
                          placeholder="Напишите сообщение" required></textarea>
    </div>
    <div class="form-group text-right">
        <button class="btn btn-success" type="submit">Отправить</button>
    </div>
</form>