package ru.dom24x7.model.reference.currency;

import ru.dom24x7.model.Model;

/**
 * Валюта
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Currency extends Model {

	public static final int DEFAULT_CURRENCY_ID = 1; // Российский рубль

	private String symbol;
	private String code;

	public Currency(int id) {

		super(id);
	}
	
	public Currency(int id, String name, String symbol, String code) {
		
		super(id, name);
		this.symbol = symbol;
		this.code = code;
	}

	public String getSymbol() {

		return symbol;
	}

	public void setSymbol(String symbol) {

		this.symbol = symbol;
	}

	public String getCode() {

		return code;
	}

	public void setCode(String code) {

		this.code = code;
	}

	@Override
	public String toString() {
		
		return symbol;
	}
}