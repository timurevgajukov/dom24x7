<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h1>${title}</h1>

<div class="hpanel">
    <div class="panel-body">
        <c:if test="${companies != null && companies.size() != 0}">
            <c:forEach items="${companies}" var="company">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="/companies/${company.id}/${company.translitUrl}" class="btn-link">${company.name}</a>
                </div>
            </c:forEach>
        </c:if>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-6">
                <p><a href="/companies/regions">Вернуться к списке регионов</a></p>
            </div>
            <div class="col-md-6 text-right">
                <c:if test="${prev != null || next != null}">
                    <p>
                        <c:if test="${prev != null}"><a href="/companies/regions/${region.id}/page/${prev}" class="btn-link"><i class="fa fa-angle-left"></i> предыдущая</a></c:if>
                        <c:if test="${prev != null && next != null}">&nbsp;&nbsp;&nbsp;</c:if>
                        <c:if test="${next != null}"><a href="/companies/regions/${region.id}/page/${next}" class="btn-link">следующая <i class="fa fa-angle-right"></i></a></c:if>
                    </p>
                </c:if>
            </div>
        </div>
    </div>
</div>