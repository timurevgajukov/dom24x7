package ru.dom24x7.model.meter.dao.interfaces;

import ru.dom24x7.model.meter.MeterIcon;
import ru.dom24x7.model.meter.MeterType;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с иконками счетчика
 */
public interface MeterIconDAO {

    /**
     * Возвращает иконку
     *
     * @param id
     * @return
     */
    MeterIcon get(int id);

    /**
     * Возвращает список иконок для счетчиков определенного типа
     *
     * @param type
     * @return
     */
    List<MeterIcon> list(MeterType type);
}