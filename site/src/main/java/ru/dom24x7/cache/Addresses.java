package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.address.dao.AddressDAOImpl;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Контейнейр для хранения справочника адресов
 */
public class Addresses implements ICache<Address> {

    private static Addresses instance = null;

    private List<Address> list;

    public static Addresses getInstance() {

        if (instance == null) {
            instance = new Addresses();
        }

        return instance;
    }

    private Addresses() {

        list = new AddressDAOImpl().list(0, Integer.MAX_VALUE, null);
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Address> list() {

        return list;
    }

    @Override
    public Address get(int id) {

        if (list == null) {
            return null;
        }

        for (Address item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}