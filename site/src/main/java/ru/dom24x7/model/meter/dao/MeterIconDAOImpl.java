package ru.dom24x7.model.meter.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.meter.MeterIcon;
import ru.dom24x7.model.meter.MeterType;
import ru.dom24x7.model.meter.dao.interfaces.MeterIconDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса для работы со списком иконок
 */
public class MeterIconDAOImpl extends ModelDAOImpl implements MeterIconDAO {

    private static final String QUERY_GET_BY_ID = "{ call meter_icons_get_by_id(?) }";
    private static final String QUERY_LIST_BY_TYPE = "{ call meter_icons_get_by_type(?) }";

    @Override
    public MeterIcon get(int id) {

        List<MeterIcon> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<MeterIcon>() {

            @Override
            public MeterIcon mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<MeterIcon> list(MeterType type) {

        List<MeterIcon> list = jdbcTemplate.query(QUERY_LIST_BY_TYPE, new RowMapper<MeterIcon>() {

            @Override
            public MeterIcon mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, type.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private MeterIcon load(ResultSet rs) throws SQLException {

        return new MeterIcon(rs.getInt("id"), rs.getString("name"));
    }
}