package ru.dom24x7.site.ajax;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.cache.Currencies;
import ru.dom24x7.cache.MeterTypes;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.model.meter.MeterType;
import ru.dom24x7.model.reference.currency.Currency;
import ru.dom24x7.utils.HttpUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by timur on 20.10.16.
 *
 * Контроллер для обработки некоторых общих ajax запросов
 */
@Controller
public class AjaxController {

    @RequestMapping("/ajax/currencies/list")
    public @ResponseBody List<Currency> listCurrenciesAjax() {

        return Currencies.getInstance().list();
    }

    @RequestMapping("/ajax/meter/types/list")
    public @ResponseBody List<MeterType> listMeterTypesAjax() {

        return MeterTypes.getInstance().list();
    }

    @RequestMapping("/ajax/companies/list")
    public @ResponseBody Map<String, Object> listCompaniesAjax(HttpServletRequest request) {

        String query = HttpUtils.getQueryParam(request, "query");
        int page = Integer.parseInt(HttpUtils.getQueryParam(request, "page"));
        int count = Integer.parseInt(HttpUtils.getQueryParam(request, "count"));

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("items", new CompanyDAOImpl().list((page - 1) * count, count, query));
        result.put("total", new CompanyDAOImpl().count(query));

        return result;
    }
}