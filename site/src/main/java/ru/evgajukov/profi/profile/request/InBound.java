package ru.evgajukov.profi.profile.request;

/**
 * Created by timur on 15.06.17.
 */
public class InBound {

    private String domain;

    public String getDomain() {

        return domain;
    }

    public InBound setDomain(String domain) {

        this.domain = domain;
        return this;
    }

    @Override
    public String toString() {

        return domain;
    }
}