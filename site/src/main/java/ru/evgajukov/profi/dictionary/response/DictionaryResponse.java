package ru.evgajukov.profi.dictionary.response;

/**
 * Created by timur on 15.06.17.
 * Класс ответа со списком справочников
 */
public class DictionaryResponse {

    public static final String API_NAME = "dictionary";

    private long timestamp;
    private Dictionaries dictionaries;

    public long getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(long timestamp) {

        this.timestamp = timestamp;
    }

    public Dictionaries getDictionaries() {

        return dictionaries;
    }

    public void setDictionaries(Dictionaries dictionaries) {

        this.dictionaries = dictionaries;
    }
}
