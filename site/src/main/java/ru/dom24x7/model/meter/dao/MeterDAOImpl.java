package ru.dom24x7.model.meter.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.cache.Addresses;
import ru.dom24x7.cache.MeterTypes;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.device.Device;
import ru.dom24x7.model.device.dao.DeviceDAOImpl;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterType;
import ru.dom24x7.model.meter.dao.interfaces.MeterDAO;
import ru.dom24x7.model.template.Template;
import ru.dom24x7.model.user.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 * <p>
 * Реализует интерфейс для работы со счетчиками
 */
public class MeterDAOImpl extends ModelDAOImpl implements MeterDAO {

    private static final String QUERY_GET_BY_ID = "{ call meters_get_by_user(?,?) }";
    private static final String QUERY_LIST_BY_ADDRESS = "{ call meters_list(?) }";
    private static final String QUERY_LIST_BY_USER = "{ call meters_list_by_user(?) }";
    private static final String QUERY_SAVE = "{ call meters_save(?,?,?,?,?,?,?) }";
    private static final String QUERY_BIND_COMPANY = "{ call meters_link_company(?,?) }";
    private static final String QUERY_BIND_TEMPLATE = "{ call meters_link_template(?,?) }";
    private static final String QUERY_DELETE = "{ call meters_delete(?) }";
    private static final String QUERY_HOLD = "{ call meters_hold(?) }";

    @Override
    public Meter get(int id, User user) {

        final MeterTypes types = MeterTypes.getInstance();
        final Addresses addresses = Addresses.getInstance();

        List<Meter> list = jdbcTemplate.query(QUERY_GET_BY_ID, new Object[]{id, user.getId()},
                new RowMapper<Meter>() {

                    @Override
                    public Meter mapRow(ResultSet resultSet, int i) throws SQLException {

                        Meter meter = load(resultSet);
                        meter.setType(types.get(meter.getType().getId()));
                        meter.setAddress(addresses.get(meter.getAddress().getId()));

                        return meter;
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        Meter meter = list.get(0);
        meter.loadHistory();

        return meter;
    }

    @Override
    public List<Meter> list(Address address) {

        return list(address, true);
    }

    @Override
    public List<Meter> list(final Address address, boolean withHistory) {

        final MeterTypes types = MeterTypes.getInstance();

        List<Meter> list = jdbcTemplate.query(QUERY_LIST_BY_ADDRESS, new RowMapper<Meter>() {

            @Override
            public Meter mapRow(ResultSet resultSet, int i) throws SQLException {

                Meter meter = load(resultSet);
                meter.setType(types.get(meter.getType().getId()));
                meter.setAddress(address);

                return meter;
            }
        }, address.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        if (withHistory) {
            for (Meter meter : list) {
                meter.loadHistory();
            }
        }

        return list;
    }

    @Override
    public List<Meter> list(User user) {

        return list(user, true);
    }

    @Override
    public List<Meter> list(User user, boolean withHistory) {

        final MeterTypes types = MeterTypes.getInstance();
        final Addresses addresses = Addresses.getInstance();

        List<Meter> list = jdbcTemplate.query(QUERY_LIST_BY_USER, new RowMapper<Meter>() {

            @Override
            public Meter mapRow(ResultSet resultSet, int i) throws SQLException {

                Meter meter = load(resultSet);
                meter.setType(types.get(meter.getType().getId()));
                meter.setAddress(addresses.get(meter.getAddress().getId()));

                return meter;
            }
        }, user.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        if (withHistory) {
            for (Meter meter : list) {
                meter.loadHistory();
            }
        }

        return list;
    }

    @Override
    public boolean save(Meter meter) {

        // сохраняем также разрядность и точность в текущем прибору учета
        Device device = meter.getUsedDevice();
        if (device != null) {
            new DeviceDAOImpl().save(device);
        }

        Meter newMeter = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        meter.getId(),
                        meter.getTitle() == null || meter.getTitle().trim().length() == 0 ? null : meter.getTitle().trim(),
                        meter.getType().getId(),
                        meter.getAddress().getId(),
                        meter.getTariff(),
                        meter.getCapacity(),
                        meter.getPrecision()
                }, new RowMapper<Meter>() {

                    @Override
                    public Meter mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        meter.setId(newMeter.getId());

        return true;
    }

    @Override
    public boolean save(Meter meter, Company company) {

        return jdbcTemplate.update(QUERY_BIND_COMPANY,
                new Object[]{
                        meter.getId(),
                        company != null ? company.getId() : 0 }) != 0;
    }

    @Override
    public boolean save(Meter meter, Template template) {

        return jdbcTemplate.update(QUERY_BIND_TEMPLATE, new Object[]{ meter.getId(), template.getId() }) != 0;
    }

    @Override
    public boolean delete(Meter meter) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{ meter.getId() }) != 0;
    }

    @Override
    public boolean hold(Meter meter) {

        return jdbcTemplate.update(QUERY_HOLD, new Object[]{ meter.getId() }) != 0;
    }

    private Meter load(ResultSet rs) throws SQLException {

        Meter meter = new Meter(rs.getInt("id"));
        meter.setTitle(rs.getString("title"));
        meter.setType(new MeterType(rs.getInt("meter_type_id")));
        meter.setAddress(new Address(rs.getInt("address_id")));
        meter.setTariff(rs.getDouble("tariff"));
        meter.setCompanyId(rs.getInt("company_id"));
        meter.setIconId(rs.getInt("meter_icon_id"));
        meter.setTemplateId(rs.getInt("template_id"));

        return meter;
    }
}