package ru.dom24x7.model.reference.unit;

import ru.dom24x7.model.Model;

/**
 * Единица измерения
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Unit extends Model {

	private String title;
	
	public Unit() {
		
		super();
	}
	
	public Unit(int id) {
		
		super(id);
	}
	
	public Unit(int id, String title) {
		
		super(id);
		
		this.title = title;
	}
	
	public String getTitle() {
		
		return title;
	}
	
	public void setTitle(String title) {
		
		this.title = title;
	}
	
	@Override
	public String toString() {
		
		return title;
	}
}
