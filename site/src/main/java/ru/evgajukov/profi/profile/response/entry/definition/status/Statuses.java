package ru.evgajukov.profi.profile.response.entry.definition.status;

/**
 * Created by timur on 16.06.17.
 */
public class Statuses {

    private String activdate;
    private int vis;
    private int wants;
    private int activ;
    private int n;

    public String getActivdate() {

        return activdate;
    }

    public void setActivdate(String activdate) {

        this.activdate = activdate;
    }

    public int getVis() {

        return vis;
    }

    public void setVis(int vis) {

        this.vis = vis;
    }

    public int getWants() {

        return wants;
    }

    public void setWants(int wants) {

        this.wants = wants;
    }

    public int getActiv() {

        return activ;
    }

    public void setActiv(int activ) {

        this.activ = activ;
    }

    public int getN() {

        return n;
    }

    public void setN(int n) {

        this.n = n;
    }
}
