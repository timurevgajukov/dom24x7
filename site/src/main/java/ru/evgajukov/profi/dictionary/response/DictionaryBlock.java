package ru.evgajukov.profi.dictionary.response;

/**
 * Created by timur on 15.06.17.
 * Общий блок списка справочника
 */
public class DictionaryBlock {

    private String id;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }
}
