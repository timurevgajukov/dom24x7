package ru.dom24x7.model.template;

import ru.dom24x7.model.Model;

/**
 * Шаблоны пользователей для отправки писем
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Template extends Model {

	private String body;
	
	public Template() {
		
		super();
	}
	
	public Template(int id) {
		
		super(id);
	}
	
	public Template(int id, String name, String body) {
		
		super(id);
		
		this.name = name;
		this.body = body;
	}
	
	public String getBody() {
		
		return body;
	}
	
	public void setBody(String body) {
		
		this.body = body;
	}
}