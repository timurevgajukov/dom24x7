package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.dom24x7.cache.Categories;
import ru.dom24x7.component.TableData;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.photo.dao.PhotoDAOImpl;
import ru.dom24x7.model.post.Category;
import ru.dom24x7.model.post.Post;
import ru.dom24x7.model.post.dao.PostDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.StrUtils;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 17.02.17.
 *
 * Административная панель: управление блогом
 */
@Controller
public class AdminBlogController extends BaseController {

    @RequestMapping("/admin/blog")
    public String listAction(ModelMap model) {

        setModelMap(model, "Посты блога");
        model.addAttribute("categories", Categories.getInstance().list());

        return "admin/blog/list";
    }

    @RequestMapping("/admin/ajax/posts/list")
    public @ResponseBody TableData listPostsAjax(HttpServletRequest request) {

        int draw = Integer.parseInt(request.getParameter("draw"));
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));
        String search = request.getParameter("search[value]");

        int count = new PostDAOImpl().count(search);
        List<Post> posts = new PostDAOImpl().list(start, length, search);

        if (posts == null) {
            posts = new ArrayList<Post>();
        }

        TableData tableData = new TableData();
        tableData.setDraw(draw);
        tableData.setRecordsTotal(count);
        tableData.setRecordsFiltered(count);
        tableData.setData(posts);

        return tableData;
    }

    @RequestMapping("/admin/ajax/posts/{postId}")
    public @ResponseBody Post getPost(@PathVariable int postId) {

        Post post = new PostDAOImpl().get(postId);
        post.loadPhotos(true);

        return post;
    }

    @RequestMapping(value = "/admin/ajax/posts/save", method = RequestMethod.POST)
    public @ResponseBody Response savePostAction(
            @RequestParam(value = "id", required = false) Integer postId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "text", required = false) String text,
            @RequestParam(value = "category", required = false) Integer categoryId,
            @RequestParam(value = "published", required = false) String published,
            @RequestParam(value = "source", required = false) String source,
            @RequestParam(value = "photos", required = false) String photosBase64Str,
            @RequestParam(value = "photosFileName", required = false) String photosFileNameStr) {

        List<Message> messages = new ArrayList<Message>();

        Post post;
        if (postId == null) {
            // добавление нового поста
            post = new Post();
            post.setAuthor(security.getUser());
        } else {
            post = new PostDAOImpl().get(postId);
            if (post == null) {
                messages.add(new Message("Не удалось найти пост с указанным идентификатором", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        if (text == null || text.trim().length() == 0) {
            // пост может состоять только из одних картинок
            if (photosBase64Str == null || photosBase64Str.trim().length() == 0) {
                messages.add(new Message("Пост не может быть пустым", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        if (categoryId == null) {
            categoryId = Category.DEFAULT_CATEGORY;
        }
        Category category = Categories.getInstance().get(categoryId);
        if (category == null) {
            messages.add(new Message("Необходимо указать категорию поста", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        Date publishedDt = new Date();
        try {
            publishedDt = new SimpleDateFormat("dd.MM.yyyy hh:mm").parse(published);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Photo> photos = new ArrayList<Photo>();
        if (photosBase64Str != null && photosBase64Str.trim().length() != 0) {
            // может быть не одна, а несколько картинок
            String[] photosBase64 = photosBase64Str.split("##");
            String[] photosFileName = photosFileNameStr.split("##");
            for (int i = 0; i < photosBase64.length; i++) {
                // добавлена к посту картинка
                String photoBase64 = photosBase64[i];
                String photoFileName = photosFileName[i];

                BASE64Decoder decoder = new BASE64Decoder();
                try {
                    byte[] photoBytes = decoder.decodeBuffer(photoBase64);
                    Photo photo = new Photo(photoFileName, photoBytes);
                    new PhotoDAOImpl().save(photo);
                    photos.add(photo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // теперь можем добавить данные
        post.setTitle(StrUtils.isEmpty(title) ? null : title.trim());
        post.setText(StrUtils.isEmpty(text) ? null : text.trim());
        post.setPublishedDt(publishedDt);
        post.setCategory(category);
        post.setSource(StrUtils.isEmpty(source) ? null : source.trim());

        new PostDAOImpl().save(post);

        new PhotoDAOImpl().delete(post);
        if (photos.size() != 0) {
            for (Photo photo : photos) {
                // нужно привязать фото к посту и отвязать другие
                new PhotoDAOImpl().save(post, photo);
            }
        }

        return new Response(0, "OK", post);
    }

    @RequestMapping("/admin/ajax/posts/delete/{postId}")
    public @ResponseBody Response deletePostAction(@PathVariable int postId) {

        Post post = new PostDAOImpl().get(postId);
        if (post == null) {
            messages.add(new Message("Не удалось найти пост с указанным идентификатором", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        new PostDAOImpl().delete(post);

        return new Response(0, "OK");
    }
}