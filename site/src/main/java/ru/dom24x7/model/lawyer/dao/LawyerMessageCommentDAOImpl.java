package ru.dom24x7.model.lawyer.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.lawyer.LawyerMessage;
import ru.dom24x7.model.lawyer.LawyerMessageComment;
import ru.dom24x7.model.lawyer.dao.interfaces.LawyerMessageCommentDAO;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 * <p>
 * Реализация интерфейса для работы с комментариями юристов на сообщения пользователей
 */
public class LawyerMessageCommentDAOImpl extends ModelDAOImpl implements LawyerMessageCommentDAO {

    private static final String QUERY_LIST_BY_MESSAGE = "{ call lawyer_message_comments_list(?,?,?) }";
    private static final String QUERY_COUNT_BY_MESSAGE = "{ call lawyer_message_comments_count(?) }";
    private static final String QUERY_SAVE = "{ call lawyer_message_comments_save(?,?,?,?,?) }";

    @Override
    public List<LawyerMessageComment> list(final LawyerMessage message, int offset, int count) {

        List<LawyerMessageComment> list = jdbcTemplate.query(QUERY_LIST_BY_MESSAGE,
                new Object[]{message.getId(), offset, count}, new RowMapper<LawyerMessageComment>() {

                    @Override
                    public LawyerMessageComment mapRow(ResultSet resultSet, int i) throws SQLException {

                        LawyerMessageComment comment = load(resultSet);
                        comment.setMessage(message);

                        return comment;
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public int count(LawyerMessage message) {

        return jdbcTemplate.queryForObject(QUERY_COUNT_BY_MESSAGE, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, message.getId());
    }

    @Override
    public boolean save(LawyerMessageComment comment) {

        LawyerMessageComment newComment = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        comment.getId() == 0 ? null : comment.getId(),
                        comment.getMessage().getId(),
                        comment.getLawyer() != null ? comment.getLawyer().getId() : null,
                        comment.getUser() != null ? comment.getUser().getId() : null,
                        comment.getText()
                }, new RowMapper<LawyerMessageComment>() {

                    @Override
                    public LawyerMessageComment mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        comment.setId(newComment.getId());

        return true;
    }

    private LawyerMessageComment load(ResultSet rs) throws SQLException {

        LawyerMessageComment comment = new LawyerMessageComment(rs.getInt("id"));
        comment.setMessage(new LawyerMessage(rs.getInt("message_id")));
        comment.setText(rs.getString("text"));
        comment.setCreateDt(rs.getTimestamp("create_dt"));

        int userId = rs.getInt("user_id");
        int lawyerId = rs.getInt("lawyer_id");

        if (userId != 0) {
            comment.setUser(new UserDAOImpl().get(userId));
        }
        if (lawyerId != 0) {
            comment.setLawyer(new UserDAOImpl().get(lawyerId));
        }

        return comment;
    }
}