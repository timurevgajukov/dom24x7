package ru.dom24x7.model.photo.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.photo.dao.interfaces.PhotoDAO;
import ru.dom24x7.model.post.Post;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 26.02.17.
 * Реализация интерфейса для работы с картинками
 */
public class PhotoDAOImpl extends ModelDAOImpl implements PhotoDAO {

    private static final String QUERY_GET_BY_ID = "{ call photos_get(?) }";
    private static final String QUERY_THUMBNAIL = "{ call photos_get_thumbnail(?) }";
    private static final String QUERY_LIST_BY_POST = "{ call posts_photos_list(?,?) }";
    private static final String QUERY_SAVE = "{ call photos_save(?,?,?) }";
    private static final String QUERY_SAVE_REF_POST = "{ call posts_photos_ref_save(?,?) }";
    private static final String QYERY_DELETE_BY_POST = "{ call posts_photos_ref_delete_by_post(?) }";

    @Override
    public Photo get(int id) {

        List<Photo> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Photo>() {
            @Override
            public Photo mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public Photo thumbnail(Photo photo) {

        List<Photo> list = jdbcTemplate.query(QUERY_THUMBNAIL, new RowMapper<Photo>() {
            @Override
            public Photo mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, photo.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<Photo> list(Post post, boolean full) {

        List<Photo> list = jdbcTemplate.query(QUERY_LIST_BY_POST, new Object[]{post.getId(), full ? 1 : 0},
                new RowMapper<Photo>() {
                    @Override
                    public Photo mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Photo photo) {

        return save(photo, null);
    }

    @Override
    public boolean save(Photo photo, Photo original) {

        Photo newPhoto = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        photo.getResource(),
                        photo.getFileName(),
                        original != null ? original.getId() : null
                }, new RowMapper<Photo>() {
                    @Override
                    public Photo mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        photo.setId(newPhoto.getId());

        return true;
    }

    @Override
    public boolean save(Post post, Photo photo) {

        return jdbcTemplate.update(QUERY_SAVE_REF_POST, new Object[]{post.getId(), photo.getId()}) != 0;
    }

    @Override
    public boolean delete(Post post) {

        return jdbcTemplate.update(QYERY_DELETE_BY_POST, new Object[]{post.getId()}) != 0;
    }

    private Photo load(ResultSet rs) throws SQLException {

        Photo photo = new Photo(rs.getInt("id"), rs.getString("file_name"), rs.getBlob("photo"));

        return photo;
    }
}