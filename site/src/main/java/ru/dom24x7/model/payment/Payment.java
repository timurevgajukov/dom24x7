package ru.dom24x7.model.payment;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.photo.dao.PhotoDAOImpl;
import ru.dom24x7.model.user.User;

import java.util.Date;

/**
 * Created by timur on 22.03.17.
 * Платежное поручение
 */
public class Payment extends Model {

    private Date createDt;
    private User user;
    private Address address;
    private Photo photo;
    private Company company;
    private Date month;
    private double amount;
    private boolean paid;
    private String note;

    public Payment() {

        super();
    }

    public Payment(int id) {

        super(id);
    }

    public Date getCreateDt() {

        return createDt;
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Address getAddress() {

        return address;
    }

    public void setAddress(Address address) {

        this.address = address;
    }

    public Photo getPhoto() {

        return photo;
    }

    public void setPhoto(Photo photo) {

        this.photo = photo;
    }

    public void loadPhoto() {

        this.photo = new PhotoDAOImpl().get(this.photo.getId());
    }

    public Company getCompany() {

        return company;
    }

    public void setCompany(Company company) {

        this.company = company;
    }

    public Date getMonth() {

        return month;
    }

    public void setMonth(Date month) {

        this.month = month;
    }

    public double getAmount() {

        return amount;
    }

    public void setAmount(double amount) {

        this.amount = amount;
    }

    public boolean isPaid() {

        return paid;
    }

    public void setPaid(boolean paid) {

        this.paid = paid;
    }

    public String getNote() {

        return note;
    }

    public void setNote(String note) {

        this.note = note;
    }
}