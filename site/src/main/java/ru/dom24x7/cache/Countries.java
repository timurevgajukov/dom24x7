package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.reference.country.Country;
import ru.dom24x7.model.reference.country.dao.CountryDAOImpl;

import java.util.List;

/**
 * Created by timur on 18.09.16.
 *
 * Контейнер для хранения справочника стран
 */
public class Countries implements ICache<Country> {

    private static Countries instance = null;

    private List<Country> list;

    public static Countries getInstance() {

        if (instance == null) {
            instance = new Countries();
        }

        return instance;
    }

    private Countries() {

        list = new CountryDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Country> list() {

        return list;
    }

    @Override
    public Country get(int id) {

        if (list == null) {
            return null;
        }

        for (Country item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }

    public Country get(String name) {

        if (list == null) {
            return null;
        }

        for (Country item : list) {
            if (name.equalsIgnoreCase(item.getName())) {
                return item;
            }
        }

        return null;
    }
}