package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.reference.currency.Currency;
import ru.dom24x7.model.reference.currency.dao.CurrencyDAOImpl;

import java.util.List;

/**
 * Created by timur on 18.09.16.
 *
 * Контейнер для хранения справочника валют
 */
public class Currencies implements ICache<Currency> {

    private static Currencies instance = null;

    private List<Currency> list;

    public static Currencies getInstance() {

        if (instance == null) {
            instance = new Currencies();
        }

        return instance;
    }

    private Currencies() {

        list = new CurrencyDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Currency> list() {

        return list;
    }

    @Override
    public Currency get(int id) {

        if (list == null) {
            return null;
        }

        for (Currency item : list()) {
            if (id  == item.getId()) {
                return item;
            }
        }

        return null;
    }
}