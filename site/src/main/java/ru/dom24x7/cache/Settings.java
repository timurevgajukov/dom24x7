package ru.dom24x7.cache;

import ru.dom24x7.model.setting.dao.SettingDAOImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by timur on 13.03.17.
 * Контейнер для хранения настроек в формате ключ - значение
 */
public class Settings {

    private static Settings instance = null;

    private Map<String, String> map;

    public static Settings getInstance() {

        if (instance == null) {
            instance = new Settings();
        }

        return instance;
    }

    private Settings() {

        map = new SettingDAOImpl().map();
    }

    public void clear() {

        if (map != null) {
            map.clear();
        }
        map = null;
        instance = null;
    }

    public Map<String, String> map() {

        return map;
    }

    public String get(String key) {

        if (map == null) {
            return null;
        }

        return map.get(key);
    }
}