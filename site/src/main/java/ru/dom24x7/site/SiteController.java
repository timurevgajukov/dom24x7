package ru.dom24x7.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.component.OpenGraph;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.post.dao.PostDAOImpl;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.Email;
import ru.dom24x7.utils.StrUtils;

/**
 * Created by timur on 20.09.16.
 *
 * Контроллер для обработки основных страниц, которые доступны без авторизации
 */
@Controller
public class SiteController extends BaseController {

    @RequestMapping("/")
    public String indexAction(ModelMap model) {

        OpenGraph og = new OpenGraph();
        og.setTitle("Сервис учета коммунальных услуг");
        og.setDescription("Сервис учета показаний коммунальных счетчиков, новости и советы из мира ЖКХ, взаимодействие с ТСЖ и УК");
        og.setType(OpenGraph.OG_TYPE_WEBSITE);
        og.setUrl(TechController.URL);
        og.setImage(TechController.URL + "/resources/images/dom24x7.png");

        setModelMap(model, og, "ЖКХ, коммунальный сервис, ТСЖ, УК, коммунальные платежи", null);
        model.addAttribute("usersCount", new UserDAOImpl().count(null));
        model.addAttribute("companiesCount", new CompanyDAOImpl().count((String) null));
        model.addAttribute("posts", new PostDAOImpl().listForBlog(0, 10));

        return "site/index";
    }

    @RequestMapping("/tariffs")
    public String tariffsAction(ModelMap model) {

        setModelMap(model, "Расчет тарифов ЖКХ", "tariffs");
        return "site/tariffs";
    }

    @RequestMapping("/about")
    public String aboutAction(ModelMap model) {

        setModelMap(model, "О проекте");

        return "site/about";
    }

    @RequestMapping("/about/tech")
    public String techAction(ModelMap model) {

        setModelMap(model, "API для работы с сервисом");

        return "site/about/tech";
    }

    @RequestMapping("/about/privacy")
    public String privacyAction(ModelMap model) {

        setModelMap(model, "Политика конфиденциальности");

        return "site/about/privacy";
    }

    @RequestMapping("/about/privacy/terms")
    public String privacyTermsAction(ModelMap model) {

        setModelMap(model, "Политика конфиденциальности. Основные понятия");

        return "site/about/privacy/terms";
    }

    @RequestMapping("/faq")
    public String faqAction(ModelMap model) {

        setModelMap(model, "Часто задаваемые вопросы");

        return "site/faq";
    }

    @RequestMapping("/terms")
    public String termsAction(ModelMap model) {

        setModelMap(model, "Словарь");

        return "site/terms";
    }

    @RequestMapping("/404")
    public String error404Action(ModelMap model) {

        setModelMap(model, "404 Ресурс не найден");

        return "error/404";
    }

    @RequestMapping("/500")
    public String error500Action(ModelMap model) {

        setModelMap(model, "500 Ошибка");

        return "error/500";
    }

    @RequestMapping(value = "/about/feedback", method = RequestMethod.POST)
    public @ResponseBody Response feedbackAjax(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "message") String message) {

        String emailMessage = String.format("Имя: %s<br />Обратный адрес: %s<br /><br />%s", name, email, message);
        String userEmailMessage = String.format("Здравствуйте. Спасибо за ваше сообщение. Мы обязательно его прочитаем и " +
                "ответим!<br />Вы нам написали:<br />%s", message);

        Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z");
        // отправляем администратору
        emailSender.send("Сообщение от посетителя сайта dom24x7.ru", emailMessage, "info@dom24x7.ru");
        // отправляем копию пользователю
        emailSender.send("Вы написали службе поддержики сайта dom24x7.ru", userEmailMessage, email);

        return new Response(0, "OK");
    }
}