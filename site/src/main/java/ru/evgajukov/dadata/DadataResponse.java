package ru.evgajukov.dadata;

/**
 * Created by timur on 20.10.16.
 *
 * Ответ от сервиса dadata
 */
public class DadataResponse {

    private String value; // Адрес одной строкой (как показывается в списке подсказок)
    private String unrestricted_value; // Адрес одной строкой (полный, от региона)
    private DadataAddress data;

    public String getValue() {

        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public String getUnrestricted_value() {

        return unrestricted_value;
    }

    public void setUnrestricted_value(String unrestricted_value) {

        this.unrestricted_value = unrestricted_value;
    }

    public DadataAddress getData() {

        return data;
    }

    public void setData(DadataAddress data) {

        this.data = data;
    }
}