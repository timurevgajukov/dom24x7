<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">${title}</div>
    <div class="panel-body">
        <p>Споры с организациями, предоставляющими услуги в сфере ЖКХ, случаются достаточно часто – слишком много
            нарушений они допускают. Нередко потребитель коммунальных услуг даже не задумывается, какие услуги ему
            обязаны предоставить за те деньги, которые он платит, а иногда не знает и о льготах, которые ему положены.
            При этом все нормативы, в том числе и по качеству услуг ЖКХ, установлены нормативно-правовыми актами.
            Если действовать юридически грамотно, то можно добиться от исполнителя услуг значительно улучшить их
            качество и даже сэкономить. На нашем сайте мы собрали множество правовых советов, как отказаться от услуг
            ЖКХ, которыми потребитель не пользуется, как проверить соответствие предоставляемых услуг нормативам,
            как возместить ущерб, причиненный предоставлением некачественных услуг ЖКХ и так далее. Опытные юристы
            ответят на любой вопрос в этой области, помогут написать грамотную претензию в адрес организации,
            предоставляющей услуги ЖКХ, при необходимости помогут составить и подать иск в суд.</p>
    </div>
</div>

<c:choose>
    <c:when test="${security.auth}">
        <form method="post" action="/lawyer/question">
            <div class="form-group">
                <textarea class="form-control" name="lawyer-question" rows="8"
                          placeholder="Задайте свой вопрос юристу" required></textarea>
            </div>
            <div class="form-group text-right">
                <button class="btn btn-success" type="submit">Спросить</button>
            </div>
        </form>

        <div class="hpanel forum-box">
            <div class="panel-heading">Предыдущие ваши запросы</div>
            <c:choose>
                <c:when test="${questions != null && questions.size() != 0}">
                    <c:forEach items="${questions}" var="question">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-11 forum-heading">
                                    <a href="/lawyer/question/${question.id}">
                                        <div class="desc">${question}</div>
                                    </a>
                                </div>
                                <div class="col-md-1 forum-info">
                                    <span class="number">${question.answersCount}</span>
                                    <small>ответов</small>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div class="panel-body">Пока ни одного вопроса вами не задано</div>
                </c:otherwise>
            </c:choose>
        </div>
    </c:when>
    <c:otherwise>
        <t:insertAttribute name="form-auth-reg" defaultValue="/WEB-INF/views/site/component/form-auth-reg.jsp"/>
    </c:otherwise>
</c:choose>