package ru.dom24x7.model.rating.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.model.rating.Rating;
import ru.dom24x7.model.rating.dao.interfaces.RatingDAO;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 07.06.17.
 * Реализация интерфейса по работы с отзывами по компаниям
 */
public class RatingDAOImpl extends ModelDAOImpl implements RatingDAO {

    private static final String QUERY_GET_BY_USER_AND_COMPANY = "{ call ratings_get_by_user_and_company(?,?) }";
    private static final String QUERY_LIST_BY_COMPANY = "{ call ratings_list_by_company(?) }";
    private static final String QUERY_SAVE = "{ call ratings_save(?,?,?,?,?) }";

    @Override
    public Rating get(User user, final Company company) {

        List<Rating> list = jdbcTemplate.query(QUERY_GET_BY_USER_AND_COMPANY,
                new Object[]{user.getId(), company.getId()}, new RowMapper<Rating>() {
                    @Override
                    public Rating mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, company);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<Rating> list(final Company company) {

        List<Rating> list = jdbcTemplate.query(QUERY_LIST_BY_COMPANY, new RowMapper<Rating>() {
            @Override
            public Rating mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet, company);
            }
        }, company.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Rating rating) {

        Rating newRating = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        rating.getId() == 0 ? null : rating.getId(),
                        rating.getUser().getId(),
                        rating.getCompany().getId(),
                        rating.getMark(),
                        rating.getReview()
                }, new RowMapper<Rating>() {
                    @Override
                    public Rating mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, null);
                    }
                });

        rating.setId(newRating.getId());

        return true;
    }

    private Rating load(ResultSet rs, Company company) throws SQLException {

        Rating rating = new Rating(rs.getInt("id"));
        rating.setCreateDt(rs.getTimestamp("create_dt"));
        rating.setUser(new UserDAOImpl().get(rs.getInt("user_id")));
        if (company != null) {
            rating.setCompany(company);
        } else {
            rating.setCompany(new CompanyDAOImpl().get(rs.getInt("company_id")));
        }
        rating.setMark(rs.getInt("mark"));
        rating.setReview(rs.getString("review"));

        return rating;
    }
}