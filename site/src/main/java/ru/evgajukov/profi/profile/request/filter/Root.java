package ru.evgajukov.profi.profile.request.filter;

/**
 * Created by timur on 18.06.17.
 */
public class Root {

    private String id;
    private boolean include;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public boolean isInclude() {

        return include;
    }

    public void setInclude(boolean include) {

        this.include = include;
    }

    @Override
    public String toString() {

        return id;
    }
}