package ru.dom24x7.model.user;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import ru.dom24x7.cache.Addresses;
import ru.dom24x7.model.Model;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.address.dao.AddressDAOImpl;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.model.meter.dao.MeterHistoryDAOImpl;
import ru.dom24x7.model.photo.Photo;

/**
 * Пользователь
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class User extends Model {

    private String login;
	private String email;
    private String confirmCode;
    private boolean confirmed;
    private Photo photo;
    private Role role;
    private Date createDt;
    private Date lastAuthDt;
    private boolean subscribed;
    private String surname;
    private String midname;

    public User() {

        super();
    }

    public User(int id) {

        super(id);
    }

    public User(int id, String login, String email) {

        super(id);

        this.login = login;
        this.email = email;
    }

    public User(String login, String email) {

        super();

        this.login = login;
        this.email = email;
    }
    
    public String getLogin() {
    	
    	return login;
    }
    
    public void setLogin(String login) {
    	
    	this.login = login;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getConfirmCode() {
    	
    	return confirmCode;
    }
    
    public void setConfirmCode(String confirmCode) {
    	
    	this.confirmCode = confirmCode;
    }

    public boolean isConfirmed() {

        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {

        this.confirmed = confirmed;
    }
    
    public Photo getPhoto() {
    	
    	return photo;
    }
    
    public void setPhoto(Photo photo) {
    	
    	this.photo = photo;
    }

    public Role getRole() {

        return role;
    }

    public void setRole(Role role) {

        this.role = role;
    }

    public void clearPhoto() {
    	
    	this.photo = null;
    }

    public String getIconUrl() {

        if (photo != null) {
            return String.format("/user/%d/avatar/%s", id, photo.getFileName());
        }

        return null;
    }

    public Date getCreateDt() {

        return createDt;
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
    }

    public Date getLastAuthDt() {

        return lastAuthDt;
    }

    public void setLastAuthDt(Date lastAuthDt) {

        this.lastAuthDt = lastAuthDt;
    }

    public boolean isSubscribed() {

        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {

        this.subscribed = subscribed;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public String getMidname() {

        return midname;
    }

    public void setMidname(String midname) {

        this.midname = midname;
    }

    public String getFio() {

        String fio = null;
        if (name != null && name.trim().length() != 0) {
            fio = name;
        }
        if (midname != null && midname.trim().length() != 0) {
            if (fio != null) {
                fio += " ";
            }
            fio += midname;
        }
        if (surname != null && surname.trim().length() != 0) {
            if (fio != null) {
                fio += " ";
            }
            fio += surname;
        }

        return fio;
    }

    @Override
    public String toString() {

        String fio = getFio();

        return fio != null ? fio : email;
    }

    public Address getAddress(int addressId) {

        List<Address> addresses = new AddressDAOImpl().list(this);

        if (addresses == null || addresses.size() == 0) {
            return null;
        }

        for (Address address : addresses) {
            if (addressId == address.getId()) {
                return address;
            }
        }

        return null;
    }
    
    /**
     * Копирует все данные (адреса, счетчики и показания) к нашему пользователю
     * 
     * @param user
     */
    public void copy(User user) {
    	
    	// скопируем все адреса
    	List<Address> addressesList = Addresses.getInstance().list();
        if (addressesList == null || addressesList.size() == 0) {
    		// у копируемого пользователя нет адресов
    		return;
    	}
    	
    	for (Address address : addressesList) {
			Address newAddress = new Address();
			newAddress.setUser(this);
			newAddress.copy(address);
			// сохраняем адрес в БД
			if (new AddressDAOImpl().save(newAddress)) {
				// для каждого адреса сохраним его счетчики
				List<Meter> metersList = new MeterDAOImpl().list(address);
				if (metersList != null && metersList.size() > 0) {
					for (Meter meter : metersList) {
						Meter newMeter = new Meter();
						newMeter.setAddress(newAddress);
						newMeter.copy(meter);
						newMeter.setCompany(null); // уберем привязку к компании
						newMeter.setTemplate(null); // уберем привязку к шаблону
						if (new MeterDAOImpl().save(newMeter)) {
							// для каждого счетчика сохраним его показания
							List<MeterHistoryItem> historyList = meter.getHistory();
							if (historyList != null && historyList.size() > 0) {
								for (MeterHistoryItem item : historyList) {
									MeterHistoryItem newItem = new MeterHistoryItem();
									newItem.setMeter(newMeter);
									newItem.copy(item);
                                    new MeterHistoryDAOImpl().save(newItem);
								}
							}
						}
					}
				}
			}
		}
    }

    /**
     * Генерация кода подтверждения
     * @return
     */
    public static String generConfirmCode() {

        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }
}