package ru.evgajukov.profi.profile.response.entry.definition.image;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class Image {

    private List<String> protocols;
    private String url;

    public List<String> getProtocols() {

        return protocols;
    }

    public void setProtocols(List<String> protocols) {

        this.protocols = protocols;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    @Override
    public String toString() {

        return url;
    }
}