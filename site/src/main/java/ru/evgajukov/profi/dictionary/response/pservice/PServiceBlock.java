package ru.evgajukov.profi.dictionary.response.pservice;

import ru.evgajukov.profi.dictionary.response.DictionaryBlock;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class PServiceBlock extends DictionaryBlock {

    List<PServiceBook> books;

    public List<PServiceBook> getBooks() {

        return books;
    }

    public void setBooks(List<PServiceBook> books) {

        this.books = books;
    }
}
