package ru.dom24x7.site.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.model.application.Application;
import ru.dom24x7.model.application.dao.ApplicationDAOImpl;
import ru.dom24x7.site.api.message.BaseRequest;
import ru.dom24x7.site.api.message.BaseResponse;
import ru.dom24x7.site.api.message.ResponseCode;

/**
 * Created by timur on 22.02.17.
 * API: основная точка входа для работы с api
 */
@Controller
public class ApiController {

    @RequestMapping("/api")
    public @ResponseBody BaseResponse apiHandler(@RequestBody BaseRequest request) {

        BaseResponse response = new BaseResponse(request);

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(request.getHeader().getToken());
        if (app == null) {
            response.setCode(-1).setErrorMessages(ResponseCode.CODES.get(-1));
            return response;
        }
        if (app.isHold()) {
            response.setCode(-2).setErrorMessages(ResponseCode.CODES.get(-2));
            return response;
        }

        // теперь можем запустить нужный обработчик метода
        String method = request.getHeader().getMethod();
        if ("auth".equalsIgnoreCase(method)) {
            return new ApiUserController(request).auth();
        } else if ("reg".equalsIgnoreCase(method)) {
            return new ApiUserController(request).reg();
        }

        response.setCode(-999).setErrorMessages(ResponseCode.CODES.get(-999));
        return response;
    }
}