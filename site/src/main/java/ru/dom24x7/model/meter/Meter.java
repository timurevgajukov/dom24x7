package ru.dom24x7.model.meter;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.dom24x7.model.Model;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.model.device.Device;
import ru.dom24x7.model.device.dao.DeviceDAOImpl;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.model.meter.dao.MeterHistoryDAOImpl;
import ru.dom24x7.model.meter.dao.MeterIconDAOImpl;
import ru.dom24x7.model.template.Template;
import ru.dom24x7.model.template.dao.TemplateDAOImpl;
import ru.dom24x7.service.meter.MeterService;
import ru.dom24x7.utils.StrUtils;

/**
 * Счетчик
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Meter extends Model {
	
    private String title;
    private MeterType type;
    private Address address;
    private double tariff;
    private int capacity; // разрядность
	private int precision; // точность
    
    private int companyId;
    private Company company;
    
    private int templateId;
    private Template template;
    
    private List<MeterHistoryItem> history;
    
    private List<Device> devices;
    
    private int iconId;
    private MeterIcon icon;
    
    public Meter() {

        super();
    }

    public Meter(int id) {

        super(id);
    }

    public String getTitle() {
    	
    	if (title == null || title.length() == 0) {
    		return type.getName();
    	}

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public MeterType getType() {

        return type;
    }

    public void setType(MeterType type) {

        this.type = type;
    }

    @JsonIgnore
    public Address getAddress() {

        return address;
    }

    public void setAddress(Address address) {

        this.address = address;
    }
    
    public Company getCompany() {
    	
    	if (companyId == 0) {
    		// к счетчику не присоединена компания
    		return null;
    	}
    	
    	if (company == null) {
    		// загрузить информацию по компании
    		company = new CompanyDAOImpl().get(companyId);
    	}
    	
    	return company;
    }
    
    public void setCompanyId(int companyId) {
    	
    	this.companyId = companyId;
    }
    
    public void setCompany(Company company) {
    	
    	this.company = company;
    	
    	if (company != null) {
    		this.companyId = company.getId();
    	} else {
    		this.companyId = 0;
    	}
    	new MeterDAOImpl().save(this, new Company(companyId));
    }
    
    public MeterIcon getIcon() {
    	
    	if (iconId == 0) {
    		// к счетчику не присоединена иконка, использовать иконку по-умолчанию для типа счетчика
    		return type.getIcon();
    	}
    	
    	if (icon == null) {
    		// загрузить информацию по иконке
    		icon = new MeterIconDAOImpl().get(iconId);
    	}
    	
    	return icon;
    }
    
    public void setIconId(int iconId) {
    	
    	this.iconId = iconId;
    }
    
    public void setIcon(MeterIcon icon) {
    	
    	this.icon = icon;
    	
    	if (icon != null) {
    		this.iconId = icon.getId();
    	} else {
    		this.iconId = 0;
    	}
    }
    
    public Template getTemplate() {
    	
    	if (templateId == 0) {
    		// к счетчику не присоединен шаблон
    		return null;
    	}
    	
    	if (template == null) {
    		// загрузить шаблон
    		template = new TemplateDAOImpl().get(templateId);
    	}
    	return template;
    }
    
    public void setTemplateId(int templateId) {
    	
    	this.templateId = templateId;
    }
    
    public void setTemplate(Template template) {
    	
    	this.template = template;
    	
    	if (template != null) {
    		this.templateId = template.getId();
    	} else {
    		this.templateId = 0;
    	}
    	new MeterDAOImpl().save(this, new Template(templateId));
    }

    public double getTariff() {

        return tariff;
    }
    
    public String getStringTariff() {
    	
    	if (type.isWithoutValue()) {
			return String.format("%.2f %s", tariff, address.getCurrency().getSymbol());
		} else {
			return String.format("%.2f <span class='rouble'>%s</span>/%s", tariff,
					type.getUnit(), address.getCurrency().getSymbol());
		}
    }
    
    public void setTariff(double tariff) {

        this.tariff = tariff;
    }
    
    public int getCapacity() {
    	
    	Device device = getUsedDevice();
    	if (device == null) {
    		return capacity;
    	}
    	
    	return device.getCapacity();
    }
    
    public int getCapacity(Date date) {
    	
    	if (devices == null) {
    		loadDevices();
    	}
    	
    	if (devices == null) {
    		return getCapacity();
    	}
    	
    	if (date != null) {
    		for (int i = 0; i < devices.size(); i++) {
    			if (devices.get(i).getInstallationDate() != null) {
    				if (date.compareTo(devices.get(i).getInstallationDate()) > 0) {
    					return devices.get(i).getCapacity();
    				}
    			}
    		}
    	}
    	
    	return getCapacity();
    }
    
    public void setCapacity(int capacity) {
    	
    	this.capacity = capacity;
    	
    	Device device = getUsedDevice();
    	if (device == null) {
			return;
    	}
    	
    	device.setCapacity(capacity);
    }
    
    public int getPrecision() {
    	
    	Device device = getUsedDevice();
    	if (device == null) {
    		return precision;
    	}
    	
    	return device.getPrecision();
    }
    
    public int getPrecision(Date date) {
    	
    	if (devices == null) {
    		loadDevices();
    	}
    	
    	if (devices == null) {
    		return getPrecision();
    	}
    	
    	if (date != null) {
    		for (int i = 0; i < devices.size(); i++) {
    			if (devices.get(i).getInstallationDate() != null) {
    				if (date.compareTo(devices.get(i).getInstallationDate()) > 0) {
    					return devices.get(i).getPrecision();
    				}
    			}
    		}
    	}
    	
    	return getPrecision();
    }
    
    public void setPrecision(int precision) {
    	
    	this.precision = precision;
    	
    	Device device = getUsedDevice();
    	if (device == null) {
    		return;
    	}
    	
    	device.setPrecision(precision);
    }

    @JsonIgnore
    public List<MeterHistoryItem> getHistory() {
    	
    	return history;
    }

    @JsonIgnore
    public List<Device> getDevices() {
    	
    	if (devices == null) {
    		loadDevices();
    	}
    	
    	return devices;
    }
    
    public Device getUsedDevice() {
    	
    	if (devices == null) {
    		loadDevices();
    	}
    	
    	if (devices == null) {
    		return null;
    	}
    	
    	for (Device device : devices) {
			if (device.isUsed()) {
				return device;
			}
		}
    	
    	return null;
    }
    
    public void loadHistory() {
    	
    	if (history != null) {
			history.clear();
		}

		history = new MeterHistoryDAOImpl().list(this);
    }

    public void loadHistory(int offset, int count) {

		if (history != null) {
			history.clear();
		}

		history = new MeterHistoryDAOImpl().list(this, offset, count);
	}
    
    public void loadDevices() {
    	
    	if (devices != null) {
			devices.clear();
		}

		devices = new DeviceDAOImpl().list(this);
    	
    	if (devices != null && devices.size() != 0) {
			Device device = getUsedDevice();
			if (device != null) {
				capacity = device.getCapacity();
				precision = device.getPrecision();
			}
		}
    }

    public MeterHistoryItem getHistoryItem(int id) {

		loadHistory();

		if (history == null || history.size() == 0) {
			return null;
		}

		for (MeterHistoryItem item : history) {
			if (id == item.getId()) {
				return item;
			}
		}

		return null;
	}
    
    public double getLastValue() {
    	
    	if (history == null || history.size() == 0) {
    		return 0.0;
    	}
    	
    	return history.get(0).getValue();
    }

    public double getLastCost() {

		if (history == null || history.size() == 0) {
			return 0.0;
		}

		return history.get(0).getCost();
	}
    
    public String getStringLastValue() {
    	
    	if (history == null || history.size() == 0) {
    		return "";
    	}

    	if (type.isWithoutValue()) {
    		return StrUtils.getStrAmount(getLastCost(), address.getCurrency().toString());
		} else {
			return String.format("%s %s", StrUtils.format(getLastValue(), getCapacity(), getPrecision()), type.getUnit());
		}
    }
    
    public Date getLastValueDate() {
    	
    	if (history == null || history.size() == 0) {
    		return null;
    	}
    	
    	return history.get(0).getDate();
    }

	public String getTotalLastMonth() {

		double totalLastMonth = new MeterService(this).getTotalLastMonth();

		if (totalLastMonth == 0) {
			return "";
		} else {
			return StrUtils.getStrAmount(totalLastMonth, address.getCurrency().getSymbol());
		}
	}
    
    public double getCost() {

    	return new MeterService(this).getDifference() * tariff;
    }
    
    @Override
    public String toString() {

        if (title == null || title.length() == 0) {
            return type.toString();
        } else {
            return title;
        }
    }
    
    /**
     * Полностью копирует счетчик без идентификатора, адреса и приборов учета
     * 
     * @param meter
     */
    public void copy(Meter meter) {
    	
    	this.title = meter.title;
    	this.type = meter.type;
    	this.tariff = meter.tariff;
    	
    	// перед копирование разрядности и точности, нужно, на всякий случай перезагрузить приборы учета
    	meter.loadDevices();
    	this.capacity = meter.capacity;
    	this.precision = meter.precision;
    }
}