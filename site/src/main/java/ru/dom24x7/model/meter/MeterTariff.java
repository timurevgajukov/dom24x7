package ru.dom24x7.model.meter;

import ru.dom24x7.model.Model;

import java.util.Date;

/**
 * Created by timur on 25.06.17.
 * Тариф счетчика
 */
public class MeterTariff extends Model {

    private Date dt;
    private Double value;

    public MeterTariff() {

        super();
    }

    public MeterTariff(int id, Date dt, Double value) {

        super(id);
        this.dt = dt;
        this.value = value;
    }

    public Date getDt() {

        return dt;
    }

    public void setDt(Date dt) {

        this.dt = dt;
    }

    public Double getValue() {

        return value;
    }

    public void setValue(Double value) {

        this.value = value;
    }
}