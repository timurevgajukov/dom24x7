package ru.dom24x7.model.device.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.device.Device;
import ru.dom24x7.model.device.dao.interfaces.DeviceDAO;
import ru.dom24x7.model.meter.Meter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса работы с приборами учета коммунальных услуг
 */
public class DeviceDAOImpl extends ModelDAOImpl implements DeviceDAO {

    private static final String QUERY_LIST_BY_METER = "{ call devices_list_by_meter_id(?) }";
    private static final String QUERY_SAVE = "{ call devices_save(?,?,?,?,?,?,?) }";

    @Override
    public List<Device> list(final Meter meter) {

        List<Device> list = jdbcTemplate.query(QUERY_LIST_BY_METER, new RowMapper<Device>() {

            @Override
            public Device mapRow(ResultSet resultSet, int i) throws SQLException {

                Device device = load(resultSet);
                device.setMeter(meter);

                return device;
            }
        }, meter.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Device device) {

        Device newDevice = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        device.getId(),
                        device.getMeter().getId(),
                        device.getSerial() == null || device.getSerial().trim().length() == 0 ? null : device.getSerial().trim(),
                        device.getCapacity(),
                        device.getPrecision(),
                        device.getInstallationDate() != null ? device.getInstallationDate() : null,
                        device.getInspectionDate() != null ? device.getInspectionDate() : null
                }, new RowMapper<Device>() {

                    @Override
                    public Device mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        device.setId(newDevice.getId());

        return true;
    }

    private Device load(ResultSet rs) throws SQLException {

        Device device = new Device(rs.getInt("id"));
        device.setMeter(new Meter(rs.getInt("meter_id")));
        device.setSerial(rs.getString("serial"));
        device.setCapacity(rs.getInt("capacity"));
        device.setPrecision(rs.getInt("precision"));
        device.setInstallationDate(rs.getDate("installation_dt"));
        device.setInspectionDate(rs.getDate("inspection_dt"));
        device.setUsed(rs.getBoolean("used"));

        return device;
    }
}