package ru.dom24x7.model.reference.country.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.cache.Currencies;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.reference.ReferenceDAO;
import ru.dom24x7.model.reference.country.Country;
import ru.dom24x7.model.reference.currency.Currency;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 18.09.16.
 *
 * Реализация интерфейса для работы со справочником стран
 */
public class CountryDAOImpl extends ModelDAOImpl implements ReferenceDAO<Country> {

    private static final String QUERY_LIST = "{ call countries_list() }";

    @Override
    public List<Country> list() {

        final Currencies currencies = Currencies.getInstance();

        List<Country> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Country>() {

            @Override
            public Country mapRow(ResultSet resultSet, int i) throws SQLException {

                Country country = load(resultSet);
                country.setDefaultCurrency(currencies.get(country.getDefaultCurrency().getId()));

                return country;
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private Country load(ResultSet rs) throws SQLException {

        Country country = new Country(rs.getInt("id"), rs.getString("name"));
        country.setDefaultCountry(rs.getBoolean("is_default"));
        country.setDefaultCurrency(new Currency(rs.getInt("default_currency_id")));
        country.setCode(rs.getString("code"));

        return country;
    }
}