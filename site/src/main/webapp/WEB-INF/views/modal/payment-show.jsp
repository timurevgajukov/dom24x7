<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- SHOW PAYMENT MODAL WINDOW: BEGIN -->
<div id="payment-show-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 id="payment-show-modal-title" class="modal-title">Платежное поручение</h4>
            </div>
            <div id="payment-show-body" class="modal-body"></div>
            <div class="modal-footer">
                <%--<button id="btn-payment-show-edit" class="btn btn-success">Редактировать</button>--%>
                <%--<button id="btn-payment-show-delete" class="btn btn-danger">Удалить</button>--%>
                <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- SHOW PAYMENT MODAL WINDOW: END -->

<script type="text/html" id="tmpl-payment-show">
    <img class="img-responsive" src="/panel/payments/images/{{=it.photo.id}}/{{=it.photo.fileName}}" width="100%" alt="">
</script>

<script>
    $(document).ready(function () {
        $(document).on('click', '.btn-payment-show', function () {
            $('#payment-show-modal-title').html('Платежное поручение');
            $('#payment-show-body').empty();

            var paymentId = $(this).data('id');
            $.get('/panel/payments/' + paymentId, function (payment) {
                if (!payment) {
                    return;
                }

                $('#payment-show-modal-title').html(moment(payment.month).format('MMMM YYYY'));

                var data = {
                    id: payment.id,
                    photo: payment.photo

                };
                var tmpl = doT.template($('#tmpl-payment-show').html());
                $('#payment-show-body').html(tmpl(data));
            });
        });
    });
</script>