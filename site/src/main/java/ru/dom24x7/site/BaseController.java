package ru.dom24x7.site;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import ru.dom24x7.component.OpenGraph;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.address.dao.AddressDAOImpl;
import ru.dom24x7.model.user.User;
import ru.dom24x7.security.Message;
import ru.dom24x7.security.Security;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by timur on 22.09.16.
 *
 * Базовый класс для всех контроллеров
 */
public class BaseController {

    @Autowired
    protected ServletContext servletContext;

    @Autowired
    protected Security security;

    private static final String DESCRIPTION = "";
    private static final String KEYWORDS = "";

    protected List<Message> messages = new ArrayList<Message>();

    // сообщения об ошибках
    protected static HashMap<String, Message> ERROR_MESSAGES;

    {
        ERROR_MESSAGES = new HashMap<String, Message>();
        ERROR_MESSAGES.put("ERROR_00", new Message("Данный функционал находится в разработке", Message.TYPE_WARNING));
        ERROR_MESSAGES.put("ERROR_01", new Message("Ошибка доступа к сервису. Необходима авторизация.", Message.TYPE_ERROR));
        ERROR_MESSAGES.put("ERROR_02", new Message("Ошибка доступа к сервису. Нет прав по указанному адресу.", Message.TYPE_ERROR));
        ERROR_MESSAGES.put("ERROR_03", new Message("Ошибка доступа к сервису. Нет прав на указанный счетчик.", Message.TYPE_ERROR));
        ERROR_MESSAGES.put("ERROR_04", new Message("Ошибка доступа к сервису. Нет прав на указанное показание счетчика.", Message.TYPE_ERROR));
    }

    protected List<Address> userAddresses;

    protected void setModelMap(ModelMap model, String title) {

        setModelMap(model, title, null);
    }

    protected void setModelMap(ModelMap model, String title, String active) {

        setModelMap(model, title, DESCRIPTION, KEYWORDS, active);
    }

    protected void setModelMap(ModelMap model, String title, String description, String keywords, String active) {

        OpenGraph og = new OpenGraph();
        og.setTitle(title).setDescription(description);

        setModelMap(model, og, keywords, active);
    }

    protected void setModelMap(ModelMap model, OpenGraph og, String keywords, String active) {

        model.addAttribute("title", og.getTitle());
        model.addAttribute("description", og.getDescription());
        model.addAttribute("keywords", keywords);
        model.addAttribute("og", og);
        model.addAttribute("active", active);
        model.addAttribute("security", security);

        if (security.isAuth()) {
            User user = security.getUser();
            if (!user.isConfirmed()) {
                Message msg = new Message(null, "Необходимо подтвердить адрес электронной почты",
                        Message.TYPE_WARNING, Message.LABEL_WARN, "/profile");
                security.getNotifications().add(msg);
            }
            userAddresses = new AddressDAOImpl().list(security.getUser());
            model.addAttribute("userAddresses", userAddresses);
        }
    }
}