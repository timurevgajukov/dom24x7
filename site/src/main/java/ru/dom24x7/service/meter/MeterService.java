package ru.dom24x7.service.meter;

import ru.dom24x7.model.device.Device;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Сервис для работы со счетчиком
 */
public class MeterService {

    private Meter meter;

    public MeterService(Meter meter) {

        this.meter = meter;
        meter.loadHistory();
        meter.loadDevices();
    }

    /**
     * Если за указанный день имеется показание, то возвращает его, иначе null
     *
     * @param date
     * @return
     */
    public MeterHistoryItem getItemByDate(Date date) {

        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null || history.size() == 0) {
            return null;
        }

        for (int i=0; i<history.size(); i++) {
            long diff = DateUtils.dayCount(date, history.get(i).getDate());
            if (diff == 1) {
                // в нужную дату было снято показание
                return history.get(i);
            }
        }

        return null;
    }

    /**
     * Возвращает значение счетчика за определенный день,
     * если такого значения нет, то на основе уже имеющихся показаний расчитывает его
     *
     * @param date
     * @return
     */
    public double getValueByDate(Date date) {

        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null) {
            // пока нет ни одного показания
            return 0.0;
        }

        if (history.size() == 1) {
            // пока только одно показание и это не нужная нам дата
            return history.get(0).getValue();
        }

        // расчитываем разницы между датами снятия показаний и нужной нам датой
        // если в эту дату снимали показания, то сразу возвращаем его
        // в истории показаний значения отсортированы по дате
        List<Long> dateDiffs = new ArrayList<Long>();
        for (int i=0; i<history.size(); i++) {
            long diff = DateUtils.dayCount(date, history.get(i).getDate());
            if (diff == 1) {
                // в нужную дату было снято показание
                return history.get(i).getValue();
            }
            dateDiffs.add(DateUtils.dayCount(date, history.get(i).getDate()));
        }

        MeterHistoryItem itemStart;
        MeterHistoryItem itemEnd;

        // история отсортирована по убыванию даты

        if (date.compareTo(history.get(0).getDate()) > 0) {
            // нужная дата позже любого из имеющихся у нас, берем две первые даты
            itemStart = history.get(1);
            itemEnd = history.get(0);

            return getValueByDate(date, itemStart, itemEnd);
        }

        if (date.compareTo(history.get(history.size()-1).getDate()) < 0) {
            // нужная дата раньше любого из имеющихся у нас, берем две последние
            itemStart = history.get(history.size()-1);
            itemEnd = history.get(history.size()-2);

            return getValueByDate(date, itemStart, itemEnd);
        }

        // нужная дата находится между имеющихся у нас
        itemStart = history.get(history.size()-1);
        int i = history.size()-2;
        while (date.compareTo(history.get(i).getDate()) > 0) {
            itemStart = history.get(i);
            i--;
        }
        itemEnd = history.get(i);

        return getValueByDate(date, itemStart, itemEnd);
    }

    /**
     * Возвращает сколько было затрачено ресурсов за период
     *
     * @param dateStart
     * @param dateEnd
     * @return
     */
    public double getDiffValueByPeriod(Date dateStart, Date dateEnd) {

        double startValue = getValueByDate(dateStart);
        double endValue = getValueByDate(dateEnd);

        return getDiffValues(startValue, endValue, dateStart, dateEnd);
    }

    /**
     * Возвращает сколько было затрачено ресурсов в указанный месяц,
     * если месяц не закончен, то берем текущий день
     *
     * @param date
     * @return
     */
    public double getDiffValueByMonth(Date date) {

        Date dateStart = DateUtils.getFirstDayInMonth(date);
        Date dateEnd = DateUtils.getLastDayInMonth(date);

        double startValue = getValueByDate(dateStart);

        double endValue = startValue;
        if (new Date().compareTo(dateEnd) <= 0) {
            // расчитываем до текущей даты
            dateEnd = new Date();
        }
        endValue = getValueByDate(dateEnd);

        return getDiffValues(startValue, endValue, dateStart, dateEnd);
    }

    /**
     * Возвращает затраченные деньги по счетчику без показаний за период
     * @param dateStart
     * @param dateEnd
     * @return
     */
    public double getCostByPeriod(Date dateStart, Date dateEnd) {

        // ищем оплату в указанном месяце, если несколько, то суммируем
        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null) {
            // пока нет ни одного показания
            return 0.0;
        }

        double cost = 0.0;
        for (MeterHistoryItem item : history) {
            if (item.getDate().compareTo(dateStart) >= 0 && item.getDate().compareTo(dateEnd) <= 0) {
                cost += item.getCost() != null ? item.getCost() : 0.0;
            }
        }

        return cost;
    }

    /**
     * Возвращает затраченные деньги по счетчику без показаний за указанный месяц
     * @param date
     * @return
     */
    public double getCostByMonth(Date date) {
        Date dateStart = DateUtils.getFirstDayInMonth(date);
        Date dateEnd = DateUtils.getLastDayInMonth(date);

        return getCostByPeriod(dateStart, dateEnd);
    }

    /**
     * Возвращает сумму затрат за период
     *
     * @param dateStart
     * @param dateEnd
     * @return
     */
    public double getTotalByPeriod(Date dateStart, Date dateEnd) {

        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null || history.size() == 0) {
            return 0.0;
        }

        if (history.get(0).getMeter().getTariff() == 0.0) {
            // тариф не указан
            return 0.0;
        }

        if (meter.getType().isWithoutValue()) {
            return getCostByPeriod(dateStart, dateEnd);
        } else {
            return meter.getTariff() * getDiffValueByPeriod(dateStart, dateEnd);
        }
    }

    /**
     * Возвращает сумму затрат за указанный месяц
     *
     * @param date
     * @return
     */
    public double getTotalByMonth(Date date) {

        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null || history.size() == 0) {
            return 0.0;
        }

        if (history.size() == 1) {
            // имеется только начальное значение
            if (meter.getType().isWithoutValue()) {
                return getCostByMonth(date);
            } else {
                return 0.0;
            }
        }

        if (history.get(0).getMeter().getTariff() == 0.0) {
            // тариф не указан
            return 0.0;
        }

        if (meter.getType().isWithoutValue()) {
            return getCostByMonth(date);
        } else {
            return history.get(0).getMeter().getTariff() * getDiffValueByMonth(date);
        }
    }

    /**
     * Возвращает сумму затрат за текущий месяц
     *
     * @return
     */
    public double getTotalLastMonth() {

        return getTotalByMonth(new Date());
    }

    /**
     * Возвращает разницу между двумя соседними значениями
     *
     * @return
     */
    public double getDifference() {

        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null || history.size() < 2) {
            return 0.0;
        }

        return history.get(0).getValue() - history.get(1).getValue();
    }

    /**
     * Если между двумя показаниями мы сменили прибор учета, то возвращаем его, иначе null
     *
     * @param start
     * @param end
     * @return
     */
    public Device getChangedDevice(Date start, Date end) {

        List<Device> devices = meter.getDevices();

        if (devices == null || devices.size() == 0) {
            // не удалось загрузить устройства
            return null;
        }

        for (Device device : devices) {
            Date date = device.getInstallationDate();
            if (date != null) {
                if (date.compareTo(start) >= 0 && date.compareTo(end) < 0) {
                    return device;
                }
            }
        }

        return null;
    }

    /**
     * Возвращает разность показаний с учетом разрядности и смены прибора учета
     *
     * @param startValue
     * @param endValue
     * @param dateStart
     * @param dateEnd
     * @return
     */
    public double getDiffValues(double startValue, double endValue, Date dateStart, Date dateEnd) {

        if (endValue >= startValue) {
            // мы не вышли за пределы разрядности или разрядность не указана
            return endValue - startValue;
        } else {
            // мы вышли за пределы разрядности или установили новый прибор учета
            Device device = getChangedDevice(dateStart, dateEnd);
            if (device == null) {
                // мы именно вышли за пределы разрядности
                if (meter.getCapacity() != -1) {
                    return Math.pow(10.0, meter.getCapacity()) - startValue + endValue;
                } else {
                    return endValue - startValue;
                }
            } else {
                // мы сменили прибор учета
                return endValue - startValue + getValueByDate(device.getInstallationDate());
            }
        }
    }

    /**
     * Возвращает true, если до указанной даты были сняты показания
     *
     * @param date
     * @return
     */
    public boolean beforeDate(Date date) {

        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null || history.size() == 0) {
            return false;
        }

        if (date.compareTo(history.get(history.size() - 1).getDate()) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает true, если после указанной даты были сняты показания
     *
     * @param date
     * @return
     */
    public boolean afterDate(Date date) {

        List<MeterHistoryItem> history = meter.getHistory();

        if (history == null || history.size() == 0) {
            return false;
        }

        if (date.compareTo(history.get(0).getDate()) < 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Расчитывает значение счетчика за определенную дату между двумя показаниями
     *
     * @param date
     * @param itemStart
     * @param itemEnd
     * @return
     */
    private double getValueByDate(Date date, MeterHistoryItem itemStart, MeterHistoryItem itemEnd) {

        // k = (y2-y1)/(x2-x1)
        long x2_x1 = DateUtils.dayCount(itemEnd.getDate(), itemStart.getDate()) - 1;
        if (x2_x1 == 0) {
            // даты совпали и нельзя вычислить коэффициент k
            return 0.0;
        }

        double y2_y1 = getDiffValues(itemStart.getValue(), itemEnd.getValue(), itemStart.getDate(), itemEnd.getDate());

        double k = y2_y1 / x2_x1;

        double y = 0.0;

        // признак, что меняли прибор учета
        boolean changedDevice = (getChangedDevice(itemStart.getDate(), itemEnd.getDate()) != null);

        // TODO: перевроверить при превышении разрядности
        if (date.compareTo(itemStart.getDate()) < 0) {
            // нужная дата раньше начальной
            // y = y1 - k*(x1-x)
            long x1_x = DateUtils.dayCount(itemStart.getDate(), date) - 1;
            if (changedDevice) {
                y = -k * x1_x; // TODO: ???
            } else {
                y = itemStart.getValue() - k * x1_x;
            }
        } else if (date.compareTo(itemEnd.getDate()) < 0) {
            // нужная дата между нашими
            // y = y1 + k*(x-x1)
            long x_x1 = DateUtils.dayCount(date, itemStart.getDate()) - 1;
            if (changedDevice) {
                y = k * x_x1;
            } else {
                y = itemStart.getValue() + k * x_x1;
            }
        } else if (date.compareTo(itemEnd.getDate()) > 0) {
            // нужная дата после конечной
            // y = y2 + k*(x-x2)
            long x_x2 = DateUtils.dayCount(date, itemEnd.getDate()) - 1;
            y = itemEnd.getValue() + k * x_x2;
        }

        return y;
    }
}