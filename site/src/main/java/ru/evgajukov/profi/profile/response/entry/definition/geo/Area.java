package ru.evgajukov.profi.profile.response.entry.definition.geo;

/**
 * Created by timur on 17.06.17.
 */
public class Area {

    private String id;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    @Override
    public String toString() {

        return id;
    }
}