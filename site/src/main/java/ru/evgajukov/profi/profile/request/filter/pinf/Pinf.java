package ru.evgajukov.profi.profile.request.filter.pinf;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 18.06.17.
 */
public class Pinf {

    private List<Value> values;

    public List<Value> getValues() {

        return values;
    }

    public void setValues(List<Value> values) {

        this.values = values;
    }

    public void addValue(Value value) {

        if (values == null) {
            values = new ArrayList<>();
        }

        values.add(value);
    }
}