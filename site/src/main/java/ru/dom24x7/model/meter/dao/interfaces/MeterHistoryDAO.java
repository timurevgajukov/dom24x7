package ru.dom24x7.model.meter.dao.interfaces;

import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с историей показаний счетчика
 */
public interface MeterHistoryDAO {

    /**
     * Возвращает информацию по конкретному показанию счетика
     *
     * @param id
     * @return
     */
    MeterHistoryItem get(int id);

    /**
     * Возвращает историю показаний счетчика
     *
     * @param meter
     * @return
     */
    List<MeterHistoryItem> list(Meter meter);

    /**
     * Возвращает историю по показаниям счетчика
     *
     * @param meter
     * @param offset
     * @param count
     * @return
     */
    List<MeterHistoryItem> list(Meter meter, int offset, int count);

    /**
     * Сохраняет показание счетчика
     *
     * @param item
     * @return
     */
    boolean save(MeterHistoryItem item);

    /**
     * Удаляет показание счетчика
     *
     * @param item
     * @return
     */
    boolean delete(MeterHistoryItem item);
}