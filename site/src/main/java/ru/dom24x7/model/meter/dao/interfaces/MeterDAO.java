package ru.dom24x7.model.meter.dao.interfaces;

import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.template.Template;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы со счетчиками
 */
public interface MeterDAO {

    /**
     * Возвращает конкретный счетчик пользователя
     *
     * @param id
     * @param user
     * @return
     */
    Meter get(int id, User user);

    /**
     * Возвращает список счетчиков по конкретному адресу
     *
     * @param address
     * @return
     */
    List<Meter> list(Address address);

    /**
     * Возвращает список счетчиков по конкретному адресу
     *
     * @param address
     * @param withHistory
     * @return
     */
    List<Meter> list(Address address, boolean withHistory);

    /**
     * Возвращает список всех счетчиков пользователя
     *
     * @param user
     * @return
     */
    List<Meter> list(User user);

    /**
     * Возвращает список всех счетчиков пользователя
     *
     * @param user
     * @param withHistory
     * @return
     */
    List<Meter> list(User user, boolean withHistory);

    /**
     * Сохраняет счетчик пользователя
     *
     * @param meter
     * @return
     */
    boolean save(Meter meter);

    /**
     * Сохраняет связку счетчика с компанией ЖКХ
     *
     * @param meter
     * @param company
     * @return
     */
    boolean save(Meter meter, Company company);

    /**
     * Сохраняет связку счетчика с шаблоном
     *
     * @param meter
     * @param template
     * @return
     */
    boolean save(Meter meter, Template template);

    /**
     * Удаляет счетчик пользователя
     *
     * @param meter
     * @return
     */
    boolean delete(Meter meter);

    /**
     * Блокирует счетчик пользователя
     *
     * @param meter
     * @return
     */
    boolean hold(Meter meter);
}