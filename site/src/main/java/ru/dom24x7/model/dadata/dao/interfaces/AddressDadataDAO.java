package ru.dom24x7.model.dadata.dao.interfaces;

import ru.dom24x7.model.dadata.AddressDadata;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с адресами в формате сервиса dadata.ru
 */
public interface AddressDadataDAO {

    /**
     * Возвращает список адресов
     *
     * @param offset
     * @param count
     * @return
     */
    List<AddressDadata> list(int offset, int count);

    /**
     * Сохранение адреса в формате сервиса dadata.ru
     *
     * @param address
     * @return
     */
    boolean save(AddressDadata address);
}