<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/resources/lib/select2/css/select2.min.css" />

<!-- EDIT COMPANY MODAL WINDOW: BEGIN -->
<div id="company-edit-modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div id="meter-value-content" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Привязать компанию ЖКХ к счетчикам</h4>
            </div>
            <form method="post" action="/panel/addresses/${meter.address.id}/meters/${meter.id}/company/bind">
                <div class="modal-body">
                    <div class="form-group">
                        <select id="company" name="company" class="form-control" style="width: 100%;"></select>
                        <small>Сейчас мы активно загружаем данные по всем компаниям и если вы пока не нашли своего,
                            то попробуйте через несколько дней.</small>
                    </div>
                    <div class="form-group">
                        <input id="account" name="account" type="text" class="form-control" placeholder="Лицевой счет"
                               value="${meter.company.getAccount(meter.address.user)}" />
                    </div>
                    <h4>Счетчики</h4>
                    <c:if test="${meters != null && meters.size() != 0}">
                        <c:forEach items="${meters}" var="item">
                            <div class="checkbox">
                                <input id="modal-company-meter-${item.id}" name="meters" type="checkbox" class="meter-check" value="${item.id}" /> ${item}
                            </div>
                        </c:forEach>
                    </c:if>
                </div>
                <div class="modal-footer">
                    <button id="btn-send-company" class="btn btn-success" type="submit">Сохранить</button>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT COMPANY MODAL WINDOW: END -->

<script src="/resources/lib/select2/js/select2.full.min.js"></script>
<script src="/resources/lib/select2/js/i18n/ru.min.js"></script>

<script>
    $(document).ready(function () {
        var meters = [];
        $.get('/ajax/addresses/${meter.address.id}/meters', function (list) {
            if (!list) {
                return;
            }
            meters = list;
        });

        $('.meter-check').iCheck({ checkboxClass: 'icheckbox_square-green' });

        select2CompanyList($('#company'), 30, function (repo) {
            if (meters.length != 0) {
                for (var i = 0; i < meters.length; i++) {
                    if (meters[i].company != null && meters[i].company.id == repo.id) {
                        $('#modal-company-meter-' + meters[i].id).iCheck('check');
                    } else {
                        $('#modal-company-meter-' + meters[i].id).iCheck('uncheck');
                    }
                }
            }
        });
    });
</script>