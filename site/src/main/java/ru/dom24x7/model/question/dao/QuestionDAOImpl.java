package ru.dom24x7.model.question.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.question.Question;
import ru.dom24x7.model.question.dao.interfaces.QuestionDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса для работы с вопросами
 */
public class QuestionDAOImpl extends ModelDAOImpl implements QuestionDAO {

    private static final String QUERY_LIST = "{ call questions_list() }";

    @Override
    public List<Question> list() {

        List<Question> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Question>() {

            @Override
            public Question mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private Question load(ResultSet rs) throws SQLException {

        Question question = new Question(rs.getInt("id"));
        question.setQuestion(rs.getString("question"));
        question.setAnswer(rs.getString("answer"));

        return question;
    }
}