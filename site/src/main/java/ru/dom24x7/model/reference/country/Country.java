package ru.dom24x7.model.reference.country;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.reference.currency.Currency;

/**
 * Страна
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Country extends Model {

	public static final int DEFAULT_COUNTRY_ID = 1; // Россия

	private boolean defaultCountry;
	private Currency defaultCurrency;
	private String code;

	public Country(int id) {

		super(id);
	}
	
	public Country(int id, String name) {
		
		super(id, name);
	}

	public boolean isDefaultCountry() {

		return defaultCountry;
	}

	public void setDefaultCountry(boolean defaultCountry) {

		this.defaultCountry = defaultCountry;
	}

	public Currency getDefaultCurrency() {

		return defaultCurrency;
	}

	public void setDefaultCurrency(Currency defaultCurrency) {

		this.defaultCurrency = defaultCurrency;
	}

	public String getCode() {

		return code;
	}

	public void setCode(String code) {

		this.code = code;
	}
}
