<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row m-t-lg">
    <div class="col-md-5">
        <p>Я уже пользуюсь сервисом.</p>
        <div id="auth-panel" class="hpanel hgreen">
            <div id="auth-panel-alert" class="alert" style="display: none;">
                <span id="auth-panel-alert-message" class="error-message"></span>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                        <input class="form-control" type="text" id="auth-login" name="login"
                               placeholder="Эл. почта"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                        <input class="form-control" type="password" id="auth-password" name="password"
                               placeholder="Пароль"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8 text-left">
                        <div class="checkbox"><input id="remember-me" type="checkbox" checked> запомнить
                            меня
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group text-right">
                            <button id="btn-send-auth"
                                    class="ladda-button btn btn-success text-uppercase"
                                    data-style="zoom-in">Войти
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <a href="#" data-toggle="modal" data-target="#recover-password-modal">восстановить
                        пароль</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <p>Я пока новичок и хочу пользоваться сервисом.</p>
        <div id="reg-panel" class="hpanel hblue">
            <div id="reg-panel-alert" class="alert" style="display: none;">
                <span id="reg-panel-alert-message" class="error-message"></span>
            </div>
            <div class="panel-body">
                <form id="modal-reg-form" method="post" action="/security/registration">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    <input class="form-control" type="email" id="reg-email1"
                                           name="email1" required
                                           placeholder="Введите электронную почту"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    <input class="form-control" type="email" id="reg-email2"
                                           name="email2" required
                                           placeholder="Введите электронную почту повторно"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input class="form-control" type="password" id="reg-password1"
                                           name="password1" required
                                           placeholder="Введите пароль"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input class="form-control" type="password" id="reg-password2"
                                           name="password2" required
                                           placeholder="Введите пароль повторно"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox"><input id="ch-subscribe" type="checkbox" checked> хочу
                        получать интересные новости на почту
                    </div>
                    <div class="checkbox"><input id="ch-privacy" type="checkbox"> прочитал и согласен с
                        <a href="/about/privacy" class="btn-link text-info">политикой безопасности</a>
                    </div>
                    <div class="form-group text-right">
                        <button id="btn-send-reg" class="ladda-button btn btn-success text-uppercase"
                                type="button" data-style="zoom-in">
                            Зарегистрироваться
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- RECOVER PASSWORD MODAL WINDOW: BEGIN -->
<div id="recover-password-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Восстановление пароля</h4>
            </div>
            <form id="recover" method="post">
                <div id="recover-alert" class="alert" style="display: none;">
                    <span id="recover-alert-message" class="error-message"></span>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <input class="form-control" type="text" id="recover-login"
                                   placeholder="Логин или эл. почта"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-recover-password" class="ladda-button btn btn-success" type="button"
                            data-style="zoom-in">Отправить
                    </button>
                    <button id="btn-recover-close" data-dismiss="modal" class="btn btn-primary" type="button"
                            style="display: none;">Закрыть
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- RECOVER PASSWORD MODAL WINDOW: END -->

<script>
    $(document).ready(function () {
        $('#remember-me').iCheck({checkboxClass: 'icheckbox_square-green'});
        $('#ch-subscribe').iCheck({checkboxClass: 'icheckbox_square-green'});
        $('#ch-privacy').iCheck({checkboxClass: 'icheckbox_square-green'});

        $('#auth-panel').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                $('#btn-send-auth').trigger('click');
                e.preventDefault();
                return false;
            }
        });

        // отправка запроса на авторизацию
        var btnSendAuth = $('#btn-send-auth').ladda();
        btnSendAuth.click(function () {
            var data = {
                login: $('#auth-login').val(),
                password: $('#auth-password').val()
            };

            if (data.login == null || data.login.trim().length == 0) {
                showPanelAlert('auth-panel', 'Необходимо указать логин или зарегистрированный адрес электронной почты.', 'hgreen', 3000);
                $('#auth-login').focus();
                return;
            }

            if (data.password == null || data.password.length == 0) {
                showPanelAlert('auth-panel', 'Для авторизации необходимо указать свой пароль.', 'hgreen', 3000);
                $('#auth-password').focus();
                return;
            }

            // теперь можем запросить авторизацию
            btnSendAuth.ladda('start');
            $.post('/security/auth', data, function (result) {
                btnSendAuth.ladda('stop');
                if (!result) {
                    showPanelAlert('auth-panel', 'Не удалось авторизоваться. Неизвестная ошибка.', 'hgreen', 5000);
                    return;
                }

                if (result.code != 0) {
                    if (result.body != null && result.body.length != 0) {
                        showPanelAlert('auth-panel', result.body.body, 'hgreen', 5000);
                    } else {
                        showPanelAlert('auth-panel', 'Не удалось авторизоваться. Неизвестная ошибка. Код: ' + result.code, 'hgreen', 5000);
                    }
                } else {
                    // fbq('track', 'Auth');

                    var pathname = window.location.pathname;
                    if (pathname.indexOf('lawyer') != -1 || pathname.indexOf('companies') != -1) {
                        window.location.reload();
                    } else {
                        window.location = '/panel';
                    }
                }
            }).error(function () {
                btnSendAuth.ladda('stop');
            });
        });

        // отправка запроса на регистрацию
        var btnSendReg = $('#btn-send-reg').ladda();
        btnSendReg.click(function () {
            var data = {
                login: $('#reg-email1').val(),
                email: $('#reg-email1').val(),
                password: $('#reg-password1').val(),
                subscribe: $('#ch-subscribe').is(':checked'),
                privacy: $('#ch-privacy').is(':checked')
            };

            if (data.email == null || data.email.trim().length == 0) {
                showPanelAlert('reg-panel', 'Необходимо указать электронную почту.', 'hblue', 3000);
                $('#reg-email1').focus();
                return;
            }
            var email2 = $('#reg-email2').val();
            if (email2 == null || email2.trim().length == 0 || data.email.trim() != email2.trim()) {
                showPanelAlert('reg-panel', 'Электронные почты не совпадают.', 'hblue', 3000);
                $('#reg-email2').focus();
                return;
            }

            if (data.password == null || data.password.length < 6) {
                showPanelAlert('reg-panel', 'Необходимо указать пароль не менее 6 символов.', 'hblue', 3000);
                $('#reg-email1').focus();
                return;
            }
            var password2 = $('#reg-password2').val();
            if (password2 == null || password2.length == 0 || data.password != password2) {
                showPanelAlert('reg-panel', 'Пароли не совпадают.', 'hblue', 3000);
                $('#reg-password2').focus();
                return;
            }

            if (!data.privacy) {
                showPanelAlert('reg-panel', 'Перед регистрацией необходимо прочитать и согласиться с политикой безопасности.',
                    'hblue', 3000);
                return;
            }

            // теперь можем запросить регистрацию
            btnSendReg.ladda('start');
            $.post('/security/registration', data, function (result) {
                btnSendReg.ladda('stop');
                if (!result) {
                    showPanelAlert('reg-panel', 'Не удалось зарегистрировать. Неизвестная ошибка.', 'hblue', 5000);
                    return;
                }

                if (result.code != 0) {
                    if (result.body != null && result.body.length != 0) {
                        showPanelAlert('reg-panel', result.body.body, 'hblue', 5000);
                    } else {
                        showPanelAlert('reg-panel', 'Не удалось зарегистрировать. Неизвестная ошибка. Код: ' + result.code, 'hblue', 5000);
                    }
                } else {
                    // регистрируем событие, что пользователь зарегистрировался
                    // fbq('track', 'CompleteRegistration');

                    var pathname = window.location.pathname;
                    if (pathname.indexOf('lawyer') != -1 || pathname.indexOf('companies') != -1) {
                        window.location.reload();
                    } else {
                        window.location = '/panel';
                    }
                }
            }).error(function () {
                btnSendReg.ladda('stop');
            });
        });

        // восстановление пароля
        var btnRecoverPassword = $('#btn-recover-password').ladda();
        btnRecoverPassword.click(function () {
            var data = {
                login: $('#recover-login').val()
            };

            if (data.login == null || data.login.trim().length == 0) {
                showPanelAlert('recover', 'Необходимо указать логин или адрес эл. почты', null, 3000);
                $('#recover-login').focus();
                return;
            }

            // теперь можно отправить запрос на восстановление
            btnRecoverPassword.ladda('start');
            $.post('/security/recover', data, function (result) {
                btnRecoverPassword.ladda('stop');
                if (!result) {
                    showPanelAlert('recover', 'Неизвестная ошибка.', null, 5000);
                    return;
                }

                if (result.code != 0) {
                    if (result.body != null && result.body.length != 0) {
                        showPanelAlert('recover', result.body.body, null, 5000);
                    } else {
                        showPanelAlert('recover', 'Неизвестная ошибка. Код: ' + result.code, null, 5000);
                    }
                } else {
                    showPanelAlert('recover', 'Сообщение с кодом восстановления пароля успешно отправлено', null, 5000, 'alert-info');

                    // очищаем поля
                    $('#recover-login').val('');
                    $('#btn-recover-password').hide();
                    $('#btn-recover-close').show();
                }
            }).error(function () {
                btnRecoverPassword.ladda('stop');
            });
        });

        $('#btn-recover-close').click(function () {
            $(this).hide();
            $('#btn-recover-password').show();
        });
    });
</script>