package ru.dom24x7.site.panel.meter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.dom24x7.model.account.Account;
import ru.dom24x7.model.account.dao.AccountDAOImpl;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.Email;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 25.09.16.
 *
 * Контроллер для работы с привязанными компаниями к счетчикам
 */
@Controller
public class MeterCompanyController extends BaseController {

    @RequestMapping(value = "/panel/addresses/{addressId}/meters/{meterId}/company/bind", method = RequestMethod.POST)
    public String companyBindAction(
            @PathVariable int addressId, @PathVariable int meterId,
            @RequestParam(value = "company", required = false) Integer companyId,
            @RequestParam(value = "account", required = false) String accountNumber,
            @RequestParam(value = "meters", required = false) List<Integer> meters) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            security.getMessages().add(ERROR_MESSAGES.get("ERROR_02"));
            return "redirect:/panel";
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            // счетчик не найден по указанному адресу
            security.getMessages().add(ERROR_MESSAGES.get("ERROR_03"));
            return "redirect:/panel/addresses/" + addressId;
        }

        String redirect = "redirect:/panel/addresses/" + addressId + "/meters/" + meterId;

        if (companyId != null && companyId > 0) {
            Company company = new CompanyDAOImpl().get(companyId);
            if (company == null) {
                security.getMessages().add("Ошибка при поиске компании. Привязка не удалась.", Message.TYPE_DANGER);

                return redirect;
            }

            // предварительно удаляем лицевой счет пользователя по компании
            new AccountDAOImpl().delete(security.getUser(), company);
            if (accountNumber != null && accountNumber.trim().length() != 0) {
                // указан лицевой счет и его нужно сохранить
                Account account = new Account();
                account.setUser(security.getUser());
                account.setCompany(company);
                account.setAccount(accountNumber.trim());
                new AccountDAOImpl().save(account);
            }

            // в начале от всех счетчиков отвязываем указанную компанию
            for (Meter unbindMeter : new MeterDAOImpl().list(address)) {
                if (unbindMeter.getCompany() != null && unbindMeter.getCompany().getId() == companyId) {
                    unbindMeter.setCompany(null);
                    new MeterDAOImpl().save(unbindMeter, unbindMeter.getCompany());
                }
            }

            // теперь привязываем компанию к новому списку счетчиков
            if (meters != null && meters.size() != 0) {
                for (Integer bindMeterId : meters) {
                    new MeterDAOImpl().save(new Meter(bindMeterId), company);
                }
            }

            security.getMessages().add("Успешно сменили список привязанных счетчиков к компании и обновили лицевой счет", Message.TYPE_SUCCESS);
        }

        return redirect;
    }

    @RequestMapping(value = "/ajax/company/{companyId}/report/send", method = RequestMethod.POST)
    public @ResponseBody Response sendResportAjax(
            @PathVariable int companyId,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "message") String message) {

        List<Message> messages = new ArrayList<Message>();

        Company company = new CompanyDAOImpl().get(companyId);
        if (company == null) {
            messages.add(new Message("Не удалось найти компанию", Message.TYPE_DANGER));
            return new Response(-1, "ERROR", messages);
        }

        if (email == null || email.trim().length() == 0) {
            messages.add(new Message("Необходимо указать адрес электронной почты, на которую будут отправлены показания", Message.TYPE_DANGER));
            return new Response(-1, "ERROR", messages);
        }

        Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z", servletContext.getRealPath("/resources"));
        emailSender.send("Показания счетчиков от пользователя Dom24x7.ru", message, email);

        messages.add(new Message("Показания счетчиков успешно отправлены", Message.TYPE_SUCCESS));
        return new Response(0, "OK", messages);
    }
}