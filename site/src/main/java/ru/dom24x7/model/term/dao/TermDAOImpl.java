package ru.dom24x7.model.term.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.term.Term;
import ru.dom24x7.model.term.dao.interfaces.TermDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса для работы со словарем
 */
public class TermDAOImpl extends ModelDAOImpl implements TermDAO {

    private static final String QUERY_LIST = "{ call terms_get_new_list(?) }";
    private static final String QUERY_LETTERS = "{ call terms_get_letters_list() }";
    private static final String QUERY_LIST_WITH_FILTER = "{ call terms_list(?) }";

    @Override
    public List<Term> list(int count) {

        List<Term> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Term>() {

            @Override
            public Term mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, count);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<String> letters() {

        List<String> list = jdbcTemplate.query(QUERY_LETTERS, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getString("letter");
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Term> list(String filter) {

        if (filter == null || filter.trim().length() == 0) {
            filter = null;
        } else {
            filter = filter + "%";
        }

        List<Term> list = jdbcTemplate.query(QUERY_LIST_WITH_FILTER, new RowMapper<Term>() {

            @Override
            public Term mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, filter);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private Term load(ResultSet rs) throws SQLException {

        Term term = new Term(rs.getInt("id"));
        term.setTerm(rs.getString("term"));
        term.setBody(rs.getString("body"));

        return term;
    }
}