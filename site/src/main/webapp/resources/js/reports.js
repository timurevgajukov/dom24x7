/**
 * Created by timur on 28.03.17.
 * Класс для работы с отчетами
 */
function Dom24x7Report() {
    this.dataR1 = {};
    this.dataR2 = {};
}

// отображение отчета: Доли расходов по счетчикам
Dom24x7Report.prototype.showReportPie = function (container, data) {
    // запрашиваем новые данные только если сменился фильтр
    if (this.dataR1['address'] == data['address'] && this.dataR1['date-from'] == data['date-from']
        && this.dataR1['date-to'] == data['date-to'] && this.dataR1['by-meter-type'] == data['by-meter-type']) {
        return;
    }

    this.dataR1 = data;

    $.post('/ajax/reports/pie/get', this.dataR1, function (result) {
        container.empty();

        if (result === undefined || result == null) {
            return;
        }

        if (result.code != 0) {
            return;
        }

        var reportOptions = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Доли расходов по счетчикам'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Доля счетчика',
                colorByPoint: true,
                data: result.body
            }]
        };
        container.highcharts(reportOptions);
    });
};

// отображение отчета: История расходов по счетчику
Dom24x7Report.prototype.showReportColumn = function (container, data) {
    // запрашиваем новые данные только если сменился фильтр
    if (this.dataR2['address'] == data['address'] && this.dataR2['meter'] == data['meter']
        && this.dataR2['year'] == data['year'] && this.dataR2['by-cost'] == data['by-cost']) {
        return;
    }

    this.dataR2 = data;

    if (this.dataR2.address == null || this.dataR2.meter == null || this.dataR2.year == null) {
        // не все обязательные данные заполнены
        return;
    }

    $.post('/ajax/reports/column/get', this.dataR2, function (result) {
        container.empty();

        if (result === undefined || result == null) {
            return;
        }

        if (result.code != 0) {
            return;
        }

        var reportOptions = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'История расходов по счетчику'
            },
            xAxis: {
                categories: [
                    'Янв',
                    'Фев',
                    'Мар',
                    'Апр',
                    'Май',
                    'Июн',
                    'Июл',
                    'Авг',
                    'Сен',
                    'Окт',
                    'Ноя',
                    'Дек'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: result.body.title
                }
            },
            tooltip: {
                pointFormat: result.body.pointFormat,
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: result.body.series
        };
        container.highcharts(reportOptions);
    });
};