package ru.dom24x7.model.lawyer.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.lawyer.LawyerMessage;
import ru.dom24x7.model.lawyer.dao.interfaces.LawyerMessageDAO;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 * <p>
 * Реализация интерфейса для работы с сообщениями для юристов
 */
public class LawyerMessageDAOImpl extends ModelDAOImpl implements LawyerMessageDAO {

    private static final String QUERY_GET_BY_ID = "{ call lawyer_messages_get(?,?) }";
    private static final String QUERY_LIST = "{ call lawyer_messages_list(?,?,?) }";
    private static final String QUERY_LIST_BY_USER = "{ call lawyer_messages_list_by_user(?,?,?,?) }";
    private static final String QUERY_COUNT = "{ call lawyer_messages_count(?) }";
    private static final String QUERY_SAVE = "{ call lawyer_messages_save(?,?,?) }";

    @Override
    public LawyerMessage get(int id, final User user) {

        Integer userId = null;
        if (user != null) {
            userId = user.getId();
        }

        List<LawyerMessage> list = jdbcTemplate.query(QUERY_GET_BY_ID, new Object[]{id, userId},
                new RowMapper<LawyerMessage>() {
                    @Override
                    public LawyerMessage mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, user);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<LawyerMessage> list(int offset, int count, String search) {

        if (search != null && search.trim().length() != 0) {
            search = "%" + search.trim() + "%";
        } else {
            search = null;
        }

        List<LawyerMessage> list = jdbcTemplate.query(QUERY_LIST, new Object[]{offset, count, search},
                new RowMapper<LawyerMessage>() {

                    @Override
                    public LawyerMessage mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, null);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<LawyerMessage> list(final User user, int offset, int count, String search) {

        if (search != null && search.trim().length() != 0) {
            search = "%" + search.trim() + "%";
        } else {
            search = null;
        }

        List<LawyerMessage> list = jdbcTemplate.query(QUERY_LIST_BY_USER,
                new Object[]{user.getId(), offset, count, search}, new RowMapper<LawyerMessage>() {

                    @Override
                    public LawyerMessage mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, user);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public int count(String search) {

        if (search != null && search.trim().length() != 0) {
            search = "%" + search.trim() + "%";
        } else {
            search = null;
        }

        return jdbcTemplate.queryForObject(QUERY_COUNT, new Object[]{search}, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        });
    }

    @Override
    public boolean save(LawyerMessage message) {

        LawyerMessage newMessage = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        message.getId() == 0 ? null : message.getId(),
                        message.getUser().getId(),
                        message.getText()
                }, new RowMapper<LawyerMessage>() {

                    @Override
                    public LawyerMessage mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, null);
                    }
                });

        message.setId(newMessage.getId());

        return true;
    }

    private LawyerMessage load(ResultSet rs, User user) throws SQLException {

        LawyerMessage message = new LawyerMessage(rs.getInt("id"));
        message.setCreateDt(rs.getTimestamp("create_dt"));
        message.setText(rs.getString("text"));

        if (user == null) {
            user = new UserDAOImpl().get(rs.getInt("user_id"));
        }
        message.setUser(user);

        return message;
    }
}