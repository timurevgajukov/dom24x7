package ru.evgajukov.profi.profile.response.cloud;

/**
 * Created by timur on 17.06.17.
 */
public class Price {

    private int percentage;
    private int count;
    private int from;
    private int to;
    private Histogram histogram;

    public int getPercentage() {

        return percentage;
    }

    public void setPercentage(int percentage) {

        this.percentage = percentage;
    }

    public int getCount() {

        return count;
    }

    public void setCount(int count) {

        this.count = count;
    }

    public int getFrom() {

        return from;
    }

    public void setFrom(int from) {

        this.from = from;
    }

    public int getTo() {

        return to;
    }

    public void setTo(int to) {

        this.to = to;
    }

    public Histogram getHistogram() {

        return histogram;
    }

    public void setHistogram(Histogram histogram) {

        this.histogram = histogram;
    }
}
