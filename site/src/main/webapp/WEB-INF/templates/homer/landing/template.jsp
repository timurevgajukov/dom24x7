<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="google-site-verification" content="b_kHuPP2Obc8uFzO2yld2k5CebcB09DCBQBysHOmh7I" />

    <!-- Page title -->
    <title>${title} | Dom24x7</title>

    <meta name="description" content="${description}"/>
    <meta name="keywords" content="${keywords}"/>

    <c:if test="${og != null}">
        <meta property="og:title" content="${og.title}"/>
        <meta property="og:description" content="${og.description}"/>
        <meta property="og:type" content="${og.type}"/>
        <meta property="og:url" content="${og.url}"/>
        <c:if test="${og.image != null}"><meta property="og:image" content="${og.image}"/></c:if>
    </c:if>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="/resources/images/favicon.ico" />

    <t:insertAttribute name="header-css-js-links" defaultValue="/WEB-INF/templates/homer/landing/includes/header-css-js-links.jsp"/>
    <t:insertAttribute name="vk-pixel" defaultValue="/WEB-INF/templates/homer/all/vk-pixel.jsp"/>

</head>
<body class="landing-page">

<!-- Simple splash screen-->
<t:insertAttribute name="preloader" defaultValue="/WEB-INF/templates/homer/all/preloader.jsp"/>

<t:insertAttribute name="menu" defaultValue="/WEB-INF/templates/homer/landing/includes/menu.jsp"/>

<t:insertAttribute name="content"/>

<t:insertAttribute name="footer-js-links" defaultValue="/WEB-INF/templates/homer/landing/includes/footer-js-links.jsp"/>

<t:insertAttribute name="counters" defaultValue="/WEB-INF/templates/homer/all/counters.jsp"/>

<!-- Local script for menu handle -->
<!-- It can be also directive -->
<script>
    $(document).ready(function () {

        // Page scrolling feature
        $('a.page-scroll').bind('click', function(event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
        });

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 80
        });

    });
</script>

</body>
</html>