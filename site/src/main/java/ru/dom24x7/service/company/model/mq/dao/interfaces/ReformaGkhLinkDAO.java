package ru.dom24x7.service.company.model.mq.dao.interfaces;

import ru.dom24x7.service.company.model.mq.ReformaGkhLink;

/**
 * Created by timur on 08.04.17.
 * Интерфейс для работы с очередью ссылок для парсинга сайта "Реформа ЖКХ"
 */
public interface ReformaGkhLinkDAO {

    /**
     * Возвращает ссылку из очереди
     * @return
     */
    ReformaGkhLink get();

    /**
     * Сохраняет ссылку в очередь
     * @param reformaGkhLink
     * @return
     */
    boolean save(ReformaGkhLink reformaGkhLink);

    /**
     * Удаляет ссылку из очереди
     * @param reformaGkhLink
     * @return
     */
    boolean delete(ReformaGkhLink reformaGkhLink);
}