package ru.dom24x7.model.question;

import ru.dom24x7.model.Model;

/**
 * Вопрос с ответом
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Question extends Model {

	private String question;
	private String answer;
	
	public Question() {
		
		super();
	}
	
	public Question(int id) {
		
		super(id);
	}
	
	public String getQuestion() {
		
		return question;
	}
	
	public void setQuestion(String question) {
		
		this.question = question;
	}
	
	public String getAnswer() {
		
		return answer;
	}
	
	public void setAnswer(String answer) {
		
		this.answer = answer;
	}
	
	@Override
	public String toString() {
		
		return question;
	}
}
