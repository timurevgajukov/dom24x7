<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/resources/themes/homer/vendor/codemirror/style/codemirror.css" />

<style type="text/css">
    .CodeMirror {
        border: 1px solid #eee;
        height: auto;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel blog-article-box">
            <div class="panel-heading">
                <h4>${title}</h4>
            </div>
            <div class="panel-body">
                <p>HTTP метод запроса: <strong>POST</strong></p>
                <p>Формат запроса / ответа: <strong>JSON</strong></p>
                <h4>Общая структура запроса</h4>
                <textarea id="request-code" class="form-control"></textarea>
                <p>В <strong>body</strong> расположены поля с данными запроса (записит от метода).</p>
                <h4>Общая структура ответа</h4>
                <textarea id="response-code" class="form-control"></textarea>
                <p>В <strong>body</strong> расположены поля с данными ответа (записит от метода),
                    либо массив с описанием ошибок.</p>
                <h2>Команды</h2>
                <ol>
                    <li>
                        <strong>Пользователи</strong>
                        <ol>
                            <li><a href="#auth">Авторизация</a></li>
                            <li><a href="#reg">Регистрация</a></li>
                            <li><a href="#password/change">Смена пароля</a></li>
                        </ol>
                    </li>
                    <li>
                        <strong>Адреса</strong>
                        <ol>
                            <li><a href="#address/list">Полечение списка адресов</a></li>
                            <li><a href="#address/save">Добавление / Редактирование адреса</a></li>
                            <li><a href="#address/delete">Удаление адреса</a></li>
                        </ol>
                    </li>
                    <li>
                        <strong>Счетчики</strong>
                        <ol>
                            <li><a href="#meter/list">Список счетчиков по адресу</a></li>
                            <li><a href="#meter/save">Добавление / Редактирование счетчика</a></li>
                            <li><a href="#meter/delete">Удаление счетчика</a></li>
                        </ol>
                    </li>
                    <li>
                        <strong>История показаний</strong>
                        <ol>
                            <li><a href="#meter/history/list">Список показаний по счетчику</a></li>
                            <li><a href="#meter/history/save">Добавление / Редактирование показания счетчика</a></li>
                            <li><a href="#meter/history/delete">Удаление показания счетчика</a></li>
                        </ol>
                    </li>
                    <li>
                        <strong>Справочники</strong>
                        <ol>
                            <li><a href="#currency/list">Список доступных валют</a></li>
                            <li><a href="#meter/type/list">Список доступных типов счетчиков</a></li>
                        </ol>
                    </li>
                </ol>
                <h2>Описание</h2>
                <h3><a name="auth"></a>Авторизация пользователя</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-auth" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-auth" class="form-control"></textarea>
                <h3><a name="reg"></a>Регистрация пользователя</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-reg" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-reg" class="form-control"></textarea>
                <h3><a name="password/change"></a>Смена пароля пользователя</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-password-change" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-password-change" class="form-control"></textarea>
                <h3><a name="address/list"></a>Получение списка адресов</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-address-list" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-address-list" class="form-control"></textarea>
                <h3><a name="address/save"></a>Добавление / Редактирование адреса</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-address-save" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-address-save" class="form-control"></textarea>
                <h3><a name="address/delete"></a>Удаление адреса</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-address-delete" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-address-delete" class="form-control"></textarea>
                <h3><a name="meter/list"></a>Список счетчиков по адресу</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-meter-list" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-meter-list" class="form-control"></textarea>
                <h3><a name="meter/save"></a>Добавление / Редактирование счетчика</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-meter-save" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-meter-save" class="form-control"></textarea>
                <h3><a name="meter/delete"></a>Удаление счетчика</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-meter-delete" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-meter-delete" class="form-control"></textarea>
                <h3><a name="meter/history/list"></a>Список показаний по счетчику</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-meter-history-list" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-meter-history-list" class="form-control"></textarea>
                <h3><a name="meter/history/save"></a>Добавление / Редактирование показания счетчика</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-meter-history-save" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-meter-history-save" class="form-control"></textarea>
                <h3><a name="meter/history/delete"></a>Удаление показания счетчика</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-meter-history-delete" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-meter-history-delete" class="form-control"></textarea>
                <h3><a name="currency/list"></a>Список валют</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-currency-list" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-currency-list" class="form-control"></textarea>
                <h3><a name="meter/type/list"></a>Список типов счетчика</h3>
                <h4>Запрос</h4>
                <textarea id="request-code-meter-type-list" class="form-control"></textarea>
                <h4>Ответ</h4>
                <textarea id="response-code-meter-type-list" class="form-control"></textarea>
            </div>
        </div>
    </div>
</div>

<script src="/resources/themes/homer/vendor/codemirror/script/codemirror.js"></script>
<script src="/resources/themes/homer/vendor/codemirror/javascript.js"></script>

<script>
    $(document).ready(function () {
        var requestCode = $('#request-code');
        var requestCodeJSON = {
            header: {
                method: 'название метода',
                token: 'токен приложения, выдается администратором сервиса по запросу',
                session: 'сессионный ключ пользователя, либо null при авторизации или регистрации пользователя'
            },
            body: {}
        };
        requestCode.val(JSON.stringify(requestCodeJSON, null, '\t'));

        var responseCode = $('#response-code');
        var responseCodeJSON = {
            header: {
                code: 'код ответа, если нет ошибок, то 0, иначе отрицательное число',
                description: 'OK - нет ошибок, ERROR - есть ошибки, которые описаны в body',
                method: 'название метода',
                token: 'токен приложения, выдается администратором сервиса по запросу',
                session: 'сессионный ключ пользователя, либо null при авторизации или регистрации пользователя'
            },
            body: {}
        };
        responseCode.val(JSON.stringify(responseCodeJSON, null, '\t'));

        var requestCodeAuth = $('#request-code-auth');
        var requestCodeAuthJSON = {
            header: {
                method: 'auth',
                token: 'токен приложения',
                session: null
            },
            body: {
                login: 'логин или электронная почта пользователя',
                password: 'пароль пользователя'
            }
        };
        requestCodeAuth.val(JSON.stringify(requestCodeAuthJSON, null, '\t'));

        var responseCodeAuth = $('#response-code-auth');
        var responseCodeAuthJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'auth',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeAuth.val(JSON.stringify(responseCodeAuthJSON, null, '\t'));

        var requestCodeReg = $('#request-code-reg');
        var requestCodeRegJSON = {
            header: {
                method: 'reg',
                token: 'токен приложения',
                session: null
            },
            body: {
                login: 'логин пользователя',
                email: 'электронная почта пользователя',
                password: 'пароль пользователя'
            }
        };
        requestCodeReg.val(JSON.stringify(requestCodeRegJSON, null, '\t'));

        var responseCodeReg = $('#response-code-reg');
        var responseCodeRegJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'reg',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeReg.val(JSON.stringify(responseCodeRegJSON, null, '\t'));

        var requestCodePasswordChange = $('#request-code-password-change');
        var requestCodePasswordChangeJSON = {
            header: {
                method: 'password/change',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                password: 'текущий пароль',
                'password-new': 'новый пароль'
            }
        };
        requestCodePasswordChange.val(JSON.stringify(requestCodePasswordChangeJSON, null, '\t'));

        var responseCodePasswordChange = $('#response-code-password-change');
        var responseCodePasswordChangeJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'password/change',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodePasswordChange.val(JSON.stringify(responseCodePasswordChangeJSON, null, '\t'));

        var requestCodeAddressList = $('#request-code-address-list');
        var requestCodeAddressListJSON = {
            header: {
                method: 'address/list',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {}
        };
        requestCodeAddressList.val(JSON.stringify(requestCodeAddressListJSON, null, '\t'));

        var responseCodeAddressList = $('#response-code-address-list');
        var responseCodeAddressListJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'address/list',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: [
                {
                    id: 'идентификатор адреса',
                    title: 'наименование адреса или null',
                    address: 'строка с самим адресом',
                    currency: {
                        id: 'индектификатор валюты',
                        symbol: 'символьное отображение валюты',
                        code: 'код валюты'
                    },
                    info: 'структура адреса в формате сервиса dadata.ru или null'
                }
            ]
        };
        responseCodeAddressList.val(JSON.stringify(responseCodeAddressListJSON, null, '\t'));

        var requestCodeAddressSave = $('#request-code-address-save');
        var requestCodeAddressSaveJSON = {
            header: {
                method: 'address/save',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                id: 'идентификатор адреса или null для нового',
                title: 'Наименование адреса или null',
                address: 'Непосредственно сам адрес',
                currencyId: 'Идентификатор валюты',
                info: 'структура в формате сервиса dadata.ru или null'
            }
        };
        requestCodeAddressSave.val(JSON.stringify(requestCodeAddressSaveJSON, null, '\t'));

        var responseCodeAddressSave = $('#response-code-address-save');
        var responseCodeAddressSaveJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'address/save',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeAddressSave.val(JSON.stringify(responseCodeAddressSaveJSON, null, '\t'));

        var requestCodeAddressDelete = $('#request-code-address-delete');
        var requestCodeAddressDeleteJSON = {
            header: {
                method: 'address/delete',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                id: 'идентификатор адреса'
            }
        };
        requestCodeAddressDelete.val(JSON.stringify(requestCodeAddressDeleteJSON, null, '\t'));

        var responseCodeAddressDelete = $('#response-code-address-delete');
        var responseCodeAddressDeleteJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'address/delete',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeAddressDelete.val(JSON.stringify(responseCodeAddressDeleteJSON, null, '\t'));

        var requestCodeMeterList = $('#request-code-meter-list');
        var requestCodeMeterListJSON = {
            header: {
                method: 'meter/list',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                addressId: 'идентификатор адреса'
            }
        };
        requestCodeMeterList.val(JSON.stringify(requestCodeMeterListJSON, null, '\t'));

        var responseCodeMeterList = $('#response-code-meter-list');
        var responseCodeMeterListJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'meter/list',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: [
                {
                    id: 'идентификатор счетчика',
                    title: 'наименование счетчика',
                    type: {
                        id: 'идентификатор типа счетчика',
                        name: 'наименование типа счетчика',
                        unit: 'единица измерения счетчика',
                        note: 'описание типа счетчика',
                        code: 'код типа счетчика'
                    },
                    tariff: 'тариф',
                    capacity: 'разрядность',
                    precision: 'точность',
                    icon: {
                        id: 'идентификатор иконки счетчика',
                        name: 'наименование файла счетчика'
                    }
                }
            ]
        };
        responseCodeMeterList.val(JSON.stringify(responseCodeMeterListJSON, null, '\t'));

        var requestCodeMeterSave = $('#request-code-meter-save');
        var requestCodeMeterSaveJSON = {
            header: {
                method: 'meter/save',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                id: 'идентификатор счетчика или null для нового',
                addressId: 'идентификатор адреса, в которому привязан счетчик',
                title: 'наименование счетчика',
                typeId: 'идентификатор типа счетчика',
                tariff: 'тариф',
                capacity: 'разрядность или null, если не указано',
                precision: 'точность или null, если не указано'
            }
        };
        requestCodeMeterSave.val(JSON.stringify(requestCodeMeterSaveJSON, null, '\t'));

        var responseCodeMeterSave = $('#response-code-meter-save');
        var responseCodeMeterSaveJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'meter/save',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeMeterSave.val(JSON.stringify(responseCodeMeterSaveJSON, null, '\t'));

        var requestCodeMeterDelete = $('#request-code-meter-delete');
        var requestCodeMeterDeleteJSON = {
            header: {
                method: 'meter/delete',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                id: 'идентификатор счетчика',
                addressId: 'идентификатор адреса'
            }
        };
        requestCodeMeterDelete.val(JSON.stringify(requestCodeMeterDeleteJSON, null, '\t'));

        var responseCodeMeterDelete = $('#response-code-meter-delete');
        var responseCodeMeterDeleteJSON =  {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'meter/delete',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeMeterDelete.val(JSON.stringify(responseCodeMeterDeleteJSON, null, '\t'));

        var requestCodeMeterHistoryList = $('#request-code-meter-history-list');
        var requestCodeMeterHistoryListJSON = {
            header: {
                method: 'meter/history/list',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                meterId: 'идентификатор счетчика',
                addressId: 'идентификатор адреса'
            }
        };
        requestCodeMeterHistoryList.val(JSON.stringify(requestCodeMeterHistoryListJSON, null, '\t'));

        var responseCodeMeterHistoryList = $('#response-code-meter-history-list');
        var responseCodeMeterHistoryListJSON =  {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'meter/history/list',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: [
                {
                    id: 'идентификатор истории показания счетчика',
                    date: 'дата снятия показания',
                    value: 'показание счетчика'
                }
            ]
        };
        responseCodeMeterHistoryList.val(JSON.stringify(responseCodeMeterHistoryListJSON, null, '\t'));

        var requestCodeMeterHistorySave = $('#request-code-meter-history-save');
        var requestCodeMeterHistorySaveJSON =  {
            header: {
                method: 'meter/history/save',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                id: 'идентификатор показания счетчика или null, если новое показание',
                meterId: 'идентификатор счетчика',
                addressId: 'идентификатор адреса',
                date: 'дата снятия показания счетчика',
                value: 'показание счетчика'
            }
        };
        requestCodeMeterHistorySave.val(JSON.stringify(requestCodeMeterHistorySaveJSON, null, '\t'));

        var responseCodeMeterHistorySave = $('#response-code-meter-history-save');
        var responseCodeMeterHistorySaveJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'meter/history/save',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeMeterHistorySave.val(JSON.stringify(responseCodeMeterHistorySaveJSON, null, '\t'));

        var requestCodeMeterHistoryDelete = $('#request-code-meter-history-delete');
        var requestCodeMeterHistoryDeleteJSON = {
            header: {
                method: 'meter/history/delete',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {
                id: 'идентификатор показания счетчика',
            }
        };
        requestCodeMeterHistoryDelete.val(JSON.stringify(requestCodeMeterHistoryDeleteJSON, null, '\t'));

        var responseCodeMeterHistoryDelete = $('#response-code-meter-history-delete');
        var responseCodeMeterHistoryDeleteJSON =  {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'meter/history/delete',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: {}
        };
        responseCodeMeterHistoryDelete.val(JSON.stringify(responseCodeMeterHistoryDeleteJSON, null, '\t'));

        var requestCodeCurrencyList = $('#request-code-currency-list');
        var requestCodeCurrencyListJSON = {
            header: {
                method: 'currency/list',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {}
        };
        requestCodeCurrencyList.val(JSON.stringify(requestCodeCurrencyListJSON, null, '\t'));

        var responseCodeCurrencyList = $('#response-code-currency-list');
        var responseCodeCurrencyListJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'currency/list',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: [
                {
                    id: 'индектификатор валюты',
                    symbol: 'символьное отображение валюты',
                    code: 'код валюты'
                }
            ]
        };
        responseCodeCurrencyList.val(JSON.stringify(responseCodeCurrencyListJSON, null, '\t'));

        var requestCodeMeterTypeList = $('#request-code-meter-type-list');
        var requestCodeMeterTypeListJSON = {
            header: {
                method: 'meter/type/list',
                token: 'токен приложения',
                session: 'код сессии пользователя'
            },
            body: {}
        };
        requestCodeMeterTypeList.val(JSON.stringify(requestCodeMeterTypeListJSON, null, '\t'));

        var responseCodeMeterTypeList = $('#response-code-meter-type-list');
        var responseCodeMeterTypeListJSON = {
            header: {
                code: 'код ответа',
                description: 'OK или ERROR',
                method: 'currency/list',
                token: 'токен приложения',
                session: 'сессионный ключ пользователя'
            },
            body: [
                {
                    id: 'идентификатор типа счетчика',
                    name: 'наименование типа счетчика',
                    unit: 'единица измерения счетчика',
                    note: 'описание типа счетчика',
                    code: 'код типа счетчика'
                }
            ]
        };
        responseCodeMeterTypeList.val(JSON.stringify(responseCodeMeterTypeListJSON, null, '\t'));

        // Wait until animation finished render container
        setTimeout(function(){

            var options = {
                lineNumbers: true,
                matchBrackets: true,
                styleActiveLine: true,
                readOnly: true
            };

            CodeMirror.fromTextArea(requestCode[0], options);
            CodeMirror.fromTextArea(responseCode[0], options);

            CodeMirror.fromTextArea(requestCodeAuth[0], options);
            CodeMirror.fromTextArea(responseCodeAuth[0], options);

            CodeMirror.fromTextArea(requestCodeReg[0], options);
            CodeMirror.fromTextArea(responseCodeReg[0], options);

            CodeMirror.fromTextArea(requestCodePasswordChange[0], options);
            CodeMirror.fromTextArea(responseCodePasswordChange[0], options);

            CodeMirror.fromTextArea(requestCodeAddressList[0], options);
            CodeMirror.fromTextArea(responseCodeAddressList[0], options);

            CodeMirror.fromTextArea(requestCodeAddressSave[0], options);
            CodeMirror.fromTextArea(responseCodeAddressSave[0], options);

            CodeMirror.fromTextArea(requestCodeAddressDelete[0], options);
            CodeMirror.fromTextArea(responseCodeAddressDelete[0], options);

            CodeMirror.fromTextArea(requestCodeMeterList[0], options);
            CodeMirror.fromTextArea(responseCodeMeterList[0], options);

            CodeMirror.fromTextArea(requestCodeMeterSave[0], options);
            CodeMirror.fromTextArea(responseCodeMeterSave[0], options);

            CodeMirror.fromTextArea(requestCodeMeterDelete[0], options);
            CodeMirror.fromTextArea(responseCodeMeterDelete[0], options);

            CodeMirror.fromTextArea(requestCodeMeterHistoryList[0], options);
            CodeMirror.fromTextArea(responseCodeMeterHistoryList[0], options);

            CodeMirror.fromTextArea(requestCodeMeterHistorySave[0], options);
            CodeMirror.fromTextArea(responseCodeMeterHistorySave[0], options);

            CodeMirror.fromTextArea(requestCodeMeterHistoryDelete[0], options);
            CodeMirror.fromTextArea(responseCodeMeterHistoryDelete[0], options);

            CodeMirror.fromTextArea(requestCodeCurrencyList[0], options);
            CodeMirror.fromTextArea(responseCodeCurrencyList[0], options);

            CodeMirror.fromTextArea(requestCodeMeterTypeList[0], options);
            CodeMirror.fromTextArea(responseCodeMeterTypeList[0], options);
        }, 500);

    });
</script>