package ru.dom24x7.utils;

/**
 * Created by timur on 16.09.16.
 *
 * Класс нужен для получения данных по картинкам и их обрезке
 */
public class CropData {

    private double x;
    private double y;
    private double height;
    private double width;
    private double rotate;

    public CropData(double x, double y, double height, double width, double rotate) {

        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.rotate = rotate;
    }

    public double getX() {

        return x;
    }

    public int getIntX() {

        return (int) Math.round(x);
    }

    public void setX(double x) {

        this.x = x;
    }

    public double getY() {

        return y;
    }

    public int getIntY() {

        return (int) Math.round(y);
    }

    public void setY(double y) {

        this.y = y;
    }

    public double getHeight() {

        return height;
    }

    public int getIntHeight() {

        return (int) Math.round(height);
    }

    public void setHeight(double height) {

        this.height = height;
    }

    public double getWidth() {

        return width;
    }

    public int getIntWidth() {

        return (int) Math.round(width);
    }

    public void setWidth(double width) {

        this.width = width;
    }

    public double getRotate() {

        return rotate;
    }

    public void setRotate(double rotate) {

        this.rotate = rotate;
    }
}