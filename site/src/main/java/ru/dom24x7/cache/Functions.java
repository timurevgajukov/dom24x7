package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.access.Function;
import ru.dom24x7.model.access.dao.FunctionDAOImpl;

import java.util.List;

/**
 * Created by timur on 14.02.17.
 *
 * Контейнер для хранения списка функций и разделов сайта
 */
public class Functions implements ICache<Function> {

    private static Functions instance = null;

    private List<Function> list;

    public static Functions getInstance() {

        if (instance == null) {
            instance = new Functions();
        }

        return instance;
    }

    private Functions() {

        list = new FunctionDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Function> list() {

        return list;
    }

    @Override
    public Function get(int id) {

        if (list == null) {
            return null;
        }

        for (Function item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }

    public Function get(String name) {

        if (list == null) {
            return null;
        }

        for (Function item : list) {
            if (name.equalsIgnoreCase(item.getName())) {
                return item;
            }
        }

        return null;
    }
}