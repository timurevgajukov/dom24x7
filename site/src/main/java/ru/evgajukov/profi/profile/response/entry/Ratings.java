package ru.evgajukov.profi.profile.response.entry;

import com.google.gson.annotations.SerializedName;

/**
 * Created by timur on 17.06.17.
 */
public class Ratings {

    @SerializedName("default")
    private int value;

    public int getValue() {

        return value;
    }

    public void setValue(int value) {

        this.value = value;
    }
}