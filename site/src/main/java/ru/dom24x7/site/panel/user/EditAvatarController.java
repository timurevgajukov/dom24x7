package ru.dom24x7.site.panel.user;

import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.utils.CropData;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by timur on 22.09.16.
 *
 * Редактирование аватарки пользователя
 */
@Controller
public class EditAvatarController extends BaseController {

    /**
     * Сохранение собственной картинки в качестве аватарки
     *
     * @param src
     * @param data
     * @param file
     * @return
     */
    @RequestMapping(value = "/user/profile/crop", method = RequestMethod.POST)
    public String avatarCropAction(
            @RequestParam(value = "avatar_src") String src,
            @RequestParam(value = "avatar_data") String data,
            @RequestParam(value = "avatar_file") MultipartFile file) {

        if (!security.isAuth()) {
            return "redirect:/security/auth";
        }

        System.out.println(src);
        System.out.println(data);
        System.out.println("Upload image: " + file.getOriginalFilename());
        System.out.println("Upload image MIME-type: " + file.getContentType());

        CropData cropData = new Gson().fromJson(data, CropData.class);
        if (cropData.getX() < 0) {
            cropData.setX(0.0);
        }
        if (cropData.getY() < 0) {
            cropData.setY(0.0);
        }

        ByteArrayOutputStream baos = null;
        try {
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
            image = image.getSubimage(
                    cropData.getIntX(), cropData.getIntY() < 0 ? 0 : cropData.getIntY(),
                    cropData.getIntWidth(), cropData.getIntHeight());

            baos = new ByteArrayOutputStream();
            ImageIO.write(image, file.getContentType().split("/")[1], baos);
            baos.flush();

            Photo photo = new Photo(file.getOriginalFilename(), baos.toByteArray());

            security.getUser().setPhoto(photo);
            new UserDAOImpl().savePhoto(security.getUser());

            security.getMessages().add("Успешно сменили аватарку", Message.TYPE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {}
            }
        }

        return "redirect:/profile";
    }
}
