<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        <div class="pull-right">
            <button id="btn-address-edit" class="btn btn-default btn-xs" data-toggle="modal" data-target="#address-edit-modal"
                    data-id="${meter.address.id}" data-tooltip="true" data-original-title="Редактировать адрес">
                <i class="fa fa-pencil"></i>
            </button>
        </div>
        <span id="address-title">${meter.address}</span>
    </div>
    <div class="panel-body">
        <p id="address-address">${meter.address.address}</p>
    </div>
</div>
<c:if test="${meters != null && meters.size() != 0}">
    <div class="hpanel">
        <div class="panel-body" style="height: 122px; overflow-x: scroll;">
            <div style="width: ${meters.size() * 83 + 20}px;">
                <c:forEach items="${meters}" var="item">
                    <a href="/panel/addresses/${item.address.id}/meters/${item.id}"
                       data-tooltip="true" data-original-title='Посмотреть данные по счетчику "${item}"'>
                        <img src="${item.icon.bigUrl}" width="80" height="80" />
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
</c:if>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        <div class="pull-right">
            <button id="btn-meter-edit" class="btn btn-default btn-xs" data-toggle="modal" data-target="#meter-edit-modal"
                    data-address="${meter.address.id}" data-id="${meter.id}"
                    data-tooltip="true" data-original-title="Редактировать счетчик">
                <i class="fa fa-pencil"></i>
            </button>
        </div>
        ${meter.type}
    </div>
    <div class="panel-body">
        <table>
            <tr>
                <td valign="top" width="90px">
                    <c:if test="${meter.icon != null}">
                        <img src="${meter.icon.smallUrl}" width="80" height="80" />
                    </c:if>
                </td>
                <td>
                    <h3 class="text-bold">${meter}</h3>
                    <p class="text-bold text-primary">За месяц ${meter.totalLastMonth}</p>
                    <p>
                        Тариф ${meter.stringTariff}<br />
                        <c:if test="${!meter.type.withoutValue && meter.devices != null && meter.devices.size() != 0}">
                            Прибор учета ${meter.devices.get(0)}
                            <button class="btn btn-link" data-toggle="modal" data-target="#device-edit-modal"
                                    data-tooltip="true" data-original-title="Редактировать прибор учета">
                                <i class="fa fa-pencil"></i>
                            </button><br />
                        </c:if>
                        <c:choose>
                            <c:when test="${meter.company != null}">
                                Компания: <a href="/companies/${meter.company.id}/${meter.company.translitUrl}" class="btn-link">${meter.company}</a>
                                <button id="btn-edit-company" class="btn btn-link" data-toggle="modal" data-target="#company-edit-modal"
                                        data-tooltip="true" data-original-title="Редактировать УК">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button id="btn-send-meters" class="btn btn-link" data-account="${meter.company.getAccountNumber(security.user)}"
                                        data-tooltip="true" data-original-title="Отправить показания счетчиков">
                                    <i class="fa fa-envelope"></i>
                                </button><br />
                            </c:when>
                            <c:otherwise>
                                Компания:
                                <button id="btn-add-company" class="btn btn-link" data-toggle="modal" data-target="#company-edit-modal"
                                        data-tooltip="true" data-original-title="Добавить УК">
                                    <i class="fa fa-plus"></i>
                                </button><br />
                            </c:otherwise>
                        </c:choose>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        <div class="pull-right">
            <a href="#" class="btn-add-value btn btn-default btn-xs"
               data-address="${meter.address.id}" data-meter="${meter.id}"
               data-meter-title="${meter.title}" data-meter-unit="${meter.type.unit}"
               data-meter-type-without-value="${meter.type.withoutValue}"
               data-currency="${meter.address.currency}"
               data-toggle="modal" data-target="#meter-value-edit-modal"
               data-tooltip="true" data-original-title="Добавить показание счетчика">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        Показания счетчика
    </div>
    <div class="panel-body">
        <c:if test="${meter.history != null && meter.history.size() != 0}">
            <div class="list-group bord-no infinite-container">
                <c:forEach items="${history}" var="item">
                    <a class="list-group-item infinite-item">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="text-primary mar-no">${item.stringDate}</h4>
                                <h3 class="text-bold mar-no">
                                    <c:choose>
                                        <c:when test="${meter.type.withoutValue}">
                                            ${item.stringCost}
                                        </c:when>
                                        <c:otherwise>
                                            ${item.stringValue}
                                            <c:set var="cost" value="${item.stringCostByPrevValue}" />
                                            <c:if test="${cost != ''}">
                                                &mdash; <span class="text-primary">${item.stringCostByPrevValue}</span>
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose>
                                </h3>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn-edit-value btn btn-rounded btn-primary"
                                        data-address="${meter.address.id}" data-meter="${meter.id}" data-id="${item.id}"
                                        data-meter-title="${meter.title}" data-meter-unit="${meter.type.unit}"
                                        data-meter-type-without-value="${meter.type.withoutValue}"
                                        data-currency="${meter.address.currency}"
                                        data-toggle="modal" data-target="#meter-value-edit-modal"
                                        data-tooltip="true" data-original-title="Отредактировать показание счетчика">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button class="btn-delete-value btn btn-rounded btn-danger" data-tooltip="true"
                                        data-original-title="Удалить показание счетчика"
                                        data-address="${meter.address.id}" data-meter="${meter.id}" data-id="${item.id}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </a>
                </c:forEach>
            </div>
            <a class="infinite-more-link" href="/panel/addresses/${meter.address.id}/meters/${meter.id}/more"></a>
        </c:if>
    </div>
</div>

<t:insertAttribute name="meter-edit" defaultValue="/WEB-INF/views/modal/meter-edit.jsp" />
<t:insertAttribute name="meter-value-edit" defaultValue="/WEB-INF/views/modal/meter-value-edit.jsp" />
<t:insertAttribute name="company-edit" defaultValue="/WEB-INF/views/modal/company-edit.jsp" />
<t:insertAttribute name="company-send-meters" defaultValue="/WEB-INF/views/modal/company-send-meters.jsp" />
<t:insertAttribute name="meter-device-edit" defaultValue="/WEB-INF/views/modal/meter-device-edit.jsp" />

<script src="/resources/lib/moment/moment.min.js"></script>
<script src="/resources/lib/moment/moment-timezone.min.js"></script>
<script src="/resources/lib/moment/locales.min.js"></script>
<script src="/resources/lib/waypoints/jquery.waypoints.min.js"></script>
<script src="/resources/lib/waypoints/shortcuts/infinite.min.js"></script>

<script>
    $(document).ready(function () {
        moment.locale('ru');

        var infinite = new Waypoint.Infinite({
            element: $('.infinite-container')[0]
        });

        // привязка компании к счетчику
        $('#btn-add-company').click(function () {
            $('#company').empty();
            $('#company').val('').trigger('change');
            $('#company-name').val('');
            $('#company-unbind').removeAttr('checked');
            $('#company-unbind-label').removeClass('active');
        });

        // изменение привязки компании к счетчику
        $('#btn-edit-company').click(function () {
            $('#company').empty();
            $('#company').append('<option value="${meter.company.id}">${meter.company.name}</option>');
            $('#company').val('${meter.company.id}').trigger('change');

            $('#company-name').val('');
            $('#company-unbind').removeAttr('checked');
            $('#company-unbind-label').removeClass('active');
        });

        // отправка показаний счетчиков в УК/ТСЖ
        $('#btn-send-meters').click(function () {
            $('#send-company').empty();
            $('#send-company').append('<option value="${meter.company.id}">${meter.company.name}</option>');
            $('#send-company').val('${meter.company.id}').trigger('change');
            $('#send-email').val('<c:if test="${meter.company.email != null}">${meter.company.email}</c:if>');

            var account = $(this).data('account');
            if (account == null || account.length == 0) {
                swal({
                        title: 'Не указан лицевой счет',
                        text: 'Для отправки показаний счетчиков в компанию необходимо предварительно указать свой лицевой счет',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Указать',
                        cancelButtonText: 'Закрыть',
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            // открытие модального окна редактирования компнаии
                            $('#btn-edit-company').trigger('click');
                        }
                    });
            } else {
                // открываем модальное окно с шаблоном письма и кнопкой отправки
                $.get('/ajax/addresses/${meter.address.id}/meters/${meter.id}/email', function (result) {
                    if (!result) {
                        return;
                    }

                    var data = result.body;
                    var meters = data.meters;

                    var tbl = '<p>Нет данных для отправки</p>';
                    if (meters != null && meters.length != 0) {
                        tbl = '<table border="1" cellpadding="5" cellspacing="0" width="100%">';
                        tbl += '<tr><td><b>Счетчик</b></td><td><b>Дата</b></td><td><b>Показание</b></td></tr>';
                        for (var i = 0; i < meters.length; i++) {
                            var item = meters[i];
                            tbl += '<tr><td>' + item.name + '</td><td>' + item.value.stringFullDate + '</td><td>' + item.value.stringValue + '</td></tr>';
                        }
                        tbl += '</table>'
                    }

                    var message = data.template.replace('##ADDRESS##', data.address)
                        .replace('##ACCOUNT##', data.account).replace('##EMAIL##', data.email)
                        .replace('##METERS_LIST##', tbl);

                    $('#modal-company-send-meters-text').code(message);

                    $('#company-send-meters-modal').modal('show');
                });
            }
        });

        // удаление существующего показания счетчика
        $('.btn-delete-value').click(function () {
            var addressId = $(this).data('address');
            var meterId = $(this).data('meter');
            var itemId = $(this).data('id');

            if (!addressId || !meterId || !itemId) {
                // не все данные указаны
                return;
            }

            swal({
                    title: "Вы уверены, что хотите удалить показание счетчика?",
                    text: "Удаленное показание счетчика восстановить уже нельзя!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да, удалить!",
                    cancelButtonText: "Нет, не удалять!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // отправляем запрос на удаление
                        $.get('/panel/addresses/' + addressId + '/meters/' + meterId + '/history/delete/' + itemId, function (result) {
                            if (!result) {
                                swal("Ошибка", "Неизвестная ошибка. Не удалось удалить показание счетчика.", "error");
                                return;
                            }

                            if (result.code != 0) {
                                var errorStr = '';
                                for (var i = 0; i < result.body.length; i++) {
                                    errorStr += result.body[i] + ' ';
                                }
                                swal("Ошибка", errorStr, "error");
                                return;
                            }

                            swal("Удалено!", "Ваше показание счетчика успешно удалено.", "success");
                            window.location = '/panel/addresses/' + addressId + '/meters/' + meterId;
                        });
                    } else {
                        swal("Отмена", "Ваше показание сетчика осталось доступным.", "error");
                    }
                });
        });
    });
</script>