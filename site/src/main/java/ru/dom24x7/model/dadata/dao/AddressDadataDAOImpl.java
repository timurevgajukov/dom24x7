package ru.dom24x7.model.dadata.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.dadata.AddressDadata;
import ru.dom24x7.model.dadata.dao.interfaces.AddressDadataDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса для работы с адресами в формате сервиса dadata.ru
 */
public class AddressDadataDAOImpl extends ModelDAOImpl implements AddressDadataDAO {
    
    private static final String QUERY_LIST = "{ call addresses_dadata_list(?,?) }";
    private static final String QUERY_SAVE = "{ call addresses_dadata_save(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";

    @Override
    public List<AddressDadata> list(int offset, int count) {

        List<AddressDadata> list = jdbcTemplate.query(QUERY_LIST, new Object[]{offset, count},
                new RowMapper<AddressDadata>() {

                    @Override
                    public AddressDadata mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(AddressDadata address) {

        AddressDadata newAddress = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        address.getId(),
                        getSafeValue(address.getSource()),
                        getSafeValue(address.getPostalCode()),
                        getSafeValue(address.getCountry()),
                        getSafeValue(address.getRegionType()),
                        getSafeValue(address.getRegionTypeFull()),
                        getSafeValue(address.getRegion()),
                        getSafeValue(address.getAreaType()),
                        getSafeValue(address.getAreaTypeFull()),
                        getSafeValue(address.getArea()),
                        getSafeValue(address.getCityType()),
                        getSafeValue(address.getCityTypeFull()),
                        getSafeValue(address.getCity()),
                        getSafeValue(address.getSettlementType()),
                        getSafeValue(address.getSettlementTypeFull()),
                        getSafeValue(address.getSettlement()),
                        getSafeValue(address.getStreetType()),
                        getSafeValue(address.getStreetTypeFull()),
                        getSafeValue(address.getStreet()),
                        getSafeValue(address.getHouseType()),
                        getSafeValue(address.getHouseTypeFull()),
                        getSafeValue(address.getHouse()),
                        getSafeValue(address.getBlockType()),
                        getSafeValue(address.getBlock()),
                        getSafeValue(address.getFlatType()),
                        getSafeValue(address.getFlat()),
                        getSafeValue(address.getPostalBox()),
                        getSafeValue(address.getKladrId()),
                        getSafeValue(address.getOkato()),
                        getSafeValue(address.getOktmo()),
                        getSafeValue(address.getTaxOffice()),
                        getSafeValue(address.getFlatArea()),
                        address.getQcComplete(),
                        address.getQcHouse(),
                        address.getQc(),
                        getSafeValue(address.getUnparsedParts())
                }, new RowMapper<AddressDadata>() {

                    @Override
                    public AddressDadata mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        address.setId(newAddress.getId());
        
        return true;
    }

    private AddressDadata load(ResultSet rs) throws SQLException {

        AddressDadata address = new AddressDadata(rs.getInt("id"));
        address.setPostalCode(rs.getString("postal_code"));
        address.setCountry(rs.getString("country"));
        address.setRegionType(rs.getString("region_type"));
        address.setRegionTypeFull(rs.getString("region_type_full"));
        address.setRegion(rs.getString("region"));
        address.setAreaType(rs.getString("area_type"));
        address.setAreaTypeFull(rs.getString("area_type_full"));
        address.setArea(rs.getString("area"));
        address.setCityType(rs.getString("city_type"));
        address.setCityTypeFull(rs.getString("city_type_full"));
        address.setCity(rs.getString("city"));
        address.setSettlementType(rs.getString("settlement_type"));
        address.setSettlementTypeFull(rs.getString("settlement_type_full"));
        address.setSettlement(rs.getString("settlement"));
        address.setStreetType(rs.getString("street_type"));
        address.setStreetTypeFull(rs.getString("street_type_full"));
        address.setStreet(rs.getString("street"));
        address.setHouseType(rs.getString("house_type"));
        address.setHouseTypeFull(rs.getString("house_type_full"));
        address.setHouse(rs.getString("house"));
        address.setBlockType(rs.getString("block_type"));
        address.setBlock(rs.getString("block"));
        address.setKladrId(rs.getString("kladr_id"));

        return address;
    }

    private String getSafeValue(String value) {

        if (value != null && value.trim().length() != 0) {
            return value.trim();
        } else {
            return null;
        }
    }
}