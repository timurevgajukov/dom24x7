<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="header">
    <div class="color-line">
    </div>
    <div id="logo" class="light-version">
        <span>
            <a href="/">Dom24x7</a>
        </span>
    </div>
    <nav role="navigation">
        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
        <div class="small-logo">
            <span class="text-primary"><a href="/">Dom24x7</a></span>
        </div>
        <form role="search" class="navbar-form-custom" method="post" action="#">
            <div class="form-group">
                <input type="text" placeholder="поиск..." class="form-control" name="search">
            </div>
        </form>
        <c:if test="${security.auth}">
            <div class="mobile-menu">
                <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                    <i class="fa fa-chevron-down"></i>
                </button>
                <div class="collapse mobile-navbar" id="mobile-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a class="" href="/security/logout">Выйти</a>
                        </li>
                        <li>
                            <a class="" href="/profile">Профиль</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="navbar-right">
                <ul class="nav navbar-nav no-borders">
                    <li class="dropdown">
                        <a class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown">
                            <i class="pe-7s-speaker"></i>
                            <c:if test="${security.notifications.has()}">
                                <span class="label label-success">${security.notifications.count()}</span>
                            </c:if>
                        </a>
                        <ul class="dropdown-menu hdropdown notification animated flipInX">
                            <c:choose>
                                <c:when test="${security.notifications.has()}">
                                    <c:forEach items="${security.notifications.listWithClear()}" var="notif">
                                        <li>
                                            <a href="${notif.url}">
                                                <span class="label label-${notif.type}">WARN</span> ${notif.body}
                                            </a>
                                        </li>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <li class="summary">Cообщений нет</li>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="/security/logout">
                            <i class="pe-7s-upload pe-rotate-90"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </c:if>
    </nav>
</div>