package ru.evgajukov.profi.dictionary.response;

/**
 * Created by timur on 15.06.17.
 */
public class Book {

    private long timestamp;
    private Label label;

    public long getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(long timestamp) {

        this.timestamp = timestamp;
    }

    public Label getLabel() {

        return label;
    }

    public void setLabel(Label label) {

        this.label = label;
    }
}
