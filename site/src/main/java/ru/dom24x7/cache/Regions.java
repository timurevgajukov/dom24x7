package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.company.Region;
import ru.dom24x7.model.company.dao.RegionDAOImpl;

import java.util.List;

/**
 * Created by timur on 09.06.17.
 * Контейнер для хранения списка регионов
 */
public class Regions implements ICache<Region> {

    private static Regions instance = null;

    private static final int MAX_COUNT = 10; // максимальный счетчик обращений к контейнеру
    private static int count = 0; // текущее количество обращений

    private List<Region> list;

    public static Regions getInstance() {

        count++;

        if (instance == null) {
            instance = new Regions();
        }

        if (count >= MAX_COUNT) {
            instance = new Regions();
            count = 0;
        }

        return instance;
    }

    private Regions() {

        list = new RegionDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Region> list() {

        return list;
    }

    @Override
    public Region get(int id) {

        if (list == null) {
            return null;
        }

        for (Region item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}