package ru.dom24x7.model.rating.dao.interfaces;

import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.rating.Rating;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 07.06.17.
 * Интерфейс для работы с отзывами
 */
public interface RatingDAO {

    /**
     * Возвращает отызв пользователя по компании
     * @param user
     * @param company
     * @return
     */
    Rating get(User user, Company company);

    /**
     * Возвращает список отзывов по компании
     * @param company
     * @return
     */
    List<Rating> list(Company company);

    /**
     * Сохранение отзыва по компании
     * @param rating
     * @return
     */
    boolean save(Rating rating);
}