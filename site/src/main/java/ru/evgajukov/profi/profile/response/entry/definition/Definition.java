package ru.evgajukov.profi.profile.response.entry.definition;

import ru.evgajukov.profi.profile.response.entry.definition.geo.Geo;
import ru.evgajukov.profi.profile.response.entry.definition.image.Images;
import ru.evgajukov.profi.profile.response.entry.definition.price.Price;
import ru.evgajukov.profi.profile.response.entry.definition.status.Status;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class Definition {

    public static final String GENDER_MALE = "male";
    public static final String GENDER_FEMALE = "female";

    private String id; // идентификатор профиля
    private List<Price> prices; // информация о ценах
    private Name name; // имя (название) профиля
    private String gender; // пол специалиста
    private Images images; // графические ресурсы профиля: аватары и примеры работ
    private Info info; // дополнительная информация по профилю
    private List<Object> parents; // родительские организации
    private Status status; // активность профиля
    private Geo geo; // информация о месте оказания услуг (location - у себя, leave - выезд)
    private Feedback feedback;
    private List<Pinf> pinfs;
    private Phone phone;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public List<Price> getPrices() {

        return prices;
    }

    public void setPrices(List<Price> prices) {

        this.prices = prices;
    }

    public Name getName() {

        return name;
    }

    public void setName(Name name) {

        this.name = name;
    }

    public String getGender() {

        return gender;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    public Images getImages() {

        return images;
    }

    public void setImages(Images images) {

        this.images = images;
    }

    public Info getInfo() {

        return info;
    }

    public void setInfo(Info info) {

        this.info = info;
    }

    public List<Object> getParents() {

        return parents;
    }

    public void setParents(List<Object> parents) {

        this.parents = parents;
    }

    public Status getStatus() {

        return status;
    }

    public void setStatus(Status status) {

        this.status = status;
    }

    public Geo getGeo() {

        return geo;
    }

    public void setGeo(Geo geo) {

        this.geo = geo;
    }

    public Feedback getFeedback() {

        return feedback;
    }

    public void setFeedback(Feedback feedback) {

        this.feedback = feedback;
    }

    public List<Pinf> getPinfs() {

        return pinfs;
    }

    public void setPinfs(List<Pinf> pinfs) {

        this.pinfs = pinfs;
    }

    public Phone getPhone() {

        return phone;
    }

    public void setPhone(Phone phone) {

        this.phone = phone;
    }

    @Override
    public String toString() {

        return id;
    }
}