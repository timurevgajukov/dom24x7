package ru.evgajukov.profi.dictionary.response.pservice;

import com.google.gson.annotations.SerializedName;
import ru.evgajukov.profi.dictionary.response.DictionaryItem;

/**
 * Created by timur on 15.06.17.
 * Элемент справочника предоставляемой услуги
 */
public class PServiceItem extends DictionaryItem {

    private String folder;
    private String allnames;
    private String altname;
    private String arithmop;
    private String composite;
    @SerializedName("edizm_id")
    private int edizmId;
    private Object language;
    @SerializedName("parent_id")
    private int parentId;
    private int popularity;
    private String spname;
    private String srch;
    @SerializedName("top_id")
    private int topId;
    private String typ;
    @SerializedName("u_id")
    private String uId;
    @SerializedName("z_id")
    private String zId;

    public PServiceItem(int id, String name, int status) {

        super(id, name, status);
    }

    public String getFolder() {

        return folder;
    }

    public void setFolder(String folder) {

        this.folder = folder;
    }

    public String getAllnames() {

        return allnames;
    }

    public void setAllnames(String allnames) {

        this.allnames = allnames;
    }

    public String getAltname() {

        return altname;
    }

    public void setAltname(String altname) {

        this.altname = altname;
    }

    public String getArithmop() {

        return arithmop;
    }

    public void setArithmop(String arithmop) {

        this.arithmop = arithmop;
    }

    public String getComposite() {

        return composite;
    }

    public void setComposite(String composite) {

        this.composite = composite;
    }

    public int getEdizmId() {

        return edizmId;
    }

    public void setEdizmId(int edizmId) {

        this.edizmId = edizmId;
    }

    public Object getLanguage() {

        return language;
    }

    public void setLanguage(Object language) {

        this.language = language;
    }

    public int getParentId() {

        return parentId;
    }

    public void setParentId(int parentId) {

        this.parentId = parentId;
    }

    public int getPopularity() {

        return popularity;
    }

    public void setPopularity(int popularity) {

        this.popularity = popularity;
    }

    public String getSpname() {

        return spname;
    }

    public void setSpname(String spname) {

        this.spname = spname;
    }

    public String getSrch() {

        return srch;
    }

    public void setSrch(String srch) {

        this.srch = srch;
    }

    public int getTopId() {

        return topId;
    }

    public void setTopId(int topId) {

        this.topId = topId;
    }

    public String getTyp() {

        return typ;
    }

    public void setTyp(String typ) {

        this.typ = typ;
    }

    public String getuId() {

        return uId;
    }

    public void setuId(String uId) {

        this.uId = uId;
    }

    public String getzId() {

        return zId;
    }

    public void setzId(String zId) {

        this.zId = zId;
    }
}
