package ru.dom24x7.model.rating;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.rating.dao.RatingDAOImpl;
import ru.dom24x7.model.user.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 07.06.17.
 * Отзыв по компании
 */
public class Rating extends Model {

    private Date createDt;
    private User user;
    private Company company;
    private int mark;
    private String review;

    public Rating() {

        super();
    }

    public Rating(int id) {

        super(id);
    }

    public Date getCreateDt() {

        return createDt;
    }

    public String getCreateDtStr() {

        return new SimpleDateFormat("dd.MM.yyyy").format(createDt);
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Company getCompany() {

        return company;
    }

    public void setCompany(Company company) {

        this.company = company;
    }

    public int getMark() {

        return mark;
    }

    public void setMark(int mark) {

        this.mark = mark;
    }

    public String getReview() {

        return review;
    }

    public void setReview(String review) {

        this.review = review;
    }

    @Override
    public String toString() {

        return review;
    }

    /**
     * Возвращает среднюю оценку по компании
     * @param company
     * @return
     */
    public static double averageMarkByCompany(Company company) {

        double result = 0.0;
        List<Rating> ratings = new RatingDAOImpl().list(company);
        if (ratings != null) {
            for (Rating rating : ratings) {
                result += rating.getMark();
            }
            if (ratings.size() != 0) {
                result /= ratings.size();
            }
        }

        return result;
    }
}