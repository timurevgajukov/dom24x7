package ru.evgajukov.profi.profile.request.filter;

/**
 * Created by timur on 18.06.17.
 * небольшой опыт 50
 * средний опыт 55
 * школьный учитель 60
 * преподаватель курсов 65
 * серьезный опыт 70
 * преподаватель вуза 75
 * репетитор-эксперт 80
 * профессор 85
 */
public class Experience {

    private int id;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }
}