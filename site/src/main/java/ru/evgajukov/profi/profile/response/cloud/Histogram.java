package ru.evgajukov.profi.profile.response.cloud;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class Histogram {

    private List<Interval> intervals;

    public List<Interval> getIntervals() {

        return intervals;
    }

    public void setIntervals(List<Interval> intervals) {

        this.intervals = intervals;
    }
}
