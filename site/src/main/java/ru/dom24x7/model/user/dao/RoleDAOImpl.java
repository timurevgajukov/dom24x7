package ru.dom24x7.model.user.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.reference.ReferenceDAO;
import ru.dom24x7.model.user.Role;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 14.02.17.
 *
 * Реализация интерфейса справочника ролей пользователей
 */
public class RoleDAOImpl extends ModelDAOImpl implements ReferenceDAO<Role> {

    private static final String QUERY_LIST = "{ call roles_list() }";

    @Override
    public List<Role> list() {

        List<Role> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Role>() {

            @Override
            public Role mapRow(ResultSet resultSet, int i) throws SQLException {

                return new Role(resultSet.getInt("id"), resultSet.getString("name"));
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }
}