package ru.evgajukov.profi.profile.response.entry.definition.geo;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class Station {

    private int id;
    private String name;
    private String url;
    private Boolean active;
    private City city;
    private List<Region> regions;
    private List<Line> lines;
    private List<Area> areas;
    private Language language;
    private List<Translation> translations;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public Boolean getActive() {

        return active;
    }

    public void setActive(Boolean active) {

        this.active = active;
    }

    public City getCity() {

        return city;
    }

    public void setCity(City city) {

        this.city = city;
    }

    public List<Region> getRegions() {

        return regions;
    }

    public void setRegions(List<Region> regions) {

        this.regions = regions;
    }

    public List<Line> getLines() {

        return lines;
    }

    public void setLines(List<Line> lines) {

        this.lines = lines;
    }

    public List<Area> getAreas() {

        return areas;
    }

    public void setAreas(List<Area> areas) {

        this.areas = areas;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public List<Translation> getTranslations() {

        return translations;
    }

    public void setTranslations(List<Translation> translations) {

        this.translations = translations;
    }

    @Override
    public String toString() {

        return name;
    }
}