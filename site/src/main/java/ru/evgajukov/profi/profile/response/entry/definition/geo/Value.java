package ru.evgajukov.profi.profile.response.entry.definition.geo;

/**
 * Created by timur on 17.06.17.
 */
public class Value {

    private String name;

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return name;
    }
}