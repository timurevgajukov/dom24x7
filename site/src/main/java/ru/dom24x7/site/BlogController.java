package ru.dom24x7.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.cache.Categories;
import ru.dom24x7.component.OpenGraph;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.photo.dao.PhotoDAOImpl;
import ru.dom24x7.model.post.Category;
import ru.dom24x7.model.post.Post;
import ru.dom24x7.model.post.dao.PostDAOImpl;
import ru.dom24x7.utils.StrUtils;

import java.util.List;

/**
 * Created by timur on 15.02.17.
 *
 * Контроллер для работы блога
 */
@Controller
public class BlogController extends BaseController {

    private static final String ACTIVE = "blog";

    @RequestMapping("/blog")
    public String indexAction(ModelMap model) {

        setModelMap(model, "Блог", ACTIVE);

        return "blog/index";
    }

    @RequestMapping("/blog/{categoryUrl}")
    public String categoryAction(@PathVariable String categoryUrl, ModelMap model) {

        Category category = Categories.getInstance().get(categoryUrl);
        if (category == null) {
            return "redirect:/blog";
        }

        setModelMap(model, "Блог. Посты категории: " + category.getName(), ACTIVE);
        model.addAttribute("category", category);

        return "blog/index";
    }

    @RequestMapping("/blog/post/{postId}/{translit}")
    public String postAction(@PathVariable int postId, @PathVariable String translit, ModelMap model) {

        Post post = new PostDAOImpl().get(postId);
        if (post == null) {
            return "redirect:/blog";
        }

        List<Post> postsForNav = new PostDAOImpl().listForNav();

        OpenGraph og = new OpenGraph();
        og.setTitle(post.getShowTitle());
        og.setDescription(StrUtils.isEmpty(post.getText()) ? post.getShowTitle() : post.getText().split("<hr>")[0].replaceAll("\\<[^>]*>",""));
        og.setType(OpenGraph.OG_TYPE_ARTICLE);
        og.setUrl(TechController.URL + "/blog/post/" + postId + "/" + translit);
        if (post.getPhotos() != null && post.getPhotos().size() != 0) {
            Photo photo = post.getPhotos().get(0);
            og.setImage(String.format(TechController.URL + "/blog/images/%d/%s", photo.getId(), photo.getFileName()));
        }

        setModelMap(model, og, null, ACTIVE);
        model.addAttribute("categories", Categories.getInstance().list());
        model.addAttribute("post", post);
        model.addAttribute("prev", getPrevPost(post, postsForNav));
        model.addAttribute("next", getNextPost(post, postsForNav));
        model.addAttribute("last", new PostDAOImpl().listForBlog(0, 5));

        return "blog/post";
    }

    @RequestMapping("/blog/ajax/posts/{offset}/{count}")
    public @ResponseBody List<Post> listAjax(
            @PathVariable int offset, @PathVariable int count,
            @RequestParam(value = "category", required = false) String categoryUrl) {

        if (categoryUrl != null) {
            Category category = Categories.getInstance().get(categoryUrl);
            if (category != null) {
                return new PostDAOImpl().listForBlog(offset, count, category);
            } else {
                return new PostDAOImpl().listForBlog(offset, count);
            }
        } else {
            return new PostDAOImpl().listForBlog(offset, count);
        }
    }

    @RequestMapping("/blog/images/{imgId}/{imgFileName}")
    public @ResponseBody byte[] postImageAction(@PathVariable int imgId, @PathVariable String imgFileName) {

        Photo photo = new PhotoDAOImpl().get(imgId);
        if (photo == null) {
            return null;
        }

        return photo.getResource();
    }

    /**
     * Возвращает индекс поста в массиве
     * @param post
     * @param posts
     * @return
     */
    private Integer getPostIndex(Post post, List<Post> posts) {

        for (int i = 0; i < posts.size(); i++) {
            if (post.getId() == posts.get(i).getId()) {
                return i;
            }
        }

        return null;
    }

    /**
     * Возвращает предыдущий относительно указанного пост
     * @param post
     * @param posts
     * @return
     */
    private Post getPrevPost(Post post, List<Post> posts) {

        Integer index = getPostIndex(post, posts);
        if (index == null || index == posts.size() - 1) {
            // нет предыдущего поста
            return null;
        }

        return posts.get(index + 1);
    }

    /**
     * Возвращает следующий пост относительно указанного
     * @param post
     * @param posts
     * @return
     */
    private Post getNextPost(Post post, List<Post> posts) {

        Integer index = getPostIndex(post, posts);
        if (index == null || index == 0) {
            // нет следующего поста
            return null;
        }

        return posts.get(index - 1);
    }
}