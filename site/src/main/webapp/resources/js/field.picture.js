/**
 * Created by timur on 25.12.16.
 * Поля для работы с картинками
 */
function PictureField(options) {
    var self = this;

    this._settings = $.extend(true, {
        data: null,
        value: null,
        fileName: null,
        required: null,
        readonly: null,
        guid: null,
        index: 0
    }, options);

    this._html = null;
    this._isChanged = false;

    this._methods = {
        /**
         * Инициализация статичного поля
         */
        init: function () {
            var id = self._settings.data.name;
            var elemId = id + '-' + self._settings.guid;

            var elem = '';
            if (self._settings.value) {
                // есть картинка, нужно отобразить с кнопкой удаления
                elem = '<div id="picture-' + elemId + '"></div>';
                setTimeout(function () {
                    var value = self._settings.value;
                    if (value.match('image')) {
                        self._methods.showPicture($('#picture-' + elemId), elemId, self._settings.value, self._settings.data.readOnly);
                    } else if (value.match('application/pdf')) {
                        self._methods.showPDF($('#picture-' + elemId), elemId, self._settings.value, self._settings.data.readOnly);
                    }
                }, 1000);
            } else {
                if (!self._settings.readonly) {
                    // нет картинки, нужно отобразить кнопку добавления
                    // но в начале нужно проверить, что браузер поддерживает нужные нам функции
                    if (window.File && window.FileReader && window.FileList && window.Blob) {
                        // нужный нам функционал поддерживается
                        elem = self._methods.showAddBtn(elemId)
                    } else {
                        elem = 'Используемый браузер не поддерживает HTML5 File API';
                    }
                }
            }

            self._html = elem;
        },
        /**
         * Отображает кнопку добавления картинки
         * @param id
         */
        showAddBtn: function (id, withContainer) {
            if (withContainer === undefined) {
                withContainer = true;
            }

            var elemId = 'input-file-' + id;
            var elem = '';
            if (withContainer) {
                elem += '<div id="picture-' + id + '">';
            }
            elem += '<input type="file" class="form-control" id="' + elemId + '" data-id="' + id + '" multiple />';
            if (withContainer) {
                elem += '</div><br />';
            }

            $(document).off('change', '#' + elemId).on('change', '#' + elemId, function (e) {
                var id = $(this).data('id');
                var pictureBlock = $('#picture-' + id);
                pictureBlock.empty();

                var f = e.target.files[0];
                if (!f.type.match('image.*') && !f.type.match('application/pdf')) {
                    // TODO: тут вновь отобразить внопку добавления
                    pictureBlock.html('Не является картинкой или PDF документом');
                    return;
                }

                self._settings.fileName = f.name;

                var reader = new FileReader();
                reader.onload = (function () {
                    return function (e) {
                        // загружаем картинку в структуру формы
                        self._settings.data.values[self._settings.index] = e.target.result;
                        if (e.target.result.match('image')) {
                            self._methods.showPicture(pictureBlock, id, self._settings.data.values[self._settings.index], self._settings.readonly);
                        } else if (e.target.result.match('application/pdf')) {
                            self._methods.showPDF(pictureBlock, id, self._settings.data.values[self._settings.index], self._settings.readonly);
                        } else {
                            console.log('unknown picture type');
                        }
                        if (self._settings.block) {
                            var field = self._settings.block.field(self._settings.index, self._settings.data.name);
                            if (field) {
                                field._isChanged = true;
                            }
                        } else {
                            self._isChanged = true;
                        }
                    };
                })(f);
                reader.readAsDataURL(f);
            });

            return elem;
        },
        /**
         * Отображает картинку
         * @param src
         * @param picture
         * @param readonly
         */
        showPicture: function (src, id, picture, readonly) {
            if (picture == null || picture.trim().length == 0) {
                // нечего отображать
                return;
            }

            var elem = '<div id="' + id + '" class="thumbnail">';
            elem += '<img id="img-' + id + '" src="">';
            if (!readonly) {
                elem += '<div class="caption"><button id="btn-del-' + id + '" class="btn red small" type="button">Удалить</button></div>';

                $(document).off('click', '#btn-del-' + id).on('click', '#btn-del-' + id, function () {
                    self._settings.fileName = null;
                    self._settings.data.values[self._settings.index] = null;
                    src.empty();
                    src.append(self._methods.showAddBtn(id, false));
                    if (self._settings.block) {
                        var field = self._settings.block.field(self._settings.index, self._settings.data.name);
                        if (field) {
                            field._isChanged = false;
                        }
                    } else {
                        self._isChanged = false;
                    }
                });
            }
            elem += '</div>';

            src.empty();
            src.append(elem);

            var img = document.getElementById('img-' + id);
            img.src = picture;
        },
        /**
         * Отображает PDF
         * @param src
         * @param picture
         * @param readonly
         */
        showPDF: function (src, id, picture, readonly) {
            if (picture == null || picture.trim().length == 0) {
                // нечего отображать
                return;
            }

            var elem = '<div id="' + id + '" class="thumbnail" style="overflow-x: scroll;">';
            // elem += '<a href="#" id="img-link-' + id + '" data-index="' + self._settings.index + '"><canvas id="img-' + id + '"></canvas></a>';
            elem += '<a href="#" id="img-link-' + id + '" data-index="' + self._settings.index + '"><i class="fa fa-file-pdf-o fa-3x"></i></a>';
            if (!readonly) {
                elem += '<div class="caption"><button id="btn-del-' + id + '" class="btn red small" type="button">Удалить</button></div>';

                $(document).off('click', '#btn-del-' + id).on('click', '#btn-del-' + id, function () {
                    self._settings.data.values[self._settings.index] = null;
                    src.empty();
                    src.append(self._methods.showAddBtn(id, false));
                    if (self._settings.block) {
                        var field = self._settings.block.field(self._settings.index, self._settings.data.name);
                        if (field) {
                            field._isChanged = false;
                        }
                    } else {
                        self._isChanged = false;
                    }
                });
            }
            elem += '</div>';

            // отирытие pdf документа в отдельном окне
            $(document).off('click', '#img-link-' + id).on('click', '#img-link-' + id, function () {
                var index = $(this).data('index');
                var data = self._settings.data.values[index];
                if (data.match('application/pdf')) {
                    window.open(data);
                }

                return false;
            });

            src.empty();
            src.append(elem);

            /*
            var pdfData = atob(picture.replace('data:application/pdf;base64,', ''));
            PDFJS.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
            var loadingTask = PDFJS.getDocument({data: pdfData});
            loadingTask.promise.then(function(pdf) {
                console.log('PDF loaded');

                var pageNumber = 1;
                pdf.getPage(pageNumber).then(function(page) {
                    console.log('Page loaded');

                    var scale = 1.5;
                    var viewport = page.getViewport(scale);

                    var canvas = document.getElementById('img-' + id);
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    var renderTask = page.render(renderContext);
                    renderTask.then(function () {
                        console.log('Page rendered');
                    });
                });
            }, function (reason) {
                console.error(reason);
            });
            */
        }
    };

    this._methods.init();
}

/**
 * Перерисовка значения поля
 */
PictureField.prototype.draw = function () {
    var data = this._settings.data;
    var index = this._settings.index;
    var value = '';
    if (data.values != null && data.values.length != 0 && index < data.values.length) {
        value = data.values[index];
    }

    var elemId = data.name  + '-' + this._settings.guid;

    var self = this;
    setTimeout(function () {
        var src = $('#picture-' + elemId);
        if (value) {
            if (value.match('image')) {
                self._methods.showPicture(src, elemId, value, self._settings.data.readOnly);
            } else if (value.match('application/pdf')) {
                self._methods.showPDF(src, elemId, value, self._settings.data.readOnly);
            }
        } else {
            src.empty();
            src.append(self._methods.showAddBtn(elemId, false));
        }
    }, 1000);
};

/**
 * Возвращает html представление поля
 */
PictureField.prototype.html = function () {
    return this._html;
};

/**
 * Возвращает признак, что поле было изменено
 * @returns {boolean}
 */
PictureField.prototype.isChanged = function () {
    return this._isChanged;
};

/**
 * Очищает признак изменения поля
 */
PictureField.prototype.clearChanged = function () {
    this._isChanged = false;
};

/**
 * Валидация поля
 * @returns {*}
 */
PictureField.prototype.validate = function () {
    if (!this._settings.data.required) {
        return {
            status: true,
            description: 'OK'
        }
    }

    var value = this._settings.data.values[this._settings.index];
    if (value != null && value.length != 0) {
        return {
            status: true,
            description: 'OK'
        }
    } else {
        return {
            status: false,
            description: 'Поле "' + this._settings.data.title + '" должно быть заполнено'
        }
    }
};

/**
 * Возвращает данные
 */
PictureField.prototype.get = function () {
    var value = null;
    if (this._settings.data.values[this._settings.index] != null) {
        value = this._settings.data.values[this._settings.index].split('base64,')[1];
    }
    return {
        fileName: this._settings.fileName,
        value: value
    }
};