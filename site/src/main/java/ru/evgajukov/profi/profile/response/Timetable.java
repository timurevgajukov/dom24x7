package ru.evgajukov.profi.profile.response;

/**
 * Created by timur on 15.06.17.
 */
public class Timetable {

    private long start;
    private int prepare;
    private int complete;

    public long getStart() {

        return start;
    }

    public void setStart(long start) {

        this.start = start;
    }

    public int getPrepare() {

        return prepare;
    }

    public void setPrepare(int prepare) {

        this.prepare = prepare;
    }

    public int getComplete() {

        return complete;
    }

    public void setComplete(int complete) {

        this.complete = complete;
    }
}