package ru.evgajukov.profi.dictionary.response;

/**
 * Created by timur on 15.06.17.
 */
public class Label {

    private Project project;

    public Project getProject() {

        return project;
    }

    public void setProject(Project project) {

        this.project = project;
    }
}
