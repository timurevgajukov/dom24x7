package ru.evgajukov.profi.profile.response.entry.definition.image;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class Images {

    private List<Image> avatars; // аватары профиля
    private List<Portfolio> portfolio; // примеры работ профиля

    public List<Image> getAvatars() {

        return avatars;
    }

    public void setAvatars(List<Image> avatars) {

        this.avatars = avatars;
    }

    public List<Portfolio> getPortfolio() {

        return portfolio;
    }

    public void setPortfolio(List<Portfolio> portfolio) {

        this.portfolio = portfolio;
    }
}