package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;

import java.util.List;

/**
 * Created by timur on 25.09.16.
 *
 * Контейнер для хранение списка компаний ЖКХ
 */
public class Companies implements ICache<Company> {

    private static Companies instance = null;

    private List<Company> list;

    public static Companies getInstance() {

        if (instance == null) {
            instance = new Companies();
        }

        return instance;
    }

    private Companies() {

        list = new CompanyDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Company> list() {

        return list;
    }

    @Override
    public Company get(int id) {

        if (list == null) {
            return null;
        }

        for (Company item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}