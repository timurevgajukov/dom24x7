<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-md-9">
        <div class="hpanel blog-article-box">
            <div class="panel-heading">
                <h4>${title}</h4>
                <div class="text-muted small">
                    Опубликован: <fmt:formatDate value="${post.publishedDt}" pattern="dd.MM.yyyy HH:mm"/><br />
                    Категория: ${post.category.name}
                </div>
                <br /><br />
                <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,gplus" data-counter=""></div>

            </div>
            <div class="panel-body">
                ${post.text}

                <c:if test="${post.category.id == 4 && post.photos != null && post.photos.size() != 0}">
                    <c:if test="${post.text != null && post.text.trim().length() != 0}"><br /><br /></c:if>
                    <img src="/blog/images/${post.photos.get(0).id}/${post.photos.get(0).fileName}" alt="${post.showTitle}" style="max-width: 100%;">
                    <br />
                </c:if>

                <c:if test="${post.source != null && post.source.length() != 0}">
                    <br />
                    <p>Источник: <a href="${post.source}">${post.source}</a></p>
                </c:if>

                <br /><br />
                <div class="row">
                    <div class="col-xs-6">
                        <c:if test="${next != null}">
                            <a href="/blog/post/${next.id}/${next.translitUrl}"><i class="fa fa-long-arrow-left"></i> вперед</a>&nbsp;&nbsp;&nbsp;
                        </c:if>
                        <c:if test="${prev != null}">
                            <a href="/blog/post/${prev.id}/${prev.translitUrl}">назад <i class="fa fa-long-arrow-right"></i></a>
                        </c:if>
                    </div>
                    <div class="col-xs-6 text-right">
                        <div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,gplus" data-counter=""></div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <i class="fa fa-eye"></i> ${post.views} просмотров
            </div>
        </div>
        <c:if test="${post.photos != null && post.photos.size() != 0}">
            <c:if test="${post.category.id != 4 || post.photos.size() > 1}">
                <link rel="stylesheet" href="/resources/themes/homer/vendor/blueimp-gallery/css/blueimp-gallery.min.css" />

                <style>
                    .lightBoxGallery {
                        text-align: center;
                    }
                    .lightBoxGallery a {
                        margin: 5px;
                        display: inline-block;
                    }
                </style>

                <div class="hpanel">
                    <div class="panel-body">
                        <div class="lightBoxGallery">
                            <c:forEach items="${post.photos}" var="photo">
                                <a href="/blog/images/${photo.id}/${photo.fileName}" title="${post.showTitle}" data-gallery=""><img src="/blog/images/${photo.id}/${photo.fileName}" style="max-width: 240px; max-height: 160px"></a>
                            </c:forEach>
                        </div>
                    </div>
                </div>

                <div id="blueimp-gallery" class="blueimp-gallery">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>

                <script src="/resources/themes/homer/vendor/blueimp-gallery/js/jquery.blueimp-gallery.min.js"></script>
            </c:if>
        </c:if>
        <div class="hpanel">
            <div class="panel-body">
                еще по теме:
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="hpanel">
            <div class="panel-body">
                <p>последние посты:</p>
                <c:if test="${last != null && last.size() != 0}">
                    <ul>
                        <c:forEach items="${last}" var="lastPost">
                            <li><a href="/blog/post/${lastPost.id}/${lastPost.translitUrl}">${lastPost.showTitle}</a></li>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
        <div class="hpanel">
            <div class="panel-body">
                <p>категории:</p>
                <c:if test="${categories != null && categories.size() != 0}">
                    <ul>
                        <c:forEach items="${categories}" var="category">
                            <li><a href="/blog/${category.urlName}">${category.name}</a></li>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
    </div>
</div>

<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="//yastatic.net/share2/share.js"></script>