package ru.dom24x7.model.meter.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.model.meter.dao.interfaces.MeterHistoryDAO;
import ru.dom24x7.model.meter.Meter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 * <p>
 * Реализация интерфейса для работы с историей показаний счетчика
 */
public class MeterHistoryDAOImpl extends ModelDAOImpl implements MeterHistoryDAO {

    private static final String QUERY_GET_BY_ID = "{ call meters_history_get(?) }";
    private static final String QUERY_LIST_BY_METER = "{ call meters_history_list(?) }";
    private static final String QUERY_LIST_BY_METER2 = "{ call meters_history_list2(?,?,?) }";
    private static final String QUERY_SAVE = "{ call meters_history_save(?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call meters_history_delete(?) }";

    @Override
    public MeterHistoryItem get(int id) {

        List<MeterHistoryItem> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<MeterHistoryItem>() {

            @Override
            public MeterHistoryItem mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<MeterHistoryItem> list(final Meter meter) {

        List<MeterHistoryItem> list = jdbcTemplate.query(QUERY_LIST_BY_METER, new RowMapper<MeterHistoryItem>() {

            @Override
            public MeterHistoryItem mapRow(ResultSet resultSet, int i) throws SQLException {

                MeterHistoryItem item = load(resultSet);
                item.setMeter(meter);

                return item;
            }
        }, meter.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<MeterHistoryItem> list(final Meter meter, int offset, int count) {

        List<MeterHistoryItem> list = jdbcTemplate.query(QUERY_LIST_BY_METER2, new Object[]{meter.getId(), offset, count},
                new RowMapper<MeterHistoryItem>() {

                    @Override
                    public MeterHistoryItem mapRow(ResultSet resultSet, int i) throws SQLException {

                        MeterHistoryItem item = load(resultSet);
                        item.setMeter(meter);

                        return item;
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(MeterHistoryItem item) {

        MeterHistoryItem newItem = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        item.getId(),
                        item.getMeter().getId(),
                        item.getDate(),
                        item.getValue(),
                        item.getCost()
                }, new RowMapper<MeterHistoryItem>() {

                    @Override
                    public MeterHistoryItem mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        item.setId(newItem.getId());

        return true;
    }

    @Override
    public boolean delete(MeterHistoryItem item) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{item.getId()}) != 0;
    }

    private MeterHistoryItem load(ResultSet rs) throws SQLException {

        MeterHistoryItem item = new MeterHistoryItem(rs.getInt("id"));
        item.setDate(rs.getDate("dt"));
        item.setMeter(new Meter(rs.getInt("meter_id")));
        item.setValue(rs.getDouble("value"));
        item.setCost(rs.getDouble("cost"));

        return item;
    }
}