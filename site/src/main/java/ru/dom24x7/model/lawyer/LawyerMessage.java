package ru.dom24x7.model.lawyer;

import java.util.Date;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.lawyer.dao.LawyerMessageCommentDAOImpl;
import ru.dom24x7.model.user.User;
import ru.dom24x7.utils.DateUtils;

/**
 * Сообщение для юриста
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class LawyerMessage extends Model {
	
	private User user;
	private Date createDt;
	private String text;
	
	public LawyerMessage() {
		
		super();
	}
	
	public LawyerMessage(int id) {
		
		super(id);
	}
	
	public User getUser() {
		
		return user;
	}
	
	public LawyerMessage setUser(User user) {
		
		this.user = user;
		
		return this;
	}
	
	public Date getCreateDt() {
		
		return createDt;
	}

	public String getCreateDtStr() {

		return DateUtils.getStringDate(createDt);
	}
	
	public LawyerMessage setCreateDt(Date dt) {
		
		this.createDt = dt;
		
		return this;
	}
	
	public String getText() {
		
		return text;
	}
	
	public LawyerMessage setText(String text) {
		
		this.text = text;
		
		return this;
	}

	public int getAnswersCount() {

		return new LawyerMessageCommentDAOImpl().count(this);
	}
	
	@Override
	public String toString() {
		
		return text;
	}
}