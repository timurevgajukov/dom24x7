package ru.evgajukov.profi.profile.response.entry;

import com.google.gson.annotations.SerializedName;

/**
 * Created by timur on 17.06.17.
 */
public class Ranks {

    @SerializedName("default")
    private double value;

    public double getValue() {

        return value;
    }

    public void setValue(double value) {

        this.value = value;
    }
}
