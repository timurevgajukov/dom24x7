package ru.dom24x7.service.company;

/**
 * Created by timur on 09.04.17.
 * Данные по прокси
 */
public class ProxyMapItem {

    private String link;
    private int port;
    private boolean forbidden;

    public ProxyMapItem(String link) {

        this.link = link;
        port = 8080;
        forbidden = false;
    }

    public ProxyMapItem(String link, int port) {

        this.link = link;
        this.port = port;
        forbidden = false;
    }

    public ProxyMapItem(String link, int port, boolean forbidden) {

        this.link = link;
        this.port = port;
        this.forbidden = forbidden;
    }

    public String getLink() {

        return link;
    }

    public void setLink(String link) {

        this.link = link;
    }

    public int getPort() {

        return port;
    }

    public void setPort(int port) {

        this.port = port;
    }

    public boolean isForbidden() {

        return forbidden;
    }

    public void setForbidden(boolean forbidden) {

        this.forbidden = forbidden;
    }
}
