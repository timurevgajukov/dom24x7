package ru.dom24x7.model.meter.dao.interfaces;

import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterTariff;

import java.util.List;

/**
 * Created by timur on 25.06.17.
 * Интерфейс для работы с тарифами
 */
public interface MeterTariffDAO {

    /**
     * Возвращает список тарифов для счетчика
     * @param meter
     * @return
     */
    List<MeterTariff> list(Meter meter);

    /**
     * Сохранение тарифа для счетчика
     * @param meter
     * @param tariff
     * @return
     */
    boolean save(Meter meter, MeterTariff tariff);

    /**
     * Удаление тарифа для счетчика
     * @param tariff
     * @return
     */
    boolean delete(MeterTariff tariff);
}