<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="/resources/lib/cropper/2.2.5/cropper.min.css" rel="stylesheet" />
<link href="/resources/css/avatars.css" rel="stylesheet" />

<!-- CHANGE AVATAR MODAL WINDOW: BEGIN -->
<div id="avatar-modal" class="modal fade crop-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Загрузка картинки</h4>
            </div>
            <div class="modal-body">
                <div class="crop-avatar">
                    <form class="avatar-form" action="/user/profile/crop" enctype="multipart/form-data"
                          method="post">
                        <div class="avatar-body">

                            <!-- Upload image and data -->
                            <div class="avatar-upload">
                                <input type="hidden" class="avatar-src" name="avatar_src">
                                <input type="hidden" class="avatar-data" name="avatar_data">
                                <label for="avatarInput">Загрузить файл</label>
                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                            </div>

                            <!-- Crop and preview -->
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="avatar-wrapper"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-preview preview-lg"></div>
                                    <div class="avatar-preview preview-md"></div>
                                    <div class="avatar-preview preview-sm"></div>
                                </div>
                            </div>

                            <div class="row avatar-btns">
                                <div class="col-md-9"></div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary btn-block avatar-save">
                                        Сохранить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CHANGE AVATAR MODAL WINDOW: END -->

<script src="/resources/lib/cropper/2.2.5/cropper.min.js"></script>
<script src="/resources/js/avatar.js"></script>