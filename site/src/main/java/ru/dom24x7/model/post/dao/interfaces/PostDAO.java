package ru.dom24x7.model.post.dao.interfaces;

import ru.dom24x7.model.post.Category;
import ru.dom24x7.model.post.Post;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с постами пользователей
 */
public interface PostDAO {

    /**
     * Возвращает данные по посту
     * @param id
     * @return
     */
    Post get(int id);

    /**
     * Возвращает список постов для блога
     * @param offset
     * @param count
     * @return
     */
    List<Post> listForBlog(int offset, int count);

    /**
     * Возвращает список постов для блога с фильтрацией по категории
     * @param offset
     * @param count
     * @param category
     * @return
     */
    List<Post> listForBlog(int offset, int count, Category category);

    /**
     * Возвращает полный список постов для работы навигатора
     * @return
     */
    List<Post> listForNav();

    /**
     * Сохраняет пост
     *
     * @param post
     * @return
     */
    boolean save(Post post);

    /**
     * Удаление поста
     * @param post
     * @return
     */
    boolean delete(Post post);

    /**
     * Возвращает список постов
     * @param offset
     * @param count
     * @param search
     * @return
     */
    List<Post> list(int offset, int count, String search);

    /**
     * Возвращает количество постов с учетом фильтра
     * @param search
     * @return
     */
    int count(String search);
}