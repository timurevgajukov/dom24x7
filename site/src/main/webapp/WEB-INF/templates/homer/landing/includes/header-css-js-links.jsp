<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Vendor styles -->
<link rel="stylesheet" href="/resources/themes/homer/vendor/fontawesome/css/font-awesome.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/metisMenu/dist/metisMenu.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/animate.css/animate.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/bootstrap/dist/css/bootstrap.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/ladda/dist/ladda-themeless.min.css" />

<!-- App styles -->
<link rel="stylesheet" href="/resources/themes/homer/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
<link rel="stylesheet" href="/resources/themes/homer/fonts/pe-icon-7-stroke/css/helper.css" />
<link rel="stylesheet" href="/resources/themes/homer/styles/style.css">

<link rel="stylesheet" href="/resources/css/styles.css" />

<!-- jQuery -->
<script src="/resources/themes/homer/vendor/jquery/dist/jquery.min.js"></script>