package ru.dom24x7.model.user.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.cache.Roles;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.user.Role;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.interfaces.UserDAO;
import ru.dom24x7.utils.DBase;

import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 18.09.16.
 * <p>
 * Реализация интерфейса для работы с пользователями
 */
public class UserDAOImpl extends ModelDAOImpl implements UserDAO {

    private static final String QUERY_GET_BY_ID = "{ call users_get(?) }";
    private static final String QUERY_GET_BY_LOGIN = "{ call users_get_by_login(?) }";
    private static final String QUERY_GET_BY_EMAIL = "{ call users_get_by_email(?) }";
    private static final String QUERY_CONFIRM = "{ call users_confirm(?) }";
    private static final String QUERY_AUTH = "{ call users_auth(?,?) }";
    private static final String QUERY_LIST_BY_ADDRESS = "{ call users_list_by_kladr_id(?) }";
    private static final String QUERY_SAVE = "{ call users_save(?,?,?,?,?,?) }";
    private static final String QUERY_SAVE_EXTRA = "{ call users_save_extra(?,?,?,?) }";
    private static final String QUERY_SAVE_PHOTO = "{ call users_photo_save(?,?,?) }";
    private static final String QUERY_CLEAR = "{ call users_clear(?) }";
    private static final String QUERY_CHANGE_PASSWORD = "{ call users_change_password(?,?,?) }";
    private static final String QUERY_RECOVERY_PASSWORD = "{ call users_recovery_password(?,?) }";
    private static final String QUERY_LIST = "{ call users_list(?,?,?) }";
    private static final String QUERY_LIST_BY_ROLE = "{ call users_list_by_role(?) }";
    private static final String QUERY_COUNT = "{ call users_count(?) }";

    @Override
    public User get(int id) {

        List<User> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public User getByLogin(String login) {

        List<User> list = jdbcTemplate.query(QUERY_GET_BY_LOGIN, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, login);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public User getByEmail(String email) {

        List<User> list = jdbcTemplate.query(QUERY_GET_BY_EMAIL, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, email);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public User auth(String email, String password) {

        List<User> list = jdbcTemplate.query(QUERY_AUTH, new Object[]{email, password},
                new RowMapper<User>() {

                    @Override
                    public User mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public User confirm(String confirmCode) {

        List<User> list = jdbcTemplate.query(QUERY_CONFIRM, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, confirmCode);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public boolean save(User user, String password, String confirmCode) {

        User newUser = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        user.getId() == 0 ? null : user.getId(),
                        user.getLogin(),
                        user.getEmail(),
                        password,
                        confirmCode,
                        user.isSubscribed()
                }, new RowMapper<User>() {

                    @Override
                    public User mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        user.setId(newUser.getId());
        user.setRole(newUser.getRole());

        return true;
    }

    @Override
    public boolean saveExtra(User user) {

        return jdbcTemplate.update(QUERY_SAVE_EXTRA,
                new Object[]{
                        user.getId(),
                        user.getName(),
                        user.getSurname(),
                        user.getMidname()
                }) != 0;
    }

    @Override
    public boolean savePhoto(User user) {

        Photo photo = user.getPhoto();

        if (photo == null || photo.getResource() == null) {
            return false;
        }

        return jdbcTemplate.update(QUERY_SAVE_PHOTO,
                new Object[]{
                        user.getId(),
                        new ByteArrayInputStream(photo.getResource()),
                        photo.getFileName()}) != 0;
    }

    @Override
    public boolean clear(User user) {

        return jdbcTemplate.update(QUERY_CLEAR, new Object[]{user.getId()}) != 0;
    }

    @Override
    public boolean changePassword(User user, String oldPassword, String newPassword) {

        return jdbcTemplate.update(QUERY_CHANGE_PASSWORD, new Object[]{user.getId(), oldPassword, newPassword}) != 0;
    }

    @Override
    public boolean recoveryPassword(User user, String newPassword) {

        return jdbcTemplate.update(QUERY_RECOVERY_PASSWORD, new Object[]{user.getId(), newPassword}) != 0;
    }

    @Override
    public List<User> list(int offset, int count, String search) {

        List<User> list = jdbcTemplate.query(QUERY_LIST,
                new Object[]{
                        offset,
                        count,
                        DBase.getSearchForLike(search)
                }, new RowMapper<User>() {
                    @Override
                    public User mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<User> list(Role role) {

        List<User> list = jdbcTemplate.query(QUERY_LIST_BY_ROLE, new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, role.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public int count(String search) {

        return jdbcTemplate.queryForObject(QUERY_COUNT, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, DBase.getSearchForLike(search));
    }

    private User load(ResultSet rs) throws SQLException {

        User user = new User(rs.getInt("id"), rs.getString("login"), rs.getString("email"));
        user.setConfirmed(rs.getBoolean("is_confirmed"));
        user.setConfirmCode(rs.getString("confirm_code"));
        user.setCreateDt(rs.getTimestamp("create_dt"));
        user.setLastAuthDt(rs.getTimestamp("last_auth_dt"));
        user.setSubscribed(rs.getBoolean("is_subscribed"));

        int photoId = rs.getInt("photo_id");
        if (photoId != 0) {
            Photo photo = new Photo(photoId, rs.getString("file_name"), rs.getBlob("photo"));
            user.setPhoto(photo);
        }

        user.setRole(Roles.getInstance().get(rs.getInt("role_id")));

        // дополнительные данные
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setMidname(rs.getString("midname"));

        return user;
    }
}