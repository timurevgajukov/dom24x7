package ru.dom24x7.model.company.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.Region;
import ru.dom24x7.model.company.dao.interfaces.CompanyDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 * <p>
 * Реализация интерфейса для работы с компаниями сферы ЖКХ
 */
public class CompanyDAOImpl extends ModelDAOImpl implements CompanyDAO {

    private static final String QUERY_GET_BY_ID = "{ call companies_get_by_id(?) }";
    private static final String QUERY_GET_BY_INN = "{ call companies_get_by_inn(?) }";
    private static final String QUERY_GET_BY_GIS_ZKH_URL = "{ call companies_get_by_gis_zkh_url(?) }";
    private static final String QUERY_GET_BY_REFORMA_GKH_URL = "{ call companies_get_by_reforma_gkh_url(?) }";
    private static final String QUERY_LIST = "{ call companies_list() }";
    private static final String QUERY_LIST_WITH_FILTER = "{ call companies_list_with_filter(?,?,?) }";
    private static final String QUERY_LIST_BY_REGION = "{ call companies_list_by_region(?,?,?) }";
    private static final String QUERY_COUNT = "{ call companies_count(?) }";
    private static final String QUERY_COUNT_BY_REGION = "{ call companies_count_by_region(?) }";
    private static final String QUERY_SAVE = "{ call companies_save_extra(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call companies_delete(?) }";
    private static final String QUERY_HOLD = "{ call companies_hold(?) }";

    @Override
    public Company get(int id) {

        List<Company> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Company>() {

            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public Company getByINN(String inn) {

        List<Company> list = jdbcTemplate.query(QUERY_GET_BY_INN, new RowMapper<Company>() {

            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, inn);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public Company getByReformaGkhUrl(String url) {

        List<Company> list = jdbcTemplate.query(QUERY_GET_BY_REFORMA_GKH_URL, new RowMapper<Company>() {

            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, url);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public Company getByGisZkhUrl(String url) {

        List<Company> list = jdbcTemplate.query(QUERY_GET_BY_GIS_ZKH_URL, new RowMapper<Company>() {

            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, url);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<Company> list() {

        List<Company> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Company>() {

            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Company> list(int offset, int count, String search) {

        if (search == null || search.trim().length() == 0) {
            search = null;
        } else {
            search = "%" + search.trim() + "%";
        }

        List<Company> list = jdbcTemplate.query(QUERY_LIST_WITH_FILTER, new Object[]{offset, count, search}, new RowMapper<Company>() {
            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Company> list(Region region, int offset, int count) {

        List<Company> list = jdbcTemplate.query(QUERY_LIST_BY_REGION,
                new Object[]{region.getId(), offset, count}, new RowMapper<Company>() {
                    @Override
                    public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public int count(String search) {

        if (search == null || search.trim().length() == 0) {
            search = null;
        } else {
            search = "%" + search.trim() + "%";
        }

        return jdbcTemplate.queryForObject(QUERY_COUNT, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, search);
    }

    @Override
    public int count(Region region) {

        return jdbcTemplate.queryForObject(QUERY_COUNT_BY_REGION, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, region.getId());
    }

    @Override
    public boolean save(Company company) {

        Company newCompany = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        company.getId() == 0 ? null : company.getId(),
                        company.getName(),
                        company.getFullName(),
                        company.getAddress() == null || company.getAddress().trim().length() == 0 ? null : company.getAddress().trim(),
                        company.getPhone() == null || company.getPhone().trim().length() == 0 ? null : company.getPhone().trim(),
                        company.getEmail() == null || company.getEmail().trim().length() == 0 ? null : company.getEmail().trim(),
                        company.getSite() == null || company.getSite().trim().length() == 0 ? null : company.getSite().trim(),
                        company.isHold(),
                        company.getDirectorPost() == null || company.getDirectorPost().trim().length() == 0 ? null : company.getDirectorPost().trim(),
                        company.getDirectorFIO() == null || company.getDirectorFIO().trim().length() == 0 ? null : company.getDirectorFIO().trim(),
                        company.getCoordLat(),
                        company.getCoordLng(),
                        company.getInn() == null || company.getInn().trim().length() == 0 ? null : company.getInn().trim(),
                        company.getOgrn() == null || company.getOgrn().trim().length() == 0 ? null : company.getOgrn().trim(),
                        company.getOpeningHours() == null || company.getOpeningHours().trim().length() == 0 ? null : company.getOpeningHours().trim(),
                        company.getGisZkhUrl() == null || company.getGisZkhUrl().trim().length() == 0 ? null : company.getGisZkhUrl().trim(),
                        company.getGisZkhParseDt(),
                        company.getReformaGkhUrl() == null || company.getReformaGkhUrl().trim().length() == 0 ? null : company.getReformaGkhUrl().trim(),
                        company.getReformaGkhParseDt()
                }, new RowMapper<Company>() {

                    @Override
                    public Company mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        company.setId(newCompany.getId());

        return true;
    }

    @Override
    public boolean delete(Company company) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{company.getId()}) != 0;
    }

    @Override
    public boolean hold(Company company) {

        return jdbcTemplate.update(QUERY_HOLD, new Object[]{company.getId()}) != 0;
    }

    private Company load(ResultSet rs) throws SQLException {

        Company company = new Company(rs.getInt("id"), rs.getString("name"));
        company.setFullName(rs.getString("full_name"));
        company.setAddress(rs.getString("address"));
        company.setPhone(rs.getString("phone"));
        company.setEmail(rs.getString("email"));
        company.setSite(rs.getString("site"));
        company.setHold(rs.getBoolean("hold"));
        company.setDirectorPost(rs.getString("director_post"));
        company.setDirectorFIO(rs.getString("director_fio"));
        Double coordLat = rs.getDouble("coord_lat");
        if (!rs.wasNull()) {
            company.setCoordLat(coordLat);
        }
        Double coordLng = rs.getDouble("coord_lng");
        if (!rs.wasNull()) {
            company.setCoordLng(coordLng);
        }
        company.setInn(rs.getString("inn"));
        company.setOgrn(rs.getString("ogrn"));
        company.setOpeningHours(rs.getString("opening_hours"));
        company.setGisZkhUrl(rs.getString("gis_zkh_url"));
        company.setGisZkhParseDt(rs.getTimestamp("gis_zkh_parse_dt"));
        company.setReformaGkhUrl(rs.getString("reforma_gkh_url"));
        company.setReformaGkhParseDt(rs.getTimestamp("reforma_gkh_parse_dt"));

        return company;
    }
}