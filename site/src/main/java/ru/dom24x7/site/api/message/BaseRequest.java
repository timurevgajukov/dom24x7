package ru.dom24x7.site.api.message;

/**
 * Created by timur on 22.02.17.
 * API: базовый класс запроса
 */
public class BaseRequest {

    private BaseHeader header;
    private BaseBody body;

    public BaseHeader getHeader() {

        return header;
    }

    public void setHeader(BaseHeader header) {

        this.header = header;
    }

    public BaseBody getBody() {

        return body;
    }

    public void setBody(BaseBody body) {

        this.body = body;
    }
}