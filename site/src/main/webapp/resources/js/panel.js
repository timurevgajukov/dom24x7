/**
 * Created by Тимур on 19.06.2015.
 *
 * Основной файл со скриптами
 */

function showAlert(body, type) {
    toastr[type](body);
}

/**
 * Отображение уведомления для панели
 * @param panelId
 * @param message
 * @param timer
 * @param type
 */
function showPanelAlert(panelId, message, color, timer, type) {
    type = type || 'alert-danger';

    var panel = $('#' + panelId);
    var panelAlert = $('#' + panelId + '-alert');
    var panelAlertMsg = $('#' + panelId + '-alert-message');

    panelAlert.addClass(type);

    if (color) {
        panel.removeClass(color);
        panel.addClass('hred');
    }

    panelAlertMsg.html(message);
    panelAlert.show();

    if (timer) {
        setTimeout(function () {
            if (color) {
                panel.removeClass('hred');
                panel.addClass(color);
            }
            panelAlert.hide();
            panelAlert.removeClass(type);
            panelAlertMsg.empty();
        }, timer);
    }
}

/**
 * Установка для двух div блоков одинаковой высоты по максимальной из них
 * @param block1
 * @param block2
 * @param correction коррекция относительно максимального
 */
function resizeBlocks(block1, block2, correction) {
    correction = correction || 0;

    var block1Height = block1.height();
    var block2Height = block2.height();
    if (block1Height > block2Height) {
        block2.height(block1Height + correction);
    } else {
        block1.height(block2Height + correction);
    }
}

jQuery(function ($) {
    $.extend({
        form: function (url, data, method) {
            if (method == null) method = 'POST';
            if (data == null) data = {};

            var form = $('<form>').attr({
                method: method,
                action: url
            }).css({
                display: 'none'
            });

            var addData = function (name, data) {
                if ($.isArray(data)) {
                    for (var i = 0; i < data.length; i++) {
                        var value = data[i];
                        addData(name + '[]', value);
                    }
                } else if (typeof data === 'object') {
                    for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                            addData(name + '[' + key + ']', data[key]);
                        }
                    }
                } else if (data != null) {
                    form.append($('<input>').attr({
                        type: 'hidden',
                        name: String(name),
                        value: String(data)
                    }));
                }
            };

            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    addData(key, data[key]);
                }
            }

            return form.appendTo('body');
        }
    });
});