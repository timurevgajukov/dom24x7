package ru.dom24x7.model.access;

import ru.dom24x7.model.Model;

/**
 * Created by timur on 14.02.17.
 *
 * Функция или раздел сайта
 */
public class Function extends Model {

    private String description;

    public Function(int id) {

        super(id);
    }

    public Function(int id, String name, String description) {

        super(id, name);
        this.description = description;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }
}