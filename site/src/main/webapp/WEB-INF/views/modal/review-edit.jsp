<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- EDIT REVIEW MODAL WINDOW: BEGIN -->
<div id="review-edit-modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Написать отзыв</h4>
            </div>
            <form method="post" action="/companies/${company.id}/rating/save">
                <div class="modal-body">
                    <div class="form-group">
                        <textarea name="review" class="form-control" placeholder="Как вам работа управляющей компании?" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <input id="modal-rating" name="mark" class="rating-loading" data-size="xs" />
                        </div>
                        <div class="col-md-6">
                            <button id="btn-save-review" class="btn btn-success" type="submit">Сохранить</button>
                            <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT REVIEW MODAL WINDOW: END -->

<script>
    $(document).ready(function () {
        $("#modal-rating").rating({showCaption: false, showClear: false, step: 1});
    });
</script>