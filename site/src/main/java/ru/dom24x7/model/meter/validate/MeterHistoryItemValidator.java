package ru.dom24x7.model.meter.validate;

import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.security.Message;
import ru.dom24x7.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 24.09.16.
 *
 * Валидатор данных по показанию счетчика
 */
public class MeterHistoryItemValidator {

    private MeterHistoryItem item;
    private List<Message> errors;

    public MeterHistoryItemValidator(MeterHistoryItem item) {

        this.item = item;
    }

    public List<Message> getErrors() {

        return errors;
    }

    public boolean validate() {

        // должен быть привязан к счетчику
        if (item.getMeter() == null) {
            addError("К показанию должен быть привязан счетчик", Message.TYPE_DANGER);
        }

        // дата должна быть указана
        if (item.getDate() == null) {
            addError("Необходимо указать дату, когда были сняты показания", Message.TYPE_DANGER);
        }

        // дата не должна быть в будущем
        if (DateUtils.after(item.getDate(), new Date())) {
            addError("Дата не должна быть в будущем времени", Message.TYPE_DANGER);
        }

        // должна быть указано значение
        if (item.getMeter() != null) {
            Double value = null;
            if (item.getMeter().getType().isWithoutValue()) {
                value = item.getCost();
            } else {
                value = item.getValue();
            }
            if (value == null) {
                addError("Необходимо указать показание счетчика", Message.TYPE_DANGER);
            } else {
                // показание счетчика не должно быть отрицательным
                if (value < 0.0) {
                    addError("Показания счетчика не могут быть отрицательным", Message.TYPE_DANGER);
                }
            }
        }

        return errors == null || errors.size() == 0;
    }

    private void addError(String message, String type) {

        if (errors == null) {
            errors = new ArrayList<Message>();
        }

        errors.add(new Message(message, type));
    }
}