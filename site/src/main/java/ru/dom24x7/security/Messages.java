package ru.dom24x7.security;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 15.03.17.
 * Инкапсулирует в себе списки сообщений и работу с ними
 */
public class Messages {

    private List<Message> messages;

    {
        messages = new ArrayList<Message>();
    }

    public int count() {

        if (messages == null || messages.size() == 0) {
            return 0;
        } else {
            return messages.size();
        }
    }

    /**
     * Возвращает копию списка сообщений
     *
     * @return
     */
    public List<Message> list() {

        return new ArrayList<Message>(messages);
    }

    /**
     * Возвращает копию списка сообщений с очисткой
     *
     * @return
     */
    public List<Message> listWithClear() {

        List<Message> messages = new ArrayList<Message>(this.messages);
        clear();

        return messages;
    }

    public boolean has() {

        return messages.size() != 0;
    }

    public void add(Message message) {

        messages.add(message);
    }

    public void add(String message, String type) {

        add(new Message(message, type));
    }

    /**
     * Формирует список различных сообщений с одним типом
     *
     * @param messages
     * @param type
     */
    public void add(List<String> messages, String type) {

        if (messages == null || messages.size() == 0) {
            return;
        }

        for (String message : messages) {
            add(new Message(message, type));
        }
    }

    /**
     * Очистка сообщений
     */
    public void clear() {

        messages.clear();
    }
}