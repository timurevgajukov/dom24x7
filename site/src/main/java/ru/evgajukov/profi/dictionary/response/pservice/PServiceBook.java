package ru.evgajukov.profi.dictionary.response.pservice;

import ru.evgajukov.profi.dictionary.response.Book;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class PServiceBook extends Book {

    private List<PServiceItem> data;

    public List<PServiceItem> getData() {

        return data;
    }

    public void setData(List<PServiceItem> data) {

        this.data = data;
    }
}
