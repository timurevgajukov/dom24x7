package ru.evgajukov.profi.profile.response.entry.definition.geo;

/**
 * Created by timur on 17.06.17.
 */
public class Translation {

    private String lang;
    private Value value;

    public String getLang() {

        return lang;
    }

    public void setLang(String lang) {

        this.lang = lang;
    }

    public Value getValue() {

        return value;
    }

    public void setValue(Value value) {

        this.value = value;
    }

    @Override
    public String toString() {

        return lang + ": " + value;
    }
}