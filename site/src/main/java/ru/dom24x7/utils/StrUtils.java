package ru.dom24x7.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Работа со строками и строковыми представлениями
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class StrUtils {
	
	/**
	 * Из потока получить строку
	 * @param is
	 * @return
	 */
	public static String getStringFromInputStream(InputStream is) {
	 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
	 
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		return sb.toString();
	}

	/**
	 * формат отображения числа
	 *
	 * @param value значение
	 * @param capacity разрядность целой чисти
	 * @param precision точность дробной части
	 * @return
	 */
	public static String format(double value, int capacity, int precision) {
		
		DecimalFormat format = null;
		
		String pattern = "";
		if (capacity > 0) {
			pattern += copy("0", capacity);
		}
		if (precision > 0) {
			pattern += "." + copy("0", precision);
		}
		
		if (pattern.length() > 0) {
			format = new DecimalFormat(pattern);
		} else {
			format = new DecimalFormat();
		}
		
		return format.format(value);
	}
	
	/**
	 * Возвращает отформатированную сумму в рублях
	 * @param amount
	 */
	public static String getStrAmount(double amount, String currency) {
		
		return String.format("%.2f <span class='rouble'>%s</span>", amount, currency);
	}
	
	/**
	 * Возвращает строку, состоящую из symbol, повторенных n раз
	 * 
	 * @param symbol
	 * @param n
	 * @return
	 */
	public static String copy(String symbol, int n) {
		
		StringBuilder result = new StringBuilder(symbol);
		for (int i = 1; i < n; i++) {
			result.append(symbol);
		}
		
		return result.toString();
	}
	
	/**
	 * Показывает, что строка пустая
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isEmpty(String value) {
    	
    	return (value == null || value.trim().length() == 0);
    }

    public static String join(String separator, List<String> list) {

		boolean first = true;
		String result = "";
		for (String item : list) {
			if (first) {
				result += item;
				first = !first;
			} else {
				result += separator + item;
			}
		}

		return result;
	}
}
