<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        <div class="pull-right">
            <button id="btn-address-edit" class="btn btn-default btn-xs" data-toggle="modal" data-target="#address-edit-modal"
                    data-id="${address.id}" data-tooltip="true" data-original-title="Редактировать адрес">
                <i class="fa fa-pencil"></i>
            </button>
        </div>
        <span id="address-title">${address}</span>
    </div>
    <div class="panel-body">
        <p id="address-address">${address.address}</p>
    </div>
</div>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        <div class="pull-right">
            <button id="btn-meter-add" class="btn btn-default btn-xs" data-toggle="modal" data-target="#meter-edit-modal"
                    data-address="${address.id}" data-tooltip="true" data-original-title="Добавить счетчик">
                <i class="fa fa-plus"></i>
            </button>
        </div>
        Счетчики
    </div>
    <c:if test="${meters != null && meters.size() != 0}">
        <div class="panel-body">
            <div class="list-group bord-no">
                <c:forEach items="${meters}" var="meter">
                    <a href="/panel/addresses/${address.id}/meters/${meter.id}" class="list-group-item">
                        <div class="row">
                            <div class="col-sm-9">
                                <table>
                                    <tr>
                                        <td width="90px"><img src="${meter.icon.bigUrl}" width="80" height="80" /></td>
                                        <td valign="top">
                                            <h4>
                                                ${meter}
                                                <small>
                                                    <a href="#" class="btn-add-value"
                                                       data-address="${address.id}" data-meter="${meter.id}"
                                                       data-meter-title="${meter.title}" data-meter-unit="${meter.type.unit}"
                                                       data-meter-type-without-value="${meter.type.withoutValue}"
                                                       data-currency="${address.currency}"
                                                       data-toggle="modal" data-target="#meter-value-edit-modal"
                                                       data-tooltip="true" data-original-title="Добавить показание счетчика">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </small>
                                            </h4>
                                            <h3 class="text-bold">${meter.stringLastValue}</h3>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-3 text-right">
                                <h4>${meter.stringTariff}</h4>
                                <h3 class="text-bold text-primary">${meter.totalLastMonth}</h3>
                            </div>
                        </div>
                    </a>
                </c:forEach>
                <div class="list-group-item">
                    <div class="row">
                        <div class="col-sm-9 text-right">
                            <h3>Итого за месяц</h3>
                        </div>
                        <div class="col-sm-3 text-right">
                            <h3 class="text-bold text-primary">${address.totalLastMonth}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:if>
</div>

<t:insertAttribute name="meter-edit" defaultValue="/WEB-INF/views/modal/meter-edit.jsp" />
<t:insertAttribute name="meter-value-edit" defaultValue="/WEB-INF/views/modal/meter-value-edit.jsp" />