package ru.dom24x7.model.email;

import ru.dom24x7.model.Model;

/**
 * Created by timur on 17.03.17.
 * Приоритет почтового сообщения
 */
public class EmailMessagePriority extends Model {

    public static final int PRIORITY_HI = 1;
    public static final int PRIORITY_ME = 2;
    public static final int PRIORITY_LO = 3;

    public EmailMessagePriority(int id, String name) {

        super(id, name);
    }
}