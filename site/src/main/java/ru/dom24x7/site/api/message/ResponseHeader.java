package ru.dom24x7.site.api.message;

/**
 * Created by timur on 22.02.17.
 * API: заголовок для ответа
 */
public class ResponseHeader extends BaseHeader {

    private int code;
    private String description;

    public ResponseHeader() {

        super();
    }

    public ResponseHeader(int code, String description, String method, String token, String session) {

        super(method, token, session);
        this.code = code;
        this.description = description;
    }

    public int getCode() {

        return code;
    }

    public ResponseHeader setCode(int code) {

        this.code = code;

        return this;
    }

    public String getDescription() {

        return description;
    }

    public ResponseHeader setDescription(String description) {

        this.description = description;

        return this;
    }
}