package ru.dom24x7.model.application.dao.interfaces;

import ru.dom24x7.model.application.Application;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы приложений по api
 */
public interface ApplicationDAO {

    /**
     * Возвращает приложение по токену
     *
     * @param token
     * @return
     */
    Application get(String token);

    /**
     * Сохранение приложения
     *
     * @param application
     * @return
     */
    boolean save(Application application);
}