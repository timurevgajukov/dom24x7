<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Dom24x7 - коммунальный сервис</h1><p>Теперь управлять своим домом стало легко.</p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">Вы используете <strong>устаревший</strong> браузер. Необходимо <a href="http://browsehappy.com/">обновить свой браузер</a> для корректной работы сервиса.</p>
<![endif]-->