package ru.evgajukov.profi.profile.request;

import com.google.gson.Gson;
import ru.evgajukov.profi.profile.request.filter.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 15.06.17.
 * Класс запроса на получение списка профилей
 */
public class ProfileRequest {

    private Filter filter;
    private Bound bound;
    private Client client;
    private List<InBound> inbound;

    public Filter getFilter() {

        return filter;
    }

    public void setFilter(Filter filter) {

        this.filter = filter;
    }

    public Bound getBound() {

        return bound;
    }

    public void setBound(Bound bound) {

        this.bound = bound;
    }

    public Client getClient() {

        return client;
    }

    public void setClient(Client client) {

        this.client = client;
    }

    public List<InBound> getInbound() {

        return inbound;
    }

    public void setInbound(List<InBound> inbound) {

        this.inbound = inbound;
    }

    public ProfileRequest addInbound(InBound item) {

        if (this.inbound == null) {
            this.inbound = new ArrayList<>();
        }

        this.inbound.add(item);
        return this;
    }

    @Override
    public String toString() {

        Gson gson = new Gson();
        return gson.toJson(this);
    }
}