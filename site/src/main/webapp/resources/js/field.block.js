/**
 * Created by timur on 25.12.16.
 * Составное поле
 */
function BlockField(options) {
    var self = this;

    this._settings = $.extend({
        document: null, // документ, который пришел с сервера
        data: null,
        guid: null // GUID формы
    }, options);

    this._blockGUID = null; // GUID блока
    this._currentBlockInd = null; // индекс текущего блока
    this._countBlocks = 0; // общее количество блоков

    this._blocks = []; // массив блоков с объектами полей

    this._html = null;

    this._methods = {
        /**
         * Инициализация статичного поля
         */
        init: function () {
            self._blockGUID = self._methods.guid();
            var data = self._settings.data;

            // если имеются заполненные поля, то нужно создать необходимое количество блоков и заполнить в них поля
            self._methods.loadBlocks();

            var elem = '';

            if (data.collapsable) {
                elem += '<div class="panel-group">';
            }
            elem += '<div class="panel panel-default">';
            elem += '<div class="panel-heading">';
            elem += '<h4 class="panel-title">';

            var countInfo = '';
            if (self._settings.data.multiPage) {
                countInfo = '<span id="block-count-info-' + self._blockGUID + '">' + self._methods.showCountInfo() + '</span>';
            }

            if (data.collapsable) {
                elem += '<a data-toggle="collapse" href="#block-' + self._blockGUID + '">' + data.title +
                    ' ' + countInfo + ' <i id="caret-' + self._blockGUID + '" class="fa fa-caret-down"></i></a>';

                // отслеживаем сворачивание/разворачивание блока
                $(document).off('show.bs.collapse', '#block-' + self._blockGUID)
                    .on('show.bs.collapse', '#block-' + self._blockGUID, function () {
                        $('#caret-' + self._blockGUID).removeClass('fa-caret-right');
                        $('#caret-' + self._blockGUID).addClass('fa-caret-down');
                    });
                $(document).off('hide.bs.collapse', '#block-' + self._blockGUID)
                    .on('hide.bs.collapse', '#block-' + self._blockGUID, function () {
                        $('#caret-' + self._blockGUID).removeClass('fa-caret-down');
                        $('#caret-' + self._blockGUID).addClass('fa-caret-right');
                    });
            } else {
                elem += data.title + ' ' + countInfo;
            }
            elem += '</h4>';
            elem += '</div>'; // .panel-heading
            if (data.collapsable) {
                var collapse = ' collapse';
                if (!data.collapsed) {
                    collapse += ' in';
                }
                elem += '<div id="block-' + self._blockGUID + '" class="panel-collapse' + collapse + '" data-index="0" data-count="0">';
            }
            elem += '<div class="panel-body">';

            // формируем поля
            elem += self._methods.createFields();

            // формируем элементы управления
            elem += self._methods.createControls();

            elem += '</div>'; // .panel-body
            if (data.collapsable) {
                elem += '</div>'; // .panel-collapse
            }
            elem += '</div>'; // .panel
            if (data.collapsable) {
                elem += '</div>'; // .panel-group
            }

            self._html = elem;
        },
        /**
         * Формирует визуальное представление полей блока
         */
        createFields: function () {
            var fields = self._settings.data.fields;
            if (fields != null && fields.length != 0) {
                // имеется хотя бы одно поле в блочном элементе
                // проверяем количество блоков по количеству заполненных значений первого поля
                self._methods.setCountBlocks(fields[0].values.length);
                var fieldsetDisabled = '';
                if (self._countBlocks == 0) {
                    // пока нет ни одного блока
                    fieldsetDisabled = ' disabled';
                }

                var fieldsBlock = '<fieldset id="block-fieldset-' + self._blockGUID + '"' + fieldsetDisabled + '>';
                fieldsBlock += self._methods.fieldsHtml();
                fieldsBlock += '</fieldset>';

                return fieldsBlock;
            }
        },
        /**
         * Формирует объекты полей блока и возвращает их массив
         */
        fields: function (index) {
            index = index || self._currentBlockInd;
            var result  = [];
            var fields = self._settings.data.fields;
            for (var i = 0; i < fields.length; i++) {
                var field = new FormGenerator().field({
                    document: self._settings.document,
                    data: fields[i],
                    guid: self._settings.guid,
                    index: index,
                    block: self
                });
                result.push(field);
            }

            return result;
        },
        /**
         * Возвращает html представления блока полей
         */
        fieldsHtml: function (fields) {
            fields = fields || self._methods.fields();

            var result = '';
            for (var i = 0; i < fields.length; i++) {
                result += fields[i].html();
            }

            return result;
        },
        /**
         * Формирует кнопки управления
         */
        createControls: function () {
            var controlsBlock = '';
            if (self._settings.data.multiPage) {
                // блок мультистраничный и нужно установить соответствующие кнопки управления
                // ===== добавляем кнопки перемещения по блокам =====
                var blocksCount = self._settings.data.fields[0].values.length;
                if ('view' != self._settings.status || blocksCount > 1) {
                    var disabled = ' disabled';
                    if (blocksCount > 1) {
                        // кнопки навигации блока должны быть разблокированы
                        disabled = '';
                    }

                    controlsBlock = '<div class="row">' +
                        '<div class="col-sm-6">' +
                        '<button id="btn-block-left-' + self._blockGUID + '" class="btn border small" type="button"' + disabled + '><i class="fa fa-long-arrow-left"></i></button> ' +
                        '<button id="btn-block-right-' + self._blockGUID + '" class="btn border small" type="button"' + disabled + '><i class="fa fa-long-arrow-right"></i></button>' +
                        '</div>';

                    // добавляем обработчики для кнопок перемещения по блокам
                    $(document).on('click', '#btn-block-left-' + self._blockGUID, function () {
                        console.log('Отобразить предыдущий блок');

                        if (self._countBlocks < 2) {
                            // блоков пока нет или всего один, поэтому ничего не делаем
                            return;
                        }

                        if (self._currentBlockInd == 0) {
                            // мы находимся на первом блоке и предыдущего нет, поэтому ничего не делаем
                            return;
                        }

                        self._methods.setCurrentBlockInd(self._currentBlockInd - 1);

                        // отображаем данные из текущего блока
                        var fields = self._blocks[self._currentBlockInd];
                        for (var i = 0; i < fields.length; i++) {
                            fields[i].draw();
                        }
                    });
                    $(document).on('click', '#btn-block-right-' + self._blockGUID, function () {
                        console.log('Отобразить средующий блок');

                        if (self._countBlocks < 2) {
                            // блоков пока нет или всего один, поэтому ничего не делаем
                            return;
                        }

                        if (self._currentBlockInd == self._countBlocks - 1) {
                            // мы находимся на последнем блоке и следующего нет, поэтому ничего не делаем
                            return;
                        }

                        self._methods.setCurrentBlockInd(self._currentBlockInd + 1);

                        // отображаем данные из текущего блока
                        var fields = self._blocks[self._currentBlockInd];
                        for (var i = 0; i < fields.length; i++) {
                            fields[i].draw();
                        }
                    });
                }

                // ===== добавляем кнопки добавления и удаления блоков =====
                if ('view' != self._settings.status) {
                    // только если мы не в режиме просмотра
                    controlsBlock += '<div class="col-sm-6 text-right">' +
                        '<button id="btn-block-add-' + self._blockGUID + '" class="btn small" type="button" data-guid="' + self._blockGUID + '">Добавить</button> ' +
                        '<button id="btn-block-del-' + self._blockGUID + '" class="btn red small" type="button" data-guid="' + self._blockGUID + '" disabled>Удалить</button>' +
                        '</div>';

                    // добавляем обработчик кнопки добавления нового блока в конец
                    $(document).on('click', '#btn-block-add-' + self._blockGUID, function () {
                        console.log('Добавление нового блока в конец');

                        var blockGUID = $(this).data('guid');

                        self._methods.setCurrentBlockInd(self._countBlocks);
                        self._methods.setCountBlocks(self._countBlocks + 1);

                        if (self._countBlocks != 0) {
                            // есть хотя бы один блок
                            $('#block-fieldset-' + blockGUID).removeAttr('disabled');

                            if (self._countBlocks > 1) {
                                // необходимо разблокировать кнопки навигации
                                $('#btn-block-left-' + self._blockGUID).removeAttr('disabled');
                                $('#btn-block-right-' + self._blockGUID).removeAttr('disabled');
                            }
                        }

                        // формируем и сохраняем массив объектов полей для нового блока
                        var fields = self._methods.fields();
                        self._blocks.push(fields);
                        $('#block-fieldset-' + blockGUID).empty();
                        $('#block-fieldset-' + blockGUID).append(self._methods.fieldsHtml(fields));

                        // инициализируем все поля блока
                        for (var i = 0; i < fields.length; i++) {
                            fields[i].draw();
                        }
                    });
                    // добавляем обработчик кнопки удаления текущего блока
                    $(document).on('click', '#btn-block-del-' + self._blockGUID, function () {
                        console.log('Удаление текущего блока');

                        var guid = $(this).data('guid');
                        var block = $('#block-' + guid);
                        var index = block.data('index');
                        var count = block.data('count');

                        if (count == 1) {
                            // остался только один блок и его не нужно удалять
                            alert('Нельзя удалить единственный блок');
                        } else {
                            // TODO: можем удалить
                        }
                    });
                }
                controlsBlock += '</div>';
            } else {
                // если необходимо (могли прийти данные и блок ранее уже созда), сразу создаем один блок и работаем уже с ним
                var fields = self._methods.fields();
                if (self._blocks.length == 0) {
                    // формируем и сохраняем массив объектов полей для нового блока
                    self._blocks.push(fields);

                    self._methods.setCurrentBlockInd(0);
                    self._methods.setCountBlocks(1);
                }
                $('#block-fieldset-' + self._blockGUID).empty();
                $('#block-fieldset-' + self._blockGUID).append(self._methods.fieldsHtml(fields));

                // инициализируем все поля блока
                for (var i = 0; i < fields.length; i++) {
                    fields[i].draw();
                }
            }

            return controlsBlock;
        },
        /**
         * Для режима просмотра загружает во внутреннюю структуру блоки с данными
         */
        loadBlocks: function () {
            for (var i = 0; i < self._settings.data.fields[0].values.length; i++) {
                self._blocks.push(self._methods.fields(i));
            }
            self._methods.setCurrentBlockInd(0);
            self._methods.setCountBlocks(self._blocks.length);
        },
        /**
         * Устанавливает индекс текущего блока
         * @param index
         */
        setCurrentBlockInd: function (index) {
            self._currentBlockInd = index;
            self._methods.showCountInfo();
        },
        /**
         * Устанавливает общее количество блоков
         * @param count
         */
        setCountBlocks: function (count) {
            self._countBlocks = count;
            self._methods.showCountInfo();
        },
        /**
         * Если необходимо, то отображает в заголовке блока информацию по количеству и текущему блоку
         */
        showCountInfo: function () {
            var span = $('#block-count-info-' + self._blockGUID);
            span.empty();

            if (self._countBlocks == 0) {
                return '';
            }

            var info = '(' + (self._currentBlockInd + 1) + ' из ' + self._countBlocks + ')';
            span.html(info);

            return info;
        },

        /**
         * Генерация уникального кода
         * @returns {string}
         */
        guid: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + s4() + s4();
        }
    };

    this._methods.init();
}

/**
 * Возвращает поле в мультиблоке
 * @param index
 * @param name
 */
BlockField.prototype.field = function (index, name) {
    var fields = this._blocks[index];
    if (fields == null) {
        return null;
    }

    for (var i = 0; i < fields.length; i++) {
        if (name == fields[i]._settings.data.name) {
            return fields[i];
        }
    }

    return null;
};

/**
 * Возвращает html представление поля
 */
BlockField.prototype.html = function () {
    return this._html;
};

/**
 * Возвращает признак, что поле в блоке было изменено
 * @returns {boolean}
 */
BlockField.prototype.isChanged = function () {
    for (var i = 0; i < this._blocks.length; i++) {
        for (var j = 0; j < this._blocks[i].length; j++) {
            if (this._blocks[i][j].isChanged()) {
                return true;
            }
        }
    }
    return false;
};

/**
 * Оцищает признак изменения для всех полей блока
 */
BlockField.prototype.clearChanged = function () {
    for (var i = 0; i < this._blocks.length; i++) {
        for (var j = 0; j < this._blocks[i].length; j++) {
            this._blocks[i][j].clearChanged();
        }
    }
};

/**
 * Валидация блока
 * @returns {*}
 */
BlockField.prototype.validate = function () {
    var errors = [];
    var status = true;
    for (var i = 0; i < this._blocks.length; i++) {
        for (var j = 0; j < this._blocks[i].length; j++) {
            var result = this._blocks[i][j].validate();
            if (!result.status) {
                status = false;
                errors.push(result);
            }
        }
    }

    if (status) {
        return {
            status: true,
            description: 'OK'
        }
    } else {
        var messages = 'Не все поля блоков "' + this._settings.data.title + '" прошли валидацию:';
        messages += '<ul>';
        for (var i = 0; i < errors.length; i++) {
            messages += '<li>' + errors[i].description + '</li>';
        }
        messages += '</ul>';
        return {
            status: false,
            description: messages,
            fields: errors
        }
    }
};

/**
 * Возвращает список блоков в мультиблоке
 */
BlockField.prototype.blocks = function () {
    return this._blocks;
};

/**
 * Возвращает список полей блока
 * @param index
 */
BlockField.prototype.fields = function (index) {
    return this._blocks[index];
};

/**
 * Разблокирует блок
 */
BlockField.prototype.enabled = function () {
    $('#block-fieldset-' + this._blockGUID).removeAttr('disabled');
};