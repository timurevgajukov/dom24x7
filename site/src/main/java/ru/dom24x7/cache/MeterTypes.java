package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.meter.MeterType;
import ru.dom24x7.model.meter.dao.MeterTypeDAOImpl;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Контейнер для хранения справочника типов счетчиков
 */
public class MeterTypes implements ICache<MeterType> {

    private static MeterTypes instance = null;

    private List<MeterType> list;

    public static MeterTypes getInstance() {

        if (instance == null) {
            instance = new MeterTypes();
        }

        return instance;
    }

    private MeterTypes() {

        list = new MeterTypeDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<MeterType> list() {

        return list;
    }

    @Override
    public MeterType get(int id) {

        if (list == null) {
            return null;
        }

        for (MeterType item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}