package ru.evgajukov.profi.profile.response.entry.definition.geo;

/**
 * Created by timur on 17.06.17.
 */
public class Line {

    private String id;
    private String name;
    private String color;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getColor() {

        return color;
    }

    public void setColor(String color) {

        this.color = color;
    }

    @Override
    public String toString() {

        return id;
    }
}