package ru.dom24x7.model.lawyer.dao.interfaces;

import ru.dom24x7.model.lawyer.LawyerMessage;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с сообщениями для юристов
 */
public interface LawyerMessageDAO {

    /**
     * Возвращает вопрос пользователя
     * @param id
     * @param user
     * @return
     */
    LawyerMessage get(int id, User user);

    /**
     * Возвращает список сообщений пользователей юристам
     * @param offset
     * @param count
     * @param search
     * @return
     */
    List<LawyerMessage> list(int offset, int count, String search);

    /**
     * Возвращает список сообщений конкретного пользователя юристам
     * @param user
     * @param offset
     * @param count
     * @param search
     * @return
     */
    List<LawyerMessage> list(User user, int offset, int count, String search);

    /**
     * Общее количество сообщений юристам
     * @param search
     * @return
     */
    int count(String search);

    /**
     * Сохранение сообщения для юристов
     *
     * @param message
     * @return
     */
    boolean save(LawyerMessage message);
}