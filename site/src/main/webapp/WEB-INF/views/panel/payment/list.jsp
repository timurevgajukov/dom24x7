<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<button id="btn-payment-add" class="btn btn-primary" data-toggle="modal" data-target="#payment-edit-modal">Добавить</button><br /><br />

<div id="payments" class="row masonry" data-columns></div>

<t:insertAttribute name="payment-edit" defaultValue="/WEB-INF/views/modal/payment-edit.jsp" />
<t:insertAttribute name="payment-show" defaultValue="/WEB-INF/views/modal/payment-show.jsp" />

<script type="text/html" id="tmpl-payment">
    <div class="item">
        <div class="hpanel blog-box">
            <div class="panel-heading">
                <div class="media clearfix">
                    <a class="pull-left">
                        <img src="/resources/images/logo128x128.png" alt="Dom24x7">
                    </a>
                    <div class="media-body text-right">
                        <small class="text-muted">Добавлен: {{=it.create}}</small><br />
                        <small class="text-muted">{{=it.address}}</small>
                    </div>
                </div>
            </div>
            <div class="panel-image">
                <a class="btn-payment-show" href="#" data-toggle="modal" data-target="#payment-show-modal" data-id="{{=it.id}}">
                    <img class="img-responsive" src="/panel/payments/images/small/{{=it.photo.id}}/{{=it.photo.fileName}}" alt="">
                    <div class="title">
                        <h4>{{=it.month}}</h4>
                    </div>
                </a>
            </div>
            <div class="panel-body text-right"><h2 class="font-extra-bold">{{=it.amount}}</h2></div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-8 font-extra-bold">{{=it.info}}</div>
                    <div class="col-xs-4 text-right">
                        <a href="#" data-toggle="modal" data-target="#payment-edit-modal" data-id="{{=it.id}}"
                           data-tooltip="true" data-original-title="Редактирование платежного поручения" class="btn-payment-edit">
                            <i class="fa fa-pencil"></i>
                        </a>&nbsp;&nbsp;
                        <a href="#" data-id="{{=it.id}}"
                           data-tooltip="true" data-original-title="Удаление платежного поручения" class="btn-payment-delete">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script src="/resources/lib/salvattore/salvattore.min.js"></script>
<script src="/resources/lib/waypoints/noframework.waypoints.min.js"></script>

<script>
    $(document).ready(function () {
        var offset = 0;
        var count = 10;

        loadPayments(offset, count);

        new Waypoint({
            element: document.getElementById('payments'),
            handler: function (direction) {
                if ('down' == direction) {
                    offset += count;
                    loadPayments(offset, count);
                }
            }
        });

        function loadPayments(offset, count) {
            $.post('/panel/payments/' + offset + '/' + count, function (list) {
                if (!list || list.length == 0) {
                    return;
                }

                var grid = document.querySelector('#payments');

                for (var i = 0; i < list.length; i++) {
                    var payment = list[i];

                    var data = {
                        id: payment.id,
                        create: moment(payment.createDt).format('DD.MM.YYYY'),
                        address: payment.address.title,
                        info: '',
                        month: moment(payment.month).format('MMMM YYYY'),
                        photo: payment.photo,
                        amount: '0 руб.'
                    };

                    if (payment.paid) {
                        data.info = '<span class="text-success">оплачен</span>';
                        data.amount = '<span class="text-success">' + payment.amount + ' ' + payment.address.currency.symbol + '</span>';
                    } else {
                        data.info = '<span class="text-danger">не оплачен</span>';
                        data.amount = '<span class="text-danger">' + payment.amount + ' ' + payment.address.currency.symbol + '</span>';
                    }

                    var tmpl = doT.template($('#tmpl-payment').html());

                    var item = document.createElement('div');
                    salvattore.appendElements(grid, [item]);

                    item.outerHTML = tmpl(data);
                }

                // включаем подсказки для динамически созданных элементов
                $('[data-tooltip="true"]').tooltip();
            });
        }
    });
</script>