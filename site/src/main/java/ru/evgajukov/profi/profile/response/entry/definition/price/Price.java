package ru.evgajukov.profi.profile.response.entry.definition.price;

import com.google.gson.annotations.SerializedName;
import ru.evgajukov.profi.dictionary.response.pservice.PServiceItem;

/**
 * Created by timur on 15.06.17.
 */
public class Price {

    @SerializedName("edizm_id")
    private int edizmId;
    private double price4quantum;
    @SerializedName("pservice_id")
    private int pserviceId;
    private PServiceItem pservice; // услуга, к которой относится цена
    private Edizm edizm; // единица изменерия
    private Edizm edizmDefault;
    @SerializedName("prep_id")
    private String prepId;
    private String ttl; // текст, который указал специалист для отображения
    private int ckat;
    private Object price; // нижняя граница цены
    private Object price2; // вернхняя граница цены
    private int pcrank;
    private int rank;
    private String comment; // комментарий к данной цене
    @SerializedName("spread_over")
    private int spreadOver;

    public int getEdizmId() {

        return edizmId;
    }

    public void setEdizmId(int edizmId) {

        this.edizmId = edizmId;
    }

    public double getPrice4quantum() {

        return price4quantum;
    }

    public void setPrice4quantum(double price4quantum) {

        this.price4quantum = price4quantum;
    }

    public int getPserviceId() {

        return pserviceId;
    }

    public void setPserviceId(int pserviceId) {

        this.pserviceId = pserviceId;
    }

    public PServiceItem getPservice() {

        return pservice;
    }

    public void setPservice(PServiceItem pservice) {

        this.pservice = pservice;
    }

    public Edizm getEdizm() {

        return edizm;
    }

    public void setEdizm(Edizm edizm) {

        this.edizm = edizm;
    }

    public Edizm getEdizmDefault() {

        return edizmDefault;
    }

    public void setEdizmDefault(Edizm edizmDefault) {

        this.edizmDefault = edizmDefault;
    }

    public String getPrepId() {

        return prepId;
    }

    public void setPrepId(String prepId) {

        this.prepId = prepId;
    }

    public String getTtl() {

        return ttl;
    }

    public void setTtl(String ttl) {

        this.ttl = ttl;
    }

    public int getCkat() {

        return ckat;
    }

    public void setCkat(int ckat) {

        this.ckat = ckat;
    }

    public Object getPrice() {

        return price;
    }

    public void setPrice(Object price) {

        this.price = price;
    }

    public Object getPrice2() {

        return price2;
    }

    public void setPrice2(Object price2) {

        this.price2 = price2;
    }

    public int getPcrank() {

        return pcrank;
    }

    public void setPcrank(int pcrank) {

        this.pcrank = pcrank;
    }

    public int getRank() {

        return rank;
    }

    public void setRank(int rank) {

        this.rank = rank;
    }

    public String getComment() {

        return comment;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    public int getSpreadOver() {

        return spreadOver;
    }

    public void setSpreadOver(int spreadOver) {

        this.spreadOver = spreadOver;
    }
}