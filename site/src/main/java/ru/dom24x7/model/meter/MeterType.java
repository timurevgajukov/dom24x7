package ru.dom24x7.model.meter;

import java.util.List;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.meter.dao.MeterIconDAOImpl;

/**
 * Тип счетчика
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MeterType extends Model {

    private String unit;
    private String note;
    private String code;
    private boolean withoutValue;
    
    private int iconId;
    private MeterIcon icon;
    private List<MeterIcon> icons;

    public MeterType() {

        super();
    }

    public MeterType(int id) {

        super(id);
    }

    public MeterType(int id, String name) {

        super(id);

        this.name = name;
    }
    
    public String getUnit() {
    	
    	return unit;
    }
    
    public void setUnit(String unit) {
    	
    	this.unit = unit;
    }

    public String getNote() {

        return note;
    }

    public void setNote(String note) {

        this.note = note;
    }
    
    public String getCode() {
    	
    	return code;
    }
    
    public void setCode(String code) {
    	
    	this.code = code;
    }

    public boolean isWithoutValue() {

        return withoutValue;
    }

    public void setWithoutValue(boolean withoutValue) {

        this.withoutValue = withoutValue;
    }

    public MeterIcon getIcon() {
    	
    	if (iconId == 0) {
    		return null;
    	}
    	
    	if (icon == null) {
    		// загрузить информацию по иконке
    		icon = new MeterIconDAOImpl().get(iconId);
    	}
    	
    	return icon;
    }
    
    public void setIconId(int iconId) {
    	
    	this.iconId = iconId;
    }
    
    public void setIcon(MeterIcon icon) {
    	
    	this.icon = icon;
    	
    	if (icon != null) {
    		this.iconId = icon.getId();
    	} else {
    		this.iconId = 0;
    	}
    }
    
    public List<MeterIcon> getIcons() {
    	
    	// ленивая загрузка списка иконок
    	if (icons == null) {
    		icons = new MeterIconDAOImpl().list(this);
    	}
    	
    	return icons;
    }
    
    public void setIcons(List<MeterIcon> icons) {
    	
    	this.icons = icons;
    }
}