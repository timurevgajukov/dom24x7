package ru.dom24x7.model.subscribe;

import ru.dom24x7.model.Model;

import java.util.Date;

/**
 * Created by timur on 17.03.17.
 * Модель номера рассылки
 */
public class Subscribe extends Model {

    private int number;
    private Date plan;
    private Date fact;
    private String text;
    private int sentCount;
    private int receivedCount;

    public Subscribe() {

        super();
    }

    public Subscribe(int id) {

        super(id);
    }

    public Subscribe(int id, int number, String text) {

        super(id);
        this.number = number;
        this.text = text;
    }

    public int getNumber() {

        return number;
    }

    public void setNumber(int number) {

        this.number = number;
    }

    public Date getPlan() {

        return plan;
    }

    public void setPlan(Date plan) {

        this.plan = plan;
    }

    public Date getFact() {

        return fact;
    }

    public void setFact(Date fact) {

        this.fact = fact;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }

    public int getSentCount() {

        return sentCount;
    }

    public void setSentCount(int sentCount) {

        this.sentCount = sentCount;
    }

    public int getReceivedCount() {

        return receivedCount;
    }

    public void setReceivedCount(int receivedCount) {

        this.receivedCount = receivedCount;
    }

    @Override
    public String toString() {

        return text;
    }
}