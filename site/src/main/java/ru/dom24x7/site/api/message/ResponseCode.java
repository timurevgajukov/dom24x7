package ru.dom24x7.site.api.message;

import java.util.HashMap;

/**
 * Created by timur on 22.02.17.
 * Коды ответов для API
 */
public class ResponseCode {

    public static HashMap<Integer, String> CODES;

    static {
        CODES = new HashMap<Integer, String>();
        CODES.put(0, "OK");
        CODES.put(-1, "Не найдено приложение по токену.");
        CODES.put(-2, "Приложение заблокировано.");
        CODES.put(-3, "По указанному логину/электронной почте и паролю пользователь не найден");

        CODES.put(-999, "Метод не найден");
    }
}