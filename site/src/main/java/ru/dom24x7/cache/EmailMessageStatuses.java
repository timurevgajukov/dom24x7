package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.email.EmailMessageStatus;
import ru.dom24x7.model.email.dao.EmailMessageStatusDAOImpl;

import java.util.List;

/**
 * Created by timur on 17.03.17.
 * Контейнер для хранения справочника статусов почтовых сообщений
 */
public class EmailMessageStatuses implements ICache<EmailMessageStatus> {

    private static EmailMessageStatuses instance = null;

    private List<EmailMessageStatus> list;

    public static EmailMessageStatuses getInstance() {

        if (instance == null) {
            instance = new EmailMessageStatuses();
        }

        return instance;
    }

    private EmailMessageStatuses() {

        list = new EmailMessageStatusDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<EmailMessageStatus> list() {

        return list;
    }

    @Override
    public EmailMessageStatus get(int id) {

        if (list == null) {
            return null;
        }

        for (EmailMessageStatus item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}