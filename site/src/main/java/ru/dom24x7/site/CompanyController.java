package ru.dom24x7.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.dom24x7.cache.Regions;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.address.dao.AddressDAOImpl;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.Region;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.model.company.dao.RegionDAOImpl;
import ru.dom24x7.model.rating.Rating;
import ru.dom24x7.model.rating.dao.RatingDAOImpl;
import ru.dom24x7.security.Message;
import ru.evgajukov.profi.Profi;
import ru.evgajukov.profi.Profile;
import ru.evgajukov.profi.Test;
import ru.evgajukov.profi.profile.request.Bound;
import ru.evgajukov.profi.profile.request.Client;
import ru.evgajukov.profi.profile.request.InBound;
import ru.evgajukov.profi.profile.request.ProfileRequest;
import ru.evgajukov.profi.profile.request.filter.Filter;
import ru.evgajukov.profi.profile.request.filter.Model;
import ru.evgajukov.profi.profile.response.ProfileResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 19.04.17.
 * Контроллер для работы с компаниями
 */
@Controller
public class CompanyController extends BaseController {

    private static final String ACTIVE = "companies";

    private final int COUNT = 100;

    @RequestMapping("/companies")
    public String listAction(ModelMap model) {

        int size = new CompanyDAOImpl().count((String) null);

        setModelMap(model, "Список компаний ЖКХ", ACTIVE);
        model.addAttribute("companies", new CompanyDAOImpl().list(0, COUNT, null));
        if (size > COUNT) {
            model.addAttribute("next", 2);
        }

        return "company/list";
    }

    @RequestMapping("/companies/page/{page}")
    public String listPageAction(@PathVariable int page, ModelMap model) {

        if (page <= 1) {
            return "redirect:/companies";
        }

        int size = new CompanyDAOImpl().count((String) null);

        setModelMap(model, "Список компаний ЖКХ", ACTIVE);
        model.addAttribute("companies", new CompanyDAOImpl().list((page - 1) * COUNT, COUNT, null));
        model.addAttribute("prev", page - 1);
        if (size > page * COUNT) {
            model.addAttribute("next", page + 1);
        }

        return "company/list";
    }

    @RequestMapping("/companies/regions")
    public String regionsAction(ModelMap model) {

        setModelMap(model, "Список компаний ЖКХ по регионам", ACTIVE);
        model.addAttribute("regions", Regions.getInstance().list());

        return "company/regions";
    }

    @RequestMapping("/companies/regions/{id}")
    public String regionCompaniesListAction(@PathVariable int id,  ModelMap model) {

        Region region = Regions.getInstance().get(id);
        if (region == null) {
            return "redirect:/companies/regions";
        }

        setModelMap(model, "Список компаний ЖКХ по " + region.getName());
        model.addAttribute("region", region);
        model.addAttribute("companies", new CompanyDAOImpl().list(region, 0, COUNT));
        if (region.getCompanies() > COUNT) {
            model.addAttribute("next", 2);
        }

        return "company/region/list";
    }

    @RequestMapping("/companies/regions/{id}/page/{page}")
    public String regionCompaniesListPageAction(@PathVariable int id, @PathVariable int page, ModelMap model) {

        Region region = Regions.getInstance().get(id);
        if (region == null) {
            return "redirect:/companies/regions";
        }

        if (page <= 1) {
            return "redirect:/companies/regions/" + id;
        }

        setModelMap(model, "Список компаний ЖКХ по " + region.getName());
        model.addAttribute("region", region);
        model.addAttribute("companies", new CompanyDAOImpl().list(region, (page - 1) * COUNT, COUNT));
        model.addAttribute("prev", page - 1);
        if (region.getCompanies() > page * COUNT) {
            model.addAttribute("next", page + 1);
        }

        return "company/region/list";
    }

    @RequestMapping("/companies/{id}/{transCompanyName}")
    public String profileAction(@PathVariable int id, @PathVariable String transCompanyName, ModelMap model) {

        Company company = new CompanyDAOImpl().get(id);
        if (company == null) {
            if (security.isAuth()) {
                return "redirect:/panel";
            } else {
                return "redirect:/";
            }
        }

        List<Rating> ratingsAll = new RatingDAOImpl().list(company);
        List<Rating> ratings = new ArrayList<>(); // только содержащие отзывы
        if (ratingsAll != null) {
            for (Rating rating : ratingsAll) {
                if (rating.getReview() != null) {
                    ratings.add(rating);
                }
            }
        }

        double average = Rating.averageMarkByCompany(company);
        Rating userRating = null;
        if (security.isAuth()) {
            userRating = new RatingDAOImpl().get(security.getUser(), company);
        }

        setModelMap(model, company.getName(), ACTIVE);
        model.addAttribute("company", company);
        model.addAttribute("average", average);
        model.addAttribute("averageStr", String.format("%.1f", average));
        model.addAttribute("ratingsCount", ratingsAll == null ? 0 : ratingsAll.size());
        model.addAttribute("ratings", ratings);
        model.addAttribute("userRating", userRating);
        model.addAttribute("profiles", getProfiList(0, 3));

        return "company/profile";
    }

    @RequestMapping("/companies/{id}/rating/save")
    public String ratingSaveAction(
            @PathVariable int id,
            @RequestParam(value = "review") String review,
            @RequestParam(value = "mark") Integer mark) {

        Company company = new CompanyDAOImpl().get(id);
        if (company == null) {
            if (security.isAuth()) {
                return "redirect:/panel";
            } else {
                return "redirect:/";
            }
        }

        String redirect = "redirect:/companies/" + id + "/" + company.getTranslitUrl();

        // только зарегистрированные могу оценикать
        if (!security.isAuth()) {
            security.getMessages().add("Оценивать могуть только зарегистрированные пользователи", Message.TYPE_WARNING);
            return redirect;
        }

        // проверить, что пользователь еще не писал отзыв
        if (new RatingDAOImpl().get(security.getUser(), company) != null) {
            security.getMessages().add("Вы можете оставить только один отзыв по управляющей компании", Message.TYPE_WARNING);
            return redirect;
        }

        if (mark == null) {
            security.getMessages().add("Необходимо указать оценку управлающей компании", Message.TYPE_DANGER);
            return redirect;
        }
        if (mark <= 0 || 5 < mark) {
            security.getMessages().add("Некорректная оценка управляющей компании", Message.TYPE_DANGER);
            return redirect;
        }

        Rating rating = new Rating();
        rating.setUser(security.getUser());
        rating.setCompany(company);
        rating.setMark(mark);
        rating.setReview((review == null || review.trim().length() == 0) ? null : review.trim().replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
        new RatingDAOImpl().save(rating);

        security.getMessages().add("Успешно добавили отзыв по управляющей компании", Message.TYPE_SUCCESS);

        return redirect;
    }

    /**
     * Возвращает список профилей специалистов
     * @param offset
     * @param count
     * @return
     */
    private List<Profile> getProfiList(int offset, int count) {

        final String API_CLIENT = "dom24x7ru";
        final String API_KEY = "ODFjMjVmOTY4YzM2NDczZGI5OWI2NjFjYWY1ZmE3NTAxMzU1Njk1OTIxNzg0Njhm";

        Profi profi = Profi.getInstance()
                .setApiClient(Test.API_CLIENT)
                .setApiKey(Test.API_KEY);

        Filter filter = new Filter();
        filter.addModel(new Model().setModel("provider.specialist"));
        ProfileRequest profData = new ProfileRequest();
        profData.setFilter(filter);
        profData.setBound(new Bound().setFrom(offset).setCount(count).setScope("profile.mini"));
        profData.setClient(new Client().setIp("127.0.0.99").setSid("mstr-sid-123"));
        profData.addInbound(new InBound().setDomain("mstr"));
        List<Profile> profiles = profi.get(profData);

        return profiles;
    }
}