<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${regions != null && regions.size() != 0}">
    <div class="hpanel">
        <div class="panel-body no-padding">
            <div class="row">
                <div class="col-md-6 border-right">
                    <ul class="list-group">
                        <c:forEach begin="0" end="42" step="1" var="i">
                            <li class="list-group-item">
                                <span class="badge badge-primary">${regions.get(i).companies}</span>
                                <a href="/companies/regions/${regions.get(i).id}">${regions.get(i)}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group">
                        <c:forEach begin="43" end="84" step="1" var="i">
                            <li class="list-group-item">
                                <span class="badge badge-primary">${regions.get(i).companies}</span>
                                <a href="/companies/regions/${regions.get(i).id}">${regions.get(i)}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</c:if>