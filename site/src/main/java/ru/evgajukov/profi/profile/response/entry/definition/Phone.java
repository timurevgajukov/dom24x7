package ru.evgajukov.profi.profile.response.entry.definition;

/**
 * Created by timur on 17.06.17.
 */
public class Phone {

    private Redirects redirects;

    public Redirects getRedirects() {

        return redirects;
    }

    public void setRedirects(Redirects redirects) {

        this.redirects = redirects;
    }
}