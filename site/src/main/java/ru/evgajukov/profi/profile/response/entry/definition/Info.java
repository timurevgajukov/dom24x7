package ru.evgajukov.profi.profile.response.entry.definition;

/**
 * Created by timur on 16.06.17.
 */
public class Info {

    private Object category; // категория врача
    private Object education; // образование частного специалиста
    private Object academic; // научная степень частного специалиста
    private Sport sport;
    private Experience experience; // опыт работы частного специалиста
    private Object general; // общее описание профиля

    public Object getCategory() {

        return category;
    }

    public void setCategory(Object category) {

        this.category = category;
    }

    public Object getEducation() {

        return education;
    }

    public void setEducation(Object education) {

        this.education = education;
    }

    public Object getAcademic() {

        return academic;
    }

    public void setAcademic(Object academic) {

        this.academic = academic;
    }

    public Sport getSport() {

        return sport;
    }

    public void setSport(Sport sport) {

        this.sport = sport;
    }

    public Experience getExperience() {

        return experience;
    }

    public void setExperience(Experience experience) {

        this.experience = experience;
    }

    public Object getGeneral() {

        return general;
    }

    public void setGeneral(Object general) {

        this.general = general;
    }
}