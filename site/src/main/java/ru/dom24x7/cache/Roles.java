package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.user.Role;
import ru.dom24x7.model.user.dao.RoleDAOImpl;

import java.util.List;

/**
 * Created by timur on 14.02.17.
 *
 * Контейнер для хранения справочника ролей пользователей
 */
public class Roles implements ICache<Role> {

    private static Roles instance = null;

    private List<Role> list;

    public static Roles getInstance() {

        if (instance == null) {
            instance = new Roles();
        }

        return instance;
    }

    private Roles() {

        list = new RoleDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Role> list() {

        return list;
    }

    @Override
    public Role get(int id) {

        if (list == null) {
            return null;
        }

        for (Role item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }

    public Role get(String name) {

        if (list == null) {
            return null;
        }

        for (Role item : list) {
            if (name.equalsIgnoreCase(item.getName())) {
                return item;
            }
        }

        return null;
    }
}