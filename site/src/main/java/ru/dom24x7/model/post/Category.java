package ru.dom24x7.model.post;

import ru.dom24x7.model.Model;

/**
 * Created by timur on 01.03.17.
 * Модель категории поста
 */
public class Category extends Model {

    public static int DEFAULT_CATEGORY = 1;

    private String urlName;

    public Category(int id, String name, String urlName) {

        super(id, name);
        this.urlName = urlName;
    }

    public String getUrlName() {

        return urlName;
    }

    public void setUrlName(String urlName) {

        this.urlName = urlName;
    }
}