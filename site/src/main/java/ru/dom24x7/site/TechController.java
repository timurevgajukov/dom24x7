package ru.dom24x7.site;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.XMLMessage;
import ru.dom24x7.cache.Regions;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.Region;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.model.post.Post;
import ru.dom24x7.model.post.dao.PostDAOImpl;
import ru.dom24x7.utils.Translit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 02.03.17.
 * Контроллер для технических страниц сайта
 */
@Controller
public class TechController extends BaseController {

    public final static String SITE = "dom24x7.ru";
    public final static String URL = "https://" + SITE;
    private final static SimpleDateFormat SIMEMAP_DATE = new SimpleDateFormat("yyyy-MM-dd");

    private final static String ALWAYS = "always";
    private final static String HOURLY = "hourly";
    private final static String DAILY = "daily";
    private final static String WEEKLY = "weekly";
    private final static String MONTHLY = "monthly";

    @RequestMapping("/robots.txt")
    public @ResponseBody String robotsTxtAction() {

        StringBuilder result = new StringBuilder();
        result.append("User-agent: *\n");
        result.append("Disallow: /panel/\n");
        result.append("Disallow: /admin/\n");
        result.append("Disallow: /security/\n");
        result.append("Host: " + URL + "\n");
        result.append(String.format("Sitemap: %s/sitemap.xml\n", URL));

        return result.toString();
    }

    @RequestMapping(value = "/sitemap.xml", produces = "application/xml; charset=utf-8")
    public @ResponseBody String sitemapAction() {

        XMLMessage xml = new XMLMessage("urlset");
        BodyElement urlset = xml.getRoot();
        urlset.setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

        // основные разделы сайта
        urlset.addChild(createUrlElement("/", new Date(), DAILY, "0.8"));
        urlset.addChild(createUrlElement("/blog", new Date(), DAILY, "0.8"));
        urlset.addChild(createUrlElement("/profi", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/companies", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/companies/regions", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/lawyer", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/about", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/about/tech", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/about/privacy", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/about/privacy/terms", new Date(), MONTHLY, "0.8"));

        // TODO: добавляем ссылки по категориям блога

        // добавляем ссылки по постам блога
        List<Post> posts = new PostDAOImpl().listForBlog(0, Integer.MAX_VALUE);
        for (Post post : posts) {
            String postUrl = String.format("/blog/post/%d/%s", post.getId(), post.getTranslitUrl());
            urlset.addChild(createUrlElement(postUrl, new Date(), DAILY, "0.8"));
        }

        // добавляем ссылки по регионам
        List<Region> regions = Regions.getInstance().list();
        for (Region region : regions) {
            String regionUrl = String.format("/companies/regions/%d", region.getId());
            urlset.addChild(createUrlElement(regionUrl, new Date(), WEEKLY, "0.8"));
        }

        // добавляем ссылки по компаниям
        List<Company> companies = new CompanyDAOImpl().list();
        for (Company company : companies) {
            String companyUrl = String.format("/companies/%d/%s", company.getId(), company.getTranslitUrl());
            urlset.addChild(createUrlElement(companyUrl, new Date(), WEEKLY, "0.8"));
        }

        return xml.renderSafe();
    }

    private BodyElement createUrlElement(String loc, Date lastmod, String changefreq, String priority) {

        BodyElement url = new BodyElement("url");
        url.addChild(new BodyElement("loc", URL + loc));
        url.addChild(new BodyElement("lastmod", SIMEMAP_DATE.format(lastmod)));
        url.addChild(new BodyElement("changefreq", changefreq));
        url.addChild(new BodyElement("priority", priority));

        return url;
    }
}