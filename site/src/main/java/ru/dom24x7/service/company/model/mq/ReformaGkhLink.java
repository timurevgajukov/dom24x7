package ru.dom24x7.service.company.model.mq;

import ru.dom24x7.model.Model;

/**
 * Created by timur on 07.04.17.
 */
public class ReformaGkhLink extends Model {

    private String link;
    private int sorting;

    public ReformaGkhLink(int id, String link) {

        super(id);
        this.link = link;
    }

    public ReformaGkhLink(String link) {

        super();
        this.link = link;
    }

    public String getLink() {

        return link;
    }

    public void setLink(String link) {

        this.link = link;
    }

    public int getSorting() {

        return sorting;
    }

    public void setSorting(int sorting) {

        this.sorting = sorting;
    }

    @Override
    public String toString() {

        return link;
    }
}