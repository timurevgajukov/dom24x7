package ru.evgajukov.profi.profile.response.entry.definition.geo;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class Country {

    private String id;
    private List<City> cities;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public List<City> getCities() {

        return cities;
    }

    public void setCities(List<City> cities) {

        this.cities = cities;
    }

    @Override
    public String toString() {

        return id;
    }
}
