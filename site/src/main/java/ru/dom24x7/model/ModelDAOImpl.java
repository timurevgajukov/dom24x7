package ru.dom24x7.model;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.dom24x7.utils.DBase;

/**
 * Created by timur on 18.09.16.
 *
 * Реализация интерфейса для работы с моделями данных
 */
public class ModelDAOImpl {

    protected static final String QUERY_LAST_INSERT_ID = "SELECT LAST_INSERT_ID()";

    protected JdbcTemplate jdbcTemplate;

    {
        jdbcTemplate = new JdbcTemplate(DBase.getDataSource());
    }
}