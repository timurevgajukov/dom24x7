package ru.dom24x7.model.photo.dao.interfaces;

import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.post.Post;

import java.util.List;

/**
 * Created by timur on 26.02.17.
 * Интерфейс для работы с картинками
 */
public interface PhotoDAO {

    /**
     * Возвращает данные по картинке
     * @param id
     * @return
     */
    Photo get(int id);

    /**
     * если такая имеется в БД, то возвращает сохраненную миниатюру
     * @param photo
     * @return
     */
    Photo thumbnail(Photo photo);

    /**
     * Возвращает картинки, привязанные к посту
     * @param post
     * @param full
     * @return
     */
    List<Photo> list(Post post, boolean full);

    /**
     * Сохраняет картинку
     * @param photo
     * @return
     */
    boolean save(Photo photo);

    /**
     * Сохранение миниатюры
     * @param photo
     * @param original
     * @return
     */
    boolean save(Photo photo, Photo original);

    /**
     * Связывает фотографию с постом
     * @param post
     * @param photo
     * @return
     */
    boolean save(Post post, Photo photo);

    /**
     * Отвязывает фотографии от поста
     * @param post
     * @return
     */
    boolean delete(Post post);
}