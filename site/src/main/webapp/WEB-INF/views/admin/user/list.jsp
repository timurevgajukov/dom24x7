<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">${title}</div>
    <div class="panel-body">
        <table id="users-table" class="table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Логин</th>
                <th>Эл. почта</th>
                <th>Роль</th>
                <th>Зарегистрирован</th>
                <th>Заходил</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#users-table').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax/users/list",
            "bAutoWidth": false,
            "columns": [
                {"data": "id"},
                {"data": "login"},
                {
                    "data": function (row) {
                        var check = '';
                        if (row.confirmed) {
                            check += '<i class="fa fa-check"></i> ';
                        }
                        if (row.subscribed) {
                            check += '<i class="fa fa-envelope-o"></i> ';
                        }
                        return check + row.email;
                    }
                },
                {
                    "data": function (row) {
                        return row.role.name;
                    }
                },
                {
                    "data": function (row) {
                        return moment(row.createDt).format('DD.MM.YYYY HH:mm:ss');
                    }
                },
                {
                    "data": function (row) {
                        if (row.lastAuthDt == null) {
                            return '';
                        } else {
                            return moment(row.lastAuthDt).format('DD.MM.YYYY HH:mm:ss');
                        }
                    }
                },
                {
                    "data": function (row) {
                        return ''
                    }
                }
            ],
            "responsive": true,
            "language": {
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
    });
</script>