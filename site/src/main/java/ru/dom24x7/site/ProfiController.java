package ru.dom24x7.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by timur on 12.03.17.
 * Контроллер для работы с партнеркой ПРОФИ.РУ
 */
@Controller
public class ProfiController extends BaseController {

    private static final String ACTIVE = "profi";

    @RequestMapping("/profi")
    public String listAction(ModelMap model) {

        setModelMap(model, "Мастера по ремонту", ACTIVE);

        return "profi/list";
    }
}