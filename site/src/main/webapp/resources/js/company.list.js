/**
 * Created by timur on 05.06.17.
 */

/**
 * Подключает к выпадающему списку плагин select2
 * @param selectEl
 */
function select2CompanyList(selectEl, count, selectFunc) {
    count = count || 30;

    selectEl.select2({
        ajax: {
            url: '/ajax/companies/list',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                    page: params.page || 1,
                    count: count
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * count) < data.total
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 0,
        templateResult: function (repo) {
            if (repo.loading) return repo.text;

            var address = '';
            if (repo.address != null && repo.address.trim().length != 0) {
                address = ' [' + repo.address.trim() + ']';
            }

            return '<div class="select2-result-repository clearfiv">' + repo.name + address + '</div>';
        },
        templateSelection: function (repo) {
            var result = repo.text;
            if (repo.name != null && repo.name.trim().length != 0) {
                // приоритет у этого поля
                result = repo.name;
            }

            if (repo != null && repo.id != null) {
                if (selectFunc) {
                    selectFunc(repo);
                }
            }

            return result;
        },
        placeholder: 'Выберите свою управляющую компанию',
        language: 'ru',
        containerCss: function (element) {
            var style = $(element)[0].style;
            return {
                display: style.display
            };
        }
    });
}