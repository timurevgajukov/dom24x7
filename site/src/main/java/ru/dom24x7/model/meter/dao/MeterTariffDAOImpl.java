package ru.dom24x7.model.meter.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterTariff;
import ru.dom24x7.model.meter.dao.interfaces.MeterTariffDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 25.06.17.
 * Реализация интерфейса для работы с тарифами по счетчику
 */
public class MeterTariffDAOImpl extends ModelDAOImpl implements MeterTariffDAO {

    private static final String QUERY_LIST = "{ call meter_tariff_history_list(?) }";
    private static final String QUERY_SAVE = "{ call meter_tariff_history_save(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call meter_tariff_history_delete(?) }";

    @Override
    public List<MeterTariff> list(Meter meter) {

        List<MeterTariff> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<MeterTariff>() {
            @Override
            public MeterTariff mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, meter.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Meter meter, MeterTariff tariff) {

        MeterTariff newTariff = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        tariff.getId() == 0 ? null : tariff.getId(),
                        meter.getId(),
                        tariff.getDt(),
                        tariff.getValue()
                }, new RowMapper<MeterTariff>() {
                    @Override
                    public MeterTariff mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        tariff.setId(newTariff.getId());

        return true;
    }

    @Override
    public boolean delete(MeterTariff tariff) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{tariff.getId()}) != 0;
    }

    private MeterTariff load(ResultSet rs) throws SQLException {

        return new MeterTariff(rs.getInt("id"), rs.getDate("dt"), rs.getDouble("value"));
    }
}