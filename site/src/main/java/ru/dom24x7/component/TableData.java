package ru.dom24x7.component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 23.02.17.
 * Класс для отображения данных в таблице
 */
public class TableData {

    private int draw;
    private int recordsTotal;
    private int recordsFiltered;
    private List<?> data;

    public int getDraw() {

        return draw;
    }

    public void setDraw(int draw) {

        this.draw = draw;
    }

    public int getRecordsTotal() {

        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {

        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {

        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {

        this.recordsFiltered = recordsFiltered;
    }

    public List<?> getData() {

        return data;
    }

    public void setData(List<?> data) {

        if (data == null) {
            data = new ArrayList<Object>();
        }

        this.data = data;
    }
}