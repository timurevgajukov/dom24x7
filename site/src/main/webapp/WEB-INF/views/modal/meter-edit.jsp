<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- EDIT METER MODAL WINDOW: BEGIN -->
<div id="meter-edit-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 id="meter-edit-modal-title" class="modal-title">Сохранение счетчика</h4>
            </div>
            <form id="meter-form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input id="modal-meter-title" type="text" class="form-control"  placeholder="Введите название счетчика"/>
                    </div>
                    <div class="form-group">
                        <select id="modal-meter-type" class="form-control" data-placeholder="Выберите тип счетчика"></select>
                    </div>
                    <div class="form-group">
                        <input id="modal-meter-tariff" type="text" class="form-control" placeholder="Введите тариф"/>
                    </div>
                    <div id="meter-edit-modal-device-type" class="row" style="display: none;">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="modal-meter-capacity" type="text" class="form-control" placeholder="Введите разрядность"/>
                                <span class="help-block m-b-none">Количество знаков в счетчике. Например, при разрядности
                                    счетчика равного 5, показание будет отображиться как 00000. Оставьте пустым, если
                                    не хотите использовать.</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="modal-meter-precision" type="text" class="form-control" placeholder="Введите точность"/>
                                <span class="help-block m-b-none">Количество знаков в счетчике после запятой. Например,
                                    если укажете точность равным 2, то после запятой в показании счетчика будет
                                    отображаться только два знака. Оставьте пустым, если не хотите использовать.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-meter-save" class="ladda-button btn btn-success" type="button" data-style="zoom-in">Сохранить</button>
                    <button id="btn-meter-delete" class="btn btn-danger" type="button">Удалить</button>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT METER MODAL WINDOW: END -->

<script>
    $(document).ready(function () {
        var addressId = undefined;
        var meterId = undefined;

        // локальный справочник типов счетчиков
        var typesCache = {};

        loadMeterTypes();

        // открытие модального окна для добавления нового счетчика
        $('#btn-meter-add').click(function () {
            $('#meter-edit-modal-title').html('Добавление счетчика');
            $('#btn-meter-delete').hide();
            $('#meter-edit-modal-device-type').hide();

            addressId = $(this).data('address');
            meterId = undefined;

            initMeter(addressId);
        });

        // открытие модального окна для редактирования существующего счетчика
        $('#btn-meter-edit').click(function () {
            $('#address-edit-modal-title').html('Редактирование счетчика');
            $('#btn-meter-delete').show();
            $('#meter-edit-modal-device-type').hide();

            addressId = $(this).data('address');
            meterId = $(this).data('id');
            initMeter(addressId, meterId);
        });

        // отслеживаем событие изменения типа счетчика
        $('#modal-meter-type').change(function () {
            var typeId = $(this).val();
            if (typeId != null && typeId.length != 0) {
                if (typesCache[typeId].withoutValue) {
                    $('#meter-edit-modal-device-type').hide();
                } else {
                    $('#meter-edit-modal-device-type').show();
                }
            }
        });

        // сохранение данных по счетчику (новому или редактируемому)
        var btnMeterSave = $('#btn-meter-save').ladda();
        btnMeterSave.click(function () {
            var data = {
                title: $('#modal-meter-title').val(),
                type: $('#modal-meter-type').val(),
                tariff: $('#modal-meter-tariff').val(),
                capacity: $('#modal-meter-capacity').val(),
                precision: $('#modal-meter-precision').val()
            };

            if (data.title == null || data.title.trim().length == 0) {
                showAlert('Необходимо указать наименование счетчика, например, "Отопление"', 'warning');
                $('#modal-meter-title').focus();
                return;
            }

            if (data.type == null) {
                showAlert('Необходимо указать тип счетчика', 'warning');
                $('#modal-meter-type').focus();
                return;
            }

            if (data.tariff == null || data.tariff.trim().length == 0) {
                showAlert('Необходимо указать тариф', 'warning');
                $('#modal-meter-tariff').focus();
                // TODO: пока никак не проверяем формат и другие параметры
                return;
            }

            if (meterId !== undefined && meterId != null) {
                // редактирование существующего счетчика
                data.id = meterId;
            }

            // отправляем запрос на сохранение
            btnMeterSave.ladda('start');
            $.post('/ajax/addresses/' + addressId + '/meters/save', data, function (result) {
                btnMeterSave.ladda('stop');
                if (!result) {
                    showAlert('Непредвиденная ошибка', 'error');
                    return;
                }

                if (result.code == 0) {
                    var meter = result.body;
                    if (data.id) {
                        // отредактирован существующий счетчик
                        showAlert('Успешно отредактировали счетчик "' + meter.title + '"', 'info');
                        // TODO: меняем отображаемые данные

                        $('#meter-edit-modal').modal('hide');
                    } else {
                        // добавлен новый счетчки
                        // TODO: обновляем список
                        window.location = '/panel/addresses/' + addressId;
                    }
                } else {
                    for (var i = 0; i < result.body.length; i++) {
                        var error = result.body[i];
                        showAlert(error.body, error.type);
                    }
                }
            }).error(function () {
                btnMeterSave.ladda('stop');
            });
        });

        // удаление существующего адреса
        $('#btn-meter-delete').click(function () {
            if (!meterId) {
                return;
            }

            swal({
                    title: "Вы уверены, что хотите удалить счетчик?",
                    text: "После удаления счетчик с историей восстановить уже не сможете!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да, удалить!",
                    cancelButtonText: "Нет, не удалять!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // отправляем запрос на удаление
                        $.get('/ajax/addresses/' + addressId + '/meters/delete/' + meterId, function (result) {
                            if (!result) {
                                swal("Ошибка", "Неизвестная ошибка. Не удалось удалить счетчик.", "error");
                                return;
                            }

                            if (result.code != 0) {
                                var errorStr = '';
                                for (var i = 0; i < result.body.length; i++) {
                                    errorStr += result.body[i] + ' ';
                                }
                                swal("Ошибка", errorStr, "error");
                                return;
                            }

                            swal("Удалено!", "Ваш счетчик успешно удален.", "success");
                            // закрываем модальное окно
                            $('#meter-edit-modal').modal('hide');
                            window.location = '/panel/addresses/' + addressId;
                        });
                    } else {
                        swal("Отмена", "Ваш счетчик остался доступным.", "error");
                    }
                });
        });

        /**
         * Загрузка списка доступных типов счетчиков
         */
        function loadMeterTypes() {
            $.post('/ajax/meter/types/list', function (types) {
                if (types === undefined || types == null) {
                    return;
                }

                var typesList = $('#modal-meter-type');
                typesList.empty();
                for (var i = 0; i < types.length; i++) {
                    var type = types[i];
                    typesCache[type.id] = type;
                    var item = '<option value="' + type.id + '">' + type.name + '</option>';
                    typesList.append(item);
                }
            });
        }

        /**
         * Загрузка данных по счетчику
         * @param addressId
         * @param meterId
         */
        function initMeter(addressId, meterId) {
            var title = $('#modal-meter-title');
            var type = $('#modal-meter-type');
            var tariff = $('#modal-meter-tariff');
            var capacity = $('#modal-meter-capacity');
            var precision = $('#modal-meter-precision');

            title.val('');
            type.val('');
            tariff.val('');
            capacity.val('');
            precision.val('');

            if (meterId === undefined || meterId == null) {
                return;
            }

            $.post('/ajax/addresses/' + addressId + '/meters/' + meterId, function (meter) {
                if (!meter) {
                    return;
                }

                title.val(meter.title);
                type.val(meter.type.id);
                tariff.val(meter.tariff);

                if (meter.capacity == null || meter.capacity == -1) {
                    capacity.val('');
                } else {
                    capacity.val(meter.capacity);
                }
                if (meter.precision == null || meter.precision == -1) {
                    precision.val('');
                } else {
                    precision.val(meter.precision);
                }

                // если необходимо, то отображаем поля разрядности и точности
                if (meter.type.withoutValue) {
                    $('#meter-edit-modal-device-type').hide();
                } else {
                    $('#meter-edit-modal-device-type').show();
                }
            });
        }
    });
</script>