package ru.evgajukov.profi.dictionary.response;

import ru.evgajukov.profi.dictionary.response.mmetro.MMetroBlock;
import ru.evgajukov.profi.dictionary.response.pservice.PServiceBlock;

/**
 * Created by timur on 15.06.17.
 */
public class Dictionaries {

    private MMetroBlock mmetros;
    private PServiceBlock pservices;

    public MMetroBlock getMmetros() {

        return mmetros;
    }

    public void setMmetros(MMetroBlock mmetros) {

        this.mmetros = mmetros;
    }

    public PServiceBlock getPservices() {

        return pservices;
    }

    public void setPservices(PServiceBlock pservices) {

        this.pservices = pservices;
    }
}
