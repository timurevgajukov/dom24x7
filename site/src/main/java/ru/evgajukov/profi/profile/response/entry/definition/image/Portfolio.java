package ru.evgajukov.profi.profile.response.entry.definition.image;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class Portfolio extends Image {

    private String original;
    @SerializedName("mode_url")
    private String modeUrl;
    private List<Object> ptags;
    private List<Object> pservices;
    private String title;
    private String cdt;
    private String host;
    private int rank;
    private String furl;
    private String producer;
    private String category;
    private String seo;

    public String getOriginal() {

        return original;
    }

    public void setOriginal(String original) {

        this.original = original;
    }

    public String getModeUrl() {

        return modeUrl;
    }

    public void setModeUrl(String modeUrl) {

        this.modeUrl = modeUrl;
    }

    public List<Object> getPtags() {

        return ptags;
    }

    public void setPtags(List<Object> ptags) {

        this.ptags = ptags;
    }

    public List<Object> getPservices() {

        return pservices;
    }

    public void setPservices(List<Object> pservices) {

        this.pservices = pservices;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getCdt() {

        return cdt;
    }

    public void setCdt(String cdt) {

        this.cdt = cdt;
    }

    public String getHost() {

        return host;
    }

    public void setHost(String host) {

        this.host = host;
    }

    public int getRank() {

        return rank;
    }

    public void setRank(int rank) {

        this.rank = rank;
    }

    public String getFurl() {

        return furl;
    }

    public void setFurl(String furl) {

        this.furl = furl;
    }

    public String getProducer() {

        return producer;
    }

    public void setProducer(String producer) {

        this.producer = producer;
    }

    public String getCategory() {

        return category;
    }

    public void setCategory(String category) {

        this.category = category;
    }

    public String getSeo() {

        return seo;
    }

    public void setSeo(String seo) {

        this.seo = seo;
    }

    @Override
    public String toString() {

        return title;
    }
}