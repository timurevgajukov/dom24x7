package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.component.TableData;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.post.Post;
import ru.dom24x7.model.post.dao.PostDAOImpl;
import ru.dom24x7.model.subscribe.Subscribe;
import ru.dom24x7.model.subscribe.dao.SubscribeDAOImpl;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.TechController;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.Email;
import ru.dom24x7.utils.StrUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 03.03.17.
 * Контроллер службы рассылки
 */
@Controller
public class AdminSubscribeController extends BaseController {

    private static final String TAG_IMG_URL = "\\{\\{IMG_URL\\}\\}";
    private static final String TAG_TITLE = "\\{\\{TITLE\\}\\}";
    private static final String TAG_TEXT = "\\{\\{TEXT\\}\\}";
    private static final String TAG_POST_LINK = "\\{\\{POST_LINK\\}\\}";
    private static final String TAG_BLOG_LINK = "\\{\\{BLOG_LINK\\}\\}";
    private static final String TAG_POSTS = "\\{\\{POSTS\\}\\}";

    @RequestMapping("/admin/subscribe")
    public String listAction(ModelMap model) {

        setModelMap(model, "Служба почтовой рассылки");

        return "admin/subscribe/list";
    }

    @RequestMapping("/admin/ajax/subscribe/list")
    public @ResponseBody TableData listSubscribesAjax(HttpServletRequest request) {

        int draw = Integer.parseInt(request.getParameter("draw"));
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));
        String search = request.getParameter("search[value]");

        int count = new SubscribeDAOImpl().count(search);
        List<Subscribe> subscribes = new SubscribeDAOImpl().list(start, length, search);

        if (subscribes == null) {
            subscribes = new ArrayList<Subscribe>();
        }

        TableData tableData = new TableData();
        tableData.setDraw(draw);
        tableData.setRecordsTotal(count);
        tableData.setRecordsFiltered(count);
        tableData.setData(subscribes);

        return tableData;
    }

    @RequestMapping("/admin/subscribe/create")
    public String createNewSubscribeAction() {

        // получаем посты для новой рассылки
        List<Post> posts = new PostDAOImpl().listForBlog(0, 5);
        if (posts == null) {
            security.getMessages().add("Нет постов для отправки", Message.TYPE_WARNING);
            return "redirect:/admin/subscribe";
        }

        // сериализуем на основе шаблона список постов
        List<String> postsStr = new ArrayList<String>();
        String postTmpl = getTemplate("post");
        for (Post post : posts) {
            postsStr.add(post2str(post, postTmpl));
        }

        // формируем текст самой рассылки
        String subscribeStr = getTemplate("subscribe");
        subscribeStr = subscribeStr.replaceAll(TAG_POSTS, StrUtils.join("<hr>", postsStr));

        // формируем объект рассылки
        Subscribe subscribe = new Subscribe();
        subscribe.setNumber(1); // TODO: получать свободный следующий номер рассылки
        subscribe.setText(subscribeStr);
        subscribe.setPlan(new Date());

        // теперь можем сохранить
        new SubscribeDAOImpl().save(subscribe);

        security.getMessages().add("Успешно сформировали новую новостную рассылку", Message.TYPE_INFO);

        return "redirect:/admin/subscribe";
    }

    @RequestMapping("/admin/ajax/subscribe/send")
    public @ResponseBody Response sendSubscribeAjax(@RequestParam(value = "id") int id) {

        Subscribe subscribe = new SubscribeDAOImpl().get(id);
        if (subscribe == null) {
            Message message = new Message("Не удалось найти рассылку для отправки", Message.TYPE_ERROR);
            return new Response(-1, "ERROR", message);
        }

        // получаем список рассылок
        List<User> users = new UserDAOImpl().list(0, Integer.MAX_VALUE, null);

        // рассылаем
        for (User user : users) {
            if (user.isSubscribed()) {
                try {
                    Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z", servletContext.getRealPath("/resources"));
                    emailSender.send("Новостная рассылка dom24x7.ru", subscribe.getText(), user.getEmail());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        // обновляем информацию по рассылке
        subscribe.setFact(new Date());
        subscribe.setSentCount(users.size());
        new SubscribeDAOImpl().save(subscribe);

        Message message = new Message("Рассылка успешно отправлена", Message.TYPE_INFO);
        return new Response(0, "OK", message);
    }

    /**
     * По имени возвращает из файла шаблон
     * @param name
     * @return
     */
    private String getTemplate(String name) {

        String resourcePath = servletContext.getRealPath("/resources");
        String template = "";
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(resourcePath + "/email-templates/subscribe/" + name + ".html"));
            template = new String(bytes, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return template;
    }

    /**
     * Сериализует объект поста в html для рассылки
     * @param post
     * @param template
     * @return
     */
    private String post2str(Post post, String template) {

        String text = "";
        if (post.getText() != null) {
            String[] texts = post.getText().split("<hr>");
            text = texts[0];
        }

        // TODO: проверять на наличие картинки
        Photo photo = post.getPhotos().get(0);

        return template.replaceAll(TAG_IMG_URL, TechController.URL + "/blog/images/" + photo.getId() + "/" + photo.getFileName())
                .replaceAll(TAG_TITLE, post.getShowTitle()).replaceAll(TAG_TEXT, text)
                .replaceAll(TAG_POST_LINK, TechController.URL + "/blog/post/" + post.getId() + "/" + post.getTranslitUrl());
    }
}