package ru.dom24x7.model.address.dao.interfaces;

import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.address.Address;
import ru.evgajukov.dadata.DadataAddress;

import java.util.List;

/**
 * Created by timur on 18.09.16.
 *
 * Интерфейс для работы с адресом жилища
 */
public interface AddressDAO {

    /**
     * Возвращает все адреса жилищ
     *
     * @return
     */
    List<Address> list(int offset, int count, String search);

    /**
     * Возвращает адреса жилищ пользователя
     *
     * @param user
     * @return
     */
    List<Address> list(User user);

    /**
     * Возвращает список адресов по компании
     * @param company
     * @return
     */
    List<Address> list(Company company);

    /**
     * Возвращает количество адресов, удовлетворяющих условию
     * @param search
     * @return
     */
    int count(String search);

    /**
     * Сохраняет адрес жилища пользователя
     *
     * @param address
     * @return
     */
    boolean save(Address address);

    /**
     * Удаляет адрес жилища пользователя
     *
     * @param address
     * @return
     */
    boolean delete(Address address);

    /**
     * Блокирует адрес пользователя
     *
     * @param address
     * @return
     */
    boolean hold(Address address);

    /**
     * Возвращает расширенную информацию по адресу
     *
     * @param address
     * @return
     */
    DadataAddress info(Address address);
}