package ru.dom24x7.model.subscribe.dao.interfaces;

import ru.dom24x7.model.subscribe.Subscribe;

import java.util.List;

/**
 * Created by timur on 17.03.17.
 * Интерфейс для работы с рассылкой
 */
public interface SubscribeDAO {

    /**
     * Возвращает номер рассылки
     * @param id
     * @return
     */
    Subscribe get(int id);

    /**
     * Возвращает список рассылок
     * @param offset
     * @param count
     * @param search
     * @return
     */
    List<Subscribe> list(int offset, int count, String search);

    /**
     * Возвращает количество рассылок с учетом фильтра
     * @param search
     * @return
     */
    int count(String search);

    /**
     * Сохраняет рассылку
     * @param subscribe
     * @return
     */
    boolean save(Subscribe subscribe);

    /**
     * Удаляет рассылку
     * @param subscribe
     * @return
     */
    boolean delete(Subscribe subscribe);
}