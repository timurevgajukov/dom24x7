package ru.dom24x7.security;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import ru.dom24x7.model.access.Access;
import ru.dom24x7.model.access.dao.AccessDAOImpl;
import ru.dom24x7.model.user.Role;
import ru.dom24x7.model.user.User;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by timur on 18.09.16.
 *
 * Основной класс безопасности
 */
@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Security {

    private boolean demo;

    private User user; // авторизованный пользователь
    private Messages messages; // список сообщений
    private Messages notifications; // список нотификаций

    {
        user = new User();
        messages = new Messages();
        notifications = new Messages();
        demo = false;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public boolean isDemo() {

        return demo;
    }

    public void setDemo(boolean demo) {

        this.demo = demo;
    }

    public Messages getMessages() {

        return messages;
    }

    public Messages getNotifications() {

        return notifications;
    }

    /**
     * Logout пользователя
     */
    public void logout() {

        user = null;
    }

    /**
     * Проверка доступа к ресурсам пользователем
     * @param url
     * @return
     */
    public boolean isAccess(String url) {

        Role role = null;
        if (isAuth()) {
            role = user.getRole();
        }

        List<Access> list = new AccessDAOImpl().list(role);
        if (list == null) {
            return false;
        }

        for (Access access : list) {
            if (access.getUrl() != null) {
                if (Pattern.matches("^" + access.getUrl() + "$", url.toLowerCase())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Признак, что пользователь уже авторизован
     *
     * @return
     */
    public boolean isAuth() {

        return (user != null && user.getId() != 0);
    }

    /**
     * Роль администратора системы
     *
     * @return
     */
    public boolean isAdminRole() {

        if (!isAuth()) {
            return false;
        }

        return Role.ROLE_ADMIN == user.getRole().getId();
    }

    /**
     * Роль пользователя системы
     *
     * @return
     */
    public boolean isUserRole() {

        if (!isAuth()) {
            return false;
        }

        return Role.ROLE_USER == user.getRole().getId();
    }

    /**
     * Роль представителя компании ЖКХ
     *
     * @return
     */
    public boolean isCompanyRole() {

        if (!isAuth()) {
            return false;
        }

        return Role.ROLE_COMPANY == user.getRole().getId();
    }

    /**
     * Роль юриста
     * @return
     */
    public boolean isLawyerRole() {

        if (!isAuth()) {
            return false;
        }

        return Role.ROLE_LAWYER == user.getRole().getId();
    }

    /**
     * Роль редактора
     * @return
     */
    public boolean isEditorRole() {

        if (!isAuth()) {
            return false;
        }

        return Role.ROLE_EDITOR == user.getRole().getId();
    }
}