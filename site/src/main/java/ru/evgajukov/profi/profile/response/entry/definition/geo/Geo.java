package ru.evgajukov.profi.profile.response.entry.definition.geo;

/**
 * Created by timur on 16.06.17.
 */
public class Geo {

    private Areas areas;
    private Object addresses; // дополнительно для организаций, его адреса

    public Areas getAreas() {

        return areas;
    }

    public void setAreas(Areas areas) {

        this.areas = areas;
    }

    public Object getAddresses() {

        return addresses;
    }

    public void setAddresses(Object addresses) {

        this.addresses = addresses;
    }
}