<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">${title}</div>
    <div class="panel-body">
        <p>
            <a href="/admin/subscribe/create" class="btn btn-success" data-tooltip="true"
               data-original-title="Сформировать новую новостную рассылку">Сформировать</a>
        </p>
        <table id="subscribes-table" class="table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Номер</th>
                <th>Плановый выпуск</th>
                <th>Фактический выпуск</th>
                <th>Отправлено</th>
                <th>Получено</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $(document).ready(function () {
        var tbl = $('#subscribes-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax/subscribe/list",
            "bAutoWidth": false,
            "columns": [
                {"data": "id"},
                {"data": "number"},
                {
                    "data": function (row) {
                        return moment(row.plan).format('DD.MM.YYYY HH:mm');
                    }
                },
                {
                    "data": function (row) {
                        if (!row.fact) {
                            return '';
                        }
                        return moment(row.fact).format('DD.MM.YYYY HH:mm');
                    }
                },
                {"data": "sentCount"},
                {"data": "receivedCount"},
                {
                    "data": function (row) {
                        var actions = '<button class="btn btn-primary btn-run" data-id="' + row.id + '" data-tooltip="true" ' +
                            'data-original-title="Отправка рассылки"><i class="fa fa-play"></i></button> ' +
                            '<button class="btn btn-success btn-test" data-id="' + row.id + '" data-tooltip="true" ' +
                            'data-original-title="Отправка рассылки"><i class="fa fa-cogs"></i></button>';

                        return actions;
                    }
                }
            ],
            "responsive": true,
            "language": {
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $(document).on('click', '.btn-run', function () {
            var id = $(this).data('id');

            $.post('/admin/ajax/subscribe/send', {id: id}, function (result) {
                if (!result) {
                    console.log('Запрос на рассылку номера не вернул результата');
                    return;
                }

                var msg = result.body;
                showAlert(msg.body, msg.type);
                tbl.ajax.reload();
            });
        });
    });

</script>