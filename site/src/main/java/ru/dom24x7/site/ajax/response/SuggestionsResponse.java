package ru.dom24x7.site.ajax.response;

import ru.dom24x7.model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 04.10.16.
 *
 * Для плагина autocomplete
 */
public class SuggestionsResponse {

    private List<Suggestion> suggestions;

    public List<Suggestion> getSuggestions() {

        return suggestions;
    }

    public void setSuggestions(List<?> objects) {

        if (suggestions != null) {
            suggestions.clear();
        }
        suggestions = new ArrayList<Suggestion>();

        if (objects == null || objects.size() == 0) {
            return;
        }

        for (Object object : objects) {
            Model model = (Model) object;
            suggestions.add(new Suggestion(model.getName(), String.valueOf(model.getId())));
        }
    }
}