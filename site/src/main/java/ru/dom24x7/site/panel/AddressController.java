package ru.dom24x7.site.panel;

import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.dom24x7.cache.Currencies;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.address.dao.AddressDAOImpl;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.model.reference.currency.Currency;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.StrUtils;
import ru.evgajukov.dadata.DadataAddress;
import ru.evgajukov.dadata.DadataResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 22.09.16.
 *
 * Контроллер для работы с адресами
 */
@Controller
public class AddressController extends BaseController {

    private static final String ACTIVE = "addresses";

    @RequestMapping("/panel/addresses/{addressId}")
    public String metersAction(@PathVariable int addressId, ModelMap model) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            return "redirect:/panel";
        }

        setModelMap(model, address.getTitle(), ACTIVE);
        model.addAttribute("address", address);
        model.addAttribute("meters", new MeterDAOImpl().list(address));

        return "panel/address/meters";
    }

    @RequestMapping(value = "/ajax/addresses/save", method = RequestMethod.POST)
    public @ResponseBody Response saveAddressAction(
            @RequestParam(value = "id", required = false) Integer addressId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "currency", required = false) Integer currencyId,
            @RequestParam(value = "address", required = false) String addressStr,
            @RequestParam(value = "info", required = false) String dadataAddressStr) {

        List<Message> messages = new ArrayList<Message>();

        Address address;
        if (addressId == null) {
            // добавление нового адреса
            address = new Address();
            address.setUser(security.getUser());
        } else {
            // редактирование существующего адреса
            address = security.getUser().getAddress(addressId);
            if (address == null) {
                messages.add(new Message("Не удалось найти адрес с указанным идентификатором", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        Currency currency = null;
        if (currencyId == null || currencyId == 0) {
            messages.add(new Message("Необходимо указать валюту", Message.TYPE_ERROR));
        } else {
            currency = Currencies.getInstance().get(currencyId);
            if (currency == null) {
                messages.add(new Message("Не найдена валюта по указанному идентификатору", Message.TYPE_ERROR));
            }
        }

        if (addressStr == null || addressStr.trim().length() == 0) {
            messages.add(new Message("Необходимо указать адрес", Message.TYPE_ERROR));
        }

        if (messages.size() != 0) {
            return new Response(-1, "ERROR", messages);
        }

        if (dadataAddressStr != null && dadataAddressStr.trim().length() != 0) {
            // имеется дополнительная информация по адресу
            try {
                Gson gson = new Gson();
                DadataResponse dadataResponse = gson.fromJson(dadataAddressStr, DadataResponse.class);
                DadataAddress dadataAddress = dadataResponse.getData();

                address.setInfo(dadataAddress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // теперь можем обновить данные адреса
        address.setTitle(StrUtils.isEmpty(title) ? null : title.trim());
        address.setAddress(addressStr.trim());
        address.setCurrency(currency);

        // и сохранить в БД
        new AddressDAOImpl().save(address);

        return new Response(0, "OK", address);
    }

    @RequestMapping("/ajax/addresses/delete/{addressId}")
    public @ResponseBody Response deleteAddressAction(@PathVariable int addressId) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            messages.add(new Message("Не удалось найти адрес с указанным идентификатором", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        // вместо удаления адрес просто блокируется, чтобы в случае чего можно было бы легко восстановить
        new AddressDAOImpl().hold(address);

        return new Response(0, "OK");
    }

    @RequestMapping("/ajax/addresses/{addressId}/meters")
    public @ResponseBody List<Meter> listMetersAjax(@PathVariable int addressId) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            // адрес не принадлежит пользователю
            return null;
        }

        return new MeterDAOImpl().list(address, false);
    }

    @RequestMapping("/ajax/addresses/{addressId}")
    public @ResponseBody Address getAddressAjax(@PathVariable int addressId) {

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            return null;
        }

        return address;
    }
}