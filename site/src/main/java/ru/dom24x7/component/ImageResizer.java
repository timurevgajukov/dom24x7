package ru.dom24x7.component;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by timur on 24.03.17.
 * Класс для изменения размеров картинки
 */
public class ImageResizer {

    public static BufferedImage resizeByWidth(BufferedImage imageToResize, int width) {

        float dx = ((float) width) / imageToResize.getWidth();

        return resize(imageToResize, width, Math.round(dx * imageToResize.getHeight()));
    }

    public static BufferedImage resize(BufferedImage imageToResize, int width, int height) {

        float dx = ((float) width) / imageToResize.getWidth();
        float dy = ((float) height) / imageToResize.getHeight();

        int genX, genY;
        int startX, startY;

        if (imageToResize.getWidth() <= width && imageToResize.getHeight() <= height) {
            genX = imageToResize.getWidth();
            genY = imageToResize.getHeight();
        } else {
            if (dx <= dy) {
                genX = width;
                genY = (int) (dx * imageToResize.getHeight());
            } else {
                genX = (int) (dy * imageToResize.getWidth());
                genY = height;
            }
        }

        startX = (width - genX) / 2;
        startY = (height - genY) / 2;

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = null;

        try {
            graphics2D = bufferedImage.createGraphics();
            graphics2D.fillRect(0, 0, width, height);
            graphics2D.drawImage(imageToResize, startX, startY, genX, genY, null);
        } finally {
            if (graphics2D != null) {
                graphics2D.dispose();
            }
        }

        return bufferedImage;
    }
}