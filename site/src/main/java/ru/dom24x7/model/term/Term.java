package ru.dom24x7.model.term;

import ru.dom24x7.model.Model;

/**
 * Термин из словаря
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Term extends Model {

	private String term;
	private String body;
	
	public Term() {
		
		super();
	}
	
	public Term(int id) {
		
		super(id);
	}
	
	public String getTerm() {
		
		return term;
	}
	
	public Term setTerm(String term) {
		
		this.term = term;
		
		return this;
	}
	
	public String getBody() {
		
		return body;
	}
	
	public Term setBody(String body) {
		
		this.body = body;
		
		return this;
	}
	
	@Override
	public String toString() {
		
		return term;
	}
}