package ru.evgajukov.profi.dictionary.response.mmetro;

import com.google.gson.annotations.SerializedName;
import ru.evgajukov.profi.dictionary.response.DictionaryItem;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 * Элемент справочника с информацией по метро
 */
public class MMetroItem extends DictionaryItem {

    @SerializedName("city_id")
    private String cityId;
    private String nametr;
    private String txt;
    private List<Area> areas;
    private List<Region> regions;
    private List<Object> okrai;
    private String typ;
    private List<Line> lines;

    public MMetroItem(int id, String name, int status) {

        super(id, name, status);
    }

    public String getCityId() {

        return cityId;
    }

    public void setCityId(String cityId) {

        this.cityId = cityId;
    }

    public String getNametr() {

        return nametr;
    }

    public void setNametr(String nametr) {

        this.nametr = nametr;
    }

    public String getTxt() {

        return txt;
    }

    public void setTxt(String txt) {

        this.txt = txt;
    }

    public List<Area> getAreas() {

        return areas;
    }

    public void setAreas(List<Area> areas) {

        this.areas = areas;
    }

    public List<Region> getRegions() {

        return regions;
    }

    public void setRegions(List<Region> regions) {

        this.regions = regions;
    }

    public List<Object> getOkrai() {

        return okrai;
    }

    public void setOkrai(List<Object> okrai) {

        this.okrai = okrai;
    }

    public String getTyp() {

        return typ;
    }

    public void setTyp(String typ) {

        this.typ = typ;
    }

    public List<Line> getLines() {

        return lines;
    }

    public void setLines(List<Line> lines) {

        this.lines = lines;
    }
}