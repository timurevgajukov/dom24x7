package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.component.TableData;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;
import ru.dom24x7.service.company.model.mq.ReformaGkhLink;
import ru.dom24x7.service.company.model.mq.dao.ReformaGkhLinkDAOImpl;
import ru.dom24x7.site.BaseController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by timur on 17.02.17.
 *
 * Административная панель: управление компаниями УК/ТСЖ
 */
@Controller
public class AdminCompanyController extends BaseController {

    @RequestMapping("/admin/companies")
    public String listAction(ModelMap model) {

        setModelMap(model, "Список компаний");

        return "admin/company/list";
    }

    @RequestMapping("/admin/ajax/companies/list")
    public @ResponseBody TableData listCompaniesAjax(HttpServletRequest request) {

        int draw = Integer.parseInt(request.getParameter("draw"));
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));
        String search = request.getParameter("search[value]");

        int count = new CompanyDAOImpl().count(search);
        List<Company> companies = new CompanyDAOImpl().list(start, length, search);

        if (companies == null) {
            companies = new ArrayList<Company>();
        }

        TableData tableData = new TableData();
        tableData.setDraw(draw);
        tableData.setRecordsTotal(count);
        tableData.setRecordsFiltered(count);
        tableData.setData(companies);

        return tableData;
    }

    @RequestMapping("/admin/companies/parse/reformagkh")
    public String parseReformaGkhAction() {

        // кладем базовую ссылку в очередь
        new ReformaGkhLinkDAOImpl().save(new ReformaGkhLink("https://www.reformagkh.ru/mymanager?geo=reset"));

        return "redirect:/admin/companies";
    }
}