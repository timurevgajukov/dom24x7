package ru.dom24x7.model.access.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.access.Function;
import ru.dom24x7.model.reference.ReferenceDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 14.02.17.
 *
 * Реализация интерфейса справочника функций и разделов сайта
 */
public class FunctionDAOImpl extends ModelDAOImpl implements ReferenceDAO<Function> {

    private static final String QUERY_LIST = "{ call functions_list() }";

    @Override
    public List<Function> list() {

        List<Function> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Function>() {

            @Override
            public Function mapRow(ResultSet resultSet, int i) throws SQLException {

                return new Function(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("description"));
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }
}
