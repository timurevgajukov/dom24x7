package ru.evgajukov.profi.profile.request.filter;

/**
 * Created by timur on 18.06.17.
 * Ценовая категория
 * 0 Минимальная
 * 1 Средняя
 * 2 Выше среднего
 * 3 Максимальная
 */
public class Ckat {

    private int id;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }
}
