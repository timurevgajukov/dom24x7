<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<aside id="menu">
    <div id="navigation">
        <t:insertAttribute name="profile-block" defaultValue="/WEB-INF/templates/homer/panel/includes/profile-block.jsp"/>

        <t:insertAttribute name="menu" defaultValue="/WEB-INF/templates/homer/panel/includes/menu.jsp"/>
    </div>
</aside>