package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.dom24x7.site.BaseController;

/**
 * Created by timur on 17.02.17.
 *
 * Административная панель: статистика по сервису
 */
@Controller
public class AdminStaticController extends BaseController {

    @RequestMapping("/admin/static")
    public String indexAction(ModelMap model) {

        setModelMap(model, "Статистика");

        return "admin/static/index";
    }
}