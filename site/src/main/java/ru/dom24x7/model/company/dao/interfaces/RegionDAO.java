package ru.dom24x7.model.company.dao.interfaces;

import ru.dom24x7.model.company.Region;

import java.util.List;

/**
 * Created by timur on 08.06.17.
 * Интерфейс для работы с регионами
 */
public interface RegionDAO {

    /**
     * Возвращает список регионов
     * @return
     */
    List<Region> list();
}