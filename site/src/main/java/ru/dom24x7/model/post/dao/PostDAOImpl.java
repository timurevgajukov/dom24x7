package ru.dom24x7.model.post.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.cache.Categories;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.post.Category;
import ru.dom24x7.model.post.Post;
import ru.dom24x7.model.post.dao.interfaces.PostDAO;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.utils.DBase;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 * <p>
 * Реализация интерфейса для работы с постами пользователей
 */
public class PostDAOImpl extends ModelDAOImpl implements PostDAO {

    private static final String QUERY_GET_BY_ID = "{ call posts_get_by_id(?) }";
    private static final String QUERY_LIST_FOR_BLOG = "{ call posts_list_for_blog(?,?) }";
    private static final String QUERY_LIST_FOR_BLOG_BY_CATEGORY = "{ call posts_list_for_blog_by_category(?,?,?) }";
    private static final String QUERY_LIST_FOR_NAV = "{ call posts_list_for_nav() }";
    private static final String QUERY_SAVE = "{ call posts_save(?,?,?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call posts_delete(?) }";
    private static final String QUERY_LIST = "{ call posts_list(?,?,?) }";
    private static final String QUERY_COUNT = "{ call posts_count(?) }";

    @Override
    public Post get(int id) {

        List<Post> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Post>() {
            @Override
            public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<Post> listForBlog(int offset, int count) {

        List<Post> list = jdbcTemplate.query(QUERY_LIST_FOR_BLOG,
                new Object[]{
                        offset, count
                }, new RowMapper<Post>() {
                    @Override
                    public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Post> listForBlog(int offset, int count, Category category) {

        List<Post> list = jdbcTemplate.query(QUERY_LIST_FOR_BLOG_BY_CATEGORY,
                new Object[]{
                        offset, count, category.getId()
                }, new RowMapper<Post>() {
                    @Override
                    public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Post> listForNav() {

        List<Post> list = jdbcTemplate.query(QUERY_LIST_FOR_NAV, new RowMapper<Post>() {
            @Override
            public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                return new Post(resultSet.getInt("id"), resultSet.getString("title"));
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Post post) {

        Post newPost = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        post.getId() == 0 ? null : post.getId(),
                        post.getPublishedDt(),
                        post.getAuthor().getId(),
                        post.getTitle(),
                        post.getText(),
                        post.getCategory().getId(),
                        post.getSource()
                }, new RowMapper<Post>() {
                    @Override
                    public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        post.setId(newPost.getId());

        return true;
    }

    @Override
    public boolean delete(Post post) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{post.getId()}) != 0;
    }

    @Override
    public List<Post> list(int offset, int count, String search) {

        List<Post> list = jdbcTemplate.query(QUERY_LIST,
                new Object[]{
                        offset,
                        count,
                        DBase.getSearchForLike(search)
                }, new RowMapper<Post>() {
                    @Override
                    public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public int count(String search) {

        return jdbcTemplate.queryForObject(QUERY_COUNT, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, DBase.getSearchForLike(search));
    }

    private Post load(ResultSet rs) throws SQLException {

        Post post = new Post(rs.getInt("id"));
        post.setCreateDt(rs.getTimestamp("create_dt"));
        post.setPublishedDt(rs.getTimestamp("published_dt"));
        post.setAuthor(new UserDAOImpl().get(rs.getInt("user_id")));
        post.setTitle(rs.getString("title"));
        post.setText(rs.getString("text"));
        post.setViews(rs.getInt("views"));
        post.setCategory(Categories.getInstance().get(rs.getInt("category_id")));
        post.setSource(rs.getString("source"));

        return post;
    }
}