package ru.dom24x7.site.api.message;

/**
 * Created by timur on 22.02.17.
 * API: базовый заголовок
 */
public class BaseHeader {

    private String method;
    private String token;
    private String session;

    public BaseHeader() {}

    public BaseHeader(String method, String token, String session) {

        this.method = method;
        this.token = token;
        this.session = session;
    }

    public String getMethod() {

        return method;
    }

    public void setMethod(String method) {

        this.method = method;
    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {

        this.token = token;
    }

    public String getSession() {

        return session;
    }

    public void setSession(String session) {

        this.session = session;
    }
}