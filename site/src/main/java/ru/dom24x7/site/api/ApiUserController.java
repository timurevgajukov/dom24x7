package ru.dom24x7.site.api;

import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.site.api.message.AuthRequestBody;
import ru.dom24x7.site.api.message.BaseRequest;
import ru.dom24x7.site.api.message.BaseResponse;
import ru.dom24x7.site.api.message.ResponseCode;

/**
 * Created by timur on 22.02.17.
 * API: Пользователи
 */
public class ApiUserController {

    private BaseRequest request;
    private BaseResponse response;

    public ApiUserController(BaseRequest request) {

        this.request = request;
        this.response = new BaseResponse(request);
    }

    public BaseResponse auth() {

        AuthRequestBody body = (AuthRequestBody) request.getBody();
        User user = new UserDAOImpl().auth(body.getLogin(), body.getPassword());
        if (user == null) {
            return response.setCode(-3).setErrorMessages(ResponseCode.CODES.get(-3));
        }

        return null;
    }

    public BaseResponse reg() {

        return null;
    }
}