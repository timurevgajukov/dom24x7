<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-lg-8 col-md-10 col-sm-12">
        <div class="hpanel">
            <div class="panel-body">
                <div id="profi-container"></div>
            </div>
        </div>
    </div>
</div>

<script src="//partner.profi.ru/cpa/widget/bootstrap.js"></script>
<script>
    profiru.widget({
        project: 'remont',
        widget: 'listing',
        params: {
            buttonText: 'Выбрать'
        },
        container: 'profi-container',
        partner: 'dom24x7ru'
    })
</script>