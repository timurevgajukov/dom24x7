package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.dom24x7.site.BaseController;

/**
 * Created by timur on 17.02.17.
 *
 * Административная панель: управление статичными страницами
 */
@Controller
public class AdminPageController extends BaseController {

    @RequestMapping("/admin/pages")
    public String listAction(ModelMap model) {

        setModelMap(model, "Список статичных страниц");

        return "admin/page/list";
    }
}