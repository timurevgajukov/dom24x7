<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="/resources/lib/dadata/suggestions/17.2/suggestions.css" type="text/css" rel="stylesheet"/>

<!-- EDIT ADDRESS MODAL WINDOW: BEGIN -->
<div id="address-edit-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 id="address-edit-modal-title" class="modal-title">Сохранение адреса</h4>
            </div>
            <form id="address-form" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <input id="modal-address-title" type="text" class="form-control"
                                       placeholder="Введина название адреса"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <select id="modal-address-currency" class="form-control"
                                        data-placeholder="Выберите валюту"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input id="modal-address-address" type="text" class="form-control" placeholder="Введите адрес"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-address-save" class="ladda-button btn btn-success" type="button" data-style="zoom-in">Сохранить</button>
                    <button id="btn-address-delete" class="btn btn-danger" type="button">Удалить</button>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT ADDRESS MODAL WINDOW: END -->

<!--[if lt IE 10]>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/resources/lib/dadata/suggestions/17.2/jquery.suggestions.min.js"></script>

<script>
    $(document).ready(function () {
        var addressId = undefined;
        var dadataAddress = undefined; // dadata-структура по выбранному адресу

        $("#modal-address-address").suggestions({
            serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
            token: "bea5b313ae276907f1d76cc1c2ac93a9902e11af",
            type: "ADDRESS",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (suggestion) {
                console.log(suggestion);
                dadataAddress = suggestion;
            }
        });

        loadCurrencies();

        // открытие модального окна для добавления нового адреса
        $('.btn-address-add').click(function () {
            $('#address-edit-modal-title').html('Добавление адреса');
            $('#btn-address-delete').hide();

            addressId = undefined;

            initAddress();
        });

        // открытие модального окна для редактирования существующего адреса
        $('#btn-address-edit').click(function () {
            $('#address-edit-modal-title').html('Редактирование адреса');
            $('#btn-address-delete').show();

            addressId = $(this).data('id');
            initAddress(addressId);
        });

        // сохранение данных по адресу (новому или редактируемому)
        var btnAddressSave = $('#btn-address-save').ladda();
        btnAddressSave.click(function () {
            var data = {
                title: $('#modal-address-title').val(),
                address: $('#modal-address-address').val(),
                currency: $('#modal-address-currency').val()
            };

            if (data.title == null || data.title.trim().length == 0) {
                showAlert('Необходимо указать наименование адреса, например, "Мой дом"', 'warning');
                $('#modal-address-title').focus();
                return;
            }

            if (data.address == null || data.address.trim().length == 0) {
                showAlert('Необходимо указать адрес', 'warning');
                $('#modal-address-address').focus();
                return;
            }

            if (addressId !== undefined && addressId != null) {
                // редактирование существующего адреса
                data.id = addressId;
            }

            if (dadataAddress !== undefined && dadataAddress == null) {
                // имеется дополнительная информация по адресу
                data.info = JSON.stringify(dadataAddress);
                return;
            }

            btnAddressSave.ladda('start');
            $.post('/ajax/addresses/save', data, function (result) {
                btnAddressSave.ladda('stop');
                if (result === undefined || result == null) {
                    showAlert('Непредвиденная ошибка', 'error');
                    return;
                }

                if (result.code == 0) {
                    var address = result.body;
                    if (data.id) {
                        // отредактирование существующий адрес
                        showAlert('Успешно отредактировали адрес "' + address.title + '"', 'info');
                        // обновить отображаемые данные
                        $('#address-title').html(address.title);
                        $('#address-address').html(address.address);
                        // TODO: обновить боковое меню
                        // TODO: обновить валюты

                        $('#address-edit-modal').modal('hide');
                    } else {
                        // добавлен новый адрес, перейти на его страницу
                        window.location = '/panel/addresses/' + address.id;
                    }
                } else {
                    for (var i = 0; i < result.body.length; i++) {
                        var error = result.body[i];
                        showAlert(error.body, error.type);
                    }
                }
            }).error(function () {
                btnAddressSave.ladda('stop');
            });
        });

        // удаление существующего адреса
        $('#btn-address-delete').click(function () {
            if (!addressId) {
                return;
            }

            swal({
                    title: "Вы уверены, что хотите удалить адрес?",
                    text: "После удаления адрес с историей восстановить уже не сможете!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да, удалить!",
                    cancelButtonText: "Нет, не удалять!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // отправляем запрос на удаление
                        $.get('/ajax/addresses/delete/' + addressId, function (result) {
                            if (!result) {
                                swal("Ошибка", "Неизвестная ошибка. Не удалось удалить адрес.", "error");
                                return;
                            }

                            if (result.code != 0) {
                                var errorStr = '';
                                for (var i = 0; i < result.body.length; i++) {
                                    errorStr += result.body[i] + ' ';
                                }
                                swal("Ошибка", errorStr, "error");
                                return;
                            }

                            swal("Удалено!", "Ваш адрес успешно удален.", "success");
                            // закрываем модальное окно
                            $('#address-edit-modal').modal('hide');
                            window.location = '/panel';
                        });
                    } else {
                        swal("Отмена", "Ваш адрес остался доступным.", "error");
                    }
                });
        });
    });

    /**
     * Загрузка списка доступных валют
     */
    function loadCurrencies() {
        $.post('/ajax/currencies/list', function (currencies) {
            if (currencies === undefined || currencies == null) {
                return;
            }

            var currenciesList = $('#modal-address-currency');
            currenciesList.empty();
            for (var i = 0; i < currencies.length; i++) {
                var cur = currencies[i];
                var item = '<option value="' + cur.id + '">' + cur.name + '</option>';
                currenciesList.append(item);
            }
        });
    }

    /**
     * Загрузка данных по адресу
     * @param addressId
     */
    function initAddress(addressId) {
        var title = $('#modal-address-title');
        var currency = $('#modal-address-currency');
        var addressStr = $('#modal-address-address');

        title.val('');
        currency.val('');
        addressStr.val('');

        if (addressId === undefined || addressId == null) {
            return;
        }

        $.post('/ajax/addresses/' + addressId, function (address) {
            if (address === undefined || address == null) {
                return;
            }

            title.val(address.title);
            currency.val(address.currency.id);
            addressStr.val(address.address);
        });
    }
</script>