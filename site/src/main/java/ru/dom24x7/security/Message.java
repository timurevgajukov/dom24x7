package ru.dom24x7.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by timur on 18.09.16.
 *
 * Сообщения нотификации
 */
public class Message implements Serializable {

    public static final String TYPE_INFO = "info";
    public static final String TYPE_PRIMARY = "primary";
    public static final String TYPE_SUCCESS = "success";
    public static final String TYPE_WARNING = "warning";
    public static final String TYPE_DANGER = "error";
    public static final String TYPE_MINT = "mint";
    public static final String TYPE_PURPLE = "purple";
    public static final String TYPE_PINK = "pink";
    public static final String TYPE_DARK = "dark";
    public static final String TYPE_ERROR = "error";

    public static final String LABEL_NEW = "NEW";
    public static final String LABEL_INF = "INF";
    public static final String LABEL_WARN = "WARN";
    public static final String LABEL_ERR = "ERR";

    private Map<String, String> map;

    {
        map = new HashMap<String, String>();
        map.put(TYPE_INFO, LABEL_INF);
        map.put(TYPE_WARNING, LABEL_WARN);
        map.put(TYPE_ERROR, LABEL_ERR);
    }

    private String title;
    private String body;
    private String type;
    private String label;
    private String url;

    public Message(String body, String type) {

        this.body = body;
        this.type = type;
        this.label = map.get(type);
    }

    public Message(String title, String body, String type) {

        this.title = title;
        this.body = body;
        this.type = type;
        this.label = map.get(type);
    }

    public Message(String title, String body, String type, String label) {

        this.title = title;
        this.body = body;
        this.type = type;
        this.label = label;
    }

    public Message(String title, String body, String type, String label, String url) {

        this.title = title;
        this.body = body;
        this.type = type;
        this.label = label;
        this.url = url;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getLabel() {

        return label;
    }

    public void setLabel(String label) {

        this.label = label;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    @Override
    public String toString() {

        return body;
    }
}