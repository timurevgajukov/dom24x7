<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel">
    <div class="panel-heading hbuilt">
        <div class="pull-right">
            <button id="btn-post-add" class="btn btn-default btn-xs" data-toggle="modal" data-target="#post-edit-modal">
                <i class="fa fa-plus add-tooltip" data-toggle="tooltip" data-original-title="Добавить пост"></i>
            </button>
        </div>
        ${title}
    </div>
    <div class="panel-body">
        <table id="posts-table" class="table table-striped table-bordered table-hover" width="100%">
            <thead>
            <tr>
                <th>#</th>
                <th>Заголовок</th>
                <th>Категория</th>
                <th>Автор</th>
                <th>Создан</th>
                <th>Опубликовано</th>
                <th>Действия</th>
            </tr>
            </thead>
        </table>
    </div>
</div>

<t:insertAttribute name="post-edit" defaultValue="/WEB-INF/views/admin/blog/modal/post-edit.jsp"/>

<script>
    $(document).ready(function () {
        var postsTableOptions = {
            "processing": true,
            "serverSide": true,
            "ajax": "/admin/ajax/posts/list",
            "bAutoWidth": false,
            "columns": [
                {"data": "id"},
                {"data": "showTitle"},
                {
                    "data": function (row) {
                        return row.category.name;
                    }
                },
                {
                    "data": function (row) {
                        var author = row.author;
                        var result = '';
                        if (author.fio == null || author.fio.trim().length == 0) {
                            result = author.email;
                        } else {
                            result = author.fio;
                        }

                        result += ' [' + author.role.name + ']';

                        return result;
                    }
                },
                {
                    "data": function (row) {
                        return moment(row.createDt).format('DD.MM.YYYY HH:mm:ss');
                    }
                },
                {
                    "data": function (row) {
                        if (!row.publishedDt) {
                            return '';
                        } else {
                            return moment(row.publishedDt).format('DD.MM.YYYY HH:mm');
                        }
                    }
                },
                {
                    "data": function (row) {
                        var btn = '<button class="btn-post-edit btn btn-default btn-xs" data-toggle="modal" data-target="#post-edit-modal" data-id="' + row.id + '">';
                        btn += '<i class="fa fa-pencil add-tooltip" data-toggle="tooltip" data-original-title="Редактировать пост"></i>';
                        btn += '</button>';

                        return btn;
                    }
                }
            ],
            "responsive": true,
            "language": {
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        };
        var postsTable = $('#posts-table').dataTable(postsTableOptions);

        var postId = undefined;

        $('#modal-post-text').summernote({
            height: 200
        });

        var photoBlock = undefined;

        // открытие модального окна для добавления нового поста
        $('#btn-post-add').click(function () {
            $('#post-edit-modal-title').html('Добавление поста');
            $('#btn-post-delete').hide();

            postId = undefined;

            initPost(postId);
        });

        // открытие модального окна для редактирования поста
        $(document).on('click', '.btn-post-edit', function () {
            $('#post-edit-modal-title').html('Редактирование поста');
            $('#btn-post-delete').show();

            postId = $(this).data('id');

            initPost(postId);
        });

        // сохраняем данные по посту (новому или существующему)
        var btnPostSave = $('#btn-post-save').ladda();
        btnPostSave.click(function () {
            var blocks = photoBlock.blocks();
            var photos = [];
            var photosFileName = [];
            if (blocks != null && blocks.length != 0) {
                for (var i = 0; i < blocks.length; i++) {
                    var photoFieldData = blocks[i][0].get();
                    if (photoFieldData.value != null) {
                        photos.push(photoFieldData.value);
                        photosFileName.push(photoFieldData.fileName);
                    }
                }
            }

            var data = {
                title: $('#modal-post-title').val(),
                text: $('#modal-post-text').code(),
                category: $('#modal-post-category').val(),
                published: $('#modal-post-published').val(),
                source: $('#modal-post-source').val(),
                photos: photos.join('##'),
                photosFileName: photosFileName.join('##')
            };

            if (data.text == null || data.text.trim().length == 0) {
                // только если не добавлены картинки
                if (data.photos == null || data.photos.trim().length == 0) {
                    showAlert('Необходимо добавить текст поста', 'warning');
                    return;
                }
            }

            if (postId !== undefined && postId != null) {
                // редактирование существующего поста
                data.id = postId;
            }

            // отправляем запрос на сохранение
            btnPostSave.ladda('start');
            $.post('/admin/ajax/posts/save', data, function (result) {
                btnPostSave.ladda('stop');
                if (!result) {
                    showAlert('Непредвиденная ошибка', 'error');
                    return;
                }

                if (result.code  == 0) {
                    var post = result.body;
                    showAlert('Успешно сохранили пост', 'info');
                    // обновляем таблицу
                    postsTable.fnDestroy();
                    postsTable = $('#posts-table').dataTable(postsTableOptions);

                    $('#post-edit-modal').modal('hide');
                } else {
                    for (var i = 0; i < result.body.length; i++) {
                        var error = result.body[i];
                        showAlert(error.body, error.type);
                    }
                }
            }).error(function () {
                btnPostSave.ladda('stop');
            });
        });

        // удаление существующего поста
        $('#btn-post-delete').click(function () {
            if (!postId) {
                return;
            }

            swal({
                    title: "Вы уверены, что хотите удалить пост?",
                    text: "После удаления поста вы его уже не сможете восстановить!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да, удалить!",
                    cancelButtonText: "Нет, не удалять!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // отправляем запрос на удаление
                        $.get('/admin/ajax/posts/delete/' + postId, function (result) {
                            if (!result) {
                                swal("Ошибка", "Неизвестная ошибка. Не удалось удалить пост.", "error");
                                return;
                            }

                            if (result.code != 0) {
                                var errorStr = '';
                                for (var i = 0; i < result.body.length; i++) {
                                    errorStr += result.body[i] + ' ';
                                }
                                swal("Ошибка", errorStr, "error");
                                return;
                            }

                            swal("Удалено!", "Ваш пост успешно удален.", "success");
                            // закрываем модальное окно
                            $('#post-edit-modal').modal('hide');
                            // обновляем таблицу
                            postsTable.fnDestroy();
                            postsTable = $('#posts-table').dataTable(postsTableOptions);
                        });
                    } else {
                        swal("Отмена", "Ваш пост остался доступным.", "error");
                    }
                });
        });

        /**
         * Инициализация полей поста
         * @param postId
         */
        function initPost(postId) {
            var title = $('#modal-post-title');
            var text = $('#modal-post-text');
            var published = $('#modal-post-published');
            var photo = $('#modal-post-photo');
            var category = $('#modal-post-category');
            var source = $('#modal-post-source');

            title.val('');
            text.code('');
            published.val(moment().format('DD.MM.YYYY HH:mm'));
            category.val('');
            source.val('');
            photo.empty();

            var data = {
                title: 'Картинки для поста',
                collapsable: false,
                collapsed: false,
                multiPage: true,
                fields: [
                    {
                        id: 1,
                        type: 'Picture',
                        name: 'post-photo',
                        values: []
                    }
                ]
            };

            if (postId === undefined || postId == null) {
                photoBlock = new BlockField({data: data});
                photo.append(photoBlock.html());
                return;
            }

            $.get('/admin/ajax/posts/' + postId, function (post) {
                if (!post) {
                    return;
                }

                title.val(post.title);
                text.code(post.text);
                category.val(post.category.id);
                source.val(post.source);
                if (post.publishedDt) {
                    published.val(moment(post.publishedDt).format('DD.MM.YYYY HH:mm'));
                } else {
                    published.val(moment(post.createDt).format('DD.MM.YYYY HH:mm'));
                }
                if (post.photos != null && post.photos.length != 0) {
                    for (var i = 0; i < post.photos.length; i++) {
                        data.fields[0].values.push(post.photos[i].base64);
                    }
                    photoBlock = new BlockField({data: data});
                    for (i = 0; i < post.photos.length; i++) {
                        photoBlock.fields(i)[0]._settings.fileName = post.photos[i].fileName;
                    }
                } else {
                    photoBlock = new BlockField({data: data});
                }
                photo.append(photoBlock.html());
            })
        }
    });
</script>