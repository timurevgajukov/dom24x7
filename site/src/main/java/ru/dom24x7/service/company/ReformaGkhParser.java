package ru.dom24x7.service.company;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.dao.CompanyDAOImpl;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 06.04.17.
 * Парсер для получения данных по УК и ТСЖ с сайта "Реформа ЖКХ" reformagkh.ru
 */
public class ReformaGkhParser implements ISiteParser {

    private static int PROXY_INDEX;
    private static List<ProxyMapItem> PROXY_LIST;

    static {
        PROXY_INDEX = 0;
        PROXY_LIST = new ArrayList<ProxyMapItem>();
        PROXY_LIST.add(new ProxyMapItem("77.73.65.155"));
        PROXY_LIST.add(new ProxyMapItem("83.239.58.162"));
        PROXY_LIST.add(new ProxyMapItem("194.8.51.253"));
        PROXY_LIST.add(new ProxyMapItem("46.37.193.74"));
        PROXY_LIST.add(new ProxyMapItem("85.172.190.250"));
        PROXY_LIST.add(new ProxyMapItem("77.73.66.136"));
        PROXY_LIST.add(new ProxyMapItem("194.8.51.253"));
        PROXY_LIST.add(new ProxyMapItem("89.38.149.185"));
        PROXY_LIST.add(new ProxyMapItem("177.67.82.97"));
        PROXY_LIST.add(new ProxyMapItem("167.114.17.152"));
        PROXY_LIST.add(new ProxyMapItem("192.95.4.14"));
        PROXY_LIST.add(new ProxyMapItem("94.177.234.145"));
        PROXY_LIST.add(new ProxyMapItem("177.93.106.221"));
        PROXY_LIST.add(new ProxyMapItem("208.73.205.42"));
        PROXY_LIST.add(new ProxyMapItem("192.241.186.137"));
        PROXY_LIST.add(new ProxyMapItem("45.40.178.111"));
        PROXY_LIST.add(new ProxyMapItem("149.56.121.156"));
        PROXY_LIST.add(new ProxyMapItem("177.67.82.67"));
        PROXY_LIST.add(new ProxyMapItem("177.67.81.97"));
        PROXY_LIST.add(new ProxyMapItem("138.197.23.220"));
        PROXY_LIST.add(new ProxyMapItem("94.177.229.138"));
        PROXY_LIST.add(new ProxyMapItem("94.177.242.85"));
        PROXY_LIST.add(new ProxyMapItem("188.165.56.137"));
        PROXY_LIST.add(new ProxyMapItem("191.96.28.244"));
        PROXY_LIST.add(new ProxyMapItem("154.16.127.141"));
        PROXY_LIST.add(new ProxyMapItem("192.95.21.165"));
        PROXY_LIST.add(new ProxyMapItem("198.199.84.127"));
        PROXY_LIST.add(new ProxyMapItem("61.7.228.51"));
        PROXY_LIST.add(new ProxyMapItem("138.197.83.21"));
        PROXY_LIST.add(new ProxyMapItem("181.215.96.118"));
        PROXY_LIST.add(new ProxyMapItem("177.11.55.68"));
        PROXY_LIST.add(new ProxyMapItem("177.54.144.209"));
        PROXY_LIST.add(new ProxyMapItem("192.99.98.159"));
        PROXY_LIST.add(new ProxyMapItem("212.237.6.142"));
        PROXY_LIST.add(new ProxyMapItem("94.177.243.163"));
        PROXY_LIST.add(new ProxyMapItem("177.67.81.212"));
        PROXY_LIST.add(new ProxyMapItem("198.50.240.18"));
        PROXY_LIST.add(new ProxyMapItem("200.229.195.169"));
        PROXY_LIST.add(new ProxyMapItem("45.40.179.86"));
        PROXY_LIST.add(new ProxyMapItem("201.16.140.205"));
        PROXY_LIST.add(new ProxyMapItem("31.220.58.61"));
        PROXY_LIST.add(new ProxyMapItem("149.56.147.33"));
        PROXY_LIST.add(new ProxyMapItem("139.59.102.94"));
        PROXY_LIST.add(new ProxyMapItem("144.217.48.73"));
        PROXY_LIST.add(new ProxyMapItem("192.99.190.182"));
        PROXY_LIST.add(new ProxyMapItem("94.177.233.247"));
        PROXY_LIST.add(new ProxyMapItem("94.177.252.138"));
    }

    private static final String BASE_URL = "https://www.reformagkh.ru";
    private static final String LOCATION_LINK = "/mymanager?";
    private static final String RATING_LINK = "/mymanager/rating?";
    private static final String PROFILE_LINK = "/mymanager/profile";
    private static final String CAPTCHA_LINK = "captcha-form";

    private static final long ERROR_TIMEOUT = 5 * 60 * 1000; // 5 мин

    private Document doc;

    private String link; // ссылка для обработки
    private List<String> links;

    {
        links = new ArrayList<String>();
    }

    public ReformaGkhParser(String link) {

        this.link = link;
    }

    @Override
    public int parse() {

        if (link.indexOf(PROFILE_LINK) != -1) {
            // ссылка на профиль компании
            // получаем данные
            // пока не умеем обрабатывать
            parseProfile(link);
            return -1;
        } else if (link.indexOf(RATING_LINK) != -1) {
            // ссылка на рейтинг
            // получаем ссылки на профили компаний
            links = getProfileRefs(link);
        } else {
            // ссылка на регион
            // получаем ссылки на другие регионы или рейтинги
            List<String> anyLinks = getGeoRefs(link);
            for (String link : anyLinks) {
                if (link.indexOf(RATING_LINK) != -1) {
                    links.add(link + "&sort=name&order=asc&page=1&limit=1000000");
                } else if (link.indexOf(LOCATION_LINK) != -1) {
                    links.add(link);
                }
            }
        }

        return links.size();
    }

    public List<String> links() {

        return links;
    }

    /**
     * Разбор страницы профиля компании
     * @param profileLink
     */
    private void parseProfile(String profileLink) {
        // формируем ссылку на текущую анкету
        String link = profileLink.split("\\?")[0].replaceAll("profile", "profile/profile");
        int code = initDocument(link);
        if (code == 404) {
            return;
        }

        String locName = doc.getElementsByClass("loc_name").get(0).html();
        System.out.println(locName);

        // получаем вкладку: Текущая анкета -> Общие сведения об организации
        Element tab = doc.getElementById("tab2-subtab1");
        if (tab == null) {
            System.out.println("Не найдена вкладка общих сведений об организации");
            return;
        }

        Company company = new Company();
        company.setReformaGkhUrl(profileLink);
        company.setReformaGkhParseDt(new Date());

        // получаем основные данные
        Elements lefts = tab.getElementsByClass("left");
        if (lefts != null && lefts.size() != 0) {
            for (Element left : lefts) {
                try {
                    String title = left.getElementsByTag("span").get(0).html();
                    String value = left.nextElementSibling().getElementsByTag("span").get(0).html();
                    // System.out.println(String.format("%s = %s", title, value));

                    if ("Сокращенное наименование".equalsIgnoreCase(title)) {
                        company.setName(getStringValue(value));
                    } else if ("Фирменное наименование юридического лица (согласно уставу организации)".equalsIgnoreCase(title)) {
                        company.setFullName(getStringValue(value));
                    } else if ("Место государственной регистрации юридического лица (адрес юридического лица)".equalsIgnoreCase(title)) {
                        // приоритет у адреса фактического местонахождения
                        if (company.getAddress() == null) {
                            company.setAddress(getStringValue(value));
                        }
                    } else if ("Адрес фактического местонахождения органов управления".equalsIgnoreCase(title)) {
                        company.setAddress(getStringValue(value));
                    } else if ("Контактные телефоны".equalsIgnoreCase(title)) {
                        company.setPhone(getStringValue(value));
                    } else if ("Адрес электронной почты (при наличии)".equalsIgnoreCase(title)) {
                        company.setEmail(getStringValue(value));
                    } else if ("Официальный сайт в сети Интернет (при наличии)".equalsIgnoreCase(title)) {
                        company.setSite(getStringValue(value));
                    } else if ("ФИО руководителя".equalsIgnoreCase(title)) {
                        company.setDirectorFIO(getStringValue(value));
                    } else if (title.indexOf("ИНН") != -1) {
                        // проверяем, не обрабатывали ли мы ранее эту компанию
                        Company oldCompany = new CompanyDAOImpl().getByINN(value);
                        if (oldCompany != null) {
                            System.out.println(">>>>> Ссылка ранее уже была обработана");
                            return;
                        }
                        company.setInn(getStringValue(value));
                    } else if (title.indexOf("ОГРН") != -1) {
                        company.setOgrn(getStringValue(value));
                    } else if ("Режим работы, в т. ч. часы личного приема граждан".equalsIgnoreCase(title)) {
                        company.setOpeningHours(getStringValue(value));
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        // получаем координаты
        Element script = doc.getElementById("map").getElementsByTag("script").get(1);
        if (script != null) {
            String jsCodeStr = script.html();
            String coordStr = jsCodeStr.split("center: \\[")[1].split("],")[0];
            if (coordStr != null) {
                String[] coords = coordStr.split(",");
                company.setCoordLat(Double.parseDouble(coords[0]));
                company.setCoordLng(Double.parseDouble(coords[1]));
            }
        }

        // теперь можно сохранить данные по компании
        new CompanyDAOImpl().save(company);
    }

    private String getStringValue(String value) {

        if ("Не заполнено".equalsIgnoreCase(value) || value == null || value.trim().length() == 0) {
            return null;
        }

        return value.trim();
    }

    /**
     * Возвращает все ссылки на профили компаний со страницы рейтинга
     *
     * @param ratingLink
     * @return
     */
    private List<String> getProfileRefs(String ratingLink) {

        List<String> links = new ArrayList<String>();

        int code = initDocument(ratingLink);
        if (code == 404) {
            return links;
        }

        Elements elements = doc.getElementsByTag("a");
        for (Element element : elements) {
            String href = BASE_URL + element.attr("href").split("&PHPSESSID")[0];
            if (href.indexOf(PROFILE_LINK) != -1) {
                links.add(href);
            }
        }

        return links;
    }

    /**
     * Возвращает гео ссылки с указанной страницы
     *
     * @param baseLink
     * @return
     */
    private List<String> getGeoRefs(String baseLink) {

        List<String> links = new ArrayList<String>();

        int code = initDocument(baseLink);
        if (code == 404) {
            return links;
        }

        Elements elements = doc.getElementsByClass("georefs");
        for (Element element : elements) {
            String href = BASE_URL + element.attr("href").split("&PHPSESSID")[0];
            links.add(href);
        }

        return links;
    }

    /**
     * Пытается инициализировать document с учетом возможных блокировок
     *
     * @param link
     * @return
     */
    private int initDocument(String link) {

        int responseCode = 0;

        boolean connectFlag = false;
        while (!connectFlag) {
            if (PROXY_INDEX == PROXY_LIST.size()) {
                PROXY_INDEX = 0;
            }
            ProxyMapItem proxyMapItem = PROXY_LIST.get(PROXY_INDEX++);

            if (!proxyMapItem.isForbidden()) {
                HttpsURLConnection conn = null;
                BufferedReader in = null;
                try {
                    System.setProperty("https.protocols", "TLSv1.1");
                    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyMapItem.getLink(), 8080));
                    conn = (HttpsURLConnection) new URL(link).openConnection(proxy);
                    conn.setConnectTimeout(15000);
                    conn.setReadTimeout(15000);
                    conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0;    H010818)");
                    conn.connect();
                    responseCode = conn.getResponseCode();
                    if (responseCode == conn.HTTP_OK) {
                        in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String strLine;
                        StringBuilder finalHTML = new StringBuilder();
                        while ((strLine = in.readLine()) != null) {
                            finalHTML.append(strLine);
                        }

                        doc = Jsoup.parse(finalHTML.toString());

                        // оказывается у них еще и технические работы периодически бывают
                        if (doc.title().toLowerCase().indexOf("технические работы") != -1) {
                            System.out.println(">>>>> Ведутся технические работы. Подождем немного и вновь попытаемся.");
                            Thread.sleep(ERROR_TIMEOUT);
                        } else {
                            // все хорошо
                            connectFlag = true;
                        }
                    } else if (responseCode == conn.HTTP_NOT_FOUND) {
                        // не нашли ресурс по ссылке
                        connectFlag = true;
                        doc = null;
                        System.out.println(">>>>> По указанной ссылке ресурс не найден");
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    if (e.getMessage().indexOf("403 Forbidden") != -1) {
                        // сайт заблокировал прокси
                        proxyMapItem.setForbidden(true);
                        System.out.println(">>>>> Сайт заблокировал используемый прокси. Больше его не используем");
                    }
                    System.out.println(">>>>> Попробуем загрузить еще раз по ссылке: " + link);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    System.out.println(">>>>> Попробуем загрузить еще раз по ссылке: " + link);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            }
        }

        return responseCode;
    }
}
