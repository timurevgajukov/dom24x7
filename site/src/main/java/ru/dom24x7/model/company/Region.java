package ru.dom24x7.model.company;

import ru.dom24x7.model.Model;

/**
 * Created by timur on 08.06.17.
 * Регион размещения управляющей компании
 */
public class Region extends Model {

    private int companies;

    public Region(int id, String name, int companies) {

        super(id, name);
        this.companies = companies;
    }

    public int getCompanies() {

        return companies;
    }

    public void setCompanies(int companies) {

        this.companies = companies;
    }
}