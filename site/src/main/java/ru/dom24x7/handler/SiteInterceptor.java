package ru.dom24x7.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.security.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by timur on 18.09.16.
 *
 * Для перехвата запросов к сайту
 */
@Controller
public class SiteInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    protected Security security;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // для локального сайта работает отладочный режим
        debugMode(request);

        // избавляться от www
        String url = request.getRequestURL().toString();
        if (url.indexOf("www") != -1) {
            // редирект на ссылку без www
            String urlWithoutWWW = url.replaceAll("www\\.", "");
            response.sendRedirect(urlWithoutWWW);
            return false;
        }

        if (!security.isAccess(request.getServletPath())) {
            // нет доступа
            response.sendRedirect("/");
            return false;
        }

        return super.preHandle(request, response, handler);
    }

    /**
     * Автоматически авторизует администратора, если сайт запущен локально
     */
    private void debugMode(HttpServletRequest request) {
        if ("localhost".equalsIgnoreCase(request.getServerName())) {
            security.setDemo(true);
            if (!security.isAuth()) {
                security.setUser(new UserDAOImpl().getByEmail("evgajukov@gmail.com"));
            }
        }
    }
}