package ru.dom24x7.cache.interfaces;

import java.util.List;

/**
 * Created by Тимур on 26.04.2016.
 *
 * Интерфейс для работы с кэшем
 */
public interface ICache<T> {

    /**
     * Очищает кэш для перезагрузки
     */
    void clear();

    /**
     * Возвращает список закешированных данных
     *
     * @return
     */
    List<T> list();

    /**
     * Возвращает элемент по его порядковому номеру
     *
     * @param id
     * @return
     */
    T get(int id);
}