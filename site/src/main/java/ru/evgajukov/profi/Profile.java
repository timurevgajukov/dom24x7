package ru.evgajukov.profi;

import ru.evgajukov.profi.dictionary.response.pservice.PServiceItem;
import ru.evgajukov.profi.profile.response.entry.Entry;
import ru.evgajukov.profi.profile.response.entry.definition.Name;
import ru.evgajukov.profi.profile.response.entry.definition.image.Image;
import ru.evgajukov.profi.profile.response.entry.definition.price.Price;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 19.06.17.
 * Обработанный профиль мастера
 */
public class Profile {

    private Entry entry;

    public Profile(Entry entry) {

        this.entry = entry;
    }

    public Entry getEntry() {

        return entry;
    }

    public String getId() {

        return entry.getId();
    }

    public Name getName() {

        return entry.getDefinition().getName();
    }

    public Image getAvatar() {

        return entry.getDefinition().getImages().getAvatars().get(0);
    }

    public List<PServiceItem> getServices() {

        List<PServiceItem> services = new ArrayList<>();
        for (Price price : entry.getDefinition().getPrices()) {
            services.add(price.getPservice());
        }

        return services;
    }

    @Override
    public String toString() {

        return getName().toString();
    }
}