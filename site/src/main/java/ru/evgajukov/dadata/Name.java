package ru.evgajukov.dadata;

/**
 * Created by Тимур on 23.03.2016.
 *
 * Модель ФИО для сервиса dadata.ru
 */
public class Name {

    private String source;     // Исходное ФИО одной строкой
    private String result;     // Стандартизированное ФИО одной строкой
    private String surname;    // Фамилия
    private String name;       // Имя
    private String patronymic; // Отчество
    private String gender;     // Пол
    private Integer qc;        // Код качества

    public String getSource() {

        return source;
    }

    public void setSource(String source) {

        this.source = source;
    }

    public String getResult() {

        return result;
    }

    public void setResult(String result) {

        this.result = result;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getPatronymic() {

        return patronymic;
    }

    public void setPatronymic(String patronymic) {

        this.patronymic = patronymic;
    }

    public String getGender() {

        return gender;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    public Integer getQc() {

        return qc;
    }

    public void setQc(Integer qc) {

        this.qc = qc;
    }
}
