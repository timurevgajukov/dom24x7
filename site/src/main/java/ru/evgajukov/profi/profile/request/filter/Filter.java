package ru.evgajukov.profi.profile.request.filter;

import com.google.gson.annotations.SerializedName;

/**
 * Created by timur on 15.06.17.
 */
public class Filter {

    @SerializedName("static")
    private Static aStatic;

    public Static getStatic() {

        return aStatic;
    }

    public void setStatic(Static aStatic) {

        this.aStatic = aStatic;
    }

    public void addModel(Model model) {

        if (aStatic == null) {
            aStatic = new Static();
        }

        aStatic.addModel(model);
    }
}
