package ru.dom24x7.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Вспомогательные методы для работы с датами
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class DateUtils {

	public static String getStringDate(Date date) {
		
		String[] months = {
				"января",
				"февраля",
				"марта",
				"апреля",
				"мая",
				"июня",
				"июля",
				"августа",
				"сентября",
				"октября",
				"ноября",
				"декабря"
		};
		
		if (isToday(date)) {
			return "сегодня";
		}
		
		if (isYesterday(date)) {
			return "вчера";
		}
		
		StringBuilder strDate = new StringBuilder();
		strDate.append(new SimpleDateFormat("dd").format(date));
		strDate.append(" ");
		strDate.append(months[Integer.parseInt(new SimpleDateFormat("M").format(date)) - 1]);

		if (!isThisYear(date)) {
			strDate.append(" ");
			strDate.append(new SimpleDateFormat("yyyy").format(date)).append(" г.");
		}
		
		return strDate.toString();
	}
	
	public static String getMonthYear(Date date) {
		
		String[] months = {
				"янв",
				"фев",
				"мaр",
				"апр",
				"май",
				"июн",
				"июл",
				"авг",
				"сен",
				"окт",
				"ноя",
				"дек"
		};
		
		StringBuilder strDate = new StringBuilder();
		strDate.append(months[Integer.parseInt(new SimpleDateFormat("M").format(date)) - 1]);
		strDate.append(" ");
		strDate.append(new SimpleDateFormat("yyyy").format(date));
		
		return strDate.toString();
	}
	
	public static boolean isToday(Date date) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		String strDate = dateFormat.format(date);
		String strToday = dateFormat.format(new Date());
		
		return strToday.equals(strDate);
	}
	
	public static boolean isYesterday(Date date) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		
		String strDate = dateFormat.format(date);
		String strYesterday = dateFormat.format(cal.getTime());
		
		return strYesterday.equals(strDate);
	}

	public static boolean isThisYear(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		String strDate = dateFormat.format(date);
		String strToday = dateFormat.format(new Date());

		return strToday.equals(strDate);
	}
	
	/**
	 * Предыдущий месяц относительно текущего
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isPrevMonth(Date date) {
		
		Calendar today = Calendar.getInstance();
    	today.setTime(new Date());
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	
    	if (cal.get(Calendar.YEAR) > today.get(Calendar.YEAR)) {
    		// указана дата в будущем
    		return false;
    	}
    	
    	if (cal.get(Calendar.YEAR) < today.get(Calendar.YEAR)) {
    		// прошлый год
    		return true;
    	}
    	
		return (cal.get(Calendar.MONTH) < today.get(Calendar.MONTH));
	}
	
	/**
	 * Количество дней между двумя датами с учетом текущего дня,
	 * т.е. если даты совпадают, то возвратит 1
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static long dayCount(Date date1, Date date2) {
		
		long diff = Math.abs(date1.getTime() - date2.getTime());
		return diff / (24 * 60 * 60 * 1000) + 1;
	}
	
	/**
	 * Сравнивает две даты на равенство
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean equals(Date date1, Date date2) {
		
		return dayCount(date1, date2) == 1;
	}

	/**
	 * Показывает, что первая дата после второй
	 *
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean after(Date date1, Date date2) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = dateFormat.format(date1);
		String strDate2 = dateFormat.format(date2);
		return strDate1.compareTo(strDate2) > 0;
	}
	
	/**
	 * Первый день месяца
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFirstDayInMonth(Date date) {
		
		String strFirstMonthDay = String.format("01.%s", new SimpleDateFormat("MM.yyyy").format(date));
		try {
			return new SimpleDateFormat("dd.MM.yyyy").parse(strFirstMonthDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}
	
	public static Date getLastDayInMonth(Date date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		String strLastMonthDay = String.format("%d.%s", lastDay, new SimpleDateFormat("MM.yyyy").format(date));
		try {
			return new SimpleDateFormat("dd.MM.yyyy").parse(strLastMonthDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}
	
	/**
	 * Количество прошедших дней в текущем месяце
	 * 
	 * @return
	 */
	public static long getLastDaysOfMonth() {
		
		return dayCount(new Date(), getFirstDayInMonth(new Date()));
	}
	
	public static void main(String[] args) {
		
		System.out.println(getLastDayInMonth(new Date()));
	}
}
