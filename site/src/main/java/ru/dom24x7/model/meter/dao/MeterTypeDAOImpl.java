package ru.dom24x7.model.meter.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.meter.MeterType;
import ru.dom24x7.model.reference.ReferenceDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса справочника типов счетчиков
 */
public class MeterTypeDAOImpl extends ModelDAOImpl implements ReferenceDAO<MeterType> {

    private static final String QUERY_LIST = "{ call meter_types_list() }";

    @Override
    public List<MeterType> list() {

        List<MeterType> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<MeterType>() {

            @Override
            public MeterType mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private MeterType load(ResultSet rs) throws SQLException {

        MeterType type = new MeterType(rs.getInt("id"), rs.getString("name"));
        type.setUnit(rs.getString("unit"));
        type.setNote(rs.getString("note"));
        type.setCode(rs.getString("code"));
        type.setWithoutValue(rs.getBoolean("without_value"));
        type.setIconId(rs.getInt("icon_id"));

        return type;
    }
}