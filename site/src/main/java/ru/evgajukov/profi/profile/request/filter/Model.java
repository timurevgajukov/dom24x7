package ru.evgajukov.profi.profile.request.filter;

/**
 * Created by timur on 15.06.17.
 */
public class Model {

    private String model;

    public String getModel() {

        return model;
    }

    public Model setModel(String model) {

        this.model = model;
        return this;
    }

    @Override
    public String toString() {

        return model;
    }
}
