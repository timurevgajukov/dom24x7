package ru.dom24x7.model.account.dao.interfaces;

import ru.dom24x7.model.account.Account;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 27.04.17.
 * Интерфейс для работы с лицевыми счетами
 */
public interface AccountDAO {

    /**
     * Возвращает лицевой счет пользователя в компании
     * @param user
     * @param company
     * @return
     */
    Account get(User user, Company company);

    /**
     * Возвращает список лицевых счетов пользователя
     * @param user
     * @return
     */
    List<Account> list(User user);

    /**
     * Возвращает список лицевых счетов компании
     * @param company
     * @return
     */
    List<Account> list(Company company);

    /**
     * Сохранение данных по лицевому счету
     * @param account
     * @return
     */
    boolean save(Account account);

    /**
     * Удаление данных по лицевому счету
     * @param account
     * @return
     */
    boolean delete(Account account);

    /**
     * Удаляет лицевой счет пользователя конкретной компании
     * @param user
     * @param company
     * @return
     */
    boolean delete(User user, Company company);
}