package ru.dom24x7.model.lawyer;

import java.util.Date;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.user.User;
import ru.dom24x7.utils.DateUtils;

/**
 * Комментарий юриста к сообщению
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class LawyerMessageComment extends Model {

	private LawyerMessage message;
	private User lawyer;
	private User user;
	private Date createDt;
	private String text;
	
	public LawyerMessageComment() {
		
		super();
	}
	
	public LawyerMessageComment(int id) {
		
		super(id);
	}
	
	public LawyerMessage getMessage() {
		
		return message;
	}
	
	public void setMessage(LawyerMessage message) {
		
		this.message = message;
	}
	
	public User getLawyer() {
		
		return lawyer;
	}
	
	public void setLawyer(User lawyer) {
		
		this.lawyer = lawyer;
	}
	
	public User getUser() {
		
		return user;
	}
	
	public void setUser(User user) {
		
		this.user = user;
	}

	public User getAnswering() {

		if (user != null) {
			return user;
		} else if (lawyer != null) {
			return lawyer;
		}
		return null;
	}
	
	public Date getCreateDt() {
		
		return createDt;
	}

	public String getCreateDtStr() {

		return DateUtils.getStringDate(createDt);
	}
	
	public void setCreateDt(Date createDt) {
		
		this.createDt = createDt;
	}
	
	public String getText() {
		
		return text;
	}
	
	public void setText(String text) {
		
		this.text = text;
	}
	
	@Override
	public String toString() {
		
		return text;
	}
}