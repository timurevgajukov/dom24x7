package ru.dom24x7.model.device;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.dom24x7.model.Model;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.utils.DateUtils;

/**
 * Прибор учета коммунальных услуг
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Device extends Model {

	private Meter meter;
	private String serial;
	private int capacity; // разрядность
	private int precision; // точность
	private Date installationDate;
	private Date inspectionDate;
	private boolean used;
	
	public Device() {
		
		super();
	}
	
	public Device(int id) {
		
		super(id);
	}

	@JsonIgnore
	public Meter getMeter() {
		
		return meter;
	}
	
	public void setMeter(Meter meter) {
		
		this.meter = meter;
	}
	
	public String getSerial() {
		
		return serial;
	}
	
	public void setSerial(String serial) {
		
		this.serial = serial;
	}
	
	public int getCapacity() {
		
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		
		this.capacity = capacity;
	}
	
	public int getPrecision() {
		
		return precision;
	}
	
	public void setPrecision(int precision) {
		
		this.precision = precision;
	}
	
	public Date getInstallationDate() {
		
		return installationDate;
	}

	public String getStringInstallationDate() {

		if (installationDate == null) {
			return "";
		}

		return new SimpleDateFormat("dd.MM.yyyy").format(installationDate);
	}
	
	public void setInstallationDate(Date installationDate) {
		
		this.installationDate = installationDate;
	}
	
	public Date getInspectionDate() {
		
		return inspectionDate;
	}

	public String getStringInspectionDate() {

		if (inspectionDate == null) {
			return "";
		}

		return new SimpleDateFormat("dd.MM.yyyy").format(inspectionDate);
	}
	
	public void setInspectionDate(Date inspectionDate) {
		
		this.inspectionDate = inspectionDate;
	}
	
	public boolean isUsed() {
		
		return used;
	}
	
	public void setUsed(boolean used) {
		
		this.used = used;
	}
	
	/**
	 * Возвращает показание старого прибора учета в день его смены
	 * 
	 * @return
	 */
	public MeterHistoryItem getLastValueOldDevice() {
		
		if (installationDate == null) {
			// не указана дата установки и мы не можем определить показания
			return null;
		}
		
		List<MeterHistoryItem> history = meter.getHistory();
		
		if (history == null) {
			// еще пока нет ни одного показания
			return null;
		}
		
    	for (int i=0; i<history.size(); i++) {
    		long diff = DateUtils.dayCount(installationDate, history.get(i).getDate());
    		if (diff == 1) {
    			// в нужную дату было снято показание
    			return history.get(i);
    		}
		}
    	
    	// не нашли подходящего показания
    	return null;
	}
	
	@Override
	public String toString() {

		if (serial == null || serial.trim().length() == 0) {
			return "без серийного номера";
		}
		
		return serial;
	}
}