<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="device" value="${meter.usedDevice}" />
<!-- EDIT DEVICE MODAL WINDOW: BEGIN -->
<div id="device-edit-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div id="device-content" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Прибор учета</h4>
            </div>
            <form>
                <div class="modal-body">
                    <div class="form-group">
                        <input id="serial" name="serial" type="text" class="form-control" placeholder="Серийный номер"
                               <c:if test="${device != null && device.serial != null}">value="${device.serial}"</c:if> />
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="capacity" name="capacity" type="text" class="form-control add-tooltip"
                                       placeholder="Разрядность" data-toggle="tooltip"
                                       data-original-title="Количество знаков в приборе учета. Например, при разрядности
                                       равным 5 показание будет отображаться как 00000. Оставьте пустым, если не хотите использовать"
                                       <c:if test="${device != null && device.capacity > 0}">value="${device.capacity}"</c:if> />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="precision" name="precision" type="text" class="form-control add-tooltip"
                                       placeholder="Точночть" data-toggle="tooltip"
                                       data-original-title="Количество знаков в приборе учета после запятой. Например,
                                       если укажете точность равным 2, то после запятой в показании будет отображаться
                                       только 2 знака. Оставьте пустым, если не хотите использовать"
                                       <c:if test="${device != null && device.precision > 0}">value="${device.precision}"</c:if> />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="installation" name="installation" type="text"
                                       class="form-control exp-date add-tooltip" placeholder="Дата установки"
                                       data-toggle="tooltip" data-original-title="Укажите дату установки прибора учета"
                                       <c:if test="${device != null && device.installationDate != null}">value="${device.stringInstallationDate}"</c:if> />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="inspection" name="inspection" type="text" class="form-control exp-date add-tooltip"
                                       placeholder="Дата поверки" data-toggle="tooltip"
                                       data-original-title="Укажите дату предполагаемой поверки прибора учета"
                                       <c:if test="${device != null && device.inspectionDate != null}">value="${device.stringInspectionDate}"</c:if> />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input id="device-last-value" name="device-last-value" type="text" class="form-control add-tooltip"
                               placeholder="Старое показание" data-toggle="tooltip"
                               data-original-title="Введите показание предыдущего прибора учета перед его сменой"
                               <c:if test="${device != null && device.lastValueOldDevice != null}">value="${device.lastValueOldDevice.value}"</c:if> />
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label id="is-new-device-label" class="form-checkbox form-normal form-primary form-text">
                                    <input id="is-new-device" name="is-new-device" type="checkbox" />
                                    новый прибор учета
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a id="btn-device-history" href="#" class="btn btn-link" data-toggle="modal"
                               data-target="#device-history-modal">история установок</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-send-device" class="btn btn-success" type="button">Сохранить</button>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT DEVICE MODAL WINDOW: END -->

<!-- HISTORY DEVICE MODAL WINDOW: BEGIN -->
<div id="device-history-modal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-history-device" aria-hidden="true">
    <div class="modal-dialog">
        <div id="device-history-content" class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">История установок приборов учета</h4>
            </div>
            <div class="modal-body">
                <c:if test="${meter.devices != null && meter.devices.size() != 0}">
                    <div class="list-group">
                        <a class="list-group-item">

                        </a>
                    </div>
                </c:if>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- HISTORY DEVICE MODAL WINDOW: END -->

<script>
    $(document).ready(function () {
        $('#btn-device-history').click(function () {
            $('#device-edit-modal').modal('hide');
        });
    });
</script>