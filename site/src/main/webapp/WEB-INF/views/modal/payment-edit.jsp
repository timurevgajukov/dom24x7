<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- EDIT PAYMENT MODAL WINDOW: BEGIN -->
<div id="payment-edit-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 id="payment-edit-modal-title" class="modal-title">Сохранение платежного поручения</h4>
                <div id="loading-progress" class="progress m-t-xs full progress-striped active" style="display: none;">
                    <div style="width: 100%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class=" progress-bar progress-bar-success">
                        идет загрузка...
                    </div>
                </div>
            </div>
            <form id="payment-form" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <c:if test="${userAddresses != null}">
                                    <select id="modal-payment-address" class="form-control">
                                        <c:forEach items="${userAddresses}" var="address">
                                            <option value="${address.id}" data-currency="${address.currency.symbol}">${address}</option>
                                        </c:forEach>
                                    </select>
                                </c:if>
                            </div>
                            <div class="form-group">
                                <input id="modal-payment-month" type="text" class="form-control datetimepicker-control" placeholder="Месяц оплаты" />
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input id="modal-payment-amount" type="text" class="form-control" placeholder="Сумма платежа" style="text-align: right;" />
                                    <span id="modal-payment-amount-currency" class="input-group-addon"></span>
                                </div>
                            </div>
                            <div class="checkbox"><input id="modal-payment-paid" type="checkbox"> уже оплачен</div>
                            <div class="form-group">
                                <textarea id="modal-payment-note" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div id="modal-payment-photo"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-payment-save" class="ladda-button btn btn-success" type="button" data-style="zoom-in">Сохранить</button>
                    <button id="btn-payment-delete" class="btn btn-danger" type="button">Удалить</button>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT PAYMENT MODAL WINDOW: END -->

<script src="/resources/js/field.picture.js"></script>
<script src="/resources/js/form.generator.js"></script>
<script src="/resources/js/field.block.js"></script>

<script>
    $(document).ready(function () {
        $('.datetimepicker-control').datetimepicker({
            format: 'MM.YYYY',
            locale: 'ru'
        });
        $('#modal-payment-paid').iCheck({ checkboxClass: 'icheckbox_square-green' });

        var paymentId = undefined;
        var photoBlock = undefined;

        // открытие модального окна для добавления нового платежного поручения
        $('#btn-payment-add').click(function () {
            $('#payment-edit-modal-title').html('Добавление платежного поручения');
            $('#loading-progress').hide();
            $('#btn-payment-delete').hide();

            paymentId = undefined;

            initPayment(paymentId);
        });

        // открытие модального окна для редактировани платежного поручения
        $(document).on('click', '.btn-payment-edit', function () {
            $('#payment-edit-modal-title').html('Редактирование платежного поручения');
            $('#loading-progress').show();
            $('#btn-payment-delete').show();

            paymentId = $(this).data('id');

            initPayment(paymentId);
        });

        // сохраняем данные по платежному поручению (новому или существующему)
        var btnPaymentSave = $('#btn-payment-save').ladda();
        btnPaymentSave.click(function () {
            var data = {
                address: $('#modal-payment-address').val(),
                month: $('#modal-payment-month').val(),
                amount: $('#modal-payment-amount').val(),
                paid: $('#modal-payment-paid').is(':checked'),
                note: $('#modal-payment-note').val(),
                photos: photoBlock.get().value,
                photosFileName: photoBlock.get().fileName
            };

            if (data.address == null || data.address.trim().length == 0) {
                showAlert('Необходимо указать адрес привязки платежного поручения', 'warning');
                return;
            }

            if (data.month == null || data.month.trim().length == 0) {
                showAlert('Необходимо указать месяц по которому пришло платежное поручение', 'warning');
                return;
            }

            if (data.amount == null || data.amount.trim().length == 0) {
                showAlert('Необходимо указать сумму по платежному поручению', 'warning');
                return;
            }

            if (data.photos == null || data.photos.length == 0) {
                showAlert('Необходимо добавить скан платежного поручени', 'warning');
                return;
            }

            if (paymentId !== undefined && paymentId != null) {
                // редактирование существующего платежного поручения
                data.id = paymentId;
            }

            // отправляем запрос на сохранение
            btnPaymentSave.ladda('start');
            $.post('/panel/payments/save', data, function (result) {
                btnPaymentSave.ladda('stop');
                if (!result) {
                    showAlert('Непредвиденная ошибка', 'error');
                    return;
                }

                if (result.code  == 0) {
                    var payment = result.body;
                    showAlert('Успешно сохранили платежное поручение', 'info');
                    location.reload();
                } else {
                    for (var i = 0; i < result.body.length; i++) {
                        var error = result.body[i];
                        showAlert(error.body, error.type);
                    }
                }
            }).error(function () {
                btnPaymentSave.ladda('stop');
            });
        });

        // удаление платежного поручения из модального окна
        $('#btn-payment-delete').click(function () {
            delPayment(paymentId);
        });

        // удаление платежного поручения из общего списка
        $(document).on('click', '.btn-payment-delete', function () {
            delPayment($(this).data('id'));
        });

        $('#modal-payment-address').change(function () {
            $('#modal-payment-amount-currency').html($(this).find(':selected').data('currency'));
        });

        /**
         * Инициализация полей платежного поручения
         * @param postId
         */
        function initPayment(paymentId) {
            var address = $('#modal-payment-address');
            var month = $('#modal-payment-month');
            var amount = $('#modal-payment-amount');
            var paid = $('#modal-payment-paid');
            var note = $('#modal-payment-note');
            var photo = $('#modal-payment-photo');

            address.val('');
            month.val(moment().format('MM.YYYY'));
            amount.val('');
            paid.iCheck('uncheck');
            note.val('');
            photo.empty();

            var data = {
                id: 1,
                type: 'Picture',
                name: 'payment-photo',
                values: []
            };

            if (paymentId === undefined || paymentId == null) {
                photoBlock = new PictureField({data: data});
                photo.append(photoBlock.html());
                return;
            }

            $.get('/panel/payments/' + paymentId, function (payment) {
                if (!payment) {
                    return;
                }

                address.val(payment.address.id);
                month.val(moment(payment.month).format('MM.YYYY'));
                amount.val(payment.amount);
                note.val(payment.note);
                if (payment.paid) {
                    paid.iCheck('check');
                } else {
                    paid.iCheck('uncheck');
                }

                data.values.push(payment.photo.base64);
                photoBlock = new PictureField({
                    data: data,
                    fileName: payment.photo.fileName,
                    value: payment.photo.base64
                });
                photo.append(photoBlock.html());

                $('#loading-progress').hide();
            })
        }

        /**
         * Удаление платежного поручения
         * @param paymentId
         */
        function delPayment(paymentId) {
            if (!paymentId) {
                return;
            }

            swal({
                    title: "Вы уверены, что хотите удалить платежное поручение?",
                    text: "После удаления платежного поручения вы его уже не сможете восстановить!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да, удалить!",
                    cancelButtonText: "Нет, не удалять!",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // отправляем запрос на удаление
                        $.get('/panel/payments/delete/' + paymentId, function (result) {
                            if (!result) {
                                swal("Ошибка", "Неизвестная ошибка. Не удалось удалить платежное поручение.", "error");
                                return;
                            }

                            if (result.code != 0) {
                                var errorStr = '';
                                for (var i = 0; i < result.body.length; i++) {
                                    errorStr += result.body[i] + ' ';
                                }
                                swal("Ошибка", errorStr, "error");
                                return;
                            }

                            swal("Удалено!", "Ваше платежное поручение успешно удалено.", "success");
                            // закрываем модальное окно
                            $('#payment-edit-modal').modal('hide');

                            location.reload();
                        });
                    } else {
                        swal("Отмена", "Ваше платежное поручение осталось доступным.", "error");
                    }
                });
        }
    });
</script>