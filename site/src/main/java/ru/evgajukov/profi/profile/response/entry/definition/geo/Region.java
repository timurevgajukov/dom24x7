package ru.evgajukov.profi.profile.response.entry.definition.geo;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class Region {

    private int id;
    private String name;
    private String url;
    private List<Station> stations;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public List<Station> getStations() {

        return stations;
    }

    public void setStations(List<Station> stations) {

        this.stations = stations;
    }

    @Override
    public String toString() {

        return name;
    }
}