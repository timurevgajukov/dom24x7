package ru.dom24x7.model.email.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.email.EmailMessageStatus;
import ru.dom24x7.model.reference.ReferenceDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 17.03.17.
 * Реализация интерфейса получения справочника со списком статусом
 */
public class EmailMessageStatusDAOImpl extends ModelDAOImpl implements ReferenceDAO<EmailMessageStatus> {

    private static final String QUERY_LIST = "{ call email_message_statuses_list() }";

    @Override
    public List<EmailMessageStatus> list() {

        List<EmailMessageStatus> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<EmailMessageStatus>() {
            @Override
            public EmailMessageStatus mapRow(ResultSet resultSet, int i) throws SQLException {

                return new EmailMessageStatus(resultSet.getInt("id"), resultSet.getString("name"));
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }
}