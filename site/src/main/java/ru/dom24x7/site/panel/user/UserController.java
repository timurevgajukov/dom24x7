package ru.dom24x7.site.panel.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.Email;

/**
 * Created by timur on 22.09.16.
 *
 * Контроллер для работы с профилем пользователя
 */
@Controller
public class UserController extends BaseController {

    private static final String ACTIVE = "profile";

    @RequestMapping("/profile")
    public String profileAction(ModelMap model) {

        if (!security.isAuth()) {
            return "redirect:/";
        }

        setModelMap(model, "Личный кабинет", ACTIVE);

        return "panel/profile";
    }

    @RequestMapping("/profile/email/confirm")
    public String emailConfirmAction() {

        security.getMessages().add("Вам отправлено письмо для подтверждения адреса электронной почты", Message.TYPE_INFO);

        // отправить письмо с подтверждением адреса электронной почты
        String url = "https://dom24x7.ru/security/confirm?code=" + security.getUser().getConfirmCode();
        String emailMessage = String.format("Для подтверждения адреса электронной почты перейдите по ссылке <a href=\"%s\">%s</a>", url, url);

        Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z", servletContext.getRealPath("/resources"));
        emailSender.send("Подтверждение электронной почты на сайте dom24x7.ru", emailMessage, security.getUser().getEmail());


        return "redirect:/profile";
    }

    @RequestMapping("/profile/subscribe/check")
    public @ResponseBody Response subscribeCheckAjax(@RequestParam(value = "check") boolean check) {

        User user = security.getUser();
        user.setSubscribed(check);

        Message message;
        if (check) {
            message = new Message("Спасибо, что подписались на нашу новостную рассылку", Message.TYPE_INFO);
        } else {
            message = new Message("Вы успешно отписались от новостной рассылки", Message.TYPE_WARNING);
        }

        return new Response(0, "OK", message);
    }

    @RequestMapping(value = "/profile/password/change", method = RequestMethod.POST)
    public String passwordChangeAction(
            @RequestParam(value = "password-current") String pwdCurrent,
            @RequestParam(value = "password-new1") String pwdNew1,
            @RequestParam(value = "password-new2") String pwdNew2) {

        User user = new UserDAOImpl().auth(security.getUser().getEmail(), pwdCurrent);
        if (user == null) {
            security.getMessages().add("Текущий пароль указан не верно", Message.TYPE_ERROR);
            return "redirect:/profile";
        }

        if (pwdNew1.length() < 6) {
            security.getMessages().add("Новый пароль должен быть не менее 6 символов", Message.TYPE_WARNING);
            return "redirect:/profile";
        }

        if (!pwdNew1.equals(pwdNew2)) {
            security.getMessages().add("Новые пароли не совпадают", Message.TYPE_ERROR);
            return "redirect:/profile";
        }

        new UserDAOImpl().changePassword(security.getUser(), pwdCurrent, pwdNew1);

        security.getMessages().add("Пароль успешно сменен", Message.TYPE_INFO);

        return "redirect:/profile";
    }

    @RequestMapping(value = "/profile/extra/change", method = RequestMethod.POST)
    public String extraChangeAction(
            @RequestParam(value = "user-name", required = false) String name,
            @RequestParam(value = "user-surname", required = false) String surname,
            @RequestParam(value = "user-midname", required = false) String midname) {

        User user = security.getUser();
        user.setName(name != null && name.trim().length() != 0 ? name.trim() : null);
        user.setSurname(surname != null && surname.trim().length() != 0 ? surname.trim() : null);
        user.setMidname(midname != null && midname.trim().length() != 0 ? midname.trim() : null);

        if (new UserDAOImpl().saveExtra(user)) {
            security.getMessages().add("Успешно сохранили личные данные", Message.TYPE_INFO);
        } else {
            security.getMessages().add("Не удалось сохранить личные данные", Message.TYPE_ERROR);
        }

        return "redirect:/profile";
    }

    @RequestMapping("/user/{userId}/avatar/{fileName}")
    public @ResponseBody byte[] userAvatarAction(@PathVariable int userId, @PathVariable String fileName) {

        User user = new UserDAOImpl().get(userId);
        if (user == null) {
            return null;
        }

        Photo photo = user.getPhoto();
        if (photo == null) {
            return null;
        }

        return photo.getResource();
    }
}