package ru.dom24x7.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.dom24x7.model.lawyer.LawyerMessage;
import ru.dom24x7.model.lawyer.LawyerMessageComment;
import ru.dom24x7.model.lawyer.dao.LawyerMessageCommentDAOImpl;
import ru.dom24x7.model.lawyer.dao.LawyerMessageDAOImpl;
import ru.dom24x7.model.user.Role;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.utils.Email;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 03.05.17.
 * Контроллер для обработки пользовательских запросов на юридическую консультацию
 */
@Controller
public class LawyerController extends BaseController {

    private static final String ACTIVE = "lawyer";

    @RequestMapping("/lawyer")
    public String indexAction(ModelMap model) {

        setModelMap(model, "Бесплатная юридическая консультация по ЖКХ", ACTIVE);
        if (security.isAuth()) {
            if (security.isUserRole() || security.isAdminRole()) {
                model.addAttribute("questions", new LawyerMessageDAOImpl().list(security.getUser(), 0, Integer.MAX_VALUE, null));
                return "site/lawyer/index";
            } else if (security.isLawyerRole()) {
                model.addAttribute("questions", new LawyerMessageDAOImpl().list(0, Integer.MAX_VALUE, null));
                return "site/lawyer/lawyer";
            }
        }
        return "site/lawyer/index";
    }

    @RequestMapping("/lawyer/question/{qId}")
    public String itemAction(@PathVariable int qId, ModelMap model) {

        User user = null;
        if (!security.isLawyerRole()) {
            user = security.getUser();
        }

        LawyerMessage message = new LawyerMessageDAOImpl().get(qId, user);
        if (message == null) {
            // попытка отобразить несуществующий вопрос, либо не принадлежащий пользователю
            return "redirect:/lawyer";
        }

        setModelMap(model, "Бесплатная юридическая консультация по ЖКХ", ACTIVE);
        model.addAttribute("question", message);
        model.addAttribute("answers", new LawyerMessageCommentDAOImpl().list(message, 0, Integer.MAX_VALUE));

        return "site/lawyer/item";
    }

    @RequestMapping(value = "/lawyer/question", method = RequestMethod.POST)
    public String saveQuestionAction(@RequestParam(value = "lawyer-question") String question) {

        if (question == null || question.trim().length() == 0) {
            security.getMessages().add(new Message("Задаваемый вопрос не может быть пустым", Message.TYPE_WARNING));
        } else {
            LawyerMessage message = new LawyerMessage();
            message.setUser(security.getUser());
            message.setText(question.trim());
            new LawyerMessageDAOImpl().save(message);

            // отправляем уведомление всем юристам, что есть новый вопрос
            List<User> lawyers = new UserDAOImpl().list(new Role(Role.ROLE_LAWYER));
            if (lawyers != null && lawyers.size() != 0) {
                Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z", servletContext.getRealPath("/resources"));
                String emailMessage = "На сайте коммунального сервиса <a href='https://dom24x7.ru'>Dom24x7</a> от пользователя поступил новый вопрос. " +
                        "Для просмотра списка вопросов перейдите в раздел <a href='https://dom24x7.ru/lawyer'>консультаций</a> " +
                        "или перейдите непосредственно на страницу самого <a href='https://dom24x7.ru/lawyer/question/" + message.getId() + "'>вопроса</a>.";
                for (User lawyer : lawyers) {
                    emailSender.send("Новый вопрос на сайте dom24x7.ru", emailMessage, lawyer.getEmail());
                }
            }

            security.getMessages().add(new Message("Успешно отправили вопрос юристу. В ближайшее время он вам ответит", Message.TYPE_SUCCESS));
        }

        return "redirect:/lawyer";
    }

    @RequestMapping(value = "/lawyer/answer/{qId}", method = RequestMethod.POST)
    public String saveAnswerAction(@PathVariable int qId, @RequestParam(value = "lawyer-answer") String answer) {

        LawyerMessage message = new LawyerMessageDAOImpl().get(qId, security.isLawyerRole() ? null : security.getUser());
        if (message == null) {
            // попытка отобразить несуществующий вопрос, либо не принадлежащий пользователю
            return "redirect:/lawyer";
        }

        if (answer == null || answer.trim().length() == 0) {
            security.getMessages().add(new Message("Ответ не может быть пустым", Message.TYPE_WARNING));
        } else {
            User user = null;
            User lawyer = null;
            if (security.isLawyerRole()) {
                lawyer = security.getUser();
            } else {
                user = security.getUser();
            }

            LawyerMessageComment comment = new LawyerMessageComment();
            comment.setMessage(message);
            comment.setUser(user);
            comment.setLawyer(lawyer);
            comment.setText(answer.trim());
            new LawyerMessageCommentDAOImpl().save(comment);

            Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z", servletContext.getRealPath("/resources"));
            String emailMessage = "На сайте коммунального сервиса <a href='https://dom24x7.ru'>Dom24x7</a> по вопросу появился новый комментарий. " +
                    "Для просмотра комментарий перейдите на страницу <a href='https://dom24x7.ru/lawyer/question/" + message.getId() + "'>вопроса</a>";
            if (security.isLawyerRole()) {
                // если комментарий от юриста, то уведомляем пользователя, который задал вопрос
                emailSender.send("Новый комментарий на вопрос на сайте dom24x7.ru", emailMessage, message.getUser().getEmail());
            } else {
                // если комментарий от пользователя, то уведомляем всех юристов, которые участвуют в переписке
                List<LawyerMessageComment> comments = new LawyerMessageCommentDAOImpl().list(message, 0, Integer.MAX_VALUE);
                if (comments != null && comments.size() != 0) {
                    List<User> lawyers = new ArrayList<>();
                    for (LawyerMessageComment item : comments) {
                        if (item.getLawyer() != null) {
                            lawyers.add(item.getLawyer());
                        }
                    }

                    if (lawyers.size() != 0) {
                        for (User userLawyer : lawyers) {
                            emailSender.send("Новый комментарий на вопрос на сайте dom24x7.ru", emailMessage, userLawyer.getEmail());
                        }
                    }
                }
            }

            security.getMessages().add(new Message("Успешно отправили комментарий по вопросу", Message.TYPE_SUCCESS));
        }

        return "redirect:/lawyer/question/" + qId;
    }
}