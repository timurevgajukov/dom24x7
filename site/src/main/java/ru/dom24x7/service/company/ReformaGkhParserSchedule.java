package ru.dom24x7.service.company;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import ru.dom24x7.service.company.model.mq.ReformaGkhLink;
import ru.dom24x7.service.company.model.mq.dao.ReformaGkhLinkDAOImpl;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by timur on 08.04.17.
 * Обработчик парсера сайта по расписанию
 */
@Controller
public class ReformaGkhParserSchedule {

    @Scheduled(fixedDelay = 1000)
    public void doParse() {

        long start = System.currentTimeMillis();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.S");

        System.out.println("[" + sdf.format(date) + "] Проверка наличия ссылок для парсинга в очереди");
        ReformaGkhLink reformaGkhLink = new ReformaGkhLinkDAOImpl().get();
        if (reformaGkhLink == null) {
            System.out.println(">>>>> Очередь ссылок пуста");
            return;
        }

        // есть ссылка для обработки
        System.out.println(">>>>> Получена ссылка для обработки: " + reformaGkhLink.getLink());
        ReformaGkhParser parser = new ReformaGkhParser(reformaGkhLink.getLink());
        int count = parser.parse();
        if (count > 0) {
            System.out.println(">>>>> Получено новых ссылок: " + count);
            // имеются новые ссылки, которые нужно сохранить в очередь для дальнейшей обработки
            for (String link : parser.links()) {
                new ReformaGkhLinkDAOImpl().save(new ReformaGkhLink(link));
            }
        } else if (count == -1) {
            // ссылка на профиль компании
            System.out.println(">>>>> Ссылка на профиль компании обработана.");
            // return; // завершаем обработку до удалени ссылки
        } else {
            System.out.println(">>>>> Новых ссылок не получено");
        }
        new ReformaGkhLinkDAOImpl().delete(reformaGkhLink);
        System.out.println(">>>>> Обработка ссылки завершена: " + (System.currentTimeMillis() - start) / 1000 + " сек.");
    }
}