package ru.evgajukov.profi.dictionary.response.mmetro;

import ru.evgajukov.profi.dictionary.response.DictionaryBlock;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class MMetroBlock extends DictionaryBlock {

    private List<MMetroBook> books;

    public List<MMetroBook> getBooks() {

        return books;
    }

    public void setBooks(List<MMetroBook> books) {

        this.books = books;
    }
}
