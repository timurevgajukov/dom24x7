package ru.dom24x7.model.company.dao.interfaces;

import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.company.Region;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с компаниями ЖКХ
 */
public interface CompanyDAO {

    /**
     * Возвращает информацию по компании
     *
     * @param id
     * @return
     */
    Company get(int id);

    /**
     * Возвращает компанию по его ИНН
     * @param inn
     * @return
     */
    Company getByINN(String inn);

    /**
     * Возвращает компанию по его ссылке в ГИС ЖКХ
     * @param url
     * @return
     */
    Company getByGisZkhUrl(String url);

    /**
     * Возвращает компанию по ссылке в Реформа ЖКХ
     * @param url
     * @return
     */
    Company getByReformaGkhUrl(String url);

    /**
     * Возвращает список всех организаций сферы ЖКХ
     *
     * @return
     */
    List<Company> list();

    /**
     * Возвращает список компаний с учетом фильтра
     * @param offset
     * @param count
     * @param search
     * @return
     */
    List<Company> list(int offset, int count, String search);

    /**
     * Возвращает компании по региону
     * @param region
     * @param offset
     * @param count
     * @return
     */
    List<Company> list(Region region, int offset, int count);

    /**
     * Возвращает общее количество компаний с учетом фильтра
     * @param search
     * @return
     */
    int count(String search);

    /**
     * Возвращает количество компании в регионе
     * @param region
     * @return
     */
    int count(Region region);

    /**
     * Сохраняет компанию
     *
     * @param company
     * @return
     */
    boolean save(Company company);

    /**
     * Удаляет компанию
     *
     * @param company
     * @return
     */
    boolean delete(Company company);

    /**
     * Блокирует компанию
     *
     * @param company
     * @return
     */
    boolean hold(Company company);
}