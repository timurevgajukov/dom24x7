package ru.dom24x7.model.template.dao.interfaces;

import ru.dom24x7.model.template.Template;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Интерфейс для работы с шаблонами писем пользователей
 */
public interface TemplateDAO {

    /**
     * Возвращает шаблон письма
     *
     * @param id
     * @return
     */
    Template get(int id);

    /**
     * Возвращает список шаблонов пользователя
     *
     * @param user
     * @return
     */
    List<Template> list(User user);

    /**
     * Сохраняет шаблон письма пользователя
     *
     * @param template
     * @param user
     * @return
     */
    boolean save(Template template, User user);

    /**
     * Удаляет шаблон письма
     *
     * @param template
     * @return
     */
    boolean delete(Template template);
}