package ru.evgajukov.profi.dictionary.response.mmetro;

import ru.evgajukov.profi.dictionary.response.Book;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class MMetroBook extends Book {

    private List<MMetroItem> data;

    public List<MMetroItem> getData() {

        return data;
    }

    public void setData(List<MMetroItem> data) {

        this.data = data;
    }
}
