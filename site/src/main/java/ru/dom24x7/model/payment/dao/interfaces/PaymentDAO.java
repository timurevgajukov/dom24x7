package ru.dom24x7.model.payment.dao.interfaces;

import ru.dom24x7.model.payment.Payment;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 22.03.17.
 * Интерфейс для работы с платежными поручениями
 */
public interface PaymentDAO {

    /**
     * Возвращает платежное поручение
     * @param id
     * @param user
     * @return
     */
    Payment get(int id, User user);

    /**
     * Возвращает список платежных поручений пользователя
     * @param user
     * @param offset
     * @param count
     * @param search
     * @return
     */
    List<Payment> list(User user, int offset, int count, String search);

    /**
     * Сохранение платежного поручения
     * @param payment
     * @return
     */
    boolean save(Payment payment);

    /**
     * Удаление платежного поручния
     * @param payment
     * @return
     */
    boolean delete(Payment payment);
}