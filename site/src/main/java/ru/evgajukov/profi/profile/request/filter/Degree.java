package ru.evgajukov.profi.profile.request.filter;

/**
 * Created by timur on 18.06.17.
 * студент 15
 * выпускник/аспирант 20
 * кандидат наук 25
 * носитель языка 35
 * доктор наук 30
 */
public class Degree {

    private int from;
    private int to;

    public int getFrom() {

        return from;
    }

    public void setFrom(int from) {

        this.from = from;
    }

    public int getTo() {

        return to;
    }

    public void setTo(int to) {

        this.to = to;
    }
}
