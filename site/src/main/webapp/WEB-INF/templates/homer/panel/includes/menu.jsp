<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<ul class="nav" id="side-menu">
    <c:if test="${security.isAccess('/panel')}">
        <li<c:if test="${'dashboard' == active}"> class="active"</c:if>>
            <a href="/panel"><i class="fa fa-dashboard"></i> <span class="nav-label">Панель</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/panel/addresses/')}">
        <li>
            <a href="#"><i class="fa fa-building"></i> <span class="nav-label">Адреса</span><span
                    class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a href="#" class="btn-address-add" data-toggle="modal" data-target="#address-edit-modal"><i
                        class="fa fa-plus"></i> Добавить адрес</a></li>
                <c:if test="${userAddresses != null}">
                    <c:forEach items="${userAddresses}" var="address">
                        <li><a href="/panel/addresses/${address.id}">${address}</a></li>
                    </c:forEach>
                </c:if>
            </ul>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/panel/reports')}">
        <li<c:if test="${'reports' == active}"> class="active"</c:if>>
            <a href="/panel/reports"><i class="fa fa-area-chart"></i> <span class="nav-label">Отчеты</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/panel/payments')}">
        <li<c:if test="${'payments' == active}"> class="active"</c:if>>
            <a href="/panel/payments"><i class="fa fa-file-text"></i> <span class="nav-label">Платежки</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/companies/regions')}">
        <li<c:if test="${'companies' == active}"> class="active"</c:if>>
            <a href="/companies/regions"><i class="fa fa-building"></i> <span class="nav-label">Компании</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/tariffs')}">
        <li<c:if test="${'tariffs' == active}"> class="active"</c:if>>
            <a href="/tariffs"><i class="fa fa-calculator"></i> <span class="nav-label">Тарифы ЖКХ</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/lawyer')}">
        <li<c:if test="${'lawyer' == active}"> class="active"</c:if>>
            <a href="/lawyer"><i class="fa fa-legal"></i> <span class="nav-label">Консультация</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/profi')}">
        <li<c:if test="${'profi' == active}"> class="active"</c:if>>
            <a href="/profi"><i class="fa fa-group"></i> <span class="nav-label">Специалисты</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/blog')}">
        <li<c:if test="${'blog' == active}"> class="active"</c:if>>
            <a href="/blog"><i class="fa fa-th-large"></i> <span class="nav-label">Блог</span></a>
        </li>
    </c:if>
    <c:if test="${security.isAccess('/admin')}">
        <li class="active">
            <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Управление</span><span
                    class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <c:if test="${security.isAccess('/admin/users')}"><li><a href="/admin/users">Пользователи</a></li></c:if>
                <c:if test="${security.isAccess('/admin/addresses')}"><li><a href="/admin/addresses">Адреса</a></li></c:if>
                <c:if test="${security.isAccess('/admin/companies')}"><li><a href="/admin/companies">Компании</a></li></c:if>
                <c:if test="${security.isAccess('/admin/blog')}"><li><a href="/admin/blog">Блог</a></li></c:if>
                <c:if test="${security.isAccess('/admin/pages')}"><li><a href="/admin/pages">Страницы</a></li></c:if>
                <c:if test="${security.isAccess('/admin/subscribes')}"><li><a href="/admin/subscribe">Рассылка</a></li></c:if>
                <c:if test="${security.isAccess('/admin/templates')}"><li><a href="/admin/templates">Шаблоны</a></li></c:if>
                <c:if test="${security.isAccess('/admin/static')}"><li><a href="/admin/static">Статистика</a></li></c:if>
                <c:if test="${security.isAccess('/admin/payments')}"><li><a href="/admin/payments">Платежи</a></li></c:if>
                <c:if test="${security.isAccess('/admin/access')}"><li><a href="/admin/access">Доступ</a></li></c:if>
            </ul>
        </li>
    </c:if>
</ul>

<c:if test="${security.auth}">
    <t:insertAttribute name="address-edit" defaultValue="/WEB-INF/views/modal/address-edit.jsp"/>
</c:if>