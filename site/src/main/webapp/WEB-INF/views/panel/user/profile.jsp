<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-lg-8 col-md-10 col-sm-12">
        <div class="hpanel">
            <div class="panel-heading hbuilt">Электронная почта</div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group<c:if test="${!security.user.confirmed}"> has-error</c:if>">
                        <label class="col-sm-3 control-label">Эл. почта</label>
                        <div class="col-sm-9">
                            <c:choose>
                                <c:when test="${security.user.confirmed}">
                                    <input class="form-control" type="text" value="${security.user.email}" readonly/>
                                </c:when>
                                <c:otherwise>
                                    <div class="input-group">
                                        <input class="form-control" type="text" value="${security.user.email}"
                                               readonly/>
                                        <span class="input-group-btn">
                                            <a href="/profile/email/confirm" class="btn btn-danger">Подтвердить</a>
                                        </span>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="checkbox"><input id="ch-subscribe" type="checkbox"<c:if
                                    test="${security.user.subscribed}"> checked</c:if>> подписаться на новости
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="hpanel">
            <div class="panel-heading hbuilt">Личные данные</div>
            <form class="form-horizontal" method="post" action="/profile/extra/change">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Имя</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" placeholder="Введите свое имя"
                                   name="user-name" value="${security.user.name}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Фамилия</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" placeholder="Введите свою фамилию"
                                   name="user-surname" value="${security.user.surname}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Отчество</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" placeholder="Введите свое отчество"
                                   name="user-midname" value="${security.user.midname}"/>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button id="btn-submit-extra" class="btn btn-success" type="submit">Сохранить</button>
                </div>
            </form>
        </div>
        <div class="hpanel">
            <div class="panel-heading hbuilt">Смена пароля</div>
            <form class="form-horizontal" method="post" action="/profile/password/change">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Текущий пароль</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" placeholder="Введите текущий пароль"
                                   name="password-current" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Новый пароль</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" placeholder="Введите новый пароль"
                                   name="password-new1" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <input class="form-control" type="password" placeholder="Введите новый пароль повторно"
                                   name="password-new2" required/>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button id="btn-submit-password" class="btn btn-success" type="submit">Сменить</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#ch-subscribe').iCheck({checkboxClass: 'icheckbox_square-green'});

        // смена статуса подписки на новостную рассылку
        $('#ch-subscribe').on('ifChanged', function (event) {
            var data = {
                check: $(this).is(':checked')
            };
            $.post('/profile/subscribe/check', data, function (result) {
                if (!result) {
                    console.log('Запрос на смену статуса подписки на новостную рассылку не вернул результата');
                    return;
                }

                var msg = result.body;
                showAlert(msg.body, msg.type);
            });
        });
    });
</script>