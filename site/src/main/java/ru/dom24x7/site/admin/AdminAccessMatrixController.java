package ru.dom24x7.site.admin;

import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.cache.Roles;
import ru.dom24x7.model.access.Access;
import ru.dom24x7.model.access.dao.AccessDAOImpl;
import ru.dom24x7.model.user.Role;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;

import java.util.*;

/**
 * Created by timur on 31.05.17.
 *
 * Административная панель: контроль доступа
 */
@Controller
public class AdminAccessMatrixController extends BaseController {

    @RequestMapping("/admin/access")
    public String indexAction(ModelMap model) {

        List<Access> accessMatrix = new AccessDAOImpl().list();
        Set<String> links = new TreeSet<>();
        if (accessMatrix != null) {
            for (Access access : accessMatrix) {
                links.add(access.getUrl());
            }
        }

        Map<String, Map<String, Boolean>> matrix = new HashMap<>();
        for (String link : links) {
            Map<String, Boolean> access = new HashMap<>();
            access.put("all", new AccessDAOImpl().access(link, null));
            for (Role role : Roles.getInstance().list()) {
                access.put(role.getName(), new AccessDAOImpl().access(link, role));
            }
            matrix.put(link, access);
        }

        setModelMap(model, "Матрица доступа");
        model.addAttribute("roles", Roles.getInstance().list());
        model.addAttribute("links", links);
        model.addAttribute("matrix", matrix);
        model.addAttribute("rolesJson", new Gson().toJson(Roles.getInstance().list()));

        return "admin/access/list";
    }

    @RequestMapping(value = "/admin/ajax/access/change", method = RequestMethod.POST)
    public @ResponseBody Response changePostAjax(
            @RequestParam(value = "link") String link,
            @RequestParam(value = "role") String roleName,
            @RequestParam(value = "value") boolean value) {

        Access access = new Access();
        access.setUrl(link);

        if ("all".equalsIgnoreCase(roleName)) {
            if (value) {
                new AccessDAOImpl().delete(link);
                new AccessDAOImpl().save(access);
            } else {
                new AccessDAOImpl().delete(access);
            }
        } else {
            if (value) {
                new AccessDAOImpl().delete(access);
                access.setRole(Roles.getInstance().get(roleName));
                new AccessDAOImpl().save(access);
            } else {
                access.setRole(Roles.getInstance().get(roleName));
                new AccessDAOImpl().delete(access);
            }
        }

        return new Response(0, "OK", new Message("Доступ успешно изменен", Message.TYPE_INFO));
    }
}