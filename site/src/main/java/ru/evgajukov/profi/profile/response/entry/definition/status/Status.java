package ru.evgajukov.profi.profile.response.entry.definition.status;

/**
 * Created by timur on 16.06.17.
 */
public class Status {

    private String id;
    private String producer;
    private String hide;
    private Statuses statuses;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getProducer() {

        return producer;
    }

    public void setProducer(String producer) {

        this.producer = producer;
    }

    public String getHide() {

        return hide;
    }

    public void setHide(String hide) {

        this.hide = hide;
    }

    public Statuses getStatuses() {

        return statuses;
    }

    public void setStatuses(Statuses statuses) {

        this.statuses = statuses;
    }
}
