package ru.evgajukov.profi.profile.response.entry;

import ru.evgajukov.profi.profile.response.entry.definition.Definition;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class Entry {

    private String id; // идентификатор профиля
    private Definition definition;
    private Ratings ratings; // рейтинг профиля
    private Ranks ranks; // рейтинг профиля по 5 бальной шкале
    private String domain; // домен профиля, определяет проект (Репетитор, Мастер ремонта и т.д.) и город (Москва, Санкт-Петербург и т.д.)
    private List<Meta> metas;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public Definition getDefinition() {

        return definition;
    }

    public void setDefinition(Definition definition) {

        this.definition = definition;
    }

    public Ratings getRatings() {

        return ratings;
    }

    public void setRatings(Ratings ratings) {

        this.ratings = ratings;
    }

    public Ranks getRanks() {

        return ranks;
    }

    public void setRanks(Ranks ranks) {

        this.ranks = ranks;
    }

    public String getDomain() {

        return domain;
    }

    public void setDomain(String domain) {

        this.domain = domain;
    }

    public List<Meta> getMetas() {

        return metas;
    }

    public void setMetas(List<Meta> metas) {

        this.metas = metas;
    }

    @Override
    public String toString() {

        return id;
    }
}