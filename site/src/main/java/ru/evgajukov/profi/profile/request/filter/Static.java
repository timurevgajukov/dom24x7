package ru.evgajukov.profi.profile.request.filter;

import ru.evgajukov.profi.profile.request.filter.pinf.Pinf;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 15.06.17.
 */
public class Static {

    private List<Root> roots;
    private List<Model> models;
    private List<Service> services;
    private NGeo ngeo;
    private List<Gender> genders;
    private Degree degree;
    private Experience experience;
    private Ckat ckat;
    private List<Pinf> pinfs;

    public List<Root> getRoots() {

        return roots;
    }

    public Static setRoots(List<Root> roots) {

        this.roots = roots;
        return this;
    }

    public Static addRoot(Root root) {

        if (roots == null) {
            roots = new ArrayList<>();
        }

        roots.add(root);
        return this;
    }

    public List<Model> getModels() {

        return models;
    }

    public Static setModels(List<Model> models) {

        this.models = models;
        return this;
    }

    public Static addModel(Model model) {

        if (models == null) {
            models = new ArrayList<>();
        }

        this.models.add(model);
        return this;
    }

    public List<Service> getServices() {

        return services;
    }

    public Static setServices(List<Service> services) {

        this.services = services;
        return this;
    }

    public Static addService(Service service) {

        if (services == null) {
            services = new ArrayList<>();
        }

        services.add(service);
        return this;
    }

    public NGeo getNgeo() {

        return ngeo;
    }

    public Static setNgeo(NGeo ngeo) {

        this.ngeo = ngeo;
        return this;
    }

    public List<Gender> getGenders() {

        return genders;
    }

    public Static setGenders(List<Gender> genders) {

        this.genders = genders;
        return this;
    }

    public Static addGender(Gender gender) {

        if (genders == null) {
            genders = new ArrayList<>();
        }

        genders.add(gender);
        return this;
    }

    public Degree getDegree() {

        return degree;
    }

    public Static setDegree(Degree degree) {

        this.degree = degree;
        return this;
    }

    public Experience getExperience() {

        return experience;
    }

    public Static setExperience(Experience experience) {

        this.experience = experience;
        return this;
    }

    public Ckat getCkat() {

        return ckat;
    }

    public Static setCkat(Ckat ckat) {

        this.ckat = ckat;
        return this;
    }

    public List<Pinf> getPinfs() {

        return pinfs;
    }

    public Static setPinfs(List<Pinf> pinfs) {

        this.pinfs = pinfs;
        return this;
    }

    public Static addPinf(Pinf pinf) {

        if (pinfs == null) {
            pinfs = new ArrayList<>();
        }

        pinfs.add(pinf);
        return this;
    }
}