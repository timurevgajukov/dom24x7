package ru.dom24x7.site.api.message;

/**
 * Created by timur on 22.02.17.
 * API: request for auth method
 */
public class AuthRequestBody extends BaseBody {

    private String login;
    private String password;

    public String getLogin() {

        return login;
    }

    public void setLogin(String login) {

        this.login = login;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }
}