package ru.evgajukov.profi.profile.request;

/**
 * Created by timur on 15.06.17.
 */
public class Bound {

    private int from;
    private int count;
    private String scope;

    public int getFrom() {

        return from;
    }

    public Bound setFrom(int from) {

        this.from = from;
        return this;
    }

    public int getCount() {

        return count;
    }

    public Bound setCount(int count) {

        this.count = count;
        return this;
    }

    public String getScope() {

        return scope;
    }

    public Bound setScope(String scope) {

        this.scope = scope;
        return this;
    }
}