package ru.dom24x7.model.payment.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.cache.Companies;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.payment.Payment;
import ru.dom24x7.model.payment.dao.interfaces.PaymentDAO;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.user.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 22.03.17.
 * Реализация интерфейса для работы с платежными поручениями
 */
public class PaymentDAOImpl extends ModelDAOImpl implements PaymentDAO {

    private static final String QUERY_GET_BY_ID = "{ call payments_get(?) }";
    private static final String QUERY_LIST_BY_USER = "{ call payments_list_by_user(?,?,?,?) }";
    private static final String QUERY_SAVE = "{ call payments_save(?,?,?,?,?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call payments_delete(?) }";

    @Override
    public Payment get(int id, final User user) {

        List<Payment> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Payment>() {
            @Override
            public Payment mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(user, resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            list = null;
        }

        return list.get(0);
    }

    @Override
    public List<Payment> list(final User user, int offset, int count, String search) {

        List<Payment> list = jdbcTemplate.query(QUERY_LIST_BY_USER,
                new Object[]{user.getId(), offset, count, null}, new RowMapper<Payment>() {
                    @Override
                    public Payment mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(user, resultSet);
                    }
                });

        if (list == null || list.size() == 0) {
            list = null;
        }

        return list;
    }

    @Override
    public boolean save(Payment payment) {

        final User user = payment.getUser();

        Payment newPayment = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        payment.getId() == 0 ? null : payment.getId(),
                        payment.getUser().getId(),
                        payment.getAddress().getId(),
                        payment.getPhoto().getId(),
                        payment.getCompany() == null ? null : payment.getCompany().getId(),
                        payment.getMonth(),
                        payment.getAmount(),
                        payment.isPaid(),
                        payment.getNote()
                }, new RowMapper<Payment>() {
                    @Override
                    public Payment mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(user, resultSet);
                    }
                });

        payment.setId(newPayment.getId());

        return true;
    }

    @Override
    public boolean delete(Payment payment) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{payment.getId()}) != 0;
    }

    private Payment load(User user, ResultSet rs) throws SQLException {

        Payment payment = new Payment(rs.getInt("id"));
        payment.setCreateDt(rs.getTimestamp("create_dt"));
        payment.setUser(new User(rs.getInt("user_id")));
        payment.setAddress(user.getAddress(rs.getInt("address_id")));
        payment.setMonth(rs.getDate("month"));
        payment.setAmount(rs.getDouble("amount"));
        payment.setPaid(rs.getBoolean("is_paid"));
        payment.setNote(rs.getString("note"));

        Photo photo = new Photo(rs.getInt("photo_id"));
        photo.setFileName(rs.getString("file_name"));
        payment.setPhoto(photo);

        int companyId = rs.getInt("company_id");
        if (companyId != 0) {
            payment.setCompany(Companies.getInstance().get(companyId));
        }

        return payment;
    }
}