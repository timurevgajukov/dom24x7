<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="hpanel forum-box">
    <div class="panel-heading">Запросы пользователей</div>
    <c:choose>
        <c:when test="${questions != null && questions.size() != 0}">
            <c:forEach items="${questions}" var="question">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-11 forum-heading">
                            <a href="/lawyer/question/${question.id}">
                                <div class="desc">${question}</div>
                            </a>
                        </div>
                        <div class="col-md-1 forum-info">
                            <span class="number">${question.answersCount}</span>
                            <small>ответов</small>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <div class="panel-body">Пока ни одного вопроса</div>
        </c:otherwise>
    </c:choose>
</div>