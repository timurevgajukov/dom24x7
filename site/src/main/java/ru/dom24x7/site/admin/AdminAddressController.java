package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.component.TableData;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.address.dao.AddressDAOImpl;
import ru.dom24x7.site.BaseController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 16.02.17.
 *
 * Административная панель: управление адресами
 */
@Controller
public class AdminAddressController extends BaseController {

    @RequestMapping("/admin/addresses")
    public String listAction(ModelMap model) {

        setModelMap(model, "Список адресов");

        return "admin/address/list";
    }

    @RequestMapping("/admin/ajax/addresses/list")
    public @ResponseBody TableData listUsersAjax(HttpServletRequest request) {

        int draw = Integer.parseInt(request.getParameter("draw"));
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));
        String search = request.getParameter("search[value]");

        int count = new AddressDAOImpl().count(search);
        List<Address> addresses = new AddressDAOImpl().list(start, length, search);

        if (addresses == null) {
            addresses = new ArrayList<>();
        }

        // загружаем данные по пользователям

        TableData tableData = new TableData();
        tableData.setDraw(draw);
        tableData.setRecordsTotal(count);
        tableData.setRecordsFiltered(count);
        tableData.setData(addresses);

        return tableData;
    }
}