package ru.dom24x7.site.ajax.response;

/**
 * Created by timur on 24.09.16.
 *
 * Класс ответа через api
 */
public class Response {

    private int code;
    private String description;
    private Object body;

    public Response(int code, String description) {

        this.code = code;
        this.description = description;
    }

    public Response(int code, String description, Object body) {

        this(code, description);
        this.body = body;
    }

    public int getCode() {

        return code;
    }

    public void setCode(int code) {

        this.code = code;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public Object getBody() {

        return body;
    }

    public void setBody(Object body) {

        this.body = body;
    }
}