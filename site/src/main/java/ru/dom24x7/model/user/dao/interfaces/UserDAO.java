package ru.dom24x7.model.user.dao.interfaces;

import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.user.Role;
import ru.dom24x7.model.user.User;

import java.util.List;

/**
 * Created by timur on 18.09.16.
 *
 * Интерфейс для работы с пользователями
 */
public interface UserDAO {

    /**
     * Возвращает пользователя по его идентификатору
     * @param id
     * @return
     */
    User get(int id);

    /**
     * Возвращает пользователя по его логину
     *
     * @param login
     * @return
     */
    User getByLogin(String login);

    /**
     * Возвращает пользователя по его эл. почте
     *
     * @param email
     * @return
     */
    User getByEmail(String email);

    /**
     * Авторизация пользователя
     *
     * @param email
     * @param password
     * @return
     */
    User auth(String email, String password);

    /**
     * Подтверждает электронную почту пользователя по коду подтверждения
     *
     * @param confirmCode
     * @return
     */
    User confirm(String confirmCode);

    /**
     * Сохраняет данные по пользователю
     *
     * @param user
     * @param password
     * @param confirmCode
     * @return
     */
    boolean save(User user, String password, String confirmCode);

    /**
     * Сохранение дополнительных данных по пользователю
     * @param user
     * @return
     */
    boolean saveExtra(User user);

    /**
     * Сохраняет новое фото пользователя
     *
     * @param user
     * @return
     */
    boolean savePhoto(User user);

    /**
     * Очистка данных пользователя
     *
     * @param user
     * @return
     */
    boolean clear(User user);

    /**
     * Изменение пароля
     *
     * @param user
     * @param oldPassword
     * @param newPassword
     * @return
     */
    boolean changePassword(User user, String oldPassword, String newPassword);

    /**
     * Восстановление пароля
     *
     * @param user
     * @param newPassword
     * @return
     */
    boolean recoveryPassword(User user, String newPassword);

    /**
     * Возвращает список пользователей
     * @param offset
     * @param count
     * @param search
     * @return
     */
    List<User> list(int offset, int count, String search);

    /**
     * Возвращает список пользователей по роли
     * @param role
     * @return
     */
    List<User> list(Role role);

    /**
     * Возвращает количество пользователей с учетом фильтра
     * @param search
     * @return
     */
    int count(String search);
}