package ru.dom24x7.model.account.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.account.Account;
import ru.dom24x7.model.account.dao.interfaces.AccountDAO;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.user.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 27.04.17.
 * Реализация интерфейса для работы с лицевыми счетами
 */
public class AccountDAOImpl extends ModelDAOImpl implements AccountDAO {

    private static final String QUERY_GET = "{ call gkh_accounts_get_by_user_and_company(?,?) }";
    private static final String QUERY_LIST_BY_USER = "{ call gkh_accounts_list_by_user(?) }";
    private static final String QUERY_LIST_BY_COMPANY = "{ call gkh_accounts_list_by_company(?) }";
    private static final String QUERY_SAVE = "{ call gkh_accounts_save(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call gkh_accounts_delete(?) }";
    private static final String QUERY_DELETE_BY_USER_AND_COMPANY = "{ call gkh_accounts_delete_by_user_and_company(?,?) }";

    @Override
    public Account get(final User user, final Company company) {

        List<Account> list = jdbcTemplate.query(QUERY_GET, new Object[]{user.getId(), company.getId()}, new RowMapper<Account>() {
            @Override
            public Account mapRow(ResultSet resultSet, int i) throws SQLException {

                Account account = load(resultSet);
                account.setUser(user);
                account.setCompany(company);

                return account;
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<Account> list(final User user) {

        List<Account> list = jdbcTemplate.query(QUERY_LIST_BY_USER, new RowMapper<Account>() {
            @Override
            public Account mapRow(ResultSet resultSet, int i) throws SQLException {

                Account account = load(resultSet);
                account.setUser(user);

                return account;
            }
        }, user.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Account> list(final Company company) {

        List<Account> list = jdbcTemplate.query(QUERY_LIST_BY_COMPANY, new RowMapper<Account>() {
            @Override
            public Account mapRow(ResultSet resultSet, int i) throws SQLException {

                Account account = load(resultSet);
                account.setCompany(company);

                return account;
            }
        }, company.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Account account) {

        Account newAccount = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        account.getId() == 0 ? null : account.getId(),
                        account.getUser().getId(),
                        account.getCompany().getId(),
                        account.getAccount()
                }, new RowMapper<Account>() {
                    @Override
                    public Account mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        account.setId(newAccount.getId());

        return true;
    }

    @Override
    public boolean delete(Account account) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{account.getId()}) != 0;
    }

    @Override
    public boolean delete(User user, Company company) {

        return jdbcTemplate.update(QUERY_DELETE_BY_USER_AND_COMPANY, new Object[]{user.getId(), company.getId()}) != 0;
    }

    private Account load(ResultSet rs) throws SQLException {

        User user = new User(rs.getInt("user_id"));
        Company company = new Company(rs.getInt("company_id"));
        return new Account(rs.getInt("id"), user, company, rs.getString("account"));
    }
}