package ru.dom24x7.model.reference;

import java.util.List;

/**
 * Created by timur on 06.07.16.
 *
 * Интерфейс для работы со справочниками
 */
public interface ReferenceDAO<T> {

    /**
     * Возвращает список объектов справочника
     *
     * @return
     */
    List<T> list();
}