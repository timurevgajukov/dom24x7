<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-lg-12 text-center m-t-md">
        <h2>Добро пожаловать!</h2>
        <p>Сервис для удобного учета <strong>коммунальных услуг</strong> и взаимодействия с компаниями сферы ЖКХ.</p>
        <br/>
    </div>
</div>

<div class="row">
    <c:choose>
        <c:when test="${userAddresses != null && userAddresses.size() != 0}">
            <c:forEach items="${userAddresses}" var="address">
                <a href="/panel/addresses/${address.id}">
                    <div class="col-md-3">
                        <div class="hpanel">
                            <div class="panel-body">
                                <div class="stats-title pull-left">
                                    <h4>${address}</h4>
                                </div>
                                <div class="stats-icon pull-right">
                                    <i class="pe-7s-home fa-4x"></i>
                                </div>
                                <div class="m-t-xl">
                                    <small>Расход за текущий месяц:</small>
                                    <h1 class="text-success">${address.totalLastMonth}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <a href="#" class="btn-address-add" data-toggle="modal" data-target="#address-edit-modal">
                <div class="col-md-3">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="stats-title pull-left">
                                <h4>Добавить адрес</h4>
                            </div>
                            <div class="stats-icon pull-right">
                                <i class="pe-7s-plus fa-4x"></i>
                            </div>
                            <div class="m-t-xl">
                                <small>Для работы с сервисом необходимо добавить хотя бы один адрес</small>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </c:otherwise>
    </c:choose>
</div>

<div class="row">
    <div class="col-md-6">
        <c:choose>
            <c:when test="${history != null && history.size() != 0}">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a href="#" id="btn-add-value-dashboard"
                               data-tooltip="true" data-original-title="Добавить показание счетчика"
                               data-toggle="modal" data-target="#meter-value-edit-modal"><i class="fa fa-plus"></i></a>
                        </div>
                        Показания счетчиков
                    </div>
                    <div class="panel-body no-padding meters-block">
                        <div class="list-group">
                            <c:forEach items="${history}" var="item">
                                <a class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <p class="list-group-item-text">
                                                <span class="font-extra-bold">${item.meter}</span><br/>
                                                <span class="text-muted">${item.meter.address}</span>
                                            </p>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <p class="list-group-item-text">
                                            <span class="font-extra-bold">
                                            <c:choose>
                                                <c:when test="${item.meter.type.withoutValue}">${item.stringCost}</c:when>
                                                <c:otherwise>${item.stringValue}</c:otherwise>
                                            </c:choose>
                                            </span><br/>
                                                <span class="text-muted">${item.stringDate}</span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="hpanel">
                    <div class="panel-heading">Показания счетчиков</div>
                    <div class="panel-body meters-block">Ни одного показания счетчика не найдено</div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="col-md-6">
        <c:choose>
            <c:when test="${payments != null && payments.size() != 0}">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a href="#" id="btn-payment-add" data-toggle="modal" data-target="#payment-edit-modal"
                               data-tooltip="true" data-original-title="Добавить платежное поручение">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        Платежные поручения
                    </div>
                    <div class="panel-body no-padding payments-block">
                        <div class="list-group">
                            <c:forEach items="${payments}" var="payment">
                                <a class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <p class="list-group-item-text">
                                                <button class="btn-link btn-payment-show" data-id="${payment.id}"
                                                        data-toggle="modal" data-target="#payment-show-modal"
                                                        data-tooltip="true"
                                                        data-original-title="Посмотреть платежное поручение">
                                                    <c:choose>
                                                        <c:when test="${payment.paid}"><i
                                                                class="fa fa-file-o fa-4x text-info"></i></c:when>
                                                        <c:otherwise><i
                                                                class="fa fa-file-o fa-4x text-danger"></i></c:otherwise>
                                                    </c:choose>
                                                </button>
                                            </p>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <p class="list-group-item-text">
                                                <c:choose>
                                                    <c:when test="${payment.paid}">
                                                        <span class="font-extra-bold">${payment.amount} ${payment.address.currency.symbol}</span><br/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span class="font-extra-bold text-danger">${payment.amount} ${payment.address.currency.symbol}</span><br/>
                                                    </c:otherwise>
                                                </c:choose>
                                                <span class="text-muted">${payment.address}</span><br/>
                                                <span class="text-muted"><fmt:formatDate value="${payment.month}"
                                                                                         pattern="MMM YYYY"/></span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="hpanel">
                    <div class="panel-heading">Платежные поручения</div>
                    <div class="panel-body payments-block">Ни одного платежного поручения не найдено</div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <c:choose>
            <c:when test="${userAddresses != null && userAddresses.size() != 0}">
                <div class="hpanel">
                    <div class="panel-heading">Доли расходов по счетчикам за месяц</div>
                    <div class="panel-body chart-block">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select id="r1-address" class="form-control" data-placeholder="Выберите адрес">
                                            <c:forEach items="${userAddresses}" var="address">
                                                <option value="${address.id}">${address}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkbox"><label><input id="r1-type-meter-join" type="checkbox"> объединить
                                        по типу счетчика</label></div>
                                </div>
                            </div>
                        </form>

                        <div id="report-pie-container"></div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="hpanel">
                    <div class="panel-heading">Доли расходов по счетчикам за месяц</div>
                    <div class="panel-body chart-block">Данные отсутствуют</div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="col-md-4">
        <div class="hpanel">
            <div class="panel-heading">Последние новости</div>
            <div class="panel-body text-left news-block">
                <table>
                    <c:forEach items="${posts}" var="post">
                        <tr>
                            <td valign="top" class="text-success" width="80px"><fmt:formatDate
                                    value="${post.publishedDt}" pattern="dd.MM HH:mm"/></td>
                            <td><a href="/blog/post/${post.id}/${post.translitUrl}"
                                   class="btn-link">${post.showTitle}</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>

<t:insertAttribute name="meter-value-edit" defaultValue="/WEB-INF/views/modal/meter-value-edit.jsp"/>
<t:insertAttribute name="payment-edit" defaultValue="/WEB-INF/views/modal/payment-edit.jsp"/>
<t:insertAttribute name="payment-show" defaultValue="/WEB-INF/views/modal/payment-show.jsp"/>

<script src="//code.highcharts.com/highcharts.js"></script>
<script src="/resources/js/reports.js"></script>

<script>
    $(document).ready(function () {
        <c:if test="${userAddresses != null && userAddresses.size() != 0}">
        showReportPie();

        $('#r1-address').change(function () {
            showReportPie();
        });

        $('#r1-type-meter-join').iCheck({checkboxClass: 'icheckbox_square-green'}).on('ifToggled', function (e) {
            showReportPie();
        });

        function showReportPie() {
            var address = $('#r1-address');
            var typeMeterJoin = document.getElementById('r1-type-meter-join');

            new Dom24x7Report().showReportPie($('#report-pie-container'), {
                'address': address.val(),
                'date-from': '01.' + moment(new Date()).format('MM.YYYY'),
                'date-to': moment().format('DD.MM. YYYY'),
                'by-meter-type': typeMeterJoin.checked
            });
        }
        </c:if>

        resizeBlocks($('.chart-block'), $('.news-block'));
        resizeBlocks($('.meters-block'), $('.payments-block'));
        $(window).resize(function () {
            resizeBlocks($('.chart-block'), $('.news-block'));
            resizeBlocks($('.meters-block'), $('.payments-block'));
        });
    });
</script>