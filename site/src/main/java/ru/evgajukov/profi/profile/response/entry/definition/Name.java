package ru.evgajukov.profi.profile.response.entry.definition;

/**
 * Created by timur on 15.06.17.
 */
public class Name {

    private String patronymic;
    private String last;
    private String first;
    private String full;

    public String getPatronymic() {

        return patronymic;
    }

    public void setPatronymic(String patronymic) {

        this.patronymic = patronymic;
    }

    public String getLast() {

        return last;
    }

    public void setLast(String last) {

        this.last = last;
    }

    public String getFirst() {

        return first;
    }

    public void setFirst(String first) {

        this.first = first;
    }

    public String getFull() {

        return full;
    }

    public void setFull(String full) {

        this.full = full;
    }

    @Override
    public String toString() {

        return full;
    }
}
