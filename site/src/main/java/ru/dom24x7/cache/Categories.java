package ru.dom24x7.cache;

import ru.dom24x7.cache.interfaces.ICache;
import ru.dom24x7.model.post.Category;
import ru.dom24x7.model.post.dao.CategoryDAOImpl;

import java.util.List;

/**
 * Created by timur on 01.03.17.
 * Контейнер для хранения списка категорий постов
 */
public class Categories implements ICache<Category> {

    private static Categories instance = null;

    private List<Category> list;

    public static Categories getInstance() {

        if (instance == null) {
            instance = new Categories();
        }

        return instance;
    }

    private Categories() {

        list = new CategoryDAOImpl().list();
    }

    @Override
    public void clear() {

        if (list != null) {
            list.clear();
        }
        list = null;
        instance = null;
    }

    @Override
    public List<Category> list() {

        return list;
    }

    @Override
    public Category get(int id) {

        if (list == null) {
            return null;
        }

        for (Category item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }

    public Category get(String urlName) {

        if (list == null) {
            return null;
        }

        for (Category item : list) {
            if (urlName.equalsIgnoreCase(item.getUrlName())) {
                return item;
            }
        }

        return null;
    }
}