package ru.dom24x7.model.access.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.cache.Roles;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.access.Access;
import ru.dom24x7.model.access.dao.interfaces.AccessDAO;
import ru.dom24x7.model.user.Role;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 14.02.17.
 * <p>
 * Реализация интерфейса для работы с матрицей доступа
 */
public class AccessDAOImpl extends ModelDAOImpl implements AccessDAO {

    private static final String QUERY_GET_BY_URL_AND_ROLE = "{ call access_matrix_get_by_url_and_role(?,?) }";
    private static final String QUERY_LIST = "{ call access_matrix_list() }";
    private static final String QUERY_ACCESS_BY_ROLE = "{ call access_matrix_get_by_role(?) }";
    private static final String QUERY_ACCESS_BY_URL = "{ call access_matrix_get_by_url(?) }";
    private static final String QUERY_SAVE = "{ call access_matrix_save(?,?) }";
    private static final String QUERY_DELETE_BY_URL_AND_ROLE = "{ call access_matrix_delete_by_url_and_role(?,?) }";
    private static final String QUERY_DELETE_BY_URL = "{ call access_matrix_delete_by_url(?) }";

    @Override
    public boolean access(String url, Role role) {

        List<Access> list = jdbcTemplate.query(QUERY_GET_BY_URL_AND_ROLE, new Object[]{url, role == null ? null : role.getId()},
                new RowMapper<Access>() {
                    @Override
                    public Access mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        return list != null && list.size() != 0;
    }

    @Override
    public List<Access> list() {

        List<Access> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Access>() {
            @Override
            public Access mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Access> list(Role role) {

        List<Access> list = jdbcTemplate.query(QUERY_ACCESS_BY_ROLE, new RowMapper<Access>() {

            @Override
            public Access mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, role == null ? null : role.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Access> list(String url) {

        List<Access> list = jdbcTemplate.query(QUERY_ACCESS_BY_URL, new RowMapper<Access>() {
            @Override
            public Access mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, url);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Access access) {

        Access newAccess = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        access.getUrl(),
                        access.getRole() == null ? null : access.getRole().getId()
                }, new RowMapper<Access>() {
                    @Override
                    public Access mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        access.setId(newAccess.getId());

        return true;
    }

    @Override
    public boolean delete(Access access) {

        return jdbcTemplate.update(QUERY_DELETE_BY_URL_AND_ROLE,
                new Object[]{access.getUrl(), access.getRole() == null ? null : access.getRole().getId()}) != 0;
    }

    @Override
    public boolean delete(String url) {

        return jdbcTemplate.update(QUERY_DELETE_BY_URL, new Object[]{url}) != 0;
    }

    private Access load(ResultSet rs) throws SQLException {

        return new Access(rs.getInt("id"), Roles.getInstance().get(rs.getInt("role_id")), rs.getInt("function_id"), rs.getString("url"));
    }
}