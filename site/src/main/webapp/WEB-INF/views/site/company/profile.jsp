<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="/resources/lib/bootstrap-star-rating/css/star-rating.css" />
<script src="/resources/lib/bootstrap-star-rating/js/star-rating.min.js"></script>

<style>
    .rating-container .filled-stars {
        text-shadow: none;
        -webkit-text-stroke: 0;
        color: #777;
    }
    .rating-xs {
        font-size: 1.5em;
    }
</style>

<h1>${company.name}</h1>
<div class="row">
    <div class="col-md-6">
        <div id="map" style="width: 100%; height: 300px"></div>
    </div>
    <div class="col-md-6">
        <div class="hpanel hblue">
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Адрес:</dt>
                    <dd>${company.address}</dd>
                    <dt>ФИО руководителя:</dt>
                    <dd>${company.directorFIO}</dd>
                    <dt>Номер телефона:</dt>
                    <dd>${company.phone}</dd>
                    <dt>Электронная почта:</dt>
                    <dd>${company.email}</dd>
                    <dt>Сайт:</dt>
                    <dd>${company.site}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>

<div class="hpanel hgreen">
    <div class="panel-heading">Дополнительная информация по компании</div>
    <div class="panel-body no-padding">
        <div class="list-group">
            <a class="list-group-item">
                <div class="row">
                    <div class="col-md-6">
                        <p class="list-group-item-text">
                            Фирменное наименование юридического лица (согласно уставу организации)
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="list-group-item-text">${company.fullName}</p>
                    </div>
                </div>
            </a>
            <a class="list-group-item">
                <div class="row">
                    <div class="col-md-6">
                        <p class="list-group-item-text">
                            Идентификационный номер налогоплательщика (ИНН)
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="list-group-item-text">${company.inn}</p>
                    </div>
                </div>
            </a>
            <a class="list-group-item">
                <div class="row">
                    <div class="col-md-6">
                        <p class="list-group-item-text">
                            Основной государственный регистрационный номер / основной государственный регистрационный
                            номер индивидуального предпринимателя (ОГРН/ ОГРНИП)
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="list-group-item-text">${company.ogrn}</p>
                    </div>
                </div>
            </a>
            <a class="list-group-item">
                <div class="row">
                    <div class="col-md-6">
                        <p class="list-group-item-text">
                            Режим работы, в т. ч. часы личного приема граждан
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="list-group-item-text">${company.openingHours}</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<c:choose>
    <c:when test="${security.adminRole && profiles != null && profiles.size() != 0}">
        <div class="row">
            <c:forEach items="${profiles}" var="profile">
                <div class="col-md-3">
                    <div class="hpanel">
                        <div class="panel-body" id="profi-${profile.id}">
                            <table width="100%">
                                <tr>
                                    <td valign="top"><img src="${profile.avatar.url}" /></td>
                                    <td width="15"></td>
                                    <td valign="top">
                                        <div class="text-primary">${profile}</div>
                                        <div class="text-muted font-bold m-b-xs">${profile.services.get(0)}</div><br />
                                        <div class="text-center" style="width: 100%">
                                            <button class="btn btn-danger2">Выбрать</button>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="panel-footer contact-footer">
                            <div class="row">
                                <div class="col-md-4 border-right"> <div class="contact-stat"><span>Рейтинг: </span> <strong>{profile.ratings.value}</strong></div> </div>
                                <div class="col-md-4 border-right"> <div class="contact-stat"><span>Отзывов: </span> <strong></strong></div> </div>
                                <div class="col-md-4"> <div class="contact-stat"><span>Мин. цена: </span> <strong>от {profile.definition.prices.get(0).price} Р</strong></div> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <div class="col-md-3">
                <div class="hpanel">
                    <div class="panel-body" id="profi-search">
                        <div class="text-center">
                            <h2 class="m-b-xs">Поиск</h2>
                            <p class="font-bold text-danger">мастера по ремонту</p>
                            <div class="m">
                                <i class="pe-7s-search fa-5x"></i>
                            </div>
                            <p class="small">Выберите среди более чем 20 тыс. специалистов мастера по своим критериям.</p>
                            <a class="btn btn-danger2" href="/profi">Найти</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="row">
            <div class="col-xs-12">
                <div style="max-width: 728px;">
                    <script src="//embed.bannerboo.com/bfd88aa08efab?target_window=_top&responsive=1" async></script>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>

<div class="hpanel hred">
    <div class="panel-heading">Отзывы</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <span class="text-big font-light">${averageStr}</span><br />
                <input id="rating" class="rating-loading" data-size="xs" value="${average}" />
                <span class="font-trans">
                    <i class="fa fa-user"></i>
                    Оценок: ${ratingsCount}
                </span>
            </div>
            <c:choose>
                <c:when test="${security.auth}">
                    <c:if test="${userRating == null}">
                        <div class="col-md-4 text-right">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#review-edit-modal"><i class="fa fa-pencil"></i> Написать отзыв</button>
                        </div>

                        <t:insertAttribute name="review-edit" defaultValue="/WEB-INF/views/modal/review-edit.jsp" />
                    </c:if>
                </c:when>
                <c:otherwise>
                    <div class="col-md-4 text-right">
                        <span class="text-danger">Зарегистрируйтесь, чтобы оставить отзыв</span>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<c:if test="${ratings != null && ratings.size() != 0}">
    <div class="row social-board">
        <c:forEach items="${ratings}" var="rating">
            <div class="col-md-4">
                <div class="hpanel">
                    <div class="panel-body">
                        <div class="media social-profile clearfix">
                            <a class="pull-left">
                                <img src="${rating.user.iconUrl}" alt="profile-picture">
                            </a>
                            <div class="media-body">
                                <h5>
                                    ${rating.user}&nbsp;&nbsp;
                                    <i class="fa <c:choose><c:when test="${rating.mark > 0}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
                                    <i class="fa <c:choose><c:when test="${rating.mark > 1}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
                                    <i class="fa <c:choose><c:when test="${rating.mark > 2}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
                                    <i class="fa <c:choose><c:when test="${rating.mark > 3}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
                                    <i class="fa <c:choose><c:when test="${rating.mark > 4}">fa-star</c:when><c:otherwise>fa-star-o</c:otherwise></c:choose>"></i>
                                </h5>
                                <small class="text-muted">${rating.createDtStr}</small>
                            </div>
                        </div>

                        <div class="social-content m-t-md">${rating.review}</div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</c:if>

<c:if test="${!security.auth}">
    <div class="hpanel hred">
        <div class="panel-body">
            <p><strong>Совет:</strong> чтобы вести удобный учет по всем расходам по своему жилью, вам необходимо
                зарегистрироваться.</p>
            <p>Другие возможности сервиса:</p>
            <ul>
                <li>хранение сканов платежных поручений с признаком оплаты, чтобы не забывать какие уже оплатили, а какие еще нет;</li>
                <li>привязка УК / ТСЖ к счетчикам и отправка показаний;</li>
                <li>юридические консультации по вопросам ЖКХ;</li>
                <li>поиск и вызов мастера по ремонту на дом;</li>
                <li>ставить оценку и оставлять отзывы по своей УК/ТСЖ;</li>
                <li>многое другое, причем возможности сервиса с каждым днем все возрастают</li>
                <li>и все это совершенно <strong class="font-uppercase">бесплатно</strong>.</li>
            </ul>
            <p></p>
        </div>
    </div>
    <t:insertAttribute name="form-auth-reg" defaultValue="/WEB-INF/views/site/component/form-auth-reg.jsp"/>
</c:if>

<script>
    $(document).ready(function () {
        $("#rating").rating({displayOnly: true, showCaption: false, showClear: false});

        resizeProfiBlocks();
        $(window).resize(function () {
            resizeProfiBlocks();
        });

        function resizeProfiBlocks() {
            <c:if test="${profiles != null && profiles.size() != 0}">
            <c:forEach items="${profiles}" var="profile">
            resizeBlocks($('#profi-${profile.id}'), $('#profi-search'), -67);
            </c:forEach>
            </c:if>
        }
    });
</script>

<c:if test="${company.coordLat != null && company.coordLng != null}">
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            ymaps.ready(function () {
                var myMap = new ymaps.Map("map", {
                    center: [${company.coordLat}, ${company.coordLng}],
                    zoom: 15
                });

                var myPlacemark = new ymaps.Placemark([${company.coordLat}, ${company.coordLng}], {
                    hintContent: '${company.name}',
                    balloonContent: '${company.address}'
                });
                myMap.geoObjects.add(myPlacemark);
            });
        });
    </script>
</c:if>