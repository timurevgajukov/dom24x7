package ru.dom24x7.model.setting.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.setting.dao.interfaces.SettingDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса для работы с настройками системы
 */
public class SettingDAOImpl extends ModelDAOImpl implements SettingDAO {

    private static final String QUERY_LIST = "{ call settings_list() }";

    @Override
    public HashMap<String, String> map() {

        List<SettingItem> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<SettingItem>() {

            @Override
            public SettingItem mapRow(ResultSet resultSet, int i) throws SQLException {

                return new SettingItem(resultSet.getString("key"), resultSet.getString("value"));
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        HashMap<String, String> settings = new HashMap<String, String>();
        for (SettingItem item : list) {
            settings.put(item.key, item.value);
        }

        return settings;
    }

    class SettingItem {

        public String key;
        private String value;

        public SettingItem(String key, String value) {

            this.key = key;
            this.value = value;
        }
    }
}