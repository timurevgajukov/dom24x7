package ru.dom24x7.site.panel;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.model.payment.dao.PaymentDAOImpl;
import ru.dom24x7.model.post.dao.PostDAOImpl;
import ru.dom24x7.site.BaseController;

import java.util.*;

/**
 * Created by timur on 22.09.16.
 *
 * Контроллер для обработки главной страницы
 */
@Controller
public class DashboardController extends BaseController {

    private static final String ACTIVE = "dashboard";

    @RequestMapping("/panel")
    public String dashboardAction(ModelMap model) {

        setModelMap(model, "Главная", ACTIVE);
        model.addAttribute("posts", new PostDAOImpl().listForBlog(0, 15));
        model.addAttribute("history", listLastMetersHistory());
        model.addAttribute("payments", new PaymentDAOImpl().list(security.getUser(), 0, 5, null));

        return "panel/dashboard";
    }

    /**
     * Возвращает отсортированный список последних показаний счетчиков пользователя
     * @return
     */
    private List<MeterHistoryItem> listLastMetersHistory() {

        List<Meter> meters = new MeterDAOImpl().list(security.getUser());

        if (meters == null || meters.size() == 0) {
            return null;
        }

        // получаем последние показания счетчиков
        List<MeterHistoryItem> list = new ArrayList<MeterHistoryItem>();
        for (Meter meter : meters) {
            List<MeterHistoryItem> history = meter.getHistory();
            if (history != null && history.size() != 0) {
                list.add(history.get(0));
            }
        }

        // отсортируем показания по дате
        Collections.sort(list, new Comparator<MeterHistoryItem>() {
            @Override
            public int compare(MeterHistoryItem item1, MeterHistoryItem item2) {

                return item2.getDate().compareTo(item1.getDate());
            }
        });

        return list;
    }
}