package ru.evgajukov.profi.profile.response.entry.definition;

/**
 * Created by timur on 16.06.17.
 */
public class Common {

    private int months;
    private int years;
    private String description;

    public int getMonths() {

        return months;
    }

    public void setMonths(int months) {

        this.months = months;
    }

    public int getYears() {

        return years;
    }

    public void setYears(int years) {

        this.years = years;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    @Override
    public String toString() {

        return description;
    }
}