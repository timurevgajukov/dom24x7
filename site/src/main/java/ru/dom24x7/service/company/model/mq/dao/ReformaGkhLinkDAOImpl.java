package ru.dom24x7.service.company.model.mq.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.service.company.model.mq.ReformaGkhLink;
import ru.dom24x7.service.company.model.mq.dao.interfaces.ReformaGkhLinkDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 08.04.17.
 * Реализация интерфейса для работы с очередью ссылок для парсинга сайта "Реформа ЖКХ"
 */
public class ReformaGkhLinkDAOImpl extends ModelDAOImpl implements ReformaGkhLinkDAO {

    private static final String QUERY_GET = "{ call reforma_gkh_parser_mq_get() }";
    private static final String QUERY_SAVE = "{ call reforma_gkh_parser_mq_save(?,?,?) }";
    private static final String QUERY_DELETE = "{ call reforma_gkh_parser_mq_delete(?) }";

    @Override
    public ReformaGkhLink get() {

        List<ReformaGkhLink> link = jdbcTemplate.query(QUERY_GET, new RowMapper<ReformaGkhLink>() {
            @Override
            public ReformaGkhLink mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (link == null || link.size() == 0) {
            return null;
        }

        return link.get(0);
    }

    @Override
    public boolean save(ReformaGkhLink reformaGkhLink) {

        return jdbcTemplate.update(QUERY_SAVE,
                new Object[]{
                        reformaGkhLink.getId() == 0 ? null : reformaGkhLink.getId(),
                        reformaGkhLink.getLink(),
                        reformaGkhLink.getSorting()
        }) != 0;
    }

    @Override
    public boolean delete(ReformaGkhLink reformaGkhLink) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{reformaGkhLink.getId()}) != 0;
    }

    private ReformaGkhLink load(ResultSet rs) throws SQLException {

        ReformaGkhLink reformaGkhLink = new ReformaGkhLink(rs.getInt("id"), rs.getString("link"));
        reformaGkhLink.setSorting(rs.getInt("sorting"));

        return reformaGkhLink;
    }
}