package ru.dom24x7.model.company;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.account.Account;
import ru.dom24x7.model.account.dao.AccountDAOImpl;
import ru.dom24x7.model.user.User;
import ru.dom24x7.utils.Translit;

import java.util.Date;

/**
 * Управляющая компания
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Company extends Model {

    private String fullName;
    private String address;
    private String phone;
    private String email;
    private String site;
    private boolean hold;
    private String directorPost;
    private String directorFIO;
    private Double coordLat;
    private Double coordLng;
    private String inn;
    private String ogrn;
    private String openingHours;
    private String gisZkhUrl;
    private Date gisZkhParseDt;
    private String reformaGkhUrl;
    private Date reformaGkhParseDt;
	
	public Company() {
		
		super();
	}
	
	public Company(int id) {
		
		super(id);
	}

    public Company(int id, String name) {

        super(id, name);
    }

    public String getFullName() {

        return fullName;
    }

    public void setFullName(String fullName) {

        this.fullName = fullName;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getSite() {

        return site;
    }

    public void setSite(String site) {

        this.site = site;
    }

    public boolean isHold() {

        return hold;
    }

    public void setHold(boolean hold) {

        this.hold = hold;
    }

    public String getDirectorPost() {

        return directorPost;
    }

    public void setDirectorPost(String directorPost) {

        this.directorPost = directorPost;
    }

    public String getDirectorFIO() {

        return directorFIO;
    }

    public void setDirectorFIO(String directorFIO) {

        this.directorFIO = directorFIO;
    }

    public Double getCoordLat() {

        return coordLat;
    }

    public void setCoordLat(Double coordLat) {

        this.coordLat = coordLat;
    }

    public Double getCoordLng() {

        return coordLng;
    }

    public void setCoordLng(Double coordLng) {

        this.coordLng = coordLng;
    }

    public String getInn() {

        return inn;
    }

    public void setInn(String inn) {

        this.inn = inn;
    }

    public String getOgrn() {

        return ogrn;
    }

    public void setOgrn(String ogrn) {

        this.ogrn = ogrn;
    }

    public String getOpeningHours() {

        return openingHours;
    }

    public void setOpeningHours(String openingHours) {

        this.openingHours = openingHours;
    }

    public String getGisZkhUrl() {

        return gisZkhUrl;
    }

    public void setGisZkhUrl(String gisZkhUrl) {

        this.gisZkhUrl = gisZkhUrl;
    }

    public Date getGisZkhParseDt() {

        return gisZkhParseDt;
    }

    public void setGisZkhParseDt(Date gisZkhParseDt) {

        this.gisZkhParseDt = gisZkhParseDt;
    }

    public String getReformaGkhUrl() {

        return reformaGkhUrl;
    }

    public void setReformaGkhUrl(String reformaGkhUrl) {

        this.reformaGkhUrl = reformaGkhUrl;
    }

    public Date getReformaGkhParseDt() {

        return reformaGkhParseDt;
    }

    public void setReformaGkhParseDt(Date reformaGkhParseDt) {

        this.reformaGkhParseDt = reformaGkhParseDt;
    }

    public String getTranslitUrl() {

        return Translit.translitUrl(name);
    }

    public Account getAccount(User user) {

	    return new AccountDAOImpl().get(user, this);
    }

    public String getAccountNumber(User user) {

	    Account account = getAccount(user);
	    if (account == null) {
	        return "";
        } else {
	        return account.getAccount();
        }
    }
}