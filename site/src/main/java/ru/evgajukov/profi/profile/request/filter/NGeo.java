package ru.evgajukov.profi.profile.request.filter;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class NGeo {

    private String location;
    private List<Integer> ids;

    public String getLocation() {

        return location;
    }

    public void setLocation(String location) {

        this.location = location;
    }

    public List<Integer> getIds() {

        return ids;
    }

    public void setIds(List<Integer> ids) {

        this.ids = ids;
    }

    @Override
    public String toString() {

        return location;
    }
}