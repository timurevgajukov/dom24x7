package ru.dom24x7.model.access;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.user.Role;

/**
 * Created by timur on 16.02.17.
 *
 * Класс матрицы доступа
 */
public class Access extends Model {

    private Role role;
    private int functionId;
    private String url;

    public Access() {

        super();
    }

    public Access(int id, Role role, int functionId, String url) {

        super(id);
        this.role = role;
        this.functionId = functionId;
        this.url = url;
    }

    public Role getRole() {

        return role;
    }

    public void setRole(Role role) {

        this.role = role;
    }

    public int getFunctionId() {

        return functionId;
    }

    public void setFunctionId(int functionId) {

        this.functionId = functionId;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }
}