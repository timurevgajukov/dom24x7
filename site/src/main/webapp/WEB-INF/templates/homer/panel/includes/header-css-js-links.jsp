<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Vendor styles -->
<link rel="stylesheet" href="/resources/themes/homer/vendor/fontawesome/css/font-awesome.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/metisMenu/dist/metisMenu.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/animate.css/animate.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/bootstrap/dist/css/bootstrap.css" />
<link rel="stylesheet" href="/resources/lib/sweetalert/dist/sweetalert.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/toastr/build/toastr.min.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="/resources/themes/homer/vendor/ladda/dist/ladda-themeless.min.css" />

<!-- App styles -->
<link rel="stylesheet" href="/resources/themes/homer/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
<link rel="stylesheet" href="/resources/themes/homer/fonts/pe-icon-7-stroke/css/helper.css" />
<link rel="stylesheet" href="/resources/themes/homer/styles/style.css" />
<link rel="stylesheet" href="/resources/themes/homer/styles/static_custom.css" />

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/v/bs/dt-1.10.13/datatables.min.css"/>

<link rel="stylesheet" href="/resources/css/styles.css"/>

<!-- jQuery -->
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script>window.jQuery || document.write('<script src="/resources/lib/jquery/jquery-2.1.1.min.js"><\/script>')</script>