package ru.evgajukov.profi.profile.response.entry.definition.price;

import com.google.gson.annotations.SerializedName;

/**
 * Created by timur on 15.06.17.
 */
public class Edizm {

    private int id;
    private int nquanta;
    private String name;
    @SerializedName("quantum_id")
    private int quantumId;
    private String allnames;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public int getNquanta() {

        return nquanta;
    }

    public void setNquanta(int nquanta) {

        this.nquanta = nquanta;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getQuantumId() {

        return quantumId;
    }

    public void setQuantumId(int quantumId) {

        this.quantumId = quantumId;
    }

    public String getAllnames() {

        return allnames;
    }

    public void setAllnames(String allnames) {

        this.allnames = allnames;
    }

    @Override
    public String toString() {

        return name;
    }
}