package ru.dom24x7.model.account;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.user.User;

/**
 * Created by timur on 27.04.17.
 * Модель лицевого счета пользователя в компании
 */
public class Account extends Model {

    private User user;
    private Company company;
    private String account;

    public Account() {

        super();
    }

    public Account(int id, User user, Company company, String account) {

        super(id);
        this.user = user;
        this.company = company;
        this.account = account;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Company getCompany() {

        return company;
    }

    public void setCompany(Company company) {

        this.company = company;
    }

    public String getAccount() {

        return account;
    }

    public void setAccount(String account) {

        this.account = account;
    }

    @Override
    public String toString() {

        return account;
    }
}
