package ru.dom24x7.model.template.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.template.Template;
import ru.dom24x7.model.template.dao.interfaces.TemplateDAO;
import ru.dom24x7.model.user.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 19.09.16.
 *
 * Реализация интерфейса для работы с шаблонами писем пользователей
 */
public class TemplateDAOImpl extends ModelDAOImpl implements TemplateDAO {

    private static final String QUERY_GET_BY_ID = "{ call templates_get_by_id(?) }";
    private static final String QUERY_LIST_BY_USER = "{ call templates_list(?) }";
    private static final String QUERY_SAVE = "{ call templates_save(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call templates_delete(?) }";

    @Override
    public Template get(int id) {

        List<Template> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Template>() {

            @Override
            public Template mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<Template> list(User user) {

        List<Template> list = jdbcTemplate.query(QUERY_LIST_BY_USER, new RowMapper<Template>() {

            @Override
            public Template mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, user.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public boolean save(Template template, User user) {

        Template newTemplate = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        template.getId(),
                        user.getId(),
                        template.getName() == null || template.getName().length() == 0 ? null : template.getName(),
                        template.getBody()
                }, new RowMapper<Template>() {

                    @Override
                    public Template mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        template.setId(newTemplate.getId());

        return true;
    }

    @Override
    public boolean delete(Template template) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{ template.getId() }) != 0;
    }

    private Template load(ResultSet rs) throws SQLException {

        return new Template(rs.getInt("id"), rs.getString("name"), rs.getString("body"));
    }
}