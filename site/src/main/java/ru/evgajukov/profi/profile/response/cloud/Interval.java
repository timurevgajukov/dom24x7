package ru.evgajukov.profi.profile.response.cloud;

/**
 * Created by timur on 17.06.17.
 */
public class Interval {

    private int from;
    private int to;
    private int count;

    public int getFrom() {

        return from;
    }

    public void setFrom(int from) {

        this.from = from;
    }

    public int getTo() {

        return to;
    }

    public void setTo(int to) {

        this.to = to;
    }

    public int getCount() {

        return count;
    }

    public void setCount(int count) {

        this.count = count;
    }
}
