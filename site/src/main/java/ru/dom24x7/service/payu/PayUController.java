package ru.dom24x7.service.payu;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.site.BaseController;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by timur on 18.05.17.
 * Контроллер для работыки запросов на оплату
 */
@Controller
public class PayUController extends BaseController {

    @RequestMapping("/ajax/payu/backend")
    public @ResponseBody String backendAction(HttpServletRequest request) {

        String token = request.getParameter("token");
        if (token == null) {
            return "This was created to work with tokens";
        }

        String url = "https://secure.payu.ru/order/alu/v3";
        String secretKey = "demosecret";

        Items arParams = new Items();

        // Указываем код мерчанта
        arParams.add("MERCHANT", "demoshop");
        // order external reference number in Merchant's system
        arParams.add("ORDER_REF", getRnd(1000, 9999));
        arParams.add("ORDER_DATE", getDateUTC());

        // Детали первого товара
        arParams.add("ORDER_PNAME[0]", "Тестовый товар");
        arParams.add("ORDER_PCODE[0]", "123");
        arParams.add("ORDER_PINFO[0]", "Дополнительная информация к тестовому товару");
        arParams.add("ORDER_PRICE[0]", "1.01");
        arParams.add("ORDER_QTY[0]", "1");

        arParams.add("PRICES_CURRENCY", "RUB");
        arParams.add("PAY_METHOD", "CCVISAMC"); // to remove
        arParams.add("CC_TOKEN", token);

        // скрипт взаимодействия с 3DS, более подробное описание на странице документации ALUv3
        arParams.add("BACK_REF", "http://payu.ru/flash/3ds_hand.php");

        // данные клиента
        arParams.add("CLIENT_IP", "127.0.0.1");
        arParams.add("BILL_LNAME", "Тимур");
        arParams.add("BILL_FNAME", "Евгажуков");
        arParams.add("BILL_EMAIL", "evgajukov@gmail.com");
        arParams.add("BILL_PHONE", "+79258779819");
        arParams.add("BILL_COUNTRYCODE", "RU");

        // Информация о доставке
        arParams.add("DELIVERY_FNAME", "Тимур");
        arParams.add("DELIVERY_LNAME", "Евгажуков");
        arParams.add("DELIVERY_PHONE", "+79258779819");
        arParams.add("DELIVERY_ADDRESS", "ул. Колпакова, д. 24");
        arParams.add("DELIVERY_ZIPCODE", "141008");
        arParams.add("DELIVERY_CITY", "Мытищи");
        arParams.add("DELIVERY_STATE", "Московская обл.");
        arParams.add("DELIVERY_COUNTRYCODE", "RU");

        // Расчет подписи
        arParams.sort();

        String hashString = "";
        for (Item item : arParams.list()) {
            hashString += item.getValue().length() + item.getValue();
        }

        // базовая строка для подсчета подписи
        arParams.add("ORDER_HASH", getHmacMD5(hashString, secretKey));
        String paramsStr = httpBuildQuery(arParams);

        int responseCode = 0;
        HttpsURLConnection conn = null;
        BufferedReader in = null;
        try {
            System.setProperty("https.protocols", "TLSv1.1");
            conn = (HttpsURLConnection) new URL(url).openConnection();
            conn.setConnectTimeout(60000);
            conn.setReadTimeout(60000);
            conn.setRequestMethod("POST");

            conn.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(paramsStr);
            wr.flush();
            wr.close();

            responseCode = conn.getResponseCode();
            if (responseCode == conn.HTTP_OK) {
                in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String strLine;
                StringBuilder finalHTML = new StringBuilder();
                while ((strLine = in.readLine()) != null) {
                    finalHTML.append(strLine);
                }

                System.out.println(finalHTML);
            } else if (responseCode == conn.HTTP_NOT_FOUND) {
                // не нашли ресурс по ссылке
                System.out.println(">>>>> По указанной ссылке ресурс не найден");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }

        return "OK";
    }

    private String getDateUTC() {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));

        return dateFormatGmt.format(new Date());
    }

    private String getRnd(int min, int max) {

        return String.valueOf(Math.round((max - min) * Math.random() + min));
    }

    private String getHmacMD5(String data, String secret) {

        String result = "";

        try {
            SecretKeySpec keySpec = new SecretKeySpec(secret.getBytes(), "HmacMD5");
            Mac mac = Mac.getInstance(keySpec.getAlgorithm());
            mac.init(keySpec);
            byte[] digest = mac.doFinal(data.getBytes());
            for (byte b : digest) {
                result += String.format("%02x", b);
            }
        } catch (Exception e) {
        }

        return result;
    }

    private String httpBuildQuery(Items items) {

        String encoding = "UTF-8";
        StringBuilder sb = new StringBuilder();
        for (Item item : items.list()) {
            if (sb.length() > 0) {
                sb.append('&');
            }

            String name = item.getKey();
            Object value = item.getValue();


            if (value instanceof Map) {
                List<String> baseParam = new ArrayList<String>();
                baseParam.add(name);
                String str = buildUrlFromMap(baseParam, (Map) value, encoding);
                sb.append(str);

            } else if (value instanceof Collection) {
                List<String> baseParam = new ArrayList<String>();
                baseParam.add(name);
                String str = buildUrlFromCollection(baseParam, (Collection) value, encoding);
                sb.append(str);

            } else {
                sb.append(encodeParam(name));
                sb.append("=");
                sb.append(encodeParam(value));
            }


        }

        return sb.toString();
    }

    private static String buildUrlFromMap(List<String> baseParam, Map<Object, Object> map, String encoding) {

        StringBuilder sb = new StringBuilder();
        String token;

        //Build string of first level - related with params of provided Map
        for (Map.Entry<Object, Object> entry : map.entrySet()) {

            if (sb.length() > 0) {
                sb.append('&');
            }

            String name = String.valueOf(entry.getKey());
            Object value = entry.getValue();
            if (value instanceof Map) {
                List<String> baseParam2 = new ArrayList<String>(baseParam);
                baseParam2.add(name);
                String str = buildUrlFromMap(baseParam2, (Map) value, encoding);
                sb.append(str);

            } else if (value instanceof List) {
                List<String> baseParam2 = new ArrayList<String>(baseParam);
                baseParam2.add(name);
                String str = buildUrlFromCollection(baseParam2, (List) value, encoding);
                sb.append(str);
            } else {
                token = getBaseParamString(baseParam) + "[" + name + "]=" + encodeParam(value);
                sb.append(token);
            }
        }

        return sb.toString();
    }

    private static String buildUrlFromCollection(List<String> baseParam, Collection coll, String encoding) {

        StringBuilder sb = new StringBuilder();
        String token;
        if (!(coll instanceof List)) {
            coll = new ArrayList(coll);
        }
        List arrColl = (List) coll;

        //Build string of first level - related with params of provided Map
        for (int i = 0; i < arrColl.size(); i++) {

            if (sb.length() > 0) {
                sb.append('&');
            }

            Object value = (Object) arrColl.get(i);
            if (value instanceof Map) {
                List<String> baseParam2 = new ArrayList<String>(baseParam);
                baseParam2.add(String.valueOf(i));
                String str = buildUrlFromMap(baseParam2, (Map) value, encoding);
                sb.append(str);

            } else if (value instanceof List) {
                List<String> baseParam2 = new ArrayList<String>(baseParam);
                baseParam2.add(String.valueOf(i));
                String str = buildUrlFromCollection(baseParam2, (List) value, encoding);
                sb.append(str);
            } else {
                token = getBaseParamString(baseParam) + "[" + i + "]=" + encodeParam(value);
                sb.append(token);
            }
        }

        return sb.toString();
    }

    private static String getBaseParamString(List<String> baseParam) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < baseParam.size(); i++) {
            String s = baseParam.get(i);
            if (i == 0) {
                sb.append(s);
            } else {
                sb.append("[" + s + "]");
            }
        }
        return sb.toString();
    }

    private static String encodeParam(Object param) {

        try {
            return URLEncoder.encode(String.valueOf(param), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return String.valueOf(param);
        }
    }

    class Items {

        List<Item> items;

        {
            items = new ArrayList<Item>();
        }

        public void add(String key, String value) {

            items.add(new Item(key, value));
        }

        public void sort() {

            Collections.sort(items, new Comparator<Item>() {
                @Override
                public int compare(Item item1, Item item2) {

                    return item1.getKey().compareTo(item2.getKey());
                }
            });
        }

        public List<Item> list() {

            return items;
        }
    }

    class Item {

        private String key;
        private String value;

        public Item(String key, String value) {

            this.key = key;
            this.value = value;
        }

        public String getKey() {

            return key;
        }

        public String getValue() {

            return value;
        }
    }
}