package ru.evgajukov.profi.profile.response.entry.definition;

import com.google.gson.annotations.SerializedName;

/**
 * Created by timur on 17.06.17.
 */
public class Pinf {

    @SerializedName("key_id")
    private int keyId;
    @SerializedName("typ_id")
    private String typId;
    private String comments;

    public int getKeyId() {

        return keyId;
    }

    public void setKeyId(int keyId) {

        this.keyId = keyId;
    }

    public String getTypId() {

        return typId;
    }

    public void setTypId(String typId) {

        this.typId = typId;
    }

    public String getComments() {

        return comments;
    }

    public void setComments(String comments) {

        this.comments = comments;
    }
}