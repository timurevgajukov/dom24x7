package ru.dom24x7.site.panel.report;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.MeterHistoryItem;
import ru.dom24x7.model.meter.MeterType;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.service.meter.MeterService;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by timur on 19.10.16.
 *
 * Контроллер для работы с отчетами
 */
@Controller
public class ReportController extends BaseController {

    private static final String ACTIVE = "reports";

    @RequestMapping("/panel/reports")
    public String listAction(ModelMap model) {

        setModelMap(model, "Отчеты", ACTIVE);

        if (userAddresses == null || userAddresses.size() == 0) {
            security.getMessages().add("Для работы с разделом у вас должен быть настроен хотя бы один адрес", Message.TYPE_WARNING);
            return "redirect:/panel";
        }

        return "panel/report/list";
    }

    @RequestMapping(value = "/ajax/reports/pie/get", method = RequestMethod.POST)
    public @ResponseBody Response getPieReportAjax(
            @RequestParam(value = "address", required = false) Integer addressId,
            @RequestParam(value = "date-from", required = false) String dateFromStr,
            @RequestParam(value = "date-to", required = false) String dateToStr,
            @RequestParam(value = "by-meter-type", required = false) Boolean chByMeterType) {

        if (addressId == null || addressId == 0) {
            return null;
        }

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            return null;
        }

        Date dateFrom = convertDateFromString(dateFromStr);
        Date dateTo = convertDateFromString(dateToStr);
        if (dateFrom == null || dateTo == null) {
            return null;
        }

        List<Meter> meters = new MeterDAOImpl().list(address);
        if (meters == null || meters.size() == 0) {
            return null;
        }

        Double total = 0.0;
        HashMap<Meter, Double> list = new HashMap<Meter, Double>();
        for (Meter meter : meters) {
            Double value = new MeterService(meter).getTotalByPeriod(dateFrom, dateTo);
            list.put(meter, value);
            total += value;
        }

        if (total == 0.0) {
            return null;
        }

        // рассчитываем процентные доли
        for (Meter meter : list.keySet()) {
            Double value = list.get(meter);
            value = value / total;
            list.put(meter, value);
        }

        // формируем данные для отчета
        List<DataSeriesItem> series = new ArrayList<DataSeriesItem>();
        chByMeterType = chByMeterType == null ? false : chByMeterType;
        if (chByMeterType) {
            // показать доли по типам счетчиков
            HashMap<MeterType, Double> typeList = new HashMap<MeterType, Double>();
            for (Meter meter : list.keySet()) {
                Double value = list.get(meter);
                if (typeList.get(meter.getType()) != null) {
                    value += typeList.get(meter.getType());
                }
                typeList.put(meter.getType(), value);
            }

            for (MeterType type : typeList.keySet()) {
                series.add(new DataSeriesItem(type.getName(), typeList.get(type)));
            }
        } else {
            for (Meter meter : list.keySet()) {
                series.add(new DataSeriesItem(meter.getTitle(), list.get(meter)));
            }
        }

        return new Response(0, "OK", series);
    }

    @RequestMapping(value = "/ajax/reports/column/get", method = RequestMethod.POST)
    public @ResponseBody Response getColumnReportAjax(
            @RequestParam(value = "address", required = false) Integer addressId,
            @RequestParam(value = "meter", required = false) Integer meterId,
            @RequestParam(value = "year", required = false) Integer year,
            @RequestParam(value = "by-cost", required = false) Boolean chByCost) {

        if (addressId == null || addressId == 0) {
            return null;
        }

        Address address = security.getUser().getAddress(addressId);
        if (address == null) {
            return null;
        }

        if (meterId == null || meterId == 0) {
            return null;
        }

        Meter meter = address.getMeter(meterId);
        if (meter == null) {
            return null;
        }

        List<MeterHistoryItem> history = meter.getHistory();
        if (history == null || history.size() < 2) {
            return null;
        }

        // формируем данные для отчета
        Map<String, Object> result = new HashMap<String, Object>();
        chByCost = chByCost == null ? false : chByCost;
        if (chByCost) {
            result.put("title", "Стоимость, руб.");
            result.put("pointFormat", "{series.name}: <b>{point.y:.2f} руб.</b>");
        } else {
            String unit = meter.getType().getUnit().replaceAll("&bull;", ".");
            result.put("title", "Значение, " + unit);
            result.put("pointFormat", String.format("{series.name}: <b>{point.y:.2f} %s</b>", unit));
        }

        List<Double> data = new ArrayList<Double>();
        for (int i = 0; i < 12; i++) {
            data.add(0.0);
        }
        DataSeriesListItem seriesItem = new DataSeriesListItem(meter.getTitle(), data);

        // проставлям данные по ежемесячным тратам
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, 0, 1);

        for (int i=0; i<12; i++) {
            calendar.set(Calendar.MONTH, i);
            MeterService meterService = new MeterService(meter);
            if (meterService.beforeDate(DateUtils.getLastDayInMonth(calendar.getTime())) && meterService.afterDate(calendar.getTime())) {
                double value;
                if (chByCost) {
                    value = meterService.getTotalByMonth(calendar.getTime());
                } else {
                    value = meterService.getDiffValueByMonth(calendar.getTime());
                }
                seriesItem.getData().set(i, value);
            }
        }
        List<DataSeriesListItem> series = new ArrayList<DataSeriesListItem>();
        series.add(seriesItem);
        result.put("series", series);

        return new Response(0, "OK", result);
    }

    private Date convertDateFromString(String dateStr) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        if (dateStr != null && dateStr.trim().length() != 0) {
            try {
                return sdf.parse(dateStr.trim());
            } catch (ParseException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    class DataSeriesItem {

        private String name;
        private Double y;

        public DataSeriesItem(String name, Double y) {

            this.name = name;
            this.y = y;
        }

        public String getName() {

            return name;
        }

        public Double getY() {

            return y;
        }
    }

    class DataSeriesListItem {

        private String name;
        private List<Double> data;

        public DataSeriesListItem(String name, List<Double> data) {

            this.name = name;
            this.data = data;
        }

        public String getName() {

            return name;
        }

        public List<Double> getData() {

            return data;
        }
    }
}