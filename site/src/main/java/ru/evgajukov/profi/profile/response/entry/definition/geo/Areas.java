package ru.evgajukov.profi.profile.response.entry.definition.geo;

/**
 * Created by timur on 16.06.17.
 */
public class Areas {

    private Leave leave;
    private String producer;
    private Location location;

    public Leave getLeave() {

        return leave;
    }

    public void setLeave(Leave leave) {

        this.leave = leave;
    }

    public String getProducer() {

        return producer;
    }

    public void setProducer(String producer) {

        this.producer = producer;
    }

    public Location getLocation() {

        return location;
    }

    public void setLocation(Location location) {

        this.location = location;
    }
}