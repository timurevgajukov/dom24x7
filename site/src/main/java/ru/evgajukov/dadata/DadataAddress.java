package ru.evgajukov.dadata;

/**
 * Created by Тимур on 23.03.2016.
 *
 * Модель адреса для сервиса dadata.ru
 */
public class DadataAddress {

    private String postal_code;          // Индекс
    private String country;              // Страна
    private String region_fias_id;       // Код ФИАС региона
    private String region_kladr_id;      // Код КЛАДР региона
    private String region_with_type;     // Регион с типом
    private String region_type;          // Тип региона (сокращенный)
    private String region_type_full;     // Тип региона
    private String region;               // Регион
    private String area_fias_id;         // Код ФИАС района в регионе
    private String area_kladr_id;        // Код КЛАДР района в регионе
    private String area_with_type;       // Район в регионе с типом
    private String area_type;            // Тип района в регионе (сокращенный)
    private String area_type_full;       // Тип района в регионе
    private String area;                 // Район в регионе
    private String city_fias_id;         // Код ФИАС города
    private String city_kladr_id;        // Код КЛАДР города
    private String city_with_type;       // Город с типом
    private String city_type;            // Тип города (сокращенный)
    private String city_type_full;       // Тип города
    private String city;                 // Город
    private String city_area;            // Административный округ (только для Москвы)
    private String city_district;        // Район города
    private String settlement_fias_id;   // Код ФИАС нас. пункта
    private String settlement_kladr_id;  // Код КЛАДР нас. пункта
    private String settlement_with_type; // Населенный пункт с типом
    private String settlement_type;      // Тип населенного пункта (сокращенный)
    private String settlement_type_full; // Тип населенного пункта
    private String settlement;           // Населенный пункт
    private String street_fias_id;       // Код ФИАС улицы
    private String street_kladr_id;      // Код КЛАДР улицы
    private String street_with_type;     // Улица с типом
    private String street_type;          // Тип улицы (сокращенный)
    private String street_type_full;     // Тип улицы
    private String street;               // Улица
    private String house_fias_id;        // Код ФИАС дома
    private String house_kladr_id;       // Код КЛАДР дома
    private String house_type;           // Тип дома (сокращенный)
    private String house_type_full;      // Тип дома
    private String house;                // Дом
    private String block_type;           // Тип корпуса/строения (сокращенный)
    private String block_type_full;      // Тип корпуса/строения
    private String block;                // Корпус/строение
    private String flat_type;            // Тип квартиры (сокращенный)
    private String flat_type_full;       // Тип квартиры
    private String flat;                 // Квартира
    private String flat_area;            // Площадь квартиры
    private String square_meter_price;   // Рыночная стоимость квадратного метра
    private String flat_price;           // Рыночная стоимость квартиры
    private String postal_box;           // Абонентский ящик
    private String fias_id;              // Код ФИАС:
                                         //      HOUSE.HOUSEGUID, если дом найден в ФИАС по точному совпадению;
                                         //      HOUSEINT.INTGUID, если дом найден в ФИАС как часть интервала;
                                         //      ADDROBJ.AOGUID в противном случае.
    private Integer fias_level;          // Уровень детализации, до которого адрес найден в ФИАС:
                                         //      0 — страна;
                                         //      1 — регион;
                                         //      3 — район;
                                         //      4 — город;
                                         //      5 — район города;
                                         //      6 — населенный пункт;
                                         //      7 — улица;
                                         //      8 — дом;
                                         //      90 — доп. территория;
                                         //      91 — улица в доп. территории;
                                         //      -1 — иностранный или пустой.
    private String kladr_id;             // Код КЛАДР
    private Integer capital_marker;      // Является ли город центром:
                                         //      1 — центр района (Московская обл, Одинцовский р-н, г Одинцово)
                                         //      2 — центр региона (Новосибирская обл, г Новосибирск);
                                         //      3 — центр района и региона;
                                         //      0 — ни то, ни другое (Московская обл, г Балашиха).
    private String okato;                // Код ОКАТО
    private String oktmo;                // Код ОКТМО
    private String tax_office;           // Код ИФНС для физических лиц
    private String tax_office_legal;     // Код ИФНС для организаций
    private String timezone;             // Часовой пояс
    private Double geo_lat;              // Координаты: широта
    private Double geo_lon;              // Координаты: долгота
    private String beltway_hit;          // Внутри кольцевой?
    private Integer beltway_distance;    // Расстояние от кольцевой в км.
    private Integer qc_geo;              // Код точности координат:
                                         //      0 - Точные координаты
                                         //      1 - Ближайший дом
                                         //      2 - Улица
                                         //      3 - Населенный пункт
                                         //      4 - Город
                                         //      5 - Координаты не определены
    private Integer qc_complete;         // Код полноты
    private Integer qc_house;            // Код проверки дома
    private Integer qc;                  // Код проверки
    private String unparsed_parts;       // Нераспознанная часть адреса

    public String getPostal_code() {

        return postal_code;
    }

    public void setPostal_code(String postal_code) {

        this.postal_code = postal_code;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }

    public String getRegion_fias_id() {

        return region_fias_id;
    }

    public void setRegion_fias_id(String region_fias_id) {

        this.region_fias_id = region_fias_id;
    }

    public String getRegion_kladr_id() {

        return region_kladr_id;
    }

    public void setRegion_kladr_id(String region_kladr_id) {

        this.region_kladr_id = region_kladr_id;
    }

    public String getRegion_with_type() {

        return region_with_type;
    }

    public void setRegion_with_type(String region_with_type) {

        this.region_with_type = region_with_type;
    }

    public String getRegion_type() {

        return region_type;
    }

    public void setRegion_type(String region_type) {

        this.region_type = region_type;
    }

    public String getRegion_type_full() {

        return region_type_full;
    }

    public void setRegion_type_full(String region_type_full) {

        this.region_type_full = region_type_full;
    }

    public String getRegion() {

        return region;
    }

    public void setRegion(String region) {

        this.region = region;
    }

    public String getArea_fias_id() {

        return area_fias_id;
    }

    public void setArea_fias_id(String area_fias_id) {

        this.area_fias_id = area_fias_id;
    }

    public String getArea_kladr_id() {

        return area_kladr_id;
    }

    public void setArea_kladr_id(String area_kladr_id) {

        this.area_kladr_id = area_kladr_id;
    }

    public String getArea_with_type() {

        return area_with_type;
    }

    public void setArea_with_type(String area_with_type) {

        this.area_with_type = area_with_type;
    }

    public String getArea_type() {

        return area_type;
    }

    public void setArea_type(String area_type) {

        this.area_type = area_type;
    }

    public String getArea_type_full() {

        return area_type_full;
    }

    public void setArea_type_full(String area_type_full) {

        this.area_type_full = area_type_full;
    }

    public String getArea() {

        return area;
    }

    public void setArea(String area) {

        this.area = area;
    }

    public String getCity_fias_id() {

        return city_fias_id;
    }

    public void setCity_fias_id(String city_fias_id) {

        this.city_fias_id = city_fias_id;
    }

    public String getCity_kladr_id() {

        return city_kladr_id;
    }

    public void setCity_kladr_id(String city_kladr_id) {

        this.city_kladr_id = city_kladr_id;
    }

    public String getCity_with_type() {

        return city_with_type;
    }

    public void setCity_with_type(String city_with_type) {

        this.city_with_type = city_with_type;
    }

    public String getCity_type() {

        return city_type;
    }

    public void setCity_type(String city_type) {

        this.city_type = city_type;
    }

    public String getCity_type_full() {

        return city_type_full;
    }

    public void setCity_type_full(String city_type_full) {

        this.city_type_full = city_type_full;
    }

    public String getCity() {

        return city;
    }

    public void setCity(String city) {

        this.city = city;
    }

    public String getCity_area() {

        return city_area;
    }

    public void setCity_area(String city_area) {

        this.city_area = city_area;
    }

    public String getCity_district() {

        return city_district;
    }

    public void setCity_district(String city_district) {

        this.city_district = city_district;
    }

    public String getSettlement_fias_id() {

        return settlement_fias_id;
    }

    public void setSettlement_fias_id(String settlement_fias_id) {

        this.settlement_fias_id = settlement_fias_id;
    }

    public String getSettlement_kladr_id() {

        return settlement_kladr_id;
    }

    public void setSettlement_kladr_id(String settlement_kladr_id) {

        this.settlement_kladr_id = settlement_kladr_id;
    }

    public String getSettlement_with_type() {

        return settlement_with_type;
    }

    public void setSettlement_with_type(String settlement_with_type) {

        this.settlement_with_type = settlement_with_type;
    }

    public String getSettlement_type() {

        return settlement_type;
    }

    public void setSettlement_type(String settlement_type) {

        this.settlement_type = settlement_type;
    }

    public String getSettlement_type_full() {

        return settlement_type_full;
    }

    public void setSettlement_type_full(String settlement_type_full) {

        this.settlement_type_full = settlement_type_full;
    }

    public String getSettlement() {

        return settlement;
    }

    public void setSettlement(String settlement) {

        this.settlement = settlement;
    }

    public String getStreet_fias_id() {

        return street_fias_id;
    }

    public void setStreet_fias_id(String street_fias_id) {

        this.street_fias_id = street_fias_id;
    }

    public String getStreet_kladr_id() {

        return street_kladr_id;
    }

    public void setStreet_kladr_id(String street_kladr_id) {

        this.street_kladr_id = street_kladr_id;
    }

    public String getStreet_with_type() {

        return street_with_type;
    }

    public void setStreet_with_type(String street_with_type) {

        this.street_with_type = street_with_type;
    }

    public String getStreet_type() {

        return street_type;
    }

    public void setStreet_type(String street_type) {

        this.street_type = street_type;
    }

    public String getStreet_type_full() {

        return street_type_full;
    }

    public void setStreet_type_full(String street_type_full) {

        this.street_type_full = street_type_full;
    }

    public String getStreet() {

        return street;
    }

    public void setStreet(String street) {

        this.street = street;
    }

    public String getHouse_fias_id() {

        return house_fias_id;
    }

    public void setHouse_fias_id(String house_fias_id) {

        this.house_fias_id = house_fias_id;
    }

    public String getHouse_kladr_id() {

        return house_kladr_id;
    }

    public void setHouse_kladr_id(String house_kladr_id) {

        this.house_kladr_id = house_kladr_id;
    }

    public String getHouse_type() {

        return house_type;
    }

    public void setHouse_type(String house_type) {

        this.house_type = house_type;
    }

    public String getHouse_type_full() {

        return house_type_full;
    }

    public void setHouse_type_full(String house_type_full) {

        this.house_type_full = house_type_full;
    }

    public String getHouse() {

        return house;
    }

    public void setHouse(String house) {

        this.house = house;
    }

    public String getBlock_type() {

        return block_type;
    }

    public void setBlock_type(String block_type) {

        this.block_type = block_type;
    }

    public String getBlock_type_full() {

        return block_type_full;
    }

    public void setBlock_type_full(String block_type_full) {

        this.block_type_full = block_type_full;
    }

    public String getBlock() {

        return block;
    }

    public void setBlock(String block) {

        this.block = block;
    }

    public String getFlat_type() {

        return flat_type;
    }

    public void setFlat_type(String flat_type) {

        this.flat_type = flat_type;
    }

    public String getFlat_type_full() {

        return flat_type_full;
    }

    public void setFlat_type_full(String flat_type_full) {

        this.flat_type_full = flat_type_full;
    }

    public String getFlat() {

        return flat;
    }

    public void setFlat(String flat) {

        this.flat = flat;
    }

    public String getFlat_area() {

        return flat_area;
    }

    public void setFlat_area(String flat_area) {

        this.flat_area = flat_area;
    }

    public String getSquare_meter_price() {

        return square_meter_price;
    }

    public void setSquare_meter_price(String square_meter_price) {

        this.square_meter_price = square_meter_price;
    }

    public String getFlat_price() {

        return flat_price;
    }

    public void setFlat_price(String flat_price) {

        this.flat_price = flat_price;
    }

    public String getPostal_box() {

        return postal_box;
    }

    public void setPostal_box(String postal_box) {

        this.postal_box = postal_box;
    }

    public String getFias_id() {

        return fias_id;
    }

    public void setFias_id(String fias_id) {

        this.fias_id = fias_id;
    }

    public Integer getFias_level() {

        return fias_level;
    }

    public void setFias_level(Integer fias_level) {

        this.fias_level = fias_level;
    }

    public String getKladr_id() {

        return kladr_id;
    }

    public void setKladr_id(String kladr_id) {

        this.kladr_id = kladr_id;
    }

    public Integer getCapital_marker() {

        return capital_marker;
    }

    public void setCapital_marker(Integer capital_marker) {

        this.capital_marker = capital_marker;
    }

    public String getOkato() {

        return okato;
    }

    public void setOkato(String okato) {

        this.okato = okato;
    }

    public String getOktmo() {

        return oktmo;
    }

    public void setOktmo(String oktmo) {

        this.oktmo = oktmo;
    }

    public String getTax_office() {

        return tax_office;
    }

    public void setTax_office(String tax_office) {

        this.tax_office = tax_office;
    }

    public String getTax_office_legal() {

        return tax_office_legal;
    }

    public void setTax_office_legal(String tax_office_legal) {

        this.tax_office_legal = tax_office_legal;
    }

    public String getTimezone() {

        return timezone;
    }

    public void setTimezone(String timezone) {

        this.timezone = timezone;
    }

    public Double getGeo_lat() {

        return geo_lat;
    }

    public void setGeo_lat(Double geo_lat) {

        this.geo_lat = geo_lat;
    }

    public Double getGeo_lon() {

        return geo_lon;
    }

    public void setGeo_lon(Double geo_lon) {

        this.geo_lon = geo_lon;
    }

    public String getBeltway_hit() {

        return beltway_hit;
    }

    public void setBeltway_hit(String beltway_hit) {

        this.beltway_hit = beltway_hit;
    }

    public Integer getBeltway_distance() {

        return beltway_distance;
    }

    public void setBeltway_distance(Integer beltway_distance) {

        this.beltway_distance = beltway_distance;
    }

    public Integer getQc_geo() {

        return qc_geo;
    }

    public void setQc_geo(Integer qc_geo) {

        this.qc_geo = qc_geo;
    }

    public Integer getQc_complete() {

        return qc_complete;
    }

    public void setQc_complete(Integer qc_complete) {

        this.qc_complete = qc_complete;
    }

    public Integer getQc_house() {

        return qc_house;
    }

    public void setQc_house(Integer qc_house) {

        this.qc_house = qc_house;
    }

    public Integer getQc() {

        return qc;
    }

    public void setQc(Integer qc) {

        this.qc = qc;
    }

    public String getUnparsed_parts() {

        return unparsed_parts;
    }

    public void setUnparsed_parts(String unparsed_parts) {

        this.unparsed_parts = unparsed_parts;
    }
}