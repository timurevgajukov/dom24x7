package ru.dom24x7.site.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.dom24x7.site.BaseController;

/**
 * Created by timur on 18.05.17.
 */
@Controller
public class AdminPaymentController extends BaseController {

    @RequestMapping("/admin/payments")
    public String listAction(ModelMap model) {

        setModelMap(model, "Платежи");

        return "admin/payment/list";
    }
}