package ru.dom24x7.model.application;

import java.util.Date;
import java.util.UUID;

import ru.dom24x7.model.Model;

/**
 * Приложение для работы по API
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Application extends Model {
	
	private Date createDt;
	private String token;
	private String email;
	private String site;
	private String note;
	private boolean hold;
	
	public Application() {
		
		super();
	}
	
	public Application(int id, String name) {
		
		super(id, name);
	}
	
	public Date getCreateDt() {
		
		return createDt;
	}
	
	public void setCreateDt(Date dt) {
		
		this.createDt = dt;
	}
	
	public String getToken() {
		
		return token;
	}
	
	public void setToken(String token) {
		
		this.token = token;
	}
	
	public String generateToken() {
		
		UUID uuid = UUID.randomUUID();
		this.token = uuid.toString();
		
		return this.token;
	}
	
	public String getEmail() {
		
		return email;
	}
	
	public void setEmail(String email) {
		
		this.email = email;
	}
	
	public String getSite() {
		
		return site;
	}
	
	public void setSite(String site) {
		
		this.site = site;
	}
	
	public String getNote() {
		
		return note;
	}
	
	public void setNote(String note) {
		
		this.note = note;
	}
	
	public boolean isHold() {
		
		return hold;
	}
	
	public void setHold(boolean hold) {
		
		this.hold = hold;
	}

	public static String genCode() {

		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}