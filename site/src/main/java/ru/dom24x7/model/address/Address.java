package ru.dom24x7.model.address;

import ru.dom24x7.model.meter.Meter;
import ru.dom24x7.model.meter.dao.MeterDAOImpl;
import ru.dom24x7.model.reference.currency.Currency;
import ru.dom24x7.model.Model;
import ru.dom24x7.model.user.User;
import ru.dom24x7.service.meter.MeterService;
import ru.dom24x7.utils.StrUtils;
import ru.evgajukov.dadata.DadataAddress;

import java.util.List;

/**
 * Адрес жилища
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Address extends Model {
	
    private String title;
    private User user;
    private String address;
    private Currency currency;
    private DadataAddress info;

    public Address() {

        super();
    }

    public Address(int id) {

        super(id);
    }

    public String getTitle() {

        return title;
    }
    
    public String getShortTitle(int maxLen) {
    	
    	if (title != null && title.length() > 0) {
            if (title.length() < maxLen) {
            	return title;
            } else {
            	return title.substring(0, maxLen-3) + "...";
            }
        } else {
        	if (address.length() < maxLen) {
        		return address;
        	} else {
        		return "..." + address.substring(address.length()-maxLen-2, address.length()-1);
        	}
        }
    }

    public Address setTitle(String title) {

        this.title = title;

        return this;
    }
    
    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }
    
    public Currency getCurrency() {
    	
    	return currency;
    }
    
    public void setCurrency(Currency currency) {
    	
    	this.currency = currency;
    }

    public DadataAddress getInfo() {

        return info;
    }

    public void setInfo(DadataAddress info) {

        this.info = info;
    }

    @Override
    public String toString() {

        if (title != null && title.length() > 0) {
            return title;
        } else {
            return address;
        }
    }

    public Meter getMeter(int meterId) {

        List<Meter> meters = new MeterDAOImpl().list(this);

        if (meters == null || meters.size() == 0) {
            return null;
        }

        for (Meter meter : meters) {
            if (meterId == meter.getId()) {
                return meter;
            }
        }

        return null;
    }

    /**
     * Возвращает затраты по всем счетчикам за последний месяц
     *
     * @return
     */
    public String getTotalLastMonth() {

        List<Meter> meters = new MeterDAOImpl().list(this);

        if (meters == null || meters.size() == 0) {
            return "отсутствуют";
        }

        double totalLastMonth = 0.0;
        for (Meter meter : meters) {
            totalLastMonth += new MeterService(meter).getTotalLastMonth();
        }

        return StrUtils.getStrAmount(totalLastMonth, currency.getSymbol());
    }
    
    /**
     * Полностью копирует адрес без идентификатора и пользователя
     * 
     * @param address
     */
    public void copy(Address address) {
    	
    	this.title = address.title;
    	this.address = address.address;
    	this.currency = address.currency;
    }
}