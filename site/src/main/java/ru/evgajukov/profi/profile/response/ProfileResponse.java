package ru.evgajukov.profi.profile.response;

import ru.evgajukov.profi.profile.response.cloud.CloudsItem;
import ru.evgajukov.profi.profile.response.entry.Entry;

import java.util.List;

/**
 * Created by timur on 15.06.17.
 * Класс ответа со списком профилей
 */
public class ProfileResponse {

    public static final String API_NAME = "pagination";

    private Statistics statistics;
    private List<Entry> entries;
    private List<List<CloudsItem>> clouds;

    public Statistics getStatistics() {

        return statistics;
    }

    public void setStatistics(Statistics statistics) {

        this.statistics = statistics;
    }

    public List<Entry> getEntries() {

        return entries;
    }

    public void setEntries(List<Entry> entries) {

        this.entries = entries;
    }

    public List<List<CloudsItem>> getClouds() {

        return clouds;
    }

    public void setClouds(List<List<CloudsItem>> clouds) {

        this.clouds = clouds;
    }
}