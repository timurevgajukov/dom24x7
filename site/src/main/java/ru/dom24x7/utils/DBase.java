package ru.dom24x7.utils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by timur on 18.09.16.
 *
 * Работа с БД
 */
public class DBase {

    public static DataSource getDataSource() {

        try {
            InitialContext initContext= new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/Dom24x7DC");

            return ds;
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Connection getConnection() throws SQLException {

        DataSource ds = getDataSource();
        if (ds != null) {
            return ds.getConnection();
        }

        return null;
    }

    /**
     * Возвращает строку поиска для SQL-запроса с LIKE
     * @param search
     * @return
     */
    public static String getSearchForLike(String search) {

        if (search == null || search.trim().length() == 0) {
            return null;
        }

        return "%" + search.trim() + "%";
    }
}