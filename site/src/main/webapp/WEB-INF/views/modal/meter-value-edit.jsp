<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- EDIT METER HISTORY ITEM MODAL WINDOW: BEGIN -->
<div id="meter-value-edit-modal" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div id="meter-value-content" class="modal-content">
            <div class="modal-header">
                <h4 id="meter-value-edit-modal-title" class="modal-title">Показание счетчика</h4>
                <small id="meter-value-edit-modal-title-small"></small>
            </div>
            <form>
                <div class="modal-body">
                    <div id="modal-meter-item-address-block" class="form-group" style="display: none;">
                        <select id="modal-meter-item-address" class="form-control">
                            <c:if test="${userAddresses != null && userAddresses.size() != 0}">
                                <option value="0" disabled selected>Выберите адрес</option>
                                <c:forEach items="${userAddresses}" var="address">
                                    <option value="${address.id}" data-currency="${address.currency.symbol}">${address}</option>
                                </c:forEach>
                            </c:if>
                        </select>
                    </div>
                    <div id="modal-meter-item-meter-block" class="form-group" style="display: none;">
                        <select id="modal-meter-item-meter" class="form-control">
                            <option value="0" disabled selected>Предварительно выберите адрес</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="modal-meter-item-date" name="date" type="text" class="form-control exp-date"
                                       placeholder="Дата снятия показания" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <input id="modal-meter-item-value" name="value" class="form-control" type="text"
                                           placeholder="Показание счетчика" style="text-align: right;"/>
                                    <span id="modal-meter-type-unit" class="input-group-addon"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="item" name="item" />
                <div class="modal-footer">
                    <button id="btn-send-value" class="ladda-button btn btn-success" type="button" data-style="zoom-in">Сохранить</button>
                    <button data-dismiss="modal" class="btn btn-primary" type="button">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- EDIT METER HISTORY ITEM MODAL WINDOW: END -->

<script>
    $(document).ready(function () {
        var addressId = undefined;
        var meterId = undefined;
        var meterItemId = undefined;

        $('.exp-date').datetimepicker({
            format: 'DD.MM.YYYY',
            locale: 'ru'
        });
        
        // отображение формы для добавления нового показания счетчика с dashboard
        $('#btn-add-value-dashboard').click(function () {
            $('#modal-meter-item-address-block').show();
            $('#modal-meter-item-meter-block').show();

            addressId = undefined;
            meterId = undefined;
            meterItemId = undefined;
        });

        // отображение формы для добавления нового показания счетчика
        $('.btn-add-value').click(function () {
            $('#modal-meter-item-address-block').hide();
            $('#modal-meter-item-meter-block').hide();

            initModal({
                meterTitle: $(this).data('meter-title'),
                unit: $(this).data('meter-unit'),
                currency: $(this).data('currency'),
                withoutValue: $(this).data('meter-type-without-value')
            });

            addressId = $(this).data('address');
            meterId = $(this).data('meter');
            meterItemId = undefined;
        });

        // отображение формы для редактирования существующего показания счетчика
        $('.btn-edit-value').click(function () {
            $('#modal-meter-item-address-block').hide();
            $('#modal-meter-item-meter-block').hide();

            initModal({
                meterTitle: $(this).data('meter-title'),
                unit: $(this).data('meter-unit'),
                currency: $(this).data('currency'),
                withoutValue: $(this).data('meter-type-without-value')
            });

            addressId = $(this).data('address');
            meterId = $(this).data('meter');
            meterItemId = $(this).data('id');

            $.post('/panel/addresses/' + addressId + '/meters/' + meterId + '/history/' + meterItemId, function (item) {
                if (item == null) {
                    return;
                }

                $('#modal-meter-item-date').val(moment(item.date).format('DD.MM.YYYY'));
                $('#modal-meter-item-value').val(item.cost != 0 ? item.cost : item.value);
            });
        });

        // смена текущего адреса для показания счетчика
        $('#modal-meter-item-address').change(function () {
            addressId = $(this).val();
            meterId = undefined;

            // загружаем счетчики по адресу
            var metersListEl = $('#modal-meter-item-meter');
            metersListEl.empty();
            metersListEl.append('<option value="0" disabled selected>Выбранный адрес без счетчиков</option>');
            $.get('/ajax/addresses/' + addressId + '/meters', function (list) {
                if (!list || list.length  == 0) {
                    return;
                }

                metersListEl.empty();
                metersListEl.append('<option value="0" disabled selected>Выберите счетчик</option>');
                for (var i = 0; i < list.length; i++) {
                    var meter = list[i];
                    var item = '<option value="' + meter.id + '" data-title="' + meter.title + '" ' +
                        'data-unit="' + meter.type.unit + '" data-without-value="' + meter.type.withoutValue + '">' +
                        meter.title + '</option>';
                    metersListEl.append(item);
                }
            });
        });

        // смена текущего счетчика для снятия показания
        $('#modal-meter-item-meter').change(function () {
            meterId = $(this).val();

            initModal({
                meterTitle: $('#modal-meter-item-meter').find(':selected').data('title'),
                unit: $('#modal-meter-item-meter').find(':selected').data('unit'),
                currency: $('#modal-meter-item-address').find(':selected').data('currency'),
                withoutValue: $('#modal-meter-item-meter').find(':selected').data('without-value')
            });
        });

        // отправка показания счетчика на сервер
        var btnSendValue = $('#btn-send-value').ladda();
        btnSendValue.click(function () {
            var data = {
                date: $('#modal-meter-item-date').val(),
                value: $('#modal-meter-item-value').val()
            };

            if (!addressId) {
                showAlert('Необходимо указать адрес, по которому снимается показание счетчика', 'warning');
                $('#modal-meter-item-address').focus();
                return;
            }

            if (!meterId) {
                showAlert('Необходимо указать счетчик, с которого снимается показание', 'warning');
                $('#modal-meter-item-meter').focus();
                return;
            }

            if (data.date == null || data.date.trim().length == 0) {
                showAlert('Необходимо указать дату снятия показания', 'warning');
                $('#modal-meter-item-date').focus();
                return;
            }

            if (data.value == null || data.value.trim().length == 0) {
                showAlert('Необходимо указать показание счетчика', 'warning');
                $('#modal-meter-item-value').focus();
                return;
            }

            if (meterItemId !== undefined && meterItemId != null) {
                // редактирование существующего показания счетчика
                data.id = meterItemId;
            }

            btnSendValue.ladda('start');
            $.post('/panel/addresses/' + addressId + '/meters/' + meterId + '/history/save', data, function (result) {
                btnSendValue.ladda('stop');
                if (result == null) {
                    return;
                }

                if (result.code != 0) {
                    // имеются ошибки, выведем нотификации
                    var errors = result.body;
                    for (var i = 0; i < errors.length; i++) {
                        showAlert(errors[i].body, errors[i].type, '#meter-value-content')
                    }
                    return;
                }

                location.reload();
            }).error(function () {
                btnSendValue.ladda('stop');
            });
        });

        // инициализация вида модального окна
        function initModal(data) {
            if (data.withoutValue) {
                $('#meter-value-edit-modal-title').html('Расход по');
                $('#modal-meter-item-date').attr('placeholder', 'Дата оплаты');
                $('#modal-meter-item-value').attr('placeholder', 'Сумма оплаты');
                $('#modal-meter-type-unit').html(data.currency);
            } else {
                $('#meter-value-edit-modal-title').html('Показание счетчика');
                $('#modal-meter-item-date').attr('placeholder', 'Дата снятия показания');
                $('#modal-meter-item-value').attr('placeholder', 'Показание счетчика');
                $('#modal-meter-type-unit').html(data.unit);
            }

            $('#meter-value-edit-modal-title-small').html(data.meterTitle);
            $('#modal-meter-item-date').val('');
            $('#modal-meter-item-value').val('');
        }
    });
</script>