package ru.evgajukov.profi.profile.response.entry.definition.geo;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class City {

    private String id;
    private Name name;
    private List<Region> regions;
    private Boolean ttl;
    private Boolean all;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public Name getName() {

        return name;
    }

    public void setName(Name name) {

        this.name = name;
    }

    public List<Region> getRegions() {

        return regions;
    }

    public void setRegions(List<Region> regions) {

        this.regions = regions;
    }

    public Boolean isTtl() {

        return ttl;
    }

    public void setTtl(Boolean ttl) {

        this.ttl = ttl;
    }

    public Boolean isAll() {

        return all;
    }

    public void setAll(Boolean all) {

        this.all = all;
    }

    @Override
    public String toString() {

        return id;
    }
}
