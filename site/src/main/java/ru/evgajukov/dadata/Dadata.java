package ru.evgajukov.dadata;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Класс для работы с сервисом dadata.ru
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Dadata {

	public static final String TYPE_ADDRESS = "address";
	public static final String TYPE_PHONE = "phone";
	public static final String TYPE_PASSPORT = "passport";
	public static final String TYPE_NAME = "name";
	public static final String TYPE_EMAIL = "email";
	public static final String TYPE_BIRTHDAY = "birthday";
	public static final String TYPE_VEHICLE = "vehicle";

	private static final String DADATA_URL = "https://dadata.ru/api/v2/clean/";
	
	private static HashMap<String, String> properties = new HashMap<String, String>();
	
	private List<String> data;
	private String type;

	{
		data = new ArrayList<String>();
	}
	
	public Dadata() {
		
		properties.put("Content-Type", "application/json");
	}
	
	public Dadata(String apiKey, String secretKey) {
		
		this();
		
		setApiKey(apiKey);
		setSecretKey(secretKey);
	}
	
	public Dadata setApiKey(String apiKey) {
		
		properties.put("Authorization", String.format("Token %s", apiKey));
		
		return this;
	}
	
	public Dadata setSecretKey(String secretKey) {
		
		properties.put("X-Secret", secretKey);
		
		return this;
	}
	
	public Dadata addData(String data) {
		
		this.data.add(data);
		
		return this;
	}

	public String getType() {

		return type;
	}

	public Dadata setType(String type) {

		this.type = type;

		return this;
	}

	/**
	 * Запрашиваем данные с сервера
	 */
	public String get() {

		Gson gson = new Gson();
		String request = gson.toJson(data);
		
		// отправляем запрос на сервер и получаем ответ
		try {
			String response = Net.getInstance().send(DADATA_URL + type, 5000, request, properties);
			System.out.println(response);
			
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		
		System.out.println("Start test");
		
		Dadata dadata = new Dadata()
				.setApiKey("bea5b313ae276907f1d76cc1c2ac93a9902e11af")
				.setSecretKey("5a720bfcd74c746cd1c4cd2dff08a62b5c1d40f0")
				.setType(Dadata.TYPE_NAME)
				.addData("тимур хасанбиеви евгожуков");

		String result = dadata.get();

		Type listType = new TypeToken<List<Name>>(){}.getType();
		List<Name> addresses = new Gson().fromJson(result, listType);

		System.out.println(addresses.get(0).getResult());
		System.out.println("Finish test");
	}
}
