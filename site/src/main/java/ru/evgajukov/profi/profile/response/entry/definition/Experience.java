package ru.evgajukov.profi.profile.response.entry.definition;

import java.util.List;

/**
 * Created by timur on 16.06.17.
 */
public class Experience {

    private List<Common> common; // опыт работы частного специалиста

    public List<Common> getCommon() {

        return common;
    }

    public void setCommon(List<Common> common) {

        this.common = common;
    }
}