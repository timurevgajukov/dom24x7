package ru.dom24x7.model.address.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.cache.Countries;
import ru.dom24x7.cache.Currencies;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.company.Company;
import ru.dom24x7.model.reference.country.Country;
import ru.dom24x7.model.reference.currency.Currency;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.address.dao.interfaces.AddressDAO;
import ru.evgajukov.dadata.DadataAddress;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 18.09.16.
 *
 * Реализация интерфейса для работы с адресами жилищ пользователей
 */
public class AddressDAOImpl extends ModelDAOImpl implements AddressDAO {

    private static final String QUERY_LIST = "{ call addresses_list_all(?,?,?) }";
    private static final String QUERY_LIST_BY_USER = "{ call addresses_list(?) }";
    private static final String QUERY_LIST_BY_COMPANY = "{ call addresses_list_by_company(?) }";
    private static final String QUERY_SAVE = "{ call addresses_save(?,?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call addresses_delete(?) }";
    private static final String QUERY_HOLD = "{ call addresses_hold(?) }";
    private static final String QUERY_COUNT = "{ call addresses_count(?) }";

    private static final String QUERY_ADDRESS_INFO_SAVE = "{ call addresses_dadata_save(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
    private static final String QUERY_ADDRESS_INFO_DELETE_HANGING = "{ call addresses_dadata_delete_hanging() }";

    private static final String QUERY_INFO_GET = "{ call addresses_dadata_get(?) }";

    @Override
    public List<Address> list(int offset, int count, String search) {

        if (search == null || search.trim().length() == 0) {
            search = null;
        } else {
            search = "%" + search + "%";
        }

        final Currencies currencies = Currencies.getInstance();

        List<Address> list = jdbcTemplate.query(QUERY_LIST, new Object[]{offset, count, search}, new RowMapper<Address>() {

            @Override
            public Address mapRow(ResultSet resultSet, int i) throws SQLException {

                Address address = load(resultSet);
                address.setCurrency(currencies.get(address.getCurrency().getId()));

                return address;
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Address> list(User user) {

        final Currencies currencies = Currencies.getInstance();

        List<Address> list = jdbcTemplate.query(QUERY_LIST_BY_USER, new RowMapper<Address>() {

            @Override
            public Address mapRow(ResultSet resultSet, int i) throws SQLException {

                Address address = load(resultSet);
                address.setCurrency(currencies.get(address.getCurrency().getId()));

                return address;
            }
        }, user.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public List<Address> list(Company company) {

        final Currencies currencies = Currencies.getInstance();

        List<Address> list = jdbcTemplate.query(QUERY_LIST_BY_COMPANY, new RowMapper<Address>() {
            @Override
            public Address mapRow(ResultSet resultSet, int i) throws SQLException {

                Address address = load(resultSet);
                address.setCurrency(currencies.get(address.getCurrency().getId()));

                return address;
            }
        }, company.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public int count(String search) {

        if (search == null || search.trim().length() == 0) {
            search = null;
        } else {
            search = "%" + search + "%";
        }

        return jdbcTemplate.queryForObject(QUERY_COUNT, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, search);
    }

    @Override
    public boolean save(Address address) {

        // сохраняем новую расширенную информацию
        DadataAddress info = address.getInfo();
        Integer addressDadataId = null;
        if (info != null) {
            addressDadataId = jdbcTemplate.queryForObject(QUERY_ADDRESS_INFO_SAVE,
                    new Object[]{
                            info.getPostal_code(),
                            info.getCountry(),
                            info.getRegion_fias_id(),
                            info.getRegion_kladr_id(),
                            info.getRegion_with_type(),
                            info.getRegion_type(),
                            info.getRegion_type_full(),
                            info.getRegion(),
                            info.getArea_fias_id(),
                            info.getArea_kladr_id(),
                            info.getArea_with_type(),
                            info.getArea_type(),
                            info.getArea_type_full(),
                            info.getArea(),
                            info.getCity_fias_id(),
                            info.getCity_kladr_id(),
                            info.getCity_with_type(),
                            info.getCity_type(),
                            info.getCity_type_full(),
                            info.getCity(),
                            info.getCity_area(),
                            info.getCity_district(),
                            info.getSettlement_fias_id(),
                            info.getSettlement_kladr_id(),
                            info.getSettlement_with_type(),
                            info.getSettlement_type(),
                            info.getSettlement_type_full(),
                            info.getSettlement(),
                            info.getStreet_fias_id(),
                            info.getStreet_kladr_id(),
                            info.getStreet_with_type(),
                            info.getStreet_type(),
                            info.getStreet_type_full(),
                            info.getStreet(),
                            info.getHouse_fias_id(),
                            info.getHouse_kladr_id(),
                            info.getHouse_type(),
                            info.getHouse_type_full(),
                            info.getHouse(),
                            info.getBlock_type(),
                            info.getBlock_type_full(),
                            info.getBlock(),
                            info.getFlat_type(),
                            info.getFlat_type_full(),
                            info.getFlat(),
                            info.getFlat_area(),
                            info.getSquare_meter_price(),
                            info.getFlat_price(),
                            info.getPostal_box(),
                            info.getFias_id(),
                            info.getFias_level(),
                            info.getKladr_id(),
                            info.getCapital_marker(),
                            info.getOkato(),
                            info.getOktmo(),
                            info.getTax_office(),
                            info.getTax_office_legal(),
                            info.getTimezone(),
                            info.getGeo_lat(),
                            info.getGeo_lon(),
                            info.getBeltway_hit(),
                            info.getBeltway_distance(),
                            info.getQc_geo(),
                            info.getQc_complete(),
                            info.getQc_house(),
                            info.getQc(),
                            info.getUnparsed_parts()
                    }, new RowMapper<Integer>() {

                        @Override
                        public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                            return resultSet.getInt("id");
                        }
                    });
        }

        // сохраняем сам адрес
        Address newAddress = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        address.getId(),
                        address.getTitle() == null || address.getTitle().length() == 0 ? null : address.getTitle(),
                        address.getUser().getId(),
                        address.getAddress(),
                        address.getCurrency() == null ? Currency.DEFAULT_CURRENCY_ID : address.getCurrency().getId(),
                        addressDadataId
                }, new RowMapper<Address>() {

                    @Override
                    public Address mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });
        address.setId(newAddress.getId());

        // очищаем подвисшие данные
        jdbcTemplate.update(QUERY_ADDRESS_INFO_DELETE_HANGING);

        return true;
    }

    @Override
    public boolean delete(Address address) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{ address.getId() }) != 0;
    }

    @Override
    public boolean hold(Address address) {

        return jdbcTemplate.update(QUERY_HOLD, new Object[]{ address.getId() }) != 0;
    }

    @Override
    public DadataAddress info(Address address) {

        List<DadataAddress> list = jdbcTemplate.query(QUERY_INFO_GET, new RowMapper<DadataAddress>() {

            @Override
            public DadataAddress mapRow(ResultSet resultSet, int i) throws SQLException {

                return loadInfo(resultSet);
            }
        }, address.getId());

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    private Address load(ResultSet rs) throws SQLException {

        Address address = new Address(rs.getInt("id"));
        address.setUser(new User(rs.getInt("user_id")));
        address.setTitle(rs.getString("title"));
        address.setAddress(rs.getString("address"));
        address.setCurrency(new Currency(rs.getInt("currency_id")));
        address.setInfo(info(address));

        return address;
    }

    private DadataAddress loadInfo(ResultSet rs) throws SQLException {

        DadataAddress info = new DadataAddress();
        info.setPostal_code(rs.getString("postal_code"));
        info.setCountry(rs.getString("country"));
        info.setRegion_fias_id(rs.getString("region_fias_id"));
        info.setRegion_kladr_id(rs.getString("region_kladr_id"));
        info.setRegion_with_type(rs.getString("region_with_type"));
        info.setRegion_type(rs.getString("region_type"));
        info.setRegion_type_full(rs.getString("region_type_full"));
        info.setRegion(rs.getString("region"));
        info.setArea_fias_id(rs.getString("area_fias_id"));
        info.setArea_kladr_id(rs.getString("area_kladr_id"));
        info.setArea_with_type(rs.getString("area_with_type"));
        info.setArea_type(rs.getString("area_type"));
        info.setArea_type_full(rs.getString("area_type_full"));
        info.setArea(rs.getString("area"));
        info.setCity_fias_id(rs.getString("city_fias_id"));
        info.setCity_kladr_id(rs.getString("city_kladr_id"));
        info.setCity_with_type(rs.getString("city_with_type"));
        info.setCity_type(rs.getString("city_type"));
        info.setCity_type_full(rs.getString("city_type_full"));
        info.setCity(rs.getString("city"));
        info.setCity_area(rs.getString("city_area"));
        info.setCity_district(rs.getString("city_district"));
        info.setSettlement_fias_id(rs.getString("settlement_fias_id"));
        info.setSettlement_kladr_id(rs.getString("settlement_kladr_id"));
        info.setSettlement_with_type(rs.getString("settlement_with_type"));
        info.setSettlement_type(rs.getString("settlement_type"));
        info.setSettlement_type_full(rs.getString("settlement_type_full"));
        info.setSettlement(rs.getString("settlement"));
        info.setStreet_fias_id(rs.getString("street_fias_id"));
        info.setStreet_kladr_id(rs.getString("street_kladr_id"));
        info.setStreet_with_type(rs.getString("street_with_type"));
        info.setStreet_type(rs.getString("street_type"));
        info.setStreet_type_full(rs.getString("street_type_full"));
        info.setStreet(rs.getString("street"));
        info.setHouse_fias_id(rs.getString("house_fias_id"));
        info.setHouse_kladr_id(rs.getString("house_kladr_id"));
        info.setHouse_type(rs.getString("house_type"));
        info.setHouse_type_full(rs.getString("house_type_full"));
        info.setHouse(rs.getString("house"));
        info.setBlock_type(rs.getString("block_type"));
        info.setBlock_type_full(rs.getString("block_type_full"));
        info.setBlock(rs.getString("block"));
        info.setFlat_type(rs.getString("flat_type"));
        info.setFlat_type_full(rs.getString("flat_type_full"));
        info.setFlat(rs.getString("flat"));
        info.setFlat_area(rs.getString("flat_area"));
        info.setSquare_meter_price(rs.getString("square_meter_price"));
        info.setFlat_price(rs.getString("flat_price"));
        info.setPostal_box(rs.getString("postal_box"));
        info.setFias_id(rs.getString("fias_id"));
        info.setFias_level(rs.getInt("fias_level"));
        info.setKladr_id(rs.getString("kladr_id"));
        info.setCapital_marker(rs.getInt("capital_marker"));
        info.setOkato(rs.getString("okato"));
        info.setOktmo(rs.getString("oktmo"));
        info.setTax_office(rs.getString("tax_office"));
        info.setTax_office_legal(rs.getString("tax_office_legal"));
        info.setTimezone(rs.getString("timezone"));
        info.setGeo_lat(rs.getDouble("geo_lat"));
        info.setGeo_lon(rs.getDouble("geo_lon"));
        info.setBeltway_hit(rs.getString("beltway_hit"));
        info.setBeltway_distance(rs.getInt("beltway_distance"));
        info.setQc_geo(rs.getInt("qc_geo"));
        info.setQc_complete(rs.getInt("qc_complete"));
        info.setQc_house(rs.getInt("qc_house"));
        info.setQc(rs.getInt("qc"));
        info.setUnparsed_parts(rs.getString("unparsed_parts"));

        return info;
    }
}