package ru.evgajukov.profi;

import ru.evgajukov.profi.dictionary.request.DictionaryRequest;
import ru.evgajukov.profi.dictionary.response.DictionaryResponse;
import ru.evgajukov.profi.profile.request.*;
import ru.evgajukov.profi.profile.request.filter.Filter;
import ru.evgajukov.profi.profile.request.filter.Model;
import ru.evgajukov.profi.profile.response.ProfileResponse;

import java.util.List;

/**
 * Created by timur on 14.06.17.
 * Тестирование запросов к API Profi.ru
 */
public class Test {

    public static final String API_CLIENT = "dom24x7ru";
    public static final String API_KEY = "ODFjMjVmOTY4YzM2NDczZGI5OWI2NjFjYWY1ZmE3NTAxMzU1Njk1OTIxNzg0Njhm";

    public static void main(String[] arguments) {
        System.out.println("Тестирование запросов к API Profi.ru");

        Profi profi = Profi.getInstance()
                .setApiClient(Test.API_CLIENT)
                .setApiKey(Test.API_KEY);

        // получение справочников
        // DictionaryRequest dicData = new DictionaryRequest().add(DictionaryRequest.DIC_MMETROS).add(DictionaryRequest.DIC_PSERVICES);
        // DictionaryResponse dictionaryResponse = profi.get(dicData);

        // получение списка профилей
        Filter filter = new Filter();
        filter.addModel(new Model().setModel("provider.specialist"));
        ProfileRequest profData = new ProfileRequest();
        profData.setFilter(filter);
        profData.setBound(new Bound().setFrom(0).setCount(10).setScope("profile.mini"));
        profData.setClient(new Client().setIp("127.0.0.99").setSid("mstr-sid-123"));
        profData.addInbound(new InBound().setDomain("mstr"));
        List<Profile> profiles = profi.get(profData);

        System.out.println("Вернул профилей: " + profiles.size());

        System.out.println("Завершили тестирование запросов к API Profi.ru");
    }
}