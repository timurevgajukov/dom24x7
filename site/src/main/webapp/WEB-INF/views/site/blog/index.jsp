<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${category != null}">
    <h1>${category.name}</h1>
    <hr />
</c:if>

<div id="posts" class="row masonry" data-columns></div>
<div id="more-posts"></div>

<script type="text/html" id="tmpl-post-without-photo">
    <div class="item">
        <div class="hpanel blog-box">
            <div class="panel-heading">
                <div class="media clearfix">
                    <a class="pull-left">
                        <img src="/resources/images/logo128x128.png" alt="Dom24x7">
                    </a>
                    <div class="media-body text-right">
                        <small class="text-muted">{{=it.published}}</small><br />
                        <small class="text-muted">{{=it.category}}</small>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <a href="{{=it.url}}"><h4>{{=it.title}}</h4></a>
                {{=it.text}}{{=it.more}}
            </div>
            <div class="panel-footer">
                <i class="fa fa-eye"></i> {{=it.views}} просмотров
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="tmpl-post-with-photo">
    <div class="item">
        <div class="hpanel blog-box">
            <div class="panel-heading">
                <div class="media clearfix">
                    <a class="pull-left">
                        <img src="/resources/images/logo128x128.png" alt="Dom24x7">
                    </a>
                    <div class="media-body text-right">
                        <small class="text-muted">{{=it.published}}</small><br />
                        <small class="text-muted">{{=it.category}}</small>
                    </div>
                </div>
            </div>
            <div class="panel-image">
                <img class="img-responsive" src="/blog/images/{{=it.photo.id}}/{{=it.photo.fileName}}" alt="">
                <div class="title">
                    <a href="{{=it.url}}"><h4>{{=it.title}}</h4></a>
                </div>
            </div>
            <div class="panel-body">{{=it.text}}{{=it.more}}</div>
            <div class="panel-footer">
                <i class="fa fa-eye"></i> {{=it.views}} просмотров
            </div>
        </div>
    </div>
</script>
<script type="text/html" id="tmpl-post-only-photo">
    <div class="item">
        <div class="hpanel blog-box">
            <div class="panel-heading">
                <div class="media clearfix">
                    <a class="pull-left">
                        <img src="/resources/images/logo128x128.png" alt="Dom24x7">
                    </a>
                    <div class="media-body text-right">
                        <small class="text-muted">{{=it.published}}</small><br />
                        <small class="text-muted">{{=it.category}}</small>
                    </div>
                </div>
            </div>
            <div class="panel-image">
                <img class="img-responsive" src="/blog/images/{{=it.photo.id}}/{{=it.photo.fileName}}" alt="">
                <div class="title">
                    <a href="{{=it.url}}"><h4>{{=it.title}}</h4></a>
                </div>
            </div>
            <div class="panel-footer">
                <i class="fa fa-eye"></i> {{=it.views}} просмотров
            </div>
        </div>
    </div>
</script>

<script src="/resources/lib/salvattore/salvattore.min.js"></script>

<script>
    $(document).ready(function () {
        var offset = 0;
        var count = 10;
        var more = false;

        loadPosts(offset, count);

        $(document).scroll(function () {
            if (isScrolledIntoView($('#more-posts'))) {
                if (more) {
                    more = false;
                    offset += count;
                    loadPosts(offset, count);
                }
            } else {
                more = true;
            }
        });

        /**
         * Загрузка порции постов
         * @param offset
         * @param count
         */
        function loadPosts(offset, count) {
            var data = {};
            <c:if test="${category != null}">data.category = '${category.urlName}';</c:if>

            $.post('/blog/ajax/posts/' + offset + '/' + count, data, function (list) {
                if (!list || list.length == 0) {
                    return;
                }

                var grid = document.querySelector('#posts');

                for (var i = 0; i < list.length; i++) {
                    var post = list[i];

                    var texts = post.text == null ? [''] : post.text.split('<hr>');
                    var url = '/blog/post/' + post.id + '/' + post.translitUrl;
                    var more = '';
                    if (texts.length > 1) {
                        // имеется разделитель и нужно показать ссылку на полную версию поста
                        more = '<a href="' + url + '" class="btn btn-primary btn-xs">читать</a>';
                    }

                    var data = {
                        id: post.id,
                        published: '',
                        category: post.category.name,
                        title: '',
                        text: texts[0],
                        views: post.views,
                        more: more,
                        photo: {
                            id: '',
                            fileName: ''
                        },
                        url: url
                    };
                    if (post.publishedDt) {
                        // указана дата и время публикации
                        data.published = moment(post.publishedDt).format('DD.MM.YYYY HH:mm');
                    } else {
                        // отображаем дату и время создания
                        data.published = moment(post.createDt).format('DD.MM.YYYY HH:mm');
                    }
                    if (post.title) {
                        data.title = post.title;
                    }

                    var tmpl = doT.template($('#tmpl-post-without-photo').html());

                    if (post.photos.length != 0) {
                        var photo = post.photos[0];
                        data.photo = photo;
                        if (!post.text) {
                            tmpl = doT.template($('#tmpl-post-only-photo').html());
                        } else {
                            tmpl = doT.template($('#tmpl-post-with-photo').html());
                        }
                    }

                    var item = document.createElement('div');
                    salvattore.appendElements(grid, [item]);

                    item.outerHTML = tmpl(data);
                }
            });
        }

        /**
         * Проверяет, что элемент виден на экране или нет после скролла
         * @param elem
         * @returns {boolean}
         */
        function isScrolledIntoView(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }
    });
</script>