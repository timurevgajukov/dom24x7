package ru.dom24x7.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.dom24x7.model.user.User;
import ru.dom24x7.model.user.dao.UserDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.Email;

/**
 * Created by timur on 22.09.16.
 *
 * Контроллер для обработки запросов авторизации и регистрации
 */
@Controller
public class SecurityController extends BaseController {

    @RequestMapping(value = "/security/auth", method = RequestMethod.POST)
    public @ResponseBody Response authAction(
            @RequestParam(value = "login") String email,
            @RequestParam(value = "password") String password) {

        User user = new UserDAOImpl().auth(email, password);
        if (user == null) {
            // не удалось авторизоваться
            return new Response(-1, "ERROR", new Message("Не удалось авторизоваться", Message.TYPE_ERROR));
        }

        security.setUser(user);

        return new Response(0, "OK");
    }

    @RequestMapping("/security/logout")
    public String logoutAction() {

        security.logout();

        return "redirect:/";
    }

    @RequestMapping(value = "/security/registration", method = RequestMethod.POST)
    public @ResponseBody Response registrationAction(
            @RequestParam(value = "login") String login,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "subscribe") boolean subscribe,
            @RequestParam(value = "privacy") boolean privacy) {

        // проверяем, что логин указан
        if (login == null || login.trim().length() == 0) {
            return new Response(-1, "ERROR", new Message("Необходимо указать логин", Message.TYPE_ERROR));
        }

        // проверяем логин на уникальность
        User user = new UserDAOImpl().getByLogin(login.trim());
        if (user != null) {
            return new Response(-1, "ERROR", new Message("Указанный логин уже занят", Message.TYPE_ERROR));
        }

        // проверяем , что эл. почта указана
        if (email == null || email.trim().length() == 0) {
            return new Response(-1, "ERROR", new Message("Необходимо указать адрес электронной почты", Message.TYPE_ERROR));
        }

        // проверяем эл. почту на уникальность
        user = new UserDAOImpl().getByEmail(email.trim());
        if (user != null) {
            return new Response(-1, "ERROR", new Message("Указанный адрес электронной почты уже занят", Message.TYPE_ERROR));
        }

        // пароль не должен быть пустым и более 5 символов
        if (password == null || password.length() < 6) {
            return new Response(-1, "ERROR", new Message("Необходимо указать пароль. Длина пароля должна быть не менее 6 символов", Message.TYPE_ERROR));
        }

        // проверяем, что согласен с политикой безопасности
        if (!privacy) {
            return new Response(-1, "ERROR", new Message("Перед регистрацией необходимо прочитать и согласиться с политикой безопасности", Message.TYPE_ERROR));
        }

        // теперь можем зарегистрировать нового пользователя
        user = new User(login.trim(), email.trim());
        user.setSubscribed(subscribe);
        String confirmCode = User.generConfirmCode();
        new UserDAOImpl().save(user, password, confirmCode);

        security.setUser(user);
        security.getMessages().add("Спасибо, что зарегистрировались на нашем коммунальном сервисе. Вам отправлено " +
                "письмо для подтверждения адреса электронной почты", Message.TYPE_INFO);

        // отправить письмо с подтверждением адреса электронной почты
        String url = "https://dom24x7.ru/security/confirm?code=" + confirmCode;
        String emailMessage = String.format("Для подтверждения адреса электронной почты перейдите по ссылке <a href=\"%s\">%s</a>", url, url);

        Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z", servletContext.getRealPath("/resources"));
        emailSender.send("Подтверждение электронной почты на сайте dom24x7.ru", emailMessage, user.getEmail());

        return new Response(0, "OK");
    }

    @RequestMapping("/security/confirm")
    public String confirmEmailAction(@RequestParam(value = "code") String configmCode) {

        User user = new UserDAOImpl().confirm(configmCode);
        if (user == null) {
            return "redirect:/";
        } else {
            security.setUser(user);
            security.getMessages().add("Адрес электронной почты успешно подтвержден", Message.TYPE_INFO);
            return "redirect:/panel";
        }
    }

    @RequestMapping(value = "/security/recover", method = RequestMethod.POST)
    public @ResponseBody Response recoverAjax(@RequestParam(value = "login") String login) {

        User user = new UserDAOImpl().getByLogin(login);
        if (user == null) {
            user = new UserDAOImpl().getByEmail(login);
            if (user == null) {
                return new Response(-1, "ERROR", new Message("Пользователь с такими данными не ныйден", Message.TYPE_ERROR));
            }
        }

        // пользователя нашли и теперь можем отправить письмо с кодом восстановления
        String url = "https://dom24x7.ru/security/recover?code=" + user.getConfirmCode();
        String emailMessage = String.format("Для восстановления пароля перейдите по ссылке <a href=\"%s\">%s</a>", url, url);

        Email emailSender = new Email("noanswer@dom24x7.ru", "Tck89mta7s3z", servletContext.getRealPath("/resources"));
        emailSender.send("Восстановление пароля на сайте dom24x7.ru", emailMessage, user.getEmail());

        return new Response(0, "OK");
    }

    @RequestMapping(value = "/security/recover", method = RequestMethod.GET)
    public String recoverAction(@RequestParam(value = "code") String code, ModelMap model) {

        setModelMap(model, "Восстановление пароля");

        User user = new UserDAOImpl().confirm(code);
        if (user == null) {
            security.getMessages().add("Некорректный код восстановления пароля", Message.TYPE_ERROR);
            return "security/recover";
        }

        security.setUser(user);

        return "security/recover";
    }

    @RequestMapping(value = "/security/recover/change", method = RequestMethod.POST)
    public @ResponseBody Response recoverChangeAjax(
            @RequestParam(value = "pwd1") String pwd1,
            @RequestParam(value = "pwd2") String pwd2) {

        if (!security.isAuth()) {
            // на всякий случай проверяем, хотя попасть сюда без авторизованной сессии не должны
            return new Response(-1, "ERROR");
        }

        if (!pwd1.equals(pwd2)) {
            return new Response(-1, "ERROR", new Message("Пароли не совпадают", Message.TYPE_ERROR));
        }

        // теперь можем сменить пароль
        new UserDAOImpl().recoveryPassword(security.getUser(), pwd1);

        return new Response(0, "OK");
    }
}