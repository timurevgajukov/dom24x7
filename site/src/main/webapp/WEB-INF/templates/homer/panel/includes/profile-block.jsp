<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${security.auth}">
    <div class="profile-picture">
        <a href="#" data-toggle="modal" data-target="#avatar-modal">
            <c:choose>
                <c:when test="${security.user.iconUrl != null}">
                    <img src="${security.user.iconUrl}" class="img-circle m-b" alt="${security.user}" width="76">
                </c:when>
                <c:otherwise>
                    <img src="/resources/images/noavatar.svg" class="img-circle m-b" alt="${security.user}" width="76">
                </c:otherwise>
            </c:choose>
        </a>

        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>

        <div class="stats-label text-color">
            <div class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <small class="text-muted">${security.user} <b class="caret"></b></small>
                </a>
                <ul class="dropdown-menu animated flipInX m-t-xs">
                    <li><a href="/profile">Профиль</a></li>
                    <li class="divider"></li>
                    <li><a href="/security/logout">Выход</a></li>
                </ul>
            </div>
        </div>
    </div>

    <t:insertAttribute name="avatar" defaultValue="/WEB-INF/templates/homer/panel/includes/avatar-modal.jsp"/>
</c:if>