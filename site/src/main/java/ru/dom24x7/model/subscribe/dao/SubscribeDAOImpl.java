package ru.dom24x7.model.subscribe.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.dom24x7.model.ModelDAOImpl;
import ru.dom24x7.model.subscribe.Subscribe;
import ru.dom24x7.model.subscribe.dao.interfaces.SubscribeDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 17.03.17.
 * Реализация интерфейса для работы с рассылкой
 */
public class SubscribeDAOImpl extends ModelDAOImpl implements SubscribeDAO {

    private static final String QUERY_GET_BY_ID = "{ call subscribes_get(?) }";
    private static final String QUERY_LIST = "{ call subscribes_list(?,?,?) }";
    private static final String QUERY_COUNT = "{ call subscribes_count(?) }";
    private static final String QUERY_SAVE = "{ call subscribes_save(?,?,?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call subscribes_delete(?) }";

    @Override
    public Subscribe get(int id) {

        List<Subscribe> list = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Subscribe>() {
            @Override
            public Subscribe mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<Subscribe> list(int offset, int count, String search) {

        if (search == null || search.trim().length() == 0) {
            search = null;
        } else {
            search = "%" + search.trim() + "%";
        }

        List<Subscribe> list = jdbcTemplate.query(QUERY_LIST, new Object[]{offset, count, search}, new RowMapper<Subscribe>() {
            @Override
            public Subscribe mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    @Override
    public int count(String search) {

        if (search == null || search.trim().length() == 0) {
            search = null;
        } else {
            search = "%" + search.trim() + "%";
        }

        return jdbcTemplate.queryForObject(QUERY_COUNT, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, search);
    }

    @Override
    public boolean save(Subscribe subscribe) {

        Subscribe newSubscribe = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        subscribe.getId() == 0 ? null : subscribe.getId(),
                        subscribe.getNumber(),
                        subscribe.getPlan(),
                        subscribe.getFact(),
                        subscribe.getText(),
                        subscribe.getSentCount(),
                        subscribe.getReceivedCount()
                }, new RowMapper<Subscribe>() {
                    @Override
                    public Subscribe mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        subscribe.setId(newSubscribe.getId());

        return true;
    }

    @Override
    public boolean delete(Subscribe subscribe) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{subscribe.getId()}) != 0;
    }

    private Subscribe load(ResultSet rs) throws SQLException {

        Subscribe subscribe = new Subscribe(rs.getInt("id"), rs.getInt("number"), rs.getString("subscribe"));
        subscribe.setPlan(rs.getTimestamp("plan_dt"));
        subscribe.setFact(rs.getTimestamp("fact_dt"));
        subscribe.setSentCount(rs.getInt("sent_count"));
        subscribe.setReceivedCount(rs.getInt("received_count"));

        return subscribe;
    }
}