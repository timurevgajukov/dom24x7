package ru.dom24x7.model.post;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.dom24x7.model.Model;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.photo.dao.PhotoDAOImpl;
import ru.dom24x7.model.user.User;
import ru.dom24x7.utils.Translit;

/**
 * Пост
 * 
 * @author Евгажуков Т.Х.
 *
 */
public class Post extends Model {

	private Date createDt;
	private Date publishedDt;
	private User author;
	private String title;
	private String text;
	private int views;
	private Category category;
	private String source;
	private List<Photo> photos;

	public Post() {

		super();
	}

	public Post(int id) {

		super(id);
	}

	public Post(int id, String title) {

		super(id);
		this.title = title;
	}

	public Date getCreateDt() {

		return createDt;
	}

	public void setCreateDt(Date createDt) {

		this.createDt = createDt;
	}

	public Date getPublishedDt() {

		return publishedDt;
	}

	public void setPublishedDt(Date publishedDt) {

		this.publishedDt = publishedDt;
	}

	public User getAuthor() {

		return author;
	}

	public void setAuthor(User author) {

		this.author = author;
	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	public String getText() {

		return text;
	}

	public void setText(String text) {

		this.text = text;
	}

	public int getViews() {

		return views;
	}

	public void setViews(int views) {

		this.views = views;
	}

	public Category getCategory() {

		return category;
	}

	public void setCategory(Category category) {

		this.category = category;
	}

	public String getSource() {

		return source;
	}

	public void setSource(String source) {

		this.source = source;
	}

	public List<Photo> getPhotos() {

		if (photos == null) {
			loadPhotos();
		}

		return photos;
	}

	public void loadPhotos() {

		loadPhotos(false);
	}

	public void loadPhotos(boolean full) {

		photos = new PhotoDAOImpl().list(this, full);
		if (photos == null) {
			photos = new ArrayList<Photo>();
		}
	}

	public String getShowTitle() {

		if (title != null) {
			return title;
		} else {
			if (text != null) {
				return text.substring(0, (text.length() < 200 ? text.length() : 200)) + (text.length() < 200 ? "" : "...");
			} else {
				return "";
			}
		}
	}

	public String getTranslitUrl() {

		return Translit.translitUrl(getShowTitle());
	}

	@Override
	public String toString() {

		return getShowTitle();
	}
}