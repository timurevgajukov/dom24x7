<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Инициализация формы -->
<form method="POST" action="/ajax/payu/backend">
    <!--  Дополнительные параметры -->
    <input type="hidden" name="internalOrderVar1" value="554543543">
    <input type="hidden" name="internalOrderVar2" value="Order Details">

    <!-- Инициализация кнопки -->
    <input type="button" value="Pay" id="payButton">

    <!-- Инициализация JS скрипта PayU flash и передача параметров -->
    <script type="text/javascript"
            src="https://secure.payu.ru/pay/modal-checkout/integration/payu-modal-checkout.js"
            data-merchant-code="demoshop"
            data-merchant-logo="https://secure.payu.ru/images/merchant/29e11dc359bad383e1243f730bdbe032/demo.jpg"
            data-client-firstname="Тимур"
            data-client-lastname="Евгажуков"
            data-client-email="evgajukov@gmail.com"
            data-client-phone="+79258779819"
            data-order-amount="1.01"
            data-order-currency="RUB"
            data-language="ru"
            data-button="payButton"></script>
</form>