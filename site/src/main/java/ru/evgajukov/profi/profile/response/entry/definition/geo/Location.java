package ru.evgajukov.profi.profile.response.entry.definition.geo;

import java.util.List;

/**
 * Created by timur on 17.06.17.
 */
public class Location {

    private List<Country> countries;

    public List<Country> getCountries() {

        return countries;
    }

    public void setCountries(List<Country> countries) {

        this.countries = countries;
    }
}