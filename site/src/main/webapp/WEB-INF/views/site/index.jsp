<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href="/resources/lib/counter/jquery.incremental-counter.css"/>

<style>
    @media screen and (max-width: 768px) {
        .incremental-counter .num {
            width: 30px;
        }
    }
</style>

<header id="page-top">
    <div class="container">
        <div class="heading">
            <h1>Сервис коммунальных услуг</h1>
            <span>Учет коммунальных расходов еще никогда не был таким простым.</span>
            <p class="small">Расскажите о своем жилье, а все остальное мы сделаем за вас.</p>
            <a page-scroll href="#auth-reg" class="page-scroll btn btn-success btn-sm">Войти или Зарегистрироваться</a>
        </div>
        <div class="heading-image animate-panel h-bg-navy-blue" data-child="img-animate" data-effect="fadeInRight">
            <iframe width="640" height="360" src="https://www.youtube.com/embed/mzpyPKKgouc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</header>

<section id="about-1">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-3">
                <h2><span class="text-success">Идеально для</span> всех</h2>
                <p>Одновременно следите за расходами собственной квартиры,<br/>офиса или дома родителей.</p>
            </div>
        </div>
        <div class="row text-center m-t-lg">
            <div class="col-md-6 col-md-offset-3">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="/resources/images/icons/office.png" width="120" height="120"/>
                        <h4>Офис</h4>
                    </div>
                    <div class="col-sm-4">
                        <img src="/resources/images/icons/my_apartment.png" width="120" height="120"/>
                        <h4>Собственное жилье</h4>
                    </div>
                    <div class="col-sm-4">
                        <img src="/resources/images/icons/home.png" width="120" height="120"/>
                        <h4>Дом близких</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about-2" class="bg-light">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-3">
                <h2><span class="text-success">Ничего не упустите</span> из виду</h2>
                <p>Учитывайте все ваши коммунальные расходы.</p>
            </div>
        </div>
        <div class="row text-center m-t-lg">
            <div class="col-md-6 col-md-offset-3">
                <div class="row">
                    <div class="col-sm-3">
                        <img src="/resources/images/icons/electricity.png" width="86" height="86"/>
                        <h4>Электричество</h4>
                    </div>
                    <div class="col-sm-3">
                        <img src="/resources/images/icons/water.png" width="86" height="86"/>
                        <h4>Вода</h4>
                    </div>
                    <div class="col-sm-3">
                        <img src="/resources/images/icons/heating.png" width="86" height="86"/>
                        <h4>Отопление</h4>
                    </div>
                    <div class="col-sm-3">
                        <img src="/resources/images/icons/gas.png" width="86" height="86"/>
                        <h4>Газ</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about-3">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-3">
                <h2><span class="text-success">Следите и</span> экономьте</h2>
                <p>Мы подскажем как платить меньше.</p>
            </div>
        </div>
        <div class="text-center m-t-lg">
            <img src="/resources/images/total.png" style="width: auto; max-width: 100%; height: auto;"/>
        </div>
    </div>
</section>

<section id="about-4" class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
                <h2><span class="text-success">Наш сервис</span> уже используют:</h2>
                <br/>
                <div class="incremental-counter" data-start="${usersCount - 100}" data-value="${usersCount}"></div>
                <br/><br/>
                <h2><span class="text-success">Компаний ЖКХ</span> уже добавлено:</h2>
                <br/>
                <div class="incremental-counter" data-start="${companiesCount - 100}"
                     data-value="${companiesCount}"></div>
                <p class="text-center" style="padding-top: 10px;"><a href="/companies/regions" class="btn-link">посмотреть список</a></p>
            </div>
            <div class="col-md-6 text-center">
                <h2><span class="text-success">Новые статьи</span> в блоге:</h2>
                <div class="hpanel hblue">
                    <div class="panel-body text-left">
                        <table>
                            <c:forEach items="${posts}" var="post">
                                <tr>
                                    <td valign="top" class="text-success" width="80px"><fmt:formatDate
                                            value="${post.publishedDt}" pattern="dd.MM HH:mm"/></td>
                                    <td><a href="/blog/post/${post.id}/${post.translitUrl}">${post.showTitle}</a></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                    <div class="panel-footer text-right">
                        <a href="/blog">еще</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="auth-reg">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-3">
                <h2><span class="text-success">Начните</span> прямо сейчас</h2>
            </div>
        </div>
        <c:choose>
            <c:when test="${security.auth}">
                <div class="row m-t-lg">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <p>Вы уже авторизованы как ${security.user}</p>
                        <a href="/panel" class="btn btn-success">Перейти в личный кабинет</a>
                        <a href="/security/logout" class="btn btn-primary">Войти под другим именем</a>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <t:insertAttribute name="form-auth-reg" defaultValue="/WEB-INF/views/site/component/form-auth-reg.jsp"/>
            </c:otherwise>
        </c:choose>
    </div>
</section>

<section id="contact" class="bg-light">
    <div class="container">
        <div class="text-center">
            <h2><span class="text-success">Свяжитесь с нами,</span> если остались вопросы</h2>
        </div>
        <div class="row m-t-lg">
            <div class="col-md-7">
                <div id="feedback">
                    <div id="feedback-alert" class="alert" style="display: none;">
                        <span id="feedback-alert-message" class="error-message"></span>
                    </div>
                    <form class="form-horizontal" role="form" method="post" action="/about/feedback">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Имя</label>
                            <div class="col-sm-10">
                                <input type="text" class="feedback-field form-control" id="name"
                                       placeholder="Как нам вас называть?">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Эл. почта</label>
                            <div class="col-sm-10">
                                <input type="email" class="feedback-field form-control" id="email"
                                       placeholder="Куда нам написать ответ?">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-2 control-label">Сообщение</label>
                            <div class="col-sm-10">
                                <textarea class="feedback-field form-control" rows="3" id="message"
                                          placeholder="Что вы нам хотите сказать?"></textarea>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <div class="col-sm-12">
                                <button id="btn-feedback" type="button" class="ladda-button btn btn-success"
                                        data-style="zoom-in">Отправить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-5 text-left">
                <p><strong>Напишите нам:</strong></p>
                <ul>
                    <li>Общие вопросы: <a href="mailto:info@dom24x7.ru">info@dom24x7.ru</a></li>
                    <li>Технические вопросы: <a href="mailto:support@dom24x7.ru">support@dom24x7.ru</a></li>
                    <li>По вопросам партнерства: <a href="mailto:partner@dom24x7.ru">partner@dom24x7.ru</a></li>
                    <li>По вопросам размещения рекламы: <a href="mailto:ad@dom24x7.ru">ad@dom24x7.ru</a></li>
                </ul>
                <p><strong>Мы в социальных сетях:</strong></p>
                <a href="https://www.facebook.com/dom24x7/" title="Dom24x7 в Facebook"><i
                        class="fa fa-facebook fa-2x"></i></a>&nbsp;&nbsp;&nbsp;
                <a href="https://vk.com/dom24x7" title="Dom24x7 в VK"><i class="fa fa-vk fa-2x"></i></a>&nbsp;&nbsp;&nbsp;
                <a href="https://www.instagram.com/dom24x7/" title="Dom24x7 в Instagram"><i
                        class="fa fa-instagram fa-2x"></i></a>&nbsp;&nbsp;&nbsp;
                <a href="https://twitter.com/dom24x7" title="Dom24x7 в Twitter"><i class="fa fa-twitter fa-2x"></i></a>&nbsp;&nbsp;&nbsp;
                <a href="https://pinterest.com/dom24x7/" title="Dom24x7 в Pinterest"><i
                        class="fa fa-pinterest-p fa-2x"></i></a>
            </div>
        </div>
    </div>
</section>

<script src="/resources/lib/counter/jquery.incremental-counter.js"></script>

<script>
    $(document).ready(function () {
        $(".incremental-counter").incrementalCounter({numbers: 7});

        // отправка сообщения
        var btnSendFeedback = $('#btn-feedback').ladda();
        btnSendFeedback.click(function () {
            var data = {
                name: $('#name').val(),
                email: $('#email').val(),
                message: $('#message').val()
            };

            if (data.name == null || data.name.trim().length == 0) {
                showPanelAlert('feedback', 'Необходимо указать свое имя', null, 3000);
                $('#name').focus();
                return;
            }

            if (data.email == null || data.email.trim().length == 0) {
                showPanelAlert('feedback', 'Необходимо указать свой контактный адрес эл. почты', null, 3000);
                $('#email').focus();
                return;
            }

            if (data.message == null || data.message.trim().length == 0) {
                showPanelAlert('feedback', 'Сообщение не может быть пустым', null, 3000);
                $('#message').focus();
                return;
            }

            // теперь можно отправить сообщение
            btnSendFeedback.ladda('start');
            $.post('/about/feedback', data, function (result) {
                btnSendFeedback.ladda('stop');
                if (!result) {
                    showPanelAlert('feedback', 'Не удалось отправить сообщение. Неизвестная ошибка.', null, 5000);
                    return;
                }

                if (result.code != 0) {
                    if (result.body != null && result.body.length != 0) {
                        showPanelAlert('feedback', result.body.body, null, 5000);
                    } else {
                        showPanelAlert('feedback', 'Не удалось отправить сообщение. Неизвестная ошибка. Код: ' + result.code, null, 5000);
                    }
                } else {
                    showPanelAlert('feedback', 'Сообщение успешно отправлено', null, 5000, 'alert-info');

                    // очищаем поля
                    $('.feedback-field').val('');
                }
            }).error(function () {
                btnSendFeedback.ladda('stop');
            });
        });
    });
</script>