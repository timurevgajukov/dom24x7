package ru.dom24x7.site.panel.payment;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.dom24x7.component.ImageResizer;
import ru.dom24x7.model.address.Address;
import ru.dom24x7.model.payment.Payment;
import ru.dom24x7.model.payment.dao.PaymentDAOImpl;
import ru.dom24x7.model.photo.Photo;
import ru.dom24x7.model.photo.dao.PhotoDAOImpl;
import ru.dom24x7.security.Message;
import ru.dom24x7.site.BaseController;
import ru.dom24x7.site.ajax.response.Response;
import ru.dom24x7.utils.StrUtils;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 22.03.17.
 * Контроллер для работы с платежными поручениями
 */
@Controller
public class PaymentController extends BaseController {

    private static final String ACTIVE = "payments";

    @RequestMapping("/panel/payments")
    public String listAction(ModelMap model) {

        setModelMap(model, "Платежные поручения", ACTIVE);

        if (userAddresses == null || userAddresses.size() == 0) {
            security.getMessages().add("Для работы с разделом у вас должен быть настроен хотя бы один адрес", Message.TYPE_WARNING);
            return "redirect:/panel";
        }

        return "panel/payment/list";
    }

    @RequestMapping("/panel/payments/{paymentId}")
    public @ResponseBody Payment getAjax(@PathVariable int paymentId) {

        Payment payment = new PaymentDAOImpl().get(paymentId, security.getUser());
        if (security.getUser().getId() != payment.getUser().getId()) {
            return null;
        }
        payment.loadPhoto();

        return payment;
    }

    @RequestMapping("/panel/payments/{offset}/{count}")
    public @ResponseBody List<Payment> listAjax(@PathVariable int offset, @PathVariable int count) {

        return new PaymentDAOImpl().list(security.getUser(), offset, count, null);
    }

    @RequestMapping(value = "/panel/payments/save", method = RequestMethod.POST)
    public @ResponseBody Response saveAction(
            @RequestParam(value = "id", required = false) Integer paymentId,
            @RequestParam(value = "address", required = false) Integer addressId,
            @RequestParam(value = "month", required = false) String monthStr,
            @RequestParam(value = "amount", required = false) String amountStr,
            @RequestParam(value = "paid", required = false) Boolean paid,
            @RequestParam(value = "note", required = false) String note,
            @RequestParam(value = "photos", required = false) String photosBase64Str,
            @RequestParam(value = "photosFileName", required = false) String photosFileNameStr) {

        List<Message> messages = new ArrayList<Message>();

        Payment payment;
        if (paymentId == null) {
            // добавление нового платежного поручения
            payment = new Payment();
            payment.setUser(security.getUser());
        } else {
            payment = new PaymentDAOImpl().get(paymentId, security.getUser());
            if (payment == null || security.getUser().getId() != payment.getUser().getId()) {
                messages.add(new Message("Не удалось найти платежное поручение с указанным идентификатором", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        Address address;
        if (addressId == null) {
            messages.add(new Message("К платежному поручению обязательно должен быть привязан адрес", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        } else {
            address = security.getUser().getAddress(addressId);
            if (address == null) {
                messages.add(new Message("К платежному поручению обязательно должен быть привязан адрес", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        }

        Date month = null;
        try {
            month = new SimpleDateFormat("MM.yyyy").parse(monthStr);
        } catch (ParseException e) {
            messages.add(new Message("Неверный формат месяца платежа. Укажите в формате ММ.ГГГГ", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        double amount;
        try {
            amount = Double.parseDouble(amountStr.replaceAll(",", "."));
            if (amount <= 0) {
                messages.add(new Message("Сумма платежа должна быть положительным числом", Message.TYPE_ERROR));
                return new Response(-1, "ERROR", messages);
            }
        } catch (NumberFormatException e) {
            messages.add(new Message("Сумма платежа должна быть числом", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        List<Photo> photos = new ArrayList<Photo>();
        if (photosBase64Str != null && photosBase64Str.trim().length() != 0) {
            // может быть не одна, а несколько картинок
            String[] photosBase64 = photosBase64Str.split("##");
            String[] photosFileName = photosFileNameStr.split("##");
            for (int i = 0; i < photosBase64.length; i++) {
                // добавлена к посту картинка
                String photoBase64 = photosBase64[i];
                String photoFileName = photosFileName[i];

                BASE64Decoder decoder = new BASE64Decoder();
                try {
                    byte[] photoBytes = decoder.decodeBuffer(photoBase64);
                    Photo photo = new Photo(photoFileName, photoBytes);
                    new PhotoDAOImpl().save(photo);
                    photos.add(photo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            messages.add(new Message("Необходимо привязать скан платежного поручения", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        // теперь можем добавить данные
        payment.setAddress(address);
        payment.setMonth(month);
        payment.setAmount(amount);
        payment.setPaid(paid != null ? paid : false);
        payment.setNote(StrUtils.isEmpty(note) ? null : note.trim());
        payment.setPhoto(photos.get(0));

         new PaymentDAOImpl().save(payment);

        return new Response(0, "OK", payment);
    }

    @RequestMapping("/panel/payments/delete/{paymentId}")
    public @ResponseBody Response deleteAction(@PathVariable int paymentId) {

        Payment payment = new PaymentDAOImpl().get(paymentId, security.getUser());
        if (payment == null || security.getUser().getId() != payment.getUser().getId()) {
            messages.add(new Message("Не удалось найти платежное поручение с указанным идентификатором", Message.TYPE_ERROR));
            return new Response(-1, "ERROR", messages);
        }

        new PaymentDAOImpl().delete(payment);

        return new Response(0, "OK");
    }

    @RequestMapping("/panel/payments/images/{imgId}/{imgFileName}")
    public @ResponseBody byte[] paymentImageAction(@PathVariable int imgId, @PathVariable String imgFileName) {

        Photo photo = new PhotoDAOImpl().get(imgId);
        if (photo == null) {
            return null;
        }

        return photo.getResource();
    }

    @RequestMapping("/panel/payments/images/small/{imgId}/{imgFileName}")
    public @ResponseBody byte[] paymentImageSmallAction(@PathVariable int imgId, @PathVariable String imgFileName) {

        // в начале попытаемся получить миниатюру из БД
        Photo thumbnail = new PhotoDAOImpl().thumbnail(new Photo(imgId));
        if (thumbnail != null) {
            return thumbnail.getResource();
        }

        // миниатюры нет, поэтому получаем оригинал
        Photo photo = new PhotoDAOImpl().get(imgId);
        if (photo == null) {
            return null;
        }

        try {
            // формируем миниатюру
            byte[] resource = photo.getResource();
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(resource));
            BufferedImage resizeImage = ImageResizer.resizeByWidth(image, 400);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(resizeImage, photo.getFormat(), baos);

            // записываем в БД
            thumbnail = new Photo(photo.getFileName(), baos.toByteArray());
            new PhotoDAOImpl().save(thumbnail, photo);

            // отдаем пользователю
            return thumbnail.getResource();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // все плохо, не смогли создать миниатюру, отдаем оригинал
        return photo.getResource();
    }
}