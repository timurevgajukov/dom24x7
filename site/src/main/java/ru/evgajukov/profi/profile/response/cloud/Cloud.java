package ru.evgajukov.profi.profile.response.cloud;

import com.google.gson.annotations.SerializedName;

/**
 * Created by timur on 17.06.17.
 */
public class Cloud {

    @SerializedName("static")
    private Static aStaticic;

    public Static getaStaticic() {

        return aStaticic;
    }

    public void setaStaticic(Static aStaticic) {

        this.aStaticic = aStaticic;
    }
}